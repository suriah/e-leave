#FROM php:8.0-fpm
FROM php:7.2-fpm

RUN apt-get update && apt-get install -y libpq-dev && docker-php-ext-install pdo pdo_pgsql pgsql
RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql

# for zip installation
RUN apt-get update && apt-get install -y \
    zlib1g-dev \
    libzip-dev unzip

RUN docker-php-ext-install zip && docker-php-ext-install bcmath

COPY . /srv/app

#set memory limit to 256
RUN cd /usr/local/etc/php/conf.d/ && \
  echo 'memory_limit = 512M' >> /usr/local/etc/php/conf.d/docker-php-memlimit.ini

RUN chown -R www-data:www-data /srv/app 
    #&& a2enmod rewrite \
    #&& a2enmod ssl

EXPOSE 80
EXPOSE 443
