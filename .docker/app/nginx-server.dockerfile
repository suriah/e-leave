FROM nginx:1.19-alpine

RUN apk update \
    && apk add openssl

RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout \
    /etc/ssl/private/ssl-cert-eleave-local.key -out /etc/ssl/certs/ssl-cert-eleave-local.pem -subj "/C=AT/ST=Vienna/L=Vienna/O=Security/OU=Development/CN=eleave.local"
