<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_menus_4 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        //Leave Application
            //  - REQUEST
            //  - APPROVAL
            
    
            // add system admin as parent menu
            $data = array(
                'parent_id'  => 0,
                'menu_code'  => 'LEAVE_APPLICATION',
                'menu_name'  => 'Leave Application',
                'url'        => null,
                'icon'       => 'ft ft-file-text',
                'is_active'  => true,
                'menu_order' => 20
            );
    
            $this->db->insert('menus', $data);
            $parent_id = $this->db->insert_id();
    
            //prepare parent menu for leave request
            $data = array(
                'parent_id'  => $parent_id,
                'menu_code'  => 'LEAVE_APPLICATION.LEAVE_REQUEST',
                'menu_name'  => 'Leave Request',
                'url'        => 'leave_request',
                'icon'       => null,
                'is_active'  => true,
                'menu_order' => 10,
            );
            $this->db->insert('menus', $data);
            
            //prepare parent menu for leave approval
            $data = array(
                'parent_id'  => $parent_id,
                'menu_code'  => 'LEAVE_APPLICATION.LEAVE_APPROVAL',
                'menu_name'  => 'Leave Approval',
                'url'        => 'leave_approval',
                'icon'       => null,
                'is_active'  => true,
                'menu_order' => 20,
            );
    
            $this->db->insert('menus', $data);
       
        echo 'Complete update menus value<BR>';
    }

    public function down()
    {
        //delete menu 
        $this->db->where_in('menu_code', array(
            'LEAVE_APPLICATION', 
            'LEAVE_APPLICATION.LEAVE_REQUEST', 
            'LEAVE_APPLICATION.LEAVE_APPROVAL',
        ));
        $this->db->delete('menus');
        echo 'Complete undo menus value<BR>';
    }
}