<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_privileges_9 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        $data = array(
            array(
                'privilege_code'        => 'REPORT.LEAVE_REPORT',
                'privilege_description' => 'Report > Leave Report',
                'module_name'           => 'REPORT',
                'privilege_type'        => 'MENU'
            ),
            array(
                'privilege_code'        => 'REPORT.WFH_REPORT',
                'privilege_description' => 'Report > WFH Report',
                'module_name'           => 'REPORT',
                'privilege_type'        => 'MENU'
            ),
            array(
                'privilege_code'        => 'REPORT.TIME_OFF_REPORT',
                'privilege_description' => 'Report > TIME OFF Report',
                'module_name'           => 'REPORT',
                'privilege_type'        => 'MENU'
            ),
            
            
        );
           
        $this->db->insert_batch('privileges', $data);
        echo 'Complete update privileges value<BR>';
    }

    public function down()
    {
        //remove this privileges
        $privileges = array(
            'REPORT.LEAVE_REPORT', 
            'REPORT.WFH_REPORT',
            'REPORT.TIME_OFF_REPORT',
        );
        $this->db->where_in ('privilege_code', $privileges);
        $this->db->delete('privileges');
        echo 'Complete undo privileges value<BR>';
    }
}
