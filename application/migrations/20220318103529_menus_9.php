<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_menus_9 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        //Report
            //  - Leave Report
            //  - WFH Report
            //  - TIME OFF Report
            //
        $this->db->where('menu_code', 'REPORT');        
        $query = $this->db->get('menus');
        $row = $query->row();
        $parent_id = $row->id;
    
            //prepare parent menu for leave request
            $data = array(
                'parent_id'  => $parent_id,
                'menu_code'  => 'REPORT.LEAVE_REPORT',
                'menu_name'  => 'Leave Report',
                'url'        => 'report_leave',
                'icon'       => null,
                'is_active'  => true,
                'menu_order' => 10,
            );
            $this->db->insert('menus', $data);

            $data = array(
                'parent_id'  => $parent_id,
                'menu_code'  => 'REPORT.WFH_REPORT',
                'menu_name'  => 'WFH Report',
                'url'        => 'report_wfh',
                'icon'       => null,
                'is_active'  => true,
                'menu_order' => 20,
            );
            $this->db->insert('menus', $data);

            $data = array(
                'parent_id'  => $parent_id,
                'menu_code'  => 'REPORT.TIME_OFF_REPORT',
                'menu_name'  => 'TIME OFF Report',
                'url'        => 'report_time_off',
                'icon'       => null,
                'is_active'  => true,
                'menu_order' => 30,
            );
            $this->db->insert('menus', $data);
       
        echo 'Complete update menus value<BR>';
    }

    public function down()
    {
       //delete menu 
       $this->db->where_in('menu_code', array(
        'REPORT.LEAVE_REPORT', 
        'REPORT.WFH_REPORT',
        'REPORT.TIME_OFF_REPORT'
        ));
        $this->db->delete('menus');
        echo 'Complete undo menus value<BR>';
    }
}
