<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_privileges extends CI_Migration
{
    private $table_name;

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
        $this->table_name = 'privileges';
    }

    public function up()
    {
        $this->dbforge->add_field(array(
            'id'         => array(
                'type'           => 'INTEGER',
                'unsigned'       => true,
                'auto_increment' => true,
            ),
            'privilege_code'  => array(
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'unique'     => true,
            ),
            'privilege_description'        => array(
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'NULL'       => true,
            ),
            'module_name'        => array(
                'type'       => 'VARCHAR',
                'constraint' => '60',
                'NULL'       => true,
            ),
            'privilege_type'        => array(
                'type'       => 'VARCHAR',
                'constraint' => '25',
                'NULL'       => true,
            ),

        ));

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->table_name);
        echo $this->table_name . ' table created <BR>';
    }

    public function down()
    {
        $this->dbforge->drop_table($this->table_name);
        echo 'Drop Table menus<BR>';
    }
}
