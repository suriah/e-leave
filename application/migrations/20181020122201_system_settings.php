<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_system_settings extends CI_Migration {

    public function __construct() {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up() {

        $this->dbforge->add_field(array(
            'id'          => array(
                'type'           => 'MEDIUMINT',
                'constraint'     => '8',
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ),
            'app_name'        => array(
                'type'       => 'VARCHAR',
                'constraint' => '50',
            ),
            'app_description' => array(
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ),
            'app_footer' => array(
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ),
            'app_logo_image' => array(
                'type'       => 'VARCHAR',
                'constraint' => '50',
                'null'       => TRUE
            ),
            'app_icon_image' => array(
                'type'       => 'VARCHAR',
                'constraint' => '50',
                'null'       => TRUE
            ),
            'multi_language' => array(
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ),
            'menu_color_options' => array(
                'type'       => 'SMALLINT'
            ),
            'app_footer' => array(
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ),
            'navigation_color_type' => array(
                'type' => 'VARCHAR',
                'constraint' => 15                
            ),
            'navigation_color' => array(
                'type' => 'VARCHAR',
                'constraint' => 25   
            ),  
            'menu_border' => array(
                'type' => 'BOOLEAN',
                'default' => FALSE,
            ),  
            'bypass_menu_restriction' => array(
                'type' => 'BOOLEAN',
                'default' => TRUE,
            ),  
        ));

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('system_settings');

        echo 'system_settings table created <BR>';
        
        
        //create table complete, now populate data
        $data = array('app_name' => 'App Name',
                      'app_description' => 'App description',
                      'app_footer' => 'App Footer',
                      'multi_language' => 1,
                      'menu_color_options' => 1,
                      'navigation_color_type' => 'dark',
                      'navigation_color' => 'bg-gradient-x-blue'
            );
        
        $this->db->insert('system_settings', $data);
        echo 'insert default value for table system_settings is successful <BR>';        
        
    }

    public function down() {
        $this->dbforge->drop_table('system_settings', TRUE);
    }

}
