<?php 

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_employee_status_1 extends CI_Migration
{

    private $table_name;

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
        $this->table_name = 'employee_status';
    }

    public function up()
    {
        $this->dbforge->add_field(array(
            'id'                  => array(
                'type'           => 'BIGINT',
                'unsigned'       => true,
                'auto_increment' => true,
            ),
            'employee_code'      => array(
                'type'       => 'VARCHAR',
                'constraint' => '15',
                'NULL'       => false,
            ),
            'employee_type' => array(
                'type'       => 'VARCHAR',
                'constraint' => '30',
                'NULL'       => true,
            ),
            'employee_description' => array(
                'type'       => 'VARCHAR',
                'constraint' => '200',
                'NULL'       => true,
            ),
            'created_timestamp'   => array(
                'type' => 'TIMESTAMP',
            ),
            'created_by'          => array(
                'type'       => 'VARCHAR',
                'constraint' => '60',
            ),
            'updated_timestamp'   => array(
                'type' => 'TIMESTAMP',
                'NULL' => true,
            ),
            'updated_by'          => array(
                'type'       => 'VARCHAR',
                'constraint' => '60',
                'NULL' => true,
            ),
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->table_name);
        echo $this->table_name . ' table created <BR>';

    }

    public function down()
    {
        $this->dbforge->drop_table($this->table_name, TRUE);
        echo 'Drop table ' . $this->table_name . '<BR>';
    }
}
