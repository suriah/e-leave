<?php 

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_leave_3 extends CI_Migration
{

    private $table_name;

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
        $this->table_name = 'leave';

        $this->fields = array(
            'supporting_document'   => array(
                'type'       => 'VARCHAR',
                'constraint' => '120',
                'NULL'       => true,
            ),
        );
    }

    public function up()
    {
        $this->dbforge->add_column($this->table_name, $this->fields);
        echo 'Add multiple column in table ' . $this->table_name . '<BR>';
    }

    public function down()
    {

        foreach ($this->fields as $key => $value) {
            $this->dbforge->drop_column($this->table_name, $key);
            echo 'Drop column ' . $key . ' from table ' . $this->table_name  . '<BR>';
        }
    }
}
