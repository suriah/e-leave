<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_departments_1 extends CI_Migration
{
    private $table_name;

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
        $this->table_name = 'departments';
    }

    public function up()
    {

        $this->dbforge->add_field(array(
            'id'         => array(
                'type'           => 'INTEGER',
                'unsigned'       => true,
                'auto_increment' => true,
            ),
            'department_code'  => array(
                'type'       => 'VARCHAR',
                'constraint' => '10',
                'unique'     => true,
            ),
            'department_name'  => array(
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'NULL'       => true,
            ),
            'department_description'  => array(
                'type'       => 'VARCHAR',
                'constraint' => '250',
                'NULL'       => true,
            ),
            'created_timestamp'   => array(
                'type' => 'TIMESTAMP',
            ),
            'created_by'          => array(
                'type'       => 'VARCHAR',
                'constraint' => '60',
            ),
            'updated_timestamp'   => array(
                'type' => 'TIMESTAMP',
                'NULL' => true,
            ),
            'updated_by'          => array(
                'type'       => 'VARCHAR',
                'constraint' => '60',
                'NULL' => true,
            ),
        ));

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->table_name);
        echo $this->table_name . ' table created <BR>';
    }

    public function down()
    {
        $this->dbforge->drop_table($this->table_name, TRUE);
        echo 'Drop table ' . $this->table_name . '<BR>';
    }
}