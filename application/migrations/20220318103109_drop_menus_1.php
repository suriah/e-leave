<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_drop_menus_1 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        $this->db->where_in('menu_code', array(
            'REPORT.LEAVE_REPORT', 
            'REPORT.APPROVAL_REPORT'
        ));
        $this->db->delete('menus');
        echo 'Complete drop menus database value<BR>';
    }

    public function down()
    {
        $this->db->where('menu_code', 'REPORT');        
        $query = $this->db->get('menus');
        $row = $query->row();
        $parent_id = $row->id;
        //prepare parent menu for leave request
        $data = array(
            'parent_id'  => $parent_id,
            'menu_code'  => 'REPORT.LEAVE_REPORT',
            'menu_name'  => 'Leave Report',
            'url'        => 'report_leave',
            'icon'       => null,
            'is_active'  => true,
            'menu_order' => 10,
        );
        $this->db->insert('menus', $data);

        $data = array(
            'parent_id'  => $parent_id,
            'menu_code'  => 'REPORT.APPROVAL_REPORT',
            'menu_name'  => 'Approval Report',
            'url'        => 'report_approval',
            'icon'       => null,
            'is_active'  => true,
            'menu_order' => 20,
        );
        $this->db->insert('menus', $data);
        echo 'Complete add menus database value<BR>';
    }
}
