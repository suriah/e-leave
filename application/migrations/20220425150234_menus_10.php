<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_menus_10 extends CI_Migration
{

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        //Leave Application
            //  - Leave Request
            //  - Leave Approval
            //  - Cancellation
            //
        $this->db->where('menu_code', 'LEAVE_APPLICATION');        
        $query = $this->db->get('menus');
        $row = $query->row();
        $parent_id = $row->id;
    
            //prepare parent menu for leave request
            $data = array(
                'parent_id'  => $parent_id,
                'menu_code'  => 'LEAVE_APPLICATION.CANCELLATIONS_REQUEST',
                'menu_name'  => 'Cancellations Request',
                'url'        => 'leave_cancellations_request',
                'icon'       => null,
                'is_active'  => true,
                'menu_order' => 40,
            );
            $this->db->insert('menus', $data);
            $data = array(
                'parent_id'  => $parent_id,
                'menu_code'  => 'LEAVE_APPLICATION.CANCELLATIONS_APPROVAL',
                'menu_name'  => 'Cancellations Approval',
                'url'        => 'leave_cancellations_approval',
                'icon'       => null,
                'is_active'  => true,
                'menu_order' => 45,
            );
            $this->db->insert('menus', $data);
        echo 'Complete update menus value<BR>';
    }

    public function down()
    {
       //delete menu 
       $this->db->where_in('menu_code', array(
        'LEAVE_APPLICATION.CANCELLATIONS_REQUEST',
        'LEAVE_APPLICATION.CANCELLATIONS_APPROVAL',
        ));
        $this->db->delete('menus');
        echo 'Complete undo menus value<BR>';
    }
}