<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_users_1 extends CI_Migration
{

    private $table_name;
    private $fields;

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
        $this->table_name = 'users';

        $this->fields = array(
            'staff_id'   => array(
                'type'       => 'VARCHAR',
                'constraint' => 15,
                'NULL' => true
            ),
            'staff_department_id'         => array(
                'type'   => 'INTEGER',
                'NULL'   => true
            ),
            'staff_annual_leave'         => array(
                'type'   => 'INTEGER',
                'NULL'   => true
            ),
            'staff_emergency_leave'         => array(
                'type'   => 'INTEGER',
                'NULL'   => true
            ),
            'staff_mc'   => array(
                'type'   => 'INTEGER',
                'NULL'   => true
            ),
            'staff_balance_forwarded_leave'   => array(
                'type'   => 'INTEGER',
                'NULL'   => true
            ),
            'staff_maternity_leave'   => array(
                'type'   => 'INTEGER',
                'NULL'   => true
            ),
            'staff_leave_total'=> array(
                'type'   => 'INTEGER',
                'NULL'   => true
            ),
            'staff_leave_balance'=> array(
                'type'   => 'INTEGER',
                'NULL'   => true
            ),
            'updated_timestamp' => array(
                'type' => 'TIMESTAMP',
                'NULL' => true
            ),
            'updated_by'   => array(
                'type'       => 'VARCHAR',
                'constraint' => 60,
                'NULL' => true
            ),
        );
    }

    public function up()
    {
        $this->dbforge->add_column($this->table_name, $this->fields);
        echo 'Add multiple column in table ' . $this->table_name . '<BR>';
    }

    public function down()
    {

        foreach ($this->fields as $key => $value) {
            $this->dbforge->drop_column($this->table_name, $key);
            echo 'Drop column ' . $key . ' from table ' . $this->table_name  . '<BR>';
        }
    }
}
