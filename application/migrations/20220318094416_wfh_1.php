<?php 

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_wfh_1 extends CI_Migration
{

    private $table_name;

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
        $this->table_name = 'wfh';
    }

    public function up()
    {
        $this->dbforge->add_field(array(
            'id'                  => array(
                'type'           => 'BIGINT',
                'unsigned'       => true,
                'auto_increment' => true,
            ),
            'wfh_date_from'      => array(
                'type'       => 'TIMESTAMP',
                'NULL'       => true,
            ),
            'wfh_date_to'      => array(
                'type'       => 'TIMESTAMP',
                'NULL'       => true,
            ),
            'total_wfh' => array(
                'type' => 'INTEGER',
                'NULL' => true,
            ),
            'wfh_reason' => array(
                'type'       => 'VARCHAR',
                'constraint' => '120',
                'NULL'       => true,
            ),
            'wfh_status' => array(
                'type'       => 'VARCHAR',
                'constraint' => '20',
                'NULL'       => true,
            ),
            'staff_id' => array(
                'type' => 'INTEGER',
                'NULL' => true,
            ),
            'manager_id' => array(
                'type' => 'INTEGER',
                'NULL' => true,
            ),
            'manager_is_approved' => array(
                'type' => 'BOOLEAN',
                'NULL' => true,
            ),
            'manager_feedback' => array(
                'type'       => 'VARCHAR',
                'constraint' => '120',
                'NULL'       => true,
            ),
            'created_timestamp'   => array(
                'type' => 'TIMESTAMP',
            ),
            'created_by'          => array(
                'type'       => 'VARCHAR',
                'constraint' => '60',
            ),
            'updated_timestamp'   => array(
                'type' => 'TIMESTAMP',
                'NULL' => true,
            ),
            'updated_by'          => array(
                'type'       => 'VARCHAR',
                'constraint' => '60',
                'NULL' => true,
            ),
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->table_name);
        echo $this->table_name . ' table created <BR>';

    }

    public function down()
    {
        $this->dbforge->drop_table($this->table_name, TRUE);
        echo 'Drop table ' . $this->table_name . '<BR>';
    }
}
