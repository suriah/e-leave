<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_menus_6 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        //Report
            //  - Leave Report
            //      -List Leave Application
            
    
            // add system admin as parent menu
            $data = array(
                'parent_id'  => 0,
                'menu_code'  => 'REPORT',
                'menu_name'  => 'Report',
                'url'        => null,
                'icon'       => 'ft ft-folder',
                'is_active'  => true,
                'menu_order' => 30
            );
    
            $this->db->insert('menus', $data);
            $parent_id = $this->db->insert_id();
    
            //prepare parent menu for leave request
            $data = array(
                'parent_id'  => $parent_id,
                'menu_code'  => 'REPORT.LEAVE_REPORT',
                'menu_name'  => 'Leave Report',
                'url'        => 'report_leave',
                'icon'       => null,
                'is_active'  => true,
                'menu_order' => 10,
            );
            $this->db->insert('menus', $data);

            $data = array(
                'parent_id'  => $parent_id,
                'menu_code'  => 'REPORT.APPROVAL_REPORT',
                'menu_name'  => 'Approval Report',
                'url'        => 'report_approval',
                'icon'       => null,
                'is_active'  => true,
                'menu_order' => 20,
            );
            $this->db->insert('menus', $data);
       
        echo 'Complete update menus value<BR>';
    }

    public function down()
    {
        //delete menu 
        $this->db->where_in('menu_code', array(
            'REPORT', 
            'REPORT.LEAVE_REPORT', 
            'REPORT.APPROVAL_REPORT'
        ));
        $this->db->delete('menus');
        echo 'Complete undo menus value<BR>';
    }
}
