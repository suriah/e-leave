<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_menus_3 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        //SYSTEM ADMIN
        //      - MANAGE USERS
        //      - MANAGE ROLE
        //      - MANAGE DETAILS
        //          - MANAGE DEPARTMENT
        //          - MANAGE POSITION
        //          - MANAGE EMPLOYEE STATUS
        //          - MANAGE LEVEL
        //          - MANAGE LEAVE TYPE

        // get system admin as parent menu
        $this->db->where('menu_code', 'SYSTEM_ADMIN.MANAGE_DETAILS');        
        $query = $this->db->get('menus');
        $row = $query->row();
        $parent_id = $row->id;

        //prepare parent menu for manage details
        $data = array(
            'parent_id'  => $parent_id,
            'menu_code'  => 'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_LEVEL',
            'menu_name'  => 'Manage Level',
            'url'        => 'system_admin_manage_level',
            'icon'       => null,
            'is_active'  => true,
            'menu_order' => 35,
        );
        $this->db->insert('menus', $data);
        echo 'Complete update menus value<BR>';
    }

    public function down()
    {
        //delete menu 
        $this->db->where_in('menu_code', array(
            'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_LEVEL',

        ));
        $this->db->delete('menus');
        echo 'Complete undo menus value<BR>';
    }
}
