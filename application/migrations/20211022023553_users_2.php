<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_users_2 extends CI_Migration
{
    private $table_name;
    private $fields;

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
        $this->table_name = 'users';

        $this->fields = array(
            'staff_position_id'   => array(
                'type'   => 'INTEGER',
                'NULL' => true
            ),
            'staff_employee_status_id'   => array(
                'type'   => 'INTEGER',
                'NULL'   => true
            ),  
            'staff_level_id'   => array(
                'type'   => 'INTEGER',
                'NULL'   => true
            ),  
            'staff_supervisor_id'   => array(
                'type'   => 'INTEGER',
                'NULL'   => true
            ),  
            'staff_manager_id'   => array(
                'type'   => 'INTEGER',
                'NULL'   => true
            ),  
        );
    }

    public function up()
    {
        $this->dbforge->add_column($this->table_name, $this->fields);
        echo 'Add multiple column in table ' . $this->table_name . '<BR>';
    }

    public function down()
    {

        foreach ($this->fields as $key => $value) {
            $this->dbforge->drop_column($this->table_name, $key);
            echo 'Drop column ' . $key . ' from table ' . $this->table_name  . '<BR>';
        }
    }
}
