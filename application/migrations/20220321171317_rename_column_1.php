<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_rename_column_1 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        $this->db->query('ALTER TABLE "users_leaves"
        RENAME COLUMN "staff_fowarded_leave" TO "staff_forwarded_leave";');
        echo 'Complete update database value<BR>';
    }

    public function down()
    {
        $this->db->query('ALTER TABLE "users_leaves"
        RENAME COLUMN "staff_forwarded_leave" TO "staff_fowarded_leave";');
        echo 'Complete undo database value<BR>';
    }
}
