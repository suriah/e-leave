<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_privileges_2 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        $data = array(
            array(
                'privilege_code'        => 'SYSTEM_ADMIN.MANAGE_DETAILS',
                'privilege_description' => 'System Admin > Manage Details',
                'module_name'           => 'SYSTEM_ADMIN',
                'privilege_type'        => 'MENU'
            ),
            array(
                'privilege_code'        => 'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_DEPARTMENT',
                'privilege_description' => 'System Admin > Manage Details > Manage Department',
                'module_name'           => 'SYSTEM_ADMIN_MANAGE_DETAILS',
                'privilege_type'        => 'MENU'
            ),
            array(
                'privilege_code'        => 'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_POSITION',
                'privilege_description' => 'System Admin > Manage Details > Manage Position',
                'module_name'           => 'SYSTEM_ADMIN_MANAGE_DETAILS',
                'privilege_type'        => 'MENU'
            ),
            array(
                'privilege_code'        => 'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_EMPLOYEE_STATUS',
                'privilege_description' => 'System Admin > Manage Details > Manage Employee Status',
                'module_name'           => 'SYSTEM_ADMIN_MANAGE_DETAILS',
                'privilege_type'        => 'MENU'
            ),
            array(
                'privilege_code'        => 'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_LEAVE_TYPE',
                'privilege_description' => 'System Admin > Manage Details > Manage Leave Type',
                'module_name'           => 'SYSTEM_ADMIN_MANAGE_DETAILS',
                'privilege_type'        => 'MENU'
            ),
            
        );
           
        $this->db->insert_batch('privileges', $data);
        echo 'Complete update privileges value<BR>';
    }

    public function down()
    {
        //remove this privileges
        $privileges = array(
            'SYSTEM_ADMIN.MANAGE_DETAILS',
            'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_DEPARTMENT',
            'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_POSITION',
            'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_EMPLOYEE_STATUS',
            'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_LEAVE_TYPE', 
        );
        $this->db->where_in ('privilege_code', $privileges);
        $this->db->delete('privileges');
        echo 'Complete undo privileges value<BR>';
    }
}
