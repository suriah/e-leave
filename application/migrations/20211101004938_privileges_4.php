<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_privileges_4 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        $data = array(
            array(
                'privilege_code'        => 'LEAVE_APPLICATION',
                'privilege_description' => 'Leave Application',
                'module_name'           => 'LEAVE_APPLICATION',
                'privilege_type'        => 'MENU'
            ),
            array(
                'privilege_code'        => 'LEAVE_APPLICATION.LEAVE_REQUEST',
                'privilege_description' => 'Leave Application > Leave Request',
                'module_name'           => 'LEAVE_APPLICATION',
                'privilege_type'        => 'MENU'
            ),
            array(
                'privilege_code'        => 'LEAVE_APPLICATION.LEAVE_APPROVAL',
                'privilege_description' => 'Leave Application > Leave Approval',
                'module_name'           => 'LEAVE_APPLICATION',
                'privilege_type'        => 'MENU'
            ),
            
            
        );
           
        $this->db->insert_batch('privileges', $data);
        echo 'Complete update privileges value<BR>';
    }

    public function down()
    {
        //remove this privileges
        $privileges = array(
            'LEAVE_APPLICATION', 
            'LEAVE_APPLICATION.LEAVE_REQUEST', 
            'LEAVE_APPLICATION.LEAVE_APPROVAL'
        );
        $this->db->where_in ('privilege_code', $privileges);
        $this->db->delete('privileges');
        echo 'Complete undo privileges value<BR>';
    }
}
