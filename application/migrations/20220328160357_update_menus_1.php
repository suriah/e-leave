<?php 

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_update_menus_1 extends CI_Migration
{

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        $query_1 = "UPDATE menus
        SET menu_code = 'TIMEOFF_APPLICATION.TIMEOFF_REQUEST'
        WHERE menu_name = 'Time-off Request'";
        $query_2 = "UPDATE menus
        SET menu_code = 'TIMEOFF_APPLICATION.TIMEOFF_APPROVAL'
        WHERE menu_name = 'Time-off Approval'";
        $this->db->query($query_1);
        $this->db->query($query_2);
        echo 'Complete update database value<BR>';
    }

    public function down()
    {
        $query_1 = "UPDATE menus
        SET menu_code = 'TIME_OFF_APPLICATION.TIME_OFF_REQUEST'
        WHERE menu_name = 'Time-off Request'";
        $query_2 = "UPDATE menus
        SET menu_code = 'TIME_OFF_APPLICATION.TIME_OFF_APPROVAL'
        WHERE menu_name = 'Time-off Approval'";
        $this->db->query($query_1);
        $this->db->query($query_2);
        echo 'Complete undo database value<BR>';
    }
}
