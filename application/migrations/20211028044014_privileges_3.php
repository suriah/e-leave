<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_privileges_3 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        $data = array(
            array(
                'privilege_code'        => 'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_LEVEL',
                'privilege_description' => 'System Admin > Manage Details > Manage Level',
                'module_name'           => 'SYSTEM_ADMIN_MANAGE_DETAILS',
                'privilege_type'        => 'MENU'
            ),
            
        );
           
        $this->db->insert_batch('privileges', $data);
        echo 'Complete update privileges value<BR>';
    }

    public function down()
    {
        //remove this privileges
        $privileges = array(
            'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_LEVEL', 
        );
        $this->db->where_in ('privilege_code', $privileges);
        $this->db->delete('privileges');
        echo 'Complete undo privileges value<BR>';
    }
}
