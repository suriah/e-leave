<?php 

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_dhearty_1 extends CI_Migration
{

    private $table_name;

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
        $this->table_name = 'dhearty';
    }

    public function up()
    {
        $this->dbforge->add_field(array(
            'id'                  => array(
                'type'           => 'BIGINT',
                'unsigned'       => true,
                'auto_increment' => true,
            ),
            'dhearty'      => array(
                'type'       => 'VARCHAR',
                'constraint' => '120',
                'NULL'       => false,
            ),
            'dhearty_date'   => array(
                'type' => 'TIMESTAMP',
            ),
            'dhearty_time_start'   => array(
                'type' => 'TIMESTAMP',
            ),
            'dhearty_time_end'   => array(
                'type' => 'TIMESTAMP',
            ),
            'dhearty_agenda'      => array(
                'type'       => 'VARCHAR',
                'constraint' => '120',
                'NULL'       => false,
            ),
            'created_timestamp'   => array(
                'type' => 'TIMESTAMP',
            ),
            'created_by'          => array(
                'type'       => 'VARCHAR',
                'constraint' => '60',
            ),
            'updated_timestamp'   => array(
                'type' => 'TIMESTAMP',
                'NULL' => true,
            ),
            'updated_by'          => array(
                'type'       => 'VARCHAR',
                'constraint' => '60',
                'NULL' => true,
            ),
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->table_name);
        echo $this->table_name . ' table created <BR>';

    }

    public function down()
    {
        $this->dbforge->drop_table($this->table_name, TRUE);
        echo 'Drop table ' . $this->table_name . '<BR>';
    }
}
