<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_privileges_groups extends CI_Migration
{

    private $table_name;

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
        $this->table_name = 'privileges_groups';
    }

    public function up()
    {

        $this->dbforge->add_field(array(
            'id'         => array(
                'type'           => 'INTEGER',
                'unsigned'       => true,
                'auto_increment' => true,
            ),
            'privilege_id'  => array(
                'type' => 'INTEGER',
                'NULL'       => false,
            ),
            'group_id'  => array(
                'type' => 'INTEGER',
                'NULL' => false,
            ),
            'permission'  => array(
                'type' => 'BOOLEAN',
                'DEFAULT' => false,
            )
        ));

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->table_name);
        echo $this->table_name . ' table created <BR>';
    }

    public function down()
    {
        $this->dbforge->drop_table($this->table_name);
        echo 'Drop Table ' . $this->table_name . '<BR>';
    }
}
