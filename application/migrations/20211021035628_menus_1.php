<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_menus_1 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        //SYSTEM ADMIN
            //  - MANAGE USERS
            //  - MANAGE ROLE
            
    
            // add system admin as parent menu
            $data = array(
                'parent_id'  => 0,
                'menu_code'  => 'SYSTEM_ADMIN',
                'menu_name'  => 'System Admin',
                'url'        => null,
                'icon'       => 'la ft-settings',
                'is_active'  => true,
                'menu_order' => 100
            );
    
            $this->db->insert('menus', $data);
            $parent_id = $this->db->insert_id();
    
            //prepare parent menu for manage user
            $data = array(
                'parent_id'  => $parent_id,
                'menu_code'  => 'SYSTEM_ADMIN.MANAGE_USERS',
                'menu_name'  => 'Manage Users',
                'url'        => 'system_admin_manage_user',
                'icon'       => null,
                'is_active'  => true,
                'menu_order' => 10,
            );
            $this->db->insert('menus', $data);
            
            //prepare parent menu for manage role
            $data = array(
                'parent_id'  => $parent_id,
                'menu_code'  => 'SYSTEM_ADMIN.MANAGE_ROLE',
                'menu_name'  => 'Manage Role',
                'url'        => 'system_admin_manage_role',
                'icon'       => null,
                'is_active'  => true,
                'menu_order' => 20
            );
    
            $this->db->insert('menus', $data);
       
        echo 'Complete update menus value<BR>';
    }

    public function down()
    {
        //delete menu 
        $this->db->where_in('menu_code', array(
            'SYSTEM_ADMIN', 
            'SYSTEM_ADMIN.MANAGE_USERS', 
            'SYSTEM_ADMIN.MANAGE_ROLE',
        ));
        $this->db->delete('menus');
        echo 'Complete undo menus value<BR>';
    }
}