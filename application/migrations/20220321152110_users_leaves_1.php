<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_users_leaves_1 extends CI_Migration
{

    private $table_name;

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
        $this->table_name = 'users_leaves';
    }

    public function up()
    {
        $this->dbforge->add_field(array(
            'id'         => array(
                'type'           => 'INTEGER',
                'unsigned'       => true,
                'auto_increment' => true,
            ),
            'staff_id'   => array(
                'type'       => 'VARCHAR',
                'constraint' => 15,
                'NULL' => true
            ),
            'staff_annual_leave'=> array(
                'type'   => 'DECIMAL',
                'constraint' => '10,1',
                'default' => 0.0,
                'NULL'   => true
            ),
            'staff_emergency_leave'=> array(
                'type'   => 'DECIMAL',
                'constraint' => '10,1',
                'default' => 0.0,
                'NULL'   => true
            ),
            'staff_fowarded_leave'=> array(
                'type'   => 'DECIMAL',
                'constraint' => '10,1',
                'default' => 0.0,
                'NULL'   => true
            ),
            'staff_balance_leave'=> array(
                'type'   => 'DECIMAL',
                'constraint' => '10,1',
                'default' => 0.0,
                'NULL'   => true
            ),
            'staff_mc'   => array(
                'type'   => 'DECIMAL',
                'constraint' => '10,1',
                'default' => 0.0,
                'NULL'   => true
            ),
            'staff_maternity_leave'   => array(
                'type'   => 'DECIMAL',
                'constraint' => '10,1',
                'default' => 0.0,
                'NULL'   => true
            ),
            'staff_half_day'   => array(
                'type'   => 'DECIMAL',
                'constraint' => '10,1',
                'default' => 0.0,
                'NULL'   => true
            ),
            'staff_advance_leave'   => array(
                'type'   => 'DECIMAL',
                'constraint' => '10,1',
                'default' => 0.0,
                'NULL'   => true
            ),
            'staff_unpaid_leave'   => array(
                'type'   => 'DECIMAL',
                'constraint' => '10,1',
                'default' => 0.0,
                'NULL'   => true
            ),
            'staff_unrecorded_leave'   => array(
                'type'   => 'DECIMAL',
                'constraint' => '10,1',
                'default' => 0.0,
                'NULL'   => true
            ),
            'staff_total_leave'=> array(
                'type'   => 'DECIMAL',
                'constraint' => '10,1',
                'default' => 0.0,
                'NULL'   => true
            ),
        ));

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->table_name);
        echo $this->table_name . ' table created <BR>';
    }

    public function down()
    {
        $this->dbforge->drop_table($this->table_name, TRUE);
        echo 'Drop table ' . $this->table_name . '<BR>';
    }
}