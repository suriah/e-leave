<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_menus_5 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        //CALENDAR
            //  - Public Holiday
            //  - D'Hearty
            
    
            // add system admin as parent menu
            $data = array(
                'parent_id'  => 0,
                'menu_code'  => 'CALENDAR',
                'menu_name'  => 'Calendar',
                'url'        => null,
                'icon'       => 'ft ft-calendar',
                'is_active'  => true,
                'menu_order' => 30
            );
    
            $this->db->insert('menus', $data);
            $parent_id = $this->db->insert_id();
    
            //prepare parent menu for leave request
            $data = array(
                'parent_id'  => $parent_id,
                'menu_code'  => 'CALENDAR.PUBLIC_HOLIDAY',
                'menu_name'  => 'Public Holiday',
                'url'        => 'calendar_public_holiday',
                'icon'       => null,
                'is_active'  => true,
                'menu_order' => 10,
            );
            $this->db->insert('menus', $data);
            
            //prepare parent menu for leave approval
            $data = array(
                'parent_id'  => $parent_id,
                'menu_code'  => 'CALENDAR.DHEARTY',
                'menu_name'  => "D'HEARTY",
                'url'        => 'calendar_dhearty',
                'icon'       => null,
                'is_active'  => true,
                'menu_order' => 20,
            );
    
            $this->db->insert('menus', $data);
       
        echo 'Complete update menus value<BR>';
    }

    public function down()
    {
        //delete menu 
        $this->db->where_in('menu_code', array(
            'CALENDAR', 
            'CALENDAR.PUBLIC_HOLIDAY', 
            'CALENDAR.DHEARTY'
        ));
        $this->db->delete('menus');
        echo 'Complete undo menus value<BR>';
    }
}
