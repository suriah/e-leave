<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_drop_privileges_1 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        $privileges = array(
            'REPORT.LEAVE_REPORT', 
            'REPORT.APPROVAL_REPORT'
        );
        $this->db->where_in ('privilege_code', $privileges);
        $this->db->delete('privileges');
        echo 'Complete drop privileges value<BR>';
    }

    public function down()
    {
        $data = array(
            array(
                'privilege_code'        => 'REPORT.LEAVE_REPORT',
                'privilege_description' => 'Report > Leave Report',
                'module_name'           => 'REPORT',
                'privilege_type'        => 'MENU'
            ),
            array(
                'privilege_code'        => 'REPORT.APPROVAL_REPORT',
                'privilege_description' => 'Report > Approval Report',
                'module_name'           => 'REPORT',
                'privilege_type'        => 'MENU'
            ),
            
            
        );
           
        $this->db->insert_batch('privileges', $data);
        echo 'Complete add privileges value<BR>';
    }
}
