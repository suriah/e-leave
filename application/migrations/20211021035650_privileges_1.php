<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_privileges_1 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        $data = array(
            array(
                'privilege_code'        => 'SYSTEM_ADMIN',
                'privilege_description' => 'System Admin',
                'module_name'           => 'SYSTEM_ADMIN',
                'privilege_type'        => 'MENU'
            ),
            array(
                'privilege_code'        => 'SYSTEM_ADMIN.MANAGE_USERS',
                'privilege_description' => 'System Admin > Manage User',
                'module_name'           => 'SYSTEM_ADMIN',
                'privilege_type'        => 'MENU'
            ),
            
            array(
                'privilege_code'        => 'SYSTEM_ADMIN.MANAGE_ROLE',
                'privilege_description' => 'System Admin > Manage Role',
                'module_name'           => 'SYSTEM_ADMIN',
                'privilege_type'        => 'MENU'
            ),
        );
           
        $this->db->insert_batch('privileges', $data);
        echo 'Complete update privileges value<BR>';
    }

    public function down()
    {
        //remove this privileges
        $privileges = array(
            'SYSTEM_ADMIN', 
            'SYSTEM_ADMIN.MANAGE_USERS', 
            'SYSTEM_ADMIN.MANAGE_ROLE'
        );
        $this->db->where_in ('privilege_code', $privileges);
        $this->db->delete('privileges');
        echo 'Complete undo privileges value<BR>';
    }
}
