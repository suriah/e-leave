<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_menus_2 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        //SYSTEM ADMIN
        //      - MANAGE USERS
        //      - MANAGE ROLE
        //      - MANAGE DETAILS
        //          - MANAGE DEPARTMENT
        //          - MANAGE POSITION
        //          - MANAGE EMPLOYEE STATUS
        //          - MANAGE LEAVE TYPE

        // get system admin as parent menu
        $this->db->where('menu_code', 'SYSTEM_ADMIN');        
        $query = $this->db->get('menus');
        $row = $query->row();
        $parent_id = $row->id;

        //prepare parent menu for manage details
        $data = array(
            'parent_id'  => $parent_id,
            'menu_code'  => 'SYSTEM_ADMIN.MANAGE_DETAILS',
            'menu_name'  => 'Manage Details',
            'url'        => null,
            'icon'       => null,
            'is_active'  => true,
            'menu_order' => 30,
        );
        $this->db->insert('menus', $data);
        $details_parent_id = $this->db->insert_id();

        //prepare sub menu for manage details
        $data = array(
            'parent_id'  => $details_parent_id,
            'menu_code'  => 'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_DEPARTMENT',
            'menu_name'  => 'Manage Department',
            'url'        => 'system_admin_manage_department',
            'icon'       => null,
            'is_active'  => true,
            'menu_order' => 10,
        );
        $this->db->insert('menus', $data);

        //prepare sub menu for manage details
        $data = array(
            'parent_id'  => $details_parent_id,
            'menu_code'  => 'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_POSITION',
            'menu_name'  => 'Manage Position',
            'url'        => 'system_admin_manage_position',
            'icon'       => null,
            'is_active'  => true,
            'menu_order' => 20,
        );
        $this->db->insert('menus', $data);

        //prepare sub menu for manage details
        $data = array(
            'parent_id'  => $details_parent_id,
            'menu_code'  => 'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_EMPLOYEE_STATUS',
            'menu_name'  => 'Manage Employee Status',
            'url'        => 'system_admin_manage_employee_status',
            'icon'       => null,
            'is_active'  => true,
            'menu_order' => 30,
        );
        $this->db->insert('menus', $data);

        $data = array(
            'parent_id'  => $details_parent_id,
            'menu_code'  => 'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_LEAVE_TYPE',
            'menu_name'  => 'Manage Leave Type',
            'url'        => 'system_admin_manage_leave_type',
            'icon'       => null,
            'is_active'  => true,
            'menu_order' => 40,
        );
        $this->db->insert('menus', $data);

        echo 'Complete update menus value<BR>';
    }

    public function down()
    {
        //delete menu 
        $this->db->where_in('menu_code', array(
            'SYSTEM_ADMIN.MANAGE_DETAILS',
            'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_DEPARTMENT',
            'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_POSITION',
            'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_EMPLOYEE_STATUS',
            'SYSTEM_ADMIN.MANAGE_DETAILS.MANAGE_LEAVE_TYPE',

        ));
        $this->db->delete('menus');
        echo 'Complete undo menus value<BR>';
    }
}