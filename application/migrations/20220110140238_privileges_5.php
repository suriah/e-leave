<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_privileges_5 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        $data = array(
            array(
                'privilege_code'        => 'CALENDAR',
                'privilege_description' => 'Calendar',
                'module_name'           => 'CALENDAR',
                'privilege_type'        => 'MENU'
            ),
            array(
                'privilege_code'        => 'CALENDAR.PUBLIC_HOLIDAY',
                'privilege_description' => 'Calendar > Public Holiday',
                'module_name'           => 'CALENDAR',
                'privilege_type'        => 'MENU'
            ),
            array(
                'privilege_code'        => 'CALENDAR.DHEARTY',
                'privilege_description' => 'Calendar > DHearty',
                'module_name'           => 'CALENDAR',
                'privilege_type'        => 'MENU'
            ),
            
            
        );
           
        $this->db->insert_batch('privileges', $data);
        echo 'Complete update privileges value<BR>';
    }

    public function down()
    {
        //remove this privileges
        $privileges = array(
            'CALENDAR', 
            'CALENDAR.PUBLIC_HOLIDAY', 
            'CALENDAR.DHEARTY'
        );
        $this->db->where_in ('privilege_code', $privileges);
        $this->db->delete('privileges');
        echo 'Complete undo privileges value<BR>';
    }
}
