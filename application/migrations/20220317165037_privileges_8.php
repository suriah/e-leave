<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_privileges_8 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        $data = array(
            array(
                'privilege_code'        => 'TIMEOFF_APPLICATION',
                'privilege_description' => 'Time Off Application',
                'module_name'           => 'TIMEOFF_APPLICATION',
                'privilege_type'        => 'MENU'
            ),
            array(
                'privilege_code'        => 'TIMEOFF_APPLICATION.TIMEOFF_REQUEST',
                'privilege_description' => 'Time Off Application > Time Off Request',
                'module_name'           => 'TIMEOFF_APPLICATION',
                'privilege_type'        => 'MENU'
            ),
            array(
                'privilege_code'        => 'TIMEOFF_APPLICATION.TIMEOFF_APPROVAL',
                'privilege_description' => 'Time Off Application > Time Off Approval',
                'module_name'           => 'TIMEOFF_APPLICATION',
                'privilege_type'        => 'MENU'
            ),
            
            
        );
           
        $this->db->insert_batch('privileges', $data);
        echo 'Complete update privileges value<BR>';
    }

    public function down()
    {
        //remove this privileges
        $privileges = array(
            'TIMEOFF_APPLICATION', 
            'TIMEOFF_APPLICATION.TIMEOFF_REQUEST', 
            'TIMEOFF_APPLICATION.TIMEOFF_APPROVAL'
        );
        $this->db->where_in ('privilege_code', $privileges);
        $this->db->delete('privileges');
        echo 'Complete undo privileges value<BR>';
    }
}
