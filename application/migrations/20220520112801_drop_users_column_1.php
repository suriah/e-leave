<?php 

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_drop_users_column_1 extends CI_Migration
{

    private $table_name;

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        $this->db->query(
        'ALTER TABLE users
        DROP COLUMN staff_annual_leave,
        DROP COLUMN staff_emergency_leave,
        DROP COLUMN staff_mc,
        DROP COLUMN staff_balance_forwarded_leave,
        DROP COLUMN staff_maternity_leave,
        DROP COLUMN staff_leave_total,
        DROP COLUMN staff_leave_balance,
        DROP COLUMN staff_advance_leave');


        echo 'Complete drop cloumn in user table <BR>';
    }

    public function down()
    {
        $this->db->query(
            'ALTER TABLE users
                ADD COLUMN staff_annual_leave,
                ADD COLUMN staff_emergency_leave,
                ADD COLUMN staff_mc,
                ADD COLUMN staff_balance_forwarded_leave,
                ADD COLUMN staff_maternity_leave,
                ADD COLUMN staff_leave_total,
                ADD COLUMN staff_leave_balance,
                ADD COLUMN staff_advance_leave'
            );
        echo 'Complete undo drop cloumn in user table <BR>';
    }
}
