<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_rename_column_2 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        $this->db->query('ALTER TABLE users_leaves ALTER COLUMN staff_id TYPE INTEGER USING (id::integer)');
        echo 'Complete update database value<BR>';
    }

    public function down()
    {
        $this->db->query('ALTER TABLE users_leaves ALTER COLUMN staff_id TYPE VARCHAR(15)');
        echo 'Complete undo database value<BR>';
    }
}
