<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_menus_7 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        //WFH Application
            //  - REQUEST
            //  - APPROVAL
            
    
            // add system admin as parent menu
            $data = array(
                'parent_id'  => 0,
                'menu_code'  => 'WFH_APPLICATION',
                'menu_name'  => 'WFH Application',
                'url'        => null,
                'icon'       => 'ft ft-briefcase',
                'is_active'  => true,
                'menu_order' => 22
            );
    
            $this->db->insert('menus', $data);
            $parent_id = $this->db->insert_id();
    
            //prepare parent menu for WFH request
            $data = array(
                'parent_id'  => $parent_id,
                'menu_code'  => 'WFH_APPLICATION.WFH_REQUEST',
                'menu_name'  => 'WFH Request',
                'url'        => 'wfh_request',
                'icon'       => null,
                'is_active'  => true,
                'menu_order' => 10,
            );
            $this->db->insert('menus', $data);
            
            //prepare parent menu for WFH approval
            $data = array(
                'parent_id'  => $parent_id,
                'menu_code'  => 'WFH_APPLICATION.WFH_APPROVAL',
                'menu_name'  => 'WFH Approval',
                'url'        => 'wfh_approval',
                'icon'       => null,
                'is_active'  => true,
                'menu_order' => 20,
            );
    
            $this->db->insert('menus', $data);
       
        echo 'Complete update menus value<BR>';
    }

    public function down()
    {
        //delete menu 
        $this->db->where_in('menu_code', array(
            'WFH_APPLICATION.WFH_REQUEST', 
            'WFH_APPLICATION.WFH_APPROVAL',
            'WFH_APPLICATION', 
        ));
        $this->db->delete('menus');
        echo 'Complete undo menus value<BR>';
    }
}