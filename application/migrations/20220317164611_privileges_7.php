<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_privileges_7 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        $data = array(
            array(
                'privilege_code'        => 'WFH_APPLICATION',
                'privilege_description' => 'WFH Application',
                'module_name'           => 'WFH_APPLICATION',
                'privilege_type'        => 'MENU'
            ),
            array(
                'privilege_code'        => 'WFH_APPLICATION.WFH_REQUEST',
                'privilege_description' => 'WFH Application > WFH Request',
                'module_name'           => 'WFH_APPLICATION',
                'privilege_type'        => 'MENU'
            ),
            array(
                'privilege_code'        => 'WFH_APPLICATION.WFH_APPROVAL',
                'privilege_description' => 'WFH Application > WFH Approval',
                'module_name'           => 'WFH_APPLICATION',
                'privilege_type'        => 'MENU'
            ),
            
            
        );
           
        $this->db->insert_batch('privileges', $data);
        echo 'Complete update privileges value<BR>';
    }

    public function down()
    {
        //remove this privileges
        $privileges = array(
            'WFH_APPLICATION', 
            'WFH_APPLICATION.WFH_REQUEST', 
            'WFH_APPLICATION.WFH_APPROVAL'
        );
        $this->db->where_in ('privilege_code', $privileges);
        $this->db->delete('privileges');
        echo 'Complete undo privileges value<BR>';
    }
}
