<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_menus_8 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        //Time-off Application
            //  - REQUEST
            //  - APPROVAL
            
    
            // add system admin as parent menu
            $data = array(
                'parent_id'  => 0,
                'menu_code'  => 'TIMEOFF_APPLICATION',
                'menu_name'  => 'Time-off Application',
                'url'        => null,
                'icon'       => 'ft ft-clock',
                'is_active'  => true,
                'menu_order' => 24
            );
    
            $this->db->insert('menus', $data);
            $parent_id = $this->db->insert_id();
    
            //prepare parent menu for WFH request
            $data = array(
                'parent_id'  => $parent_id,
                'menu_code'  => 'TIME_OFF_APPLICATION.TIME_OFF_REQUEST',
                'menu_name'  => 'Time-off Request',
                'url'        => 'time_off_request',
                'icon'       => null,
                'is_active'  => true,
                'menu_order' => 10,
            );
            $this->db->insert('menus', $data);
            
            //prepare parent menu for TIMEOFF approval
            $data = array(
                'parent_id'  => $parent_id,
                'menu_code'  => 'TIME_OFF_APPLICATION.TIME_OFF_APPROVAL',
                'menu_name'  => 'Time-off Approval',
                'url'        => 'time_off_approval',
                'icon'       => null,
                'is_active'  => true,
                'menu_order' => 20,
            );
    
            $this->db->insert('menus', $data);
       
        echo 'Complete update menus value<BR>';
    }

    public function down()
    {
        //delete menu 
        $this->db->where_in('menu_code', array(
            'TIME_OFF_APPLICATION.TIME_OFF_REQUEST', 
            'TIME_OFF_APPLICATION.TIME_OFF_APPROVAL',
            'TIME_OFF_APPLICATION', 
        ));
        $this->db->delete('menus');
        echo 'Complete undo menus value<BR>';
    }
}