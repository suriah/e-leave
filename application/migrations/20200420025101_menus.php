<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_menus extends CI_Migration
{

    private $table_name;

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
        $this->table_name = 'menus';
    }

    public function up()
    {

        $this->dbforge->add_field(array(
            'id'         => array(
                'type'           => 'INTEGER',
                'unsigned'       => true,
                'auto_increment' => true,
            ),
            'parent_id'  => array(
                'type' => 'INTEGER',
                'NULL' => true,
            ),
            'menu_name'  => array(
                'type'       => 'VARCHAR',
                'constraint' => '30',
                'NULL'       => true,
            ),
            'menu_code'  => array(
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'unique'     => true,
            ),
            'url'        => array(
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'NULL'       => true,
            ),
            'icon'       => array(
                'type'       => 'VARCHAR',
                'constraint' => '25',
                'NULL'       => true,
            ),
            'is_active'  => array(
                'type' => 'BOOLEAN',
                'NULL' => true,
            ),
            'menu_order' => array(
                'type' => 'INTEGER',
                'NULL' => true,
            ),
        ));

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->table_name);
        echo $this->table_name . ' table created <BR>';
    }

    public function down()
    {

        $this->dbforge->drop_table('menus');
        echo 'Drop Table menus<BR>';
    }
}
