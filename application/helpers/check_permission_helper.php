<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * check_permission
 * example :
 * $permission_code = 'XXXXX';
 * $request_change_seal = check_permission($this->data['privilege_list'] , $permission_code);
 *
 * @param  mixed $privilege_list
 * @param  mixed $action_privilege_code
 * @return boolean 
 */
function check_permission($privilege_list, $action_privilege_code){
    foreach ($privilege_list  as $privilege_code){
        if ($action_privilege_code == $privilege_code){
            $permission = true;
            return $permission;
        }
    }
    return false;
}