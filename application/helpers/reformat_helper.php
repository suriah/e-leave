<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * this function will reformat from standard array list into selectize option
 * @param Array $list_record
 * @param String $identifier
 * @param String $diplay_text
 *
 */ 
function selectize_reformat($list_record, $identifier,  $display_text){
    $selectize_list =array();
    foreach ($list_record as $value) {
        $selectize_list[] = array(
            'value' => $value[$identifier],
            'text'  => $value[$display_text],
        );
    }
    return $selectize_list;
}