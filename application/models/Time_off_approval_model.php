<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Time_off_approval_model extends Ajax_datatable_model
{

    public $auth_info;
    var $table           = 'time_off';
    var $column_order    = array('time_off_date', 'time_off_time_start','time_off_time_end','time_off_reason','manager_id','time_off_status',null); //set column field database for datatable orderable
    var $column_search   = array('time_off_date', 'time_off_time_start','time_off_time_end','time_off_reason','manager_id','time_off_status'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order           = array('id' => 'asc'); // default order 
    var $filter_where_in = array('time_off_status' => array('REQ_APPROVAL'));  //sample
    // var $filter          = null;
    

    var $join = array(   
        array(
            'table'     => 'users',
            'map'       => 'time_off.staff_id = users.id',
            'join_type' => 'left'
        ),       
    );
    var $selection = 'time_off.* ,users.first_name as staff_name, users.email as staff_email';

    public function __construct()
    {
        parent::__construct();

    }

    public function list_manager_drop_down(){
        
        $this->db->select('users.* , levels.level_code');
        $this->db->join('levels', 'users.staff_level_id = levels.id', 'left');
        $bind = ['Man','DIR'];
        $this->db->where_in('level_code', $bind);
        $query  = $this->db->get('users');
        $result = $query->result_array();
    
        $data = array();
        $data[''] = 'Select manager';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data[$row['id']] = $row['first_name'];
            }
        }
        return $data;
    }


    public function view_process_data($id){
        
        $this->db->select('time_off.* , users.first_name as staff_name, users.email as staff_email');
        $this->db->where('time_off.id', $id);
        $this->db->join('users', 'time_off.staff_id = users.id','left');
        $query  = $this->db->get('time_off');
        $result = $query->row();
    
        return $result;
    }

    public function time_off_approved_notification_email($id)
    {
        //this function will send email along with reset code
        $this->load->library('email');
        $this->email->set_newline("\r\n");

        $this->db->select('time_off.*,users.first_name,users.email');
        $this->db->where('time_off.id',$id);
        $this->db->join('users','users.id = time_off.staff_id','left');
        $query=$this->db->get('time_off');
        $result=$query->row();
        $email=null;

      
        $this->db->where('id',$result->manager_id);
        $query=$this->db->get('users');
        $users = $query->row();
        

        $this->db->select('first_name, email');
        $this->db->where('position_code', 'HR');
        $this->db->join('positions','positions.id = users.staff_position_id','left');
        $query=$this->db->get('users');
        $hr = $query->row();

        $data = array(
            'staff'=>$result,
            'superior'=>$users,
            'hr'=>$hr,
        );

        $message = $this->load->view('email_template/time_off_approval.php',$data,TRUE);
        $this->email->from('eleave_admin@geoinfo.com.my');
        $this->email->to($result->email); 
        $this->email->cc($hr->email);
        $this->email->bcc('eleave_admin@geoinfo.com.my');
        $email_subject = 'Time Off Application Approved';
        $this->email->subject($email_subject);
        $this->email->message($message);
        if ($this->email->send()) 
        {
            return true;
        }else{
            show_error($this->email->print_debugger());
            return false;
        }     
        
    }
    public function time_off_rejected_notification_email($id)
    {
        //this function will send email along with reset code
        $this->load->library('email');
        $this->email->set_newline("\r\n");

        $this->db->select('time_off.*,users.first_name,users.email');
        $this->db->where('time_off.id',$id);
        $this->db->join('users','users.id = time_off.staff_id','left');
        $query=$this->db->get('time_off');
        $result=$query->row();
        $email=null;

      
            $this->db->where('id',$result->manager_id);
            $query=$this->db->get('users');
            $users = $query->row();
            $email = $users->email;

        $this->db->select('first_name, email');
        $this->db->where('position_code', 'HR');
        $this->db->join('positions','positions.id = users.staff_position_id','left');
        $query=$this->db->get('users');
        $hr = $query->row();

        $data = array(
            'staff'=>$result,
            'superior'=>$users,
            'hr'=>$hr,
        );

        $message = $this->load->view('email_template/time_off_rejected.php',$data,TRUE);
        $this->email->from('eleave_admin@geoinfo.com.my');
        $this->email->to($result->email); 
        $this->email->cc($hr->email);
        $this->email->bcc('eleave_admin@geoinfo.com.my');
        $email_subject = 'Time Off Application Rejected';
        $this->email->subject($email_subject);
        $this->email->message($message);
        if ($this->email->send()) 
        {
            return true;
        }else{
            show_error($this->email->print_debugger());
            return false;
        }     
        
    }
}