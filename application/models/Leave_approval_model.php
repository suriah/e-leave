<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Leave_approval_model extends Ajax_datatable_model
{

    public $auth_info;
    var $table           = 'leave';
    var $column_order    = array('leave_name', 'leave_date_from', 'leave_date_to','total_leave','leave_reason','supervisor_id','manager_id','leave_status',null); //set column field database for datatable orderable
    var $column_search   = array('leave_name', 'leave_date_from', 'leave_date_to','total_leave','leave_reason','supervisor_id','manager_id','leave_status'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order           = array('id' => 'asc'); // default order 
    var $filter_where_in = array('leave_status' => array('REQ_APPROVAL_SV','REQ_APPROVAL_MAN'));  //sample
    // var $filter          = null;
    

    var $join = array(
        array(
            'table'     => 'leave_type',
            'map'       => 'leave.leave_type_id = leave_type.id',
            'join_type' => 'left'
        ),       
        array(
            'table'     => 'users',
            'map'       => 'leave.staff_id = users.id',
            'join_type' => 'left'
        ),       
    );
    var $selection = 'leave.* , leave_type.leave_name, users.first_name as staff_name, users.email as staff_email';

    public function __construct()
    {
        parent::__construct();

    }

    public function is_supervisor($level_id){
        
        $this->db->select('level_code');
        $this->db->where('id', $level_id);
        $query  = $this->db->get('levels');
        // $result = $query->row();
        $result = $query->row()->level_code;
        // print_r($result);die;

        return $result;

    }



    public function list_leave_type_drop_down(){

        $query  = $this->db->get('leave_type');
        $result = $query->result_array();
    
        $data = array();
        $data[''] = 'Select leave type';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data[$row['id']] = $row['leave_name'];
            }
        }
        return $data;
    }

    public function list_supervisor_drop_down(){
        
        $this->db->select('users.* , levels.level_code');
        $this->db->join('levels', 'users.staff_level_id = levels.id', 'left');
        $bind = ['Exec'];
        $this->db->where_in('level_code', $bind);
        $query  = $this->db->get('users');
        $result = $query->result_array();
    
        $data = array();
        $data[''] = 'Select supervisor';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data[$row['id']] = $row['first_name'];
            }
        }
        return $data;
    }

    public function list_manager_drop_down(){
        
        $this->db->select('users.* , levels.level_code');
        $this->db->join('levels', 'users.staff_level_id = levels.id', 'left');
        $bind = ['Man','DIR'];
        $this->db->where_in('level_code', $bind);
        $query  = $this->db->get('users');
        $result = $query->result_array();
    
        $data = array();
        $data[''] = 'Select manager';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data[$row['id']] = $row['first_name'];
            }
        }
        return $data;
    }

    public function get_leave_type_name($id){
        
        $this->db->select('leave_name');
        $this->db->where('id', $id);
        $query  = $this->db->get('leave_type');
        $result = $query->row();
    
        return $result;
    }

    public function view_process_data($id){
        
        $this->db->select('leave.* , leave_type.leave_name, users.first_name as staff_name, users.email as staff_email');
        $this->db->where('leave.id', $id);
        $this->db->join('leave_type', 'leave_type.id = leave.leave_type_id','left');
        $this->db->join('users', 'leave.staff_id = users.id','left');
        $query  = $this->db->get('leave');
        $result = $query->row();
    
        return $result;
    }
    public function check_current_status($id){
        
        $this->db->select('leave.id,leave.leave_status');
        $this->db->where('leave.id', $id);
        $this->db->join('leave_type', 'leave_type.id = leave.leave_type_id','left');
        $query  = $this->db->get('leave');
        $result = $query->row();
    
        return $result;
    }
    public function update_users_leaves($id){
        
        $this->db->select('users_leaves.*,leave.staff_id,leave.total_leave,leave_type.leave_code');
        $this->db->where('leave.id', $id);
        $this->db->join('leave_type', 'leave_type.id = leave.leave_type_id','left');
        $this->db->join('users_leaves', 'users_leaves.staff_id = leave.staff_id','left');
        $query  = $this->db->get('leave');
        $result = $query->row();
        $this->calculate_leave($result);
        return $result;
    }

    private function calculate_leave($data){
        $data_user = array();
        if($data->leave_code == 'A' || $data->leave_code == 'E'){
            if($data->staff_balance_leave<$data->total_leave){
                $balance = $data->total_leave - $data->staff_balance_leave;
                $advance = $data->staff_advance_leave + $balance;
                $data_user = array(
                    'staff_balance_leave' => 0,
                    'staff_advance_leave' => $advance,
                );
            }else{
                $leave = $data->staff_balance_leave-$data->total_leave;
                $data_user = array(
                    'staff_balance_leave' => $leave,
                );
            }
        }
        if($data->leave_code == 'MC'){
            if($data->staff_mc<$data->total_leave){
                $balance = $data->total_leave - $data->staff_mc;
                if($data->staff_balance_leave < $balance){
                    $new_balance = $balance - $data->staff_balance_leave;
                    $advance = $data->staff_advance_leave + $new_balance;
                    $data_user = array(
                        'staff_mc' => 0,
                        'staff_balance_leave' => 0,
                        'staff_advance_leave' => $advance,
                    );
                }else{
                    $new_balance = $data->staff_balance_leave - $balance;
                    $data_user = array(
                        'staff_mc' => 0,
                        'staff_balance_leave' => $new_balance,
                    );
                }
            }else{
                $leave = $data->staff_mc-$data->total_leave;
                $data_user = array(
                    'staff_mc' => $leave,
                );
            }
        }
        if($data->leave_code == 'AD'){
            $leave = $data->staff_advance_leave+$data->total_leave;
            $data_user = array(
                'staff_advance_leave' => $leave,
            );
        }
        if($data->leave_code == 'ML'){
            $leave = $data->staff_maternity_leave-$data->total_leave;
            $data_user = array(
                'staff_maternity_leave' => $leave,
            );
        }
        if($data->leave_code == 'UP'){
            $leave = $data->staff_unpaid_leave+$data->total_leave;
            $data_user = array(
                'staff_unpaid_leave' => $leave,
            );

        }
        if($data->leave_code == 'UR'){
            $leave = $data->staff_unrecorded_leave+$data->total_leave;
            $data_user = array(
                'staff_unrecorded_leave' => $leave,
            );

        }
        if($data->leave_code == 'HD'){
            if($data->staff_balance_leave<$data->total_leave){
                $balance = $data->total_leave - $data->staff_balance_leave;
                $advance = $data->staff_advance_leave + $balance;
                $data_user = array(
                    'staff_balance_leave' => 0,
                    'staff_advance_leave' => $advance,
                );
            }else{
                $leave = $data->staff_balance_leave-0.5;
                $data_user = array(
                    'staff_balance_leave' => $leave,
                );
            }
        }

        if(!empty($data_user)){
            $this->db->where('staff_id', $data->staff_id);
            $status = $this->db->update('users_leaves', $data_user);
            json_encode(array("status" => $status));
        }
    }
    

    public function leave_request_notification_email_superior($id)
    {
        //this function will send email along with reset code
        $this->load->library('email');
        $this->email->set_newline("\r\n");

        $this->db->select('leave.*,users.first_name,users.email,leave_type.leave_name');
        $this->db->where('leave.id',$id);
        $this->db->join('users','users.id = leave.staff_id','left');
        $this->db->join('leave_type','leave_type.id = leave.leave_type_id','left');
        $query=$this->db->get('leave');
        $result=$query->row();
        $email=null;

    
        $this->db->where('id',$result->manager_id);
        $query=$this->db->get('users');
        $users = $query->row();

        $data = array(
            'staff'=>$result,
            'superior'=>$users,
        );

        $message = $this->load->view('email_template/leave_request.php',$data,TRUE);
        $this->email->from('eleave_admin@geoinfo.com.my');
        $this->email->to($users->email); 
        $email_subject = 'Leave Request Notification';
        $this->email->subject($email_subject);
        $this->email->message($message);
        if ($this->email->send()) 
        {
            return true;
        }else{
            
        //    show_error($this->email->print_debugger());
            return false;
        }     
        
    }

    public function leave_approved_notification_email($id)
    {
        //this function will send email along with reset code
        $this->load->library('email');
        $this->email->set_newline("\r\n");

        $this->db->select('leave.*,users.first_name,users.email,leave_type.leave_name');
        $this->db->where('leave.id',$id);
        $this->db->join('users','users.id = leave.staff_id','left');
        $this->db->join('leave_type','leave_type.id = leave.leave_type_id','left');
        $query=$this->db->get('leave');
        $result=$query->row();
        $email=null;

      
        $this->db->where('id',$result->manager_id);
        $query=$this->db->get('users');
        $users = $query->row();
        

        $this->db->select('first_name, email');
        $this->db->where('position_code', 'HR');
        $this->db->join('positions','positions.id = users.staff_position_id','left');
        $query=$this->db->get('users');
        $hr = $query->row();

        $data = array(
            'staff'=>$result,
            'superior'=>$users,
            'hr'=>$hr,
        );

        $message = $this->load->view('email_template/leave_approval.php',$data,TRUE);
        $this->email->from('eleave_admin@geoinfo.com.my');
        $this->email->to($result->email); 
        $this->email->cc($hr->email);
        $this->email->bcc('eleave_admin@geoinfo.com.my');
        $email_subject = 'Leave Application Approved';
        $this->email->subject($email_subject);
        $this->email->message($message);
        if ($this->email->send()) 
        {
            return true;
        }else{
            show_error($this->email->print_debugger());
            return false;
        }     
        
    }
    public function leave_rejected_notification_email($id,$decline_reasons)
    {
        //this function will send email along with reset code
        $this->load->library('email');
        $this->email->set_newline("\r\n");

        $this->db->select('leave.*,users.first_name,users.email,leave_type.leave_name');
        $this->db->where('leave.id',$id);
        $this->db->join('users','users.id = leave.staff_id','left');
        $this->db->join('leave_type','leave_type.id = leave.leave_type_id','left');
        $query=$this->db->get('leave');
        $result=$query->row();
        $email=null;

        if($result->leave_status == 'REQ_APPROVAL_SV'){
            $this->db->where('id',$result->supervisor_id);
            $query=$this->db->get('users');
            $users = $query->row();
            $email = $users->email;
        }else{
            $this->db->where('id',$result->manager_id);
            $query=$this->db->get('users');
            $users = $query->row();
            $email = $users->email;
        }

        $this->db->select('first_name, email');
        $this->db->where('position_code', 'HR');
        $this->db->join('positions','positions.id = users.staff_position_id','left');
        $query=$this->db->get('users');
        $hr = $query->row();

        $data = array(
            'staff'=>$result,
            'superior'=>$users,
            'hr'=>$hr,
            'info'=>$decline_reasons,
        );

        $message = $this->load->view('email_template/leave_rejected.php',$data,TRUE);
        $this->email->from('eleave_admin@geoinfo.com.my');
        $this->email->to($result->email); 
        $this->email->cc($hr->email);
        $this->email->bcc('eleave_admin@geoinfo.com.my');
        $email_subject = 'Leave Application Rejected';
        $this->email->subject($email_subject);
        $this->email->message($message);
        if ($this->email->send()) 
        {
            return true;
        }else{
            show_error($this->email->print_debugger());
            return false;
        }     
        
    }

    public function get_leave_type($id){
        $this->db->select('leave_type_id');
        $this->db->where('id',$id);
        $query  = $this->db->get('leave');
        $result = $query->row();

        $result = $result->leave_type_id;
        return $result;
    }
    public function add_leave_date($id){
        $this->db->select('leave_date_from,leave_date_to,staff_id');
        $this->db->where('id',$id);
        $query  = $this->db->get('leave');
        $result = $query->result_array();

        foreach ($result as $key => $value) {
            $date = $this->displayDates($value['leave_date_from'], $value['leave_date_to']);
        }
        return $date;
    }

    public function displayDates($date1, $date2, $format = 'Y-m-d') {
        $dates = array();
        $list_dates = array();
        $current = strtotime($date1);
        $date2 = strtotime($date2);
        $stepVal = '+1 day';
        while( $current <= $date2 ) {
           $dates[] = date($format, $current);
           $current = strtotime($stepVal, $current);
        }

        foreach ($dates as $key => $value) {
            $date=date_create($value);
            $query = $this->db->where('public_holiday_date',$value)->get('public_holiday');
            $is_public = $query->result();
            $day_num = $date->format("N");
            if($day_num != 6 && $day_num != 7) { /* weekday */
                if($is_public == null ){
                    $list_dates[] = $value;
                }
            }
        }
        $query_2 = $this->db->get('dhearty');
        $is_dhearty = $query_2->result();

        if($is_dhearty){

            foreach ($is_dhearty as $key => $value_dhearty) {
                foreach ($dates as $key => $value) {
                    $current = strtotime($value_dhearty->dhearty_date);
                    $dhearty_date=date('Y-m-d',$current);
                    if($value == $dhearty_date){
                        $list_dates[] =$dhearty_date;
                    }
                }
            }
        }
        return $list_dates;
    }
}