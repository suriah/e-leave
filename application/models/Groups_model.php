<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Groups_model extends Ajax_datatable_model
{

    public $table         = 'groups';
    public $column_order  = array('name', 'description', null); //set column field database for datatable orderable
    public $column_search = array('name', 'description'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    public $order         = array('id' => 'desc'); // default order
    //var $filter = array('role' => 'admin', 'email' => 'admin@admin.com');  //sample
    public $filter          = null;
    public $filter_where_in = null;

    public function __construct()
    {
        parent::__construct();
    }

    public function list_privileges()
    {
        //construct list privilege with structure module and privilege name
        //--module
        //-----privilege name
        //-----privilage name 2

        $data =array();
        $this->db->distinct();
        $this->db->select('module_name');
        $this->db->order_by('module_name', 'ASC');
        $query = $this->db->get('privileges');

        $result = $query->result();
        foreach ($result as $row){
            
            //get child data
            $this->db->where('module_name', $row->module_name);
            $query = $this->db->get('privileges');
            $result_array = $query->result_array();
            $data[$row->module_name] = $result_array;
        }

        return $data;
    }

    public function update_privilege($group_id, $privilege_id)
    {
        //clear data first before update
        $this->db->where('group_id', $group_id);
        $this->db->delete('privileges_groups');

        //privilege id is array
        $data = array();
        foreach ($privilege_id as $key => $permission) {
            $data[] = array(
                'group_id'     => $group_id,
                'privilege_id' => $key,
                'permission'   => $permission,
            );
        }
        $this->db->insert_batch('privileges_groups', $data);
    }

    public function get_privileges_detail($group_id){
        
        $this->db->where('group_id', $group_id);
        $this->db->select('privileges_groups.id, privilege_id, group_id, permission, privileges.privilege_code');
        $this->db->join('privileges', 'privileges_groups.privilege_id = privileges.id' , 'left');
        $query = $this->db->get('privileges_groups');
        return $query->result();
    
    }

}
