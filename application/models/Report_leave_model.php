<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Report_leave_model extends Ajax_datatable_model
{

    var $table           = 'leave';
    var $column_order    = array('staff_name', 'staff_email', null); //set column field database for datatable orderable
    var $column_search   = array('staff_name', 'staff_email',  null); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order           = array('id' => 'asc'); // default order 
    // var $filter = array('role' => 'admin', 'email' => 'admin@admin.com');  //sample
    // var $filter          = null;
    var $filter_where_in = array('leave_status' => array('APPROVED', 'REJECTED','CANCELLED'));

    var $join = array(
        array(
            'table'     => 'leave_type',
            'map'       => 'leave.leave_type_id = leave_type.id',
            'join_type' => 'left'
        ),       
        array(
            'table'     => 'users',
            'map'       => 'leave.staff_id = users.id',
            'join_type' => 'left'
        ),       
    );
    var $selection = 'leave.* , leave_type.leave_name, users.first_name as staff_name, users.email as staff_email';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_supervisor_name($id)
    {
        $this->db->select('users.first_name');
        $this->db->join('users','leave.supervisor_id = users.id','left');
        $this->db->where('leave.supervisor_id',$id);
        $query = $this->db->get('leave');
        $result = $query->row();
        return $result;
    }
    public function get_manager_name($id)
    {
        $this->db->select('users.first_name');
        $this->db->join('users','leave.manager_id = users.id','left');
        $this->db->where('leave.manager_id',$id);
        $query = $this->db->get('leave');
        $result = $query->row();
        return $result;
    }

    public function get_staff_list()
    {
        $query          = $this->db->get('users');
        $list_staff_array = $query->result_array();

        $list_staff = array();
        $list_staff[''] = 'All';
        foreach ($list_staff_array as $staff)
        {
            $list_staff[$staff['id']] = $staff['first_name'];
        }

        return $list_staff;
    }
    public function get_leave_type_list()
    {
        $query          = $this->db->get('leave_type');
        $list_leave_array = $query->result_array();

        $list_leave = array();
        $list_leave[''] = 'All';
        foreach ($list_leave_array as $leave)
        {
            $list_leave[$leave['id']] = $leave['leave_name'];
        }

        return $list_leave;
    }
    public function get_month_list()
    {
        // $startDate = new \DateTime('first day of next month');
        // $endDate = new \DateTime('1st january next year');
        $startDate = new \DateTime('1st january');
        $endDate = new \DateTime('1st december');
       
        $interval = new \DateInterval('P1M');
        $period = new \DatePeriod($startDate, $interval, $endDate);
       
        // Start array with current date
        $dates = array();
       $dates[''] = ['All'];
    //    array_push($dates, 'All');
       // Add all remaining dates to array
       foreach ($period as $date) {
            $dates[$date->Format('n')]= $date->Format('F');
       }                                 
       

        return $dates;
    }
    public function get_year_list()
    {
        $year_list = array();
        $year_list[''] = ['All'];
        $years = range(date('Y'), 2020);

        foreach ($years as $value) {
            $year_list[$value] = $value;
        }

        return $year_list;
    }

    public function get_all_staff_name()
    {
        $this->db->select('users.id,users.first_name,users_leaves.staff_balance_leave,users_leaves.staff_total_leave');
        $this->db->join('users_leaves','users_leaves.staff_id = users.id','left');
        $this->db->where_not_in('username', 'administrator');
        $query = $this->db->get('users');
        $result = $query->result();
        return $result;
    }
    public function get_staff_name($id)
    {
        $this->db->select('users.first_name,users_leaves.staff_balance_leave,users_leaves.staff_total_leave');
        $this->db->join('users_leaves','users_leaves.staff_id = users.id','left');
        $this->db->where('users.id',$id);
        $query = $this->db->get('users');
        $result = $query->row();
        return $result;
    }
    public function get_leave_type_name($id)
    {
        $this->db->select('leave_name');
        $this->db->where('id',$id);
        $query = $this->db->get('leave_type');
        $result = $query->row();
        return $result;
    }
    public function get_month($month)
    {
        $monthNum  = $month;
        $dateObj   = DateTime::createFromFormat('!m', $monthNum);
        $result = $dateObj->format('F');
        return $result;
    }
}