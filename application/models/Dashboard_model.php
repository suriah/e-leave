<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_model extends CI_model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function load_event_data()
    {
        $public_holiday = $this->_get_public_holiday();
        $d_hearty = $this->_get_d_hearty();

        $data = array(
            'public_holiday' => $public_holiday,
            'd_hearty' => $d_hearty,
        );

        return $data;
    }
    public function load_user_dashboard_data($auth_info)
    {
        $total_leave = $this->_get_total_leave($auth_info);
        $total_balance_leave = $this->_get_total_balance_leave($auth_info);
        $total_mc = $this->_get_total_mc($auth_info);
        $total_staff_balance_forwarded_leave = $this->_get_total_staff_balance_forwarded_leave($auth_info);
        $total_maternity_leave = $this->_get_total_maternity_leave($auth_info);
        $total_advance = $this->_get_total_advance($auth_info);
        $leave_chart = $this->_get_leave($auth_info);
        $leave_approved = $this->_get_approved($auth_info);
        $leave_rejected = $this->_get_rejected($auth_info);
        $leave_pending = $this->_get_pending($auth_info);
        $leave_cancelled = $this->_get_cancelled($auth_info);

        $data = array(
            'total_leave' => $total_leave,
            'balance_leave' => $total_balance_leave,
            'mc' => $total_mc,
            'staff_balance_forwarded_leave' => $total_staff_balance_forwarded_leave,
            'maternity_leave' => $total_maternity_leave,
            'advance_leave' => $total_advance,
            'leave_chart' => $leave_chart,
            'leave_approved' => $leave_approved,
            'leave_rejected' => $leave_rejected,
            'leave_pending' => $leave_pending,
            'leave_cancelled' => $leave_cancelled,
            'current_year' => date("Y"),
        );

        return $data;
    }

    public function load_admin_dashboard_data($auth_info)
    {
        $total_staff = $this->_get_total_staff();
        $total_leave_approved_year = $this->_get_total_leave_approved_year();
        $total_leave_approved_month = $this->_get_total_leave_approved_month();
        $total_leave_approved_day = $this->_get_total_leave_approved_day();
        $leave_details_day = $this->_get_leave_details_day();
        $leave_details_week = $this->_get_leave_details_week();
        $leave_details_month = $this->_get_leave_details_month();
        $leave_details_next_month = $this->_get_leave_details_next_month();
        $leave_details_all_status = $this->_get_leave_details_all_status();

        $data = array(
            'total_staff' => $total_staff,
            'total_leave_approved_year' => $total_leave_approved_year,
            'total_leave_approved_month' => $total_leave_approved_month,
            'total_leave_approved_day' => $total_leave_approved_day,
            'current_year' => date("Y"),
            'current_month' => date("F"),
            'next_month' => date('F',strtotime('first day of +1 month')),
            'leave_details_day' => $leave_details_day,
            'leave_details_week' => $leave_details_week,
            'leave_details_month' => $leave_details_month,
            'leave_details_next_month' => $leave_details_next_month,
            'leave_details_all_status' => $leave_details_all_status,
        );

        return $data;
    }

    public function load_superior_dashboard_data($auth_info)
    {
        $total_leave = $this->_get_total_leave($auth_info);
        $total_balance_leave = $this->_get_total_balance_leave($auth_info);
        $total_mc = $this->_get_total_mc($auth_info);
        $total_staff_balance_forwarded_leave = $this->_get_total_staff_balance_forwarded_leave($auth_info);
        $total_maternity_leave = $this->_get_total_maternity_leave($auth_info);
        $total_advance = $this->_get_total_advance($auth_info);
        $leave_chart = $this->_get_leave($auth_info);
        $leave_details_day = $this->_get_leave_details_day();
        $leave_details_week = $this->_get_leave_details_week();
        $leave_details_month = $this->_get_leave_details_month();
        $leave_details_next_month = $this->_get_leave_details_next_month();
        $total_approval = $this->_get_total_approval($auth_info);
        $total_cancelled = $this->_get_total_cancelled($auth_info);

        $data = array(
            'total_leave' => $total_leave,
            'balance_leave' => $total_balance_leave,
            'mc' => $total_mc,
            'staff_balance_forwarded_leave' => $total_staff_balance_forwarded_leave,
            'maternity_leave' => $total_maternity_leave,
            'advance_leave' => $total_advance,
            'leave_chart' => $leave_chart,
            'current_year' => date("Y"),
            'current_month' => date("F"),
            'next_month' => date('F',strtotime('first day of +1 month')),
            'staff_leave_details_day' => $leave_details_day,
            'staff_leave_details_week' => $leave_details_week,
            'staff_leave_details_month' => $leave_details_month,
            'staff_leave_details_next_month' => $leave_details_next_month,
            'total_approval' => $total_approval,
            'total_cancelled' => $total_cancelled,
        );

        return $data;
    }

    private function is_supervisor($level_id){
        $this->db->select('level_code');
        $this->db->where('id', $level_id);
        $query  = $this->db->get('levels');
        $result = $query->row()->level_code;
        return $result;
    }

    private function _get_total_approval($auth_info){
        $filter = "(manager_id = $auth_info->id or supervisor_id =$auth_info->id) AND leave_status in ('REQ_APPROVAL_SV','REQ_APPROVAL_MAN')";
        $this->db->where($filter);
        $query  = $this->db->get('leave');
        // print_r($query->result());die;
        $result = $query->num_rows();
        return $result;
    }
    private function _get_total_cancelled($auth_info){
        $filter = "(manager_id = $auth_info->id or supervisor_id =$auth_info->id) AND leave_status in ('REQ_CANCEL')";
        $this->db->where($filter);
        $query  = $this->db->get('leave');
        // print_r($query->result());die;
        $result = $query->num_rows();
        return $result;
    }


    private function _get_public_holiday()
    {
        $this->db->select("public_holiday,public_holiday_date");
        $this->db->where("date_trunc('week', now()) <= public_holiday_date");
        $this->db->where("public_holiday_date < date_trunc('week', now()) + '4 week'::interval");
        $this->db->order_by('public_holiday_date');
        $result =  $this->db->get('public_holiday')->result_array();

        return $result;
    }
    private function _get_d_hearty()
    {
        $this->db->select("dhearty,dhearty_date");
        $this->db->where("date_trunc('week', now()) <= dhearty_date");
        $this->db->where("dhearty_date < date_trunc('week', now()) + '4 week'::interval");
        $this->db->order_by('dhearty_date');
        $result =  $this->db->get('dhearty')->result_array();

        return $result;
    }

    private function _get_total_staff()
    {
        // $this->db->select('staff_leave_total');
        $this->db->where_not_in('username', 'administrator');
        $result =  $this->db->get('users')->num_rows();

        return $result;
    }
    private function _get_total_staff_leave_superior_view($auth_info)
    {
        $month = date('m');
        $today = date('Y-m-d');
        $this->db->select('users.staff_id,users.first_name,leave.total_leave,leave.leave_date_from,leave.leave_date_to, leave.leave_status');
        $this->db->where('leave_status', 'APPROVED');
        // $this->db->where('EXTRACT(MONTH FROM leave_date_from)=',$month);
        $this->db->where('supervisor_id', $auth_info->id);
        $this->db->or_where('manager_id', $auth_info->id);
        $this->db->where("'$today' BETWEEN date(leave_date_from) AND date(leave_date_to)");
        $this->db->join('users', 'leave.staff_id = users.id', 'left');
        $this->db->order_by('leave.leave_date_from', 'ACS');
        $query = $this->db->get('leave');
        $result = $query->num_rows();
        return $result;
    }

    private function _get_staff_leave_superior_view_day($auth_info)
    {
        $month = date('m');
        $today = date('Y-m-d');
        $this->db->select('users.staff_id,users.first_name,leave.total_leave,leave.leave_date_from,leave.leave_date_to, leave.leave_status');
        $this->db->where('EXTRACT(MONTH FROM leave_date_from)=', $month);
        $this->db->where('supervisor_id', $auth_info->id);
        $this->db->or_where('manager_id', $auth_info->id);
        $this->db->where("' $today' BETWEEN date(leave_date_from) AND date(leave_date_to)");
        $this->db->where('leave_status', 'APPROVED');
        $this->db->join('users', 'leave.staff_id = users.id', 'left');
        $this->db->order_by('leave.leave_date_from', 'ACS');
        $this->db->limit(5);
        $query = $this->db->get('leave');
        $result = $query->result_array();
        return $result;
    }
    private function _get_staff_leave_superior_view_week($auth_info)
    {
        $today = time();
        $wday = date('w', $today);   
        $datemon = date('Y-m-d', $today - ($wday - 1)*86400);
        $datetue = date('Y-m-d', $today - ($wday - 2)*86400);
        $datewed = date('Y-m-d', $today - ($wday - 3)*86400);
        $datethu = date('Y-m-d', $today - ($wday - 4)*86400);
        $datefri = date('Y-m-d', $today - ($wday - 5)*86400);

        $list = array($datemon,$datetue,$datewed,$datethu,$datefri);
        // $today = date('Y-m-d');
        $this->db->select('users.staff_id,users.first_name,leave.*');
        $this->db->join('leave', 'leave.id=leave_date.leave_id','left');
        $this->db->join('users', 'leave.staff_id = users.id', 'left');
        $this->db->where('supervisor_id', $auth_info->id);
        $this->db->or_where('manager_id', $auth_info->id);
        $this->db->where_in('leave_date',$list);
        $this->db->where('leave.leave_status', 'APPROVED');
        $this->db->group_by('leave.id,users.staff_id,users.first_name');
        $query = $this->db->get('leave_date');
        $result = $query->result_array();
        return $result;
    }
    private function _get_staff_leave_superior_view_month($auth_info)
    {
        $month = date('m');
        $today = date('Y-m-d');
        $this->db->select('users.staff_id,users.first_name,leave.total_leave,leave.leave_date_from,leave.leave_date_to, leave.leave_status');
        $this->db->where('supervisor_id', $auth_info->id);
        $this->db->or_where('manager_id', $auth_info->id);
        $this->db->where('leave_status', 'APPROVED');
        $this->db->where('EXTRACT(MONTH FROM leave_date_from)=', $month);
        $this->db->join('users', 'leave.staff_id = users.id', 'left');
        $this->db->order_by('leave.leave_date_from', 'ACS');
        $this->db->limit(10);
        $query = $this->db->get('leave');
        $result = $query->result_array();
        return $result;
    }
    private function _get_staff_leave_superior_view_next_month($auth_info)
    {
        // $month = date('m');
        // $today = date('Y-m-d');
        // $this->db->select('users.staff_id,users.first_name,leave.total_leave,leave.leave_date_from,leave.leave_date_to, leave.leave_status');
        // // $this->db->where('EXTRACT(MONTH FROM leave_date_from)=', $month);
        // $this->db->where('supervisor_id', $auth_info->id);
        // $this->db->or_where('manager_id', $auth_info->id);
        // $this->db->where('leave_status', 'APPROVED');
        // $this->db->where('EXTRACT(MONTH FROM leave_date_from)=', $month);
        // $this->db->join('users', 'leave.staff_id = users.id', 'left');
        // $this->db->order_by('leave.leave_date_from', 'ACS');
        // $this->db->limit(10);
        // $query = $this->db->get('leave');
        // $result = $query->result_array();
        
        $month = date('m')+1;
        $today = date('Y-m-d');
        $this->db->select('users.staff_id,users.first_name,leave.total_leave,leave.leave_date_from,leave.leave_date_to, leave.leave_status');
        $this->db->where('supervisor_id', $auth_info->id);
        $this->db->or_where('manager_id', $auth_info->id);
        $this->db->where('leave_status', 'APPROVED');
        $this->db->where('EXTRACT(MONTH FROM leave_date_from)=', $month);
        $this->db->join('users', 'leave.staff_id = users.id', 'left');
        $this->db->order_by('leave.leave_date_from', 'ACS');
        $query = $this->db->get('leave');
        $result = $query->result_array();
        return $result;
    }
    private function _get_total_leave_approved_year()
    {
        $this->db->select('*');
        $this->db->where('leave_status', 'APPROVED');
        $query = $this->db->get('leave')->num_rows();
        $result = $query;

        return $result;
    }

    private function _get_total_leave_approved_month()
    {
        $month = date('m');
        $this->db->select('*');
        $this->db->where('leave_status', 'APPROVED');
        $this->db->where('EXTRACT(MONTH FROM leave_date_from)=', $month);
        $query = $this->db->get('leave')->num_rows();
        $result = $query;

        return $result;
    }
    private function _get_total_leave_approved_day()
    {
        $today = date('Y-m-d');
        $this->db->select('leave_status');
        $this->db->where('leave_status', 'APPROVED');
        $this->db->where("' $today' BETWEEN date(leave_date_from) AND date(leave_date_to)");
        $query = $this->db->get('leave')->num_rows();
        $result = $query;

        return $result;
    }

    private function _get_leave_details_day()
    {
        $month = date('m');
        $today = date('Y-m-d');
        $this->db->select('users.staff_id,users.first_name,leave.total_leave,leave.leave_date_from,leave.leave_date_to, leave.leave_status');
        $this->db->where('leave_status', 'APPROVED');
        $this->db->where('EXTRACT(MONTH FROM leave_date_from)=', $month);
        $this->db->where("' $today' BETWEEN date(leave_date_from) AND date(leave_date_to)");
        // $this->db->where('date(leave_date_from)>=', $today);
        // $this->db->or_where('date(leave_date_to)>=', $today);
        $this->db->join('users', 'leave.staff_id = users.id', 'left');
        $this->db->order_by('leave.leave_date_from', 'ACS');
        // $query = str_replace('"',"",$query);
        $query = $this->db->get('leave');
        $result = $query->result_array();

        return $result;
    }
    private function _get_leave_details_week()
    {
        $today = time();
        $wday = date('w', $today);   
        $datemon = date('Y-m-d', $today - ($wday - 1)*86400);
        $datetue = date('Y-m-d', $today - ($wday - 2)*86400);
        $datewed = date('Y-m-d', $today - ($wday - 3)*86400);
        $datethu = date('Y-m-d', $today - ($wday - 4)*86400);
        $datefri = date('Y-m-d', $today - ($wday - 5)*86400);

        $list = array($datemon,$datetue,$datewed,$datethu,$datefri);
        // $today = date('Y-m-d');
        $this->db->select('users.staff_id,users.first_name,leave.*');
        $this->db->join('leave', 'leave.id=leave_date.leave_id','left');
        $this->db->join('users', 'leave.staff_id = users.id', 'left');
        $this->db->where_in('leave_date',$list);
        $this->db->where('leave_status', 'APPROVED');
        $this->db->group_by('leave.id,users.staff_id,users.first_name');
        $query = $this->db->get('leave_date');
        $result = $query->result_array();
        return $result;
    }
    private function _get_leave_details_month()
    {
        $month = date('m');
        $today = date('Y-m-d');
        $this->db->select('users.staff_id,users.first_name,leave.total_leave,leave.leave_date_from,leave.leave_date_to, leave.leave_status');
        $this->db->where('leave_status', 'APPROVED');
        $this->db->where('EXTRACT(MONTH FROM leave_date_from)=', $month);
        $this->db->join('users', 'leave.staff_id = users.id', 'left');
        $this->db->order_by('leave.leave_date_from', 'ACS');
        $query = $this->db->get('leave');
        $result = $query->result_array();

        return $result;
    }
    private function _get_leave_details_next_month()
    {
        $month = date('m')+1;
        $today = date('Y-m-d');
        $this->db->select('users.staff_id,users.first_name,leave.total_leave,leave.leave_date_from,leave.leave_date_to, leave.leave_status');
        $this->db->where('leave_status', 'APPROVED');
        $this->db->where('EXTRACT(MONTH FROM leave_date_from)=', $month);
        $this->db->join('users', 'leave.staff_id = users.id', 'left');
        $this->db->order_by('leave.leave_date_from', 'ACS');
        $query = $this->db->get('leave');
        $result = $query->result_array();

        return $result;
    }
    private function _get_leave_details_all_status()
    {
        $month = date('m');
        $today = date('Y-m-d');
        $this->db->select('users.staff_id,users.first_name,leave.total_leave,leave.leave_date_from,leave.leave_date_to, leave.leave_status');
        $this->db->where_not_in('leave_status', 'REJECTED');
        $this->db->where('EXTRACT(MONTH FROM leave_date_from)=', $month);
        $this->db->join('users', 'leave.staff_id = users.id' , 'left');
        $this->db->order_by('leave.leave_date_from', 'ACS');
        $query = $this->db->get('leave');
        $result = $query->result_array();
        return $result;
    }
    private function _get_total_leave($auth_info)
    {
        $this->db->select('staff_total_leave');
        $this->db->where('staff_id', $auth_info->id);
        $result =  $this->db->get('users_leaves')->row()->staff_total_leave;

        return $result;
    }
    private function _get_total_balance_leave($auth_info)
    {
        $this->db->select('staff_balance_leave');
        $this->db->where('staff_id', $auth_info->id);
        $result =  $this->db->get('users_leaves')->row()->staff_balance_leave;

        return $result;
    }
    private function _get_total_mc($auth_info)
    {
        $this->db->select('staff_mc');
        $this->db->where('staff_id', $auth_info->id);
        $result =  $this->db->get('users_leaves')->row()->staff_mc;

        return $result;
    }
    private function _get_total_staff_balance_forwarded_leave($auth_info)
    {
        $this->db->select('staff_forwarded_leave');
        $this->db->where('staff_id', $auth_info->id);
        $result =  $this->db->get('users_leaves')->row()->staff_forwarded_leave;

        return $result;
    }
    private function _get_total_maternity_leave($auth_info)
    {
        $this->db->select('staff_maternity_leave');
        $this->db->where('staff_id', $auth_info->id);
        $result =  $this->db->get('users_leaves')->row()->staff_maternity_leave;

        return $result;
    }
    private function _get_total_advance($auth_info)
    {
        $this->db->select('staff_advance_leave');
        $this->db->where('staff_id', $auth_info->id);
        $result =  $this->db->get('users_leaves')->row()->staff_advance_leave;

        return $result;
    }
    private function _get_approved($auth_info)
    {
        $this->db->select('*');
        $this->db->where('staff_id', $auth_info->id);
        $this->db->where('leave_status', 'APPROVED');
        $this->db->group_by('leave.id');
        $query = $this->db->get('leave')->num_rows();
        $result = $query;

        return $result;
    }
    private function _get_rejected($auth_info)
    {
        $this->db->select('leave_status');
        $this->db->where('staff_id', $auth_info->id);
        $this->db->where('leave_status', 'REJECTED');
        $this->db->group_by('leave_status');
        $query = $this->db->get('leave')->num_rows();
        $result = $query;

        return $result;
    }
    private function _get_pending($auth_info)
    {

        $status = array('APPROVED', 'REJECTED','CANCELLED');
        $this->db->select('leave_status');
        $this->db->where('staff_id', $auth_info->id);
        $this->db->where_not_in('leave_status', $status);
        $this->db->group_by('leave_status');
        $query = $this->db->get('leave')->num_rows();
        $result = $query;

        return $result;
    }
    private function _get_cancelled($auth_info)
    {

        $status = 'CANCELLED';
        $this->db->select('leave_status');
        $this->db->where('staff_id', $auth_info->id);
        $this->db->where('leave_status', $status);
        $this->db->group_by('leave_status');
        $query = $this->db->get('leave')->num_rows();
        $result = $query;

        return $result;
    }

    private function _get_leave($auth_info)
    {
        //get leave
        $this->db->select('leave_type.leave_name, sum(total_leave) as jumlah');
        $this->db->where_in('staff_id', $auth_info->id);
        $this->db->where('leave_status', 'APPROVED');
        $this->db->join('leave_type', 'leave_type.id = leave.leave_type_id', 'left');
        $this->db->group_by('leave_type.leave_name');
        $query_leave = $this->db->get('leave');

        $result_leave = $query_leave->result_array();

        if ($query_leave->num_rows() > 0) {
            foreach ($result_leave as $row) {
                $labels[] = $row['leave_name'];
                $data[] = $row['jumlah'];
                $bg_color[] = '#' . $this->_random_color_part() . $this->_random_color_part() . $this->_random_color_part();
            }

            $dataset = array(
                'label' => 'pie chart data',
                'data' => $data,
                'backgroundColor' => $bg_color,
            );

            $result = array(
                'labels' => $labels,
                'datasets' => array($dataset),
            );
        } else {
            $result = array(
                'labels' => '',
                'datasets' => '',
            );
        }

        return $result;
    }

    private function _random_color_part()
    {
        return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
    }
}
