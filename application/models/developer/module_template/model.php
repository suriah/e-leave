<?php

defined('BASEPATH') or exit('No direct script access allowed');

class module_name_model extends Ajax_datatable_model
{

    var $table           = 'ecm_zone';
    var $column_order    = array('zone_code', 'zone_name', null); //set column field database for datatable orderable
    var $column_search   = array('zone_code', 'zone_name', null); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order           = array('id' => 'asc'); // default order 
    //var $filter = array('role' => 'admin', 'email' => 'admin@admin.com');  //sample
    var $filter          = null;
    var $filter_where_in = null;

    public function __construct()
    {
        parent::__construct();
    }

    // public function list_zone_drop_down()
    // {
    //     $query = $this->db->get('ecm_zone');
    //     $result = $query->result_array();

    //     if ($query->num_rows() > 0) {
    //         foreach ($result as $row) {
    //             $data[$row['id']] = $row['zone_name'];
    //         }
    //     }
    //     //for empty value
    //     $data[''] = '';

    //     return $data;
    // }
}