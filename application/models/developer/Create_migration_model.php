<?php

class Create_migration_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function list_migration(){
        $this->config->load('migration');
        $migration_path = $this->config->item('migration_path');
        $filter_file = $migration_path . '*.php';
        foreach (glob($filter_file) as $filename) {
            $temp_file_name = basename($filename, '.php');
            $temp_file_name_pieces = explode('_', $temp_file_name);
            $file_list[$temp_file_name_pieces[0]] = basename($filename, '.php');
        }
        $reverse_file_list = array_reverse($file_list, true);
        return $reverse_file_list;
    }

    public function list_table(){
        $tables = $this->db->list_tables();
        $list_of_table = array();;
        
        foreach ($tables as $key => $value){
            $list_of_table[$value] = $value;
        }

        return $list_of_table;
    }

    public function create_migration_file($table_name, $migration_type)
    {
        //get migration path
        $this->config->load('migration');
        $migration_path = $this->config->item('migration_path');
        $revision       = $this->_get_file_revision($migration_path, $table_name);

        $data                 = $this->_generate_content($table_name, $migration_type, $revision);
        $migration_running_no = date('YmdHis');
        $migration_file_name  = $migration_running_no . '_' . $table_name . $revision . '.php';
        //echo $migration_file_name;
        $result = $this->_write_file($migration_path . $migration_file_name, $data);
        if ($result == true) {
            $this->update_migration_config($migration_running_no);
        }
        return $result;
    }

    public function update_migration_config($migration_running_no)
    {
        //this function will change new migration file
        $migration_config_file = APPPATH . 'config/migration.php';
        
        $handle = fopen($migration_config_file, "r");
        $temp_file = APPPATH . 'config/migration_temp.php';

        if ($handle) {
            
            $file_handle = fopen($temp_file,"a");
            while (($buffer = fgets($handle, 4096)) !== false) {
                if (strstr($buffer, '$config[\'migration_version\'] = ')) {
                    $buffer = '$config[\'migration_version\'] = ' . $migration_running_no . ';' . PHP_EOL;
                }
                fwrite($file_handle,$buffer);
            }
            fclose($file_handle);
            if (!feof($handle)) {
                echo "Error: unexpected fgets() fail\n";
            }
            fclose($handle);

            //replace migration file
            unlink ($migration_config_file);
            rename ($temp_file, $migration_config_file);
            
        }

    }

    private function _get_file_revision($migration_path, $table_name)
    {
        $file_list   = array();
        $filter_file = $migration_path . '*_' . $table_name . '_*.php';
        foreach (glob($filter_file) as $filename) {
            //echo "$filename size " . filesize($filename) . "\n";
            $file_list[] = $filename;
        }

        $last_pieces_no = array();
        if (count($file_list) > 0) {
            foreach ($file_list as $file) {
                // echo $file;
                $pieces = explode('_', $file);

                $last_pieces = end($pieces);
                $last_pieces = str_replace('.php', '', $last_pieces);

                //to cater legacy features
                if (is_numeric($last_pieces)){
                    $last_pieces_no[] = $last_pieces;
                } 
            }
            //check if array exist or not, if not just put highest_pieces_no = 0
            if (count($last_pieces_no) > 0){
                $highest_pieces_no = max($last_pieces_no);
            }else{
                $highest_pieces_no = 0;
            }

            $highest_pieces_no = $highest_pieces_no + 1;
            return '_' . $highest_pieces_no;

        } else {
            return '_1';
        }
    }
    private function _write_file($file_name, $data)
    {
        $fp = fopen($file_name, "wb");
        if ($fp == false) {
            return false;
        } else {
            fwrite($fp, $data);
            fclose($fp);
            return true;
        }
    }


    private function _generate_content($table_name, $migration_type, $revision)
    {
        //only 2 type of migration, one is to change database structure while
        //the other is to modify /add data

        if ($migration_type == 'DB_VALUE'){
            $temp = $this->_get_template_file('MODIFY_VALUE');
        }else{

            //determine whether this is new table or update table
            if ($this->db->table_exists($table_name)) {
                $temp = $this->_get_template_file('EXISTING_TABLE');
            } else {
                $temp = $this->_get_template_file('NEW_TABLE');
            }
        }

        $temp = str_replace('{table_name}', $table_name, $temp);
        $temp = str_replace('{revision}', $revision, $temp);
        return $temp;
    }

    private function _get_template_file($content_type){
        
        //initialize var for template file
        $template_content = '<?php ' . PHP_EOL;
        switch ($content_type){
            case "MODIFY_VALUE":
                $template_file = APPPATH . 'models/developer/migration_template/modify_value.php';
            break;
            
            case "EXISTING_TABLE":
                $template_file = APPPATH . 'models/developer/migration_template/existing_table.php';
            break;
            
            case "NEW_TABLE":
                $template_file = APPPATH . 'models/developer/migration_template/new_table.php';
            break;
        }
        $file_content = file_get_contents($template_file);
        $template_content = $template_content . $file_content;
        return $template_content;
    }

}