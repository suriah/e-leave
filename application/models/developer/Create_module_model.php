<?php

class Create_module_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function generate_controller_file($module_name)
    {
        $controller_path = APPPATH . 'controllers/';
        $file_name       = $controller_path . ucfirst($module_name) . '.php';

        if (file_exists($file_name)) {
            echo 'Controller file already exist <BR>';
            return false;
        } else {
            //get file content
            $template_content = $this->_get_template_file('CONTROLLER');
            //modify value and pass to function write file
            $template_content = str_replace('controller_name', ucfirst($module_name), $template_content);
            $template_content = str_replace('view_name', $module_name, $template_content);

            $status           = $this->_write_file($file_name, $template_content);
        }
        return $status;
    }

    public function generate_model_file($module_name){
        $model_path = APPPATH . 'models/';
        $file_name       = $model_path . ucfirst($module_name) . '_model.php';

        if (file_exists($file_name)) {
            echo 'Model file already exist <BR>';
            return false;
        } else {
            //get file content
            $template_content = $this->_get_template_file('MODEL');
            //modify value and pass to function write file
            $template_content = str_replace('module_name', ucfirst($module_name), $template_content);

            $status           = $this->_write_file($file_name, $template_content);
        }
        return $status;

    }

    public function generate_view_file($module_name){
        $view_path = APPPATH . 'views/' . $module_name;
        if(is_dir($view_path) == false) {
            mkdir($view_path);
        }
        
        $file_name       = $view_path . '/list_' . $module_name . '.php';

        if (file_exists($file_name)) {
            echo 'View file already exist <BR>';
            return false;
        } else {
            //get file content
            $template_content = $this->_get_template_file('VIEW');
            
            //modify value and pass to function write file
            $template_content = str_replace('module_name', ucfirst($module_name), $template_content);

            $status           = $this->_write_file($file_name, $template_content);
        }
        return $status;

    }

    private function _get_template_file($content_type)
    {
        //initialize var for template file
        switch ($content_type) {
            case "CONTROLLER":
                $template_file = APPPATH . 'models/developer/module_template/controller.php';
                break;

            case "MODEL":
                $template_file = APPPATH . 'models/developer/module_template/model.php';
                break;

            case "VIEW":
                $template_file = APPPATH . 'models/developer/module_template/view.php';
                break;
        }
        $file_content = file_get_contents($template_file);
        return $file_content;
    }

    public function copy_controller_file($new_module_name, $module_name)
    {
        $controller_path = APPPATH . 'controllers/';
        $file_name       = $controller_path . ucfirst($new_module_name) . '.php';

        if (file_exists($file_name)) {
            echo 'Controller file already exist <BR>';
            return false;
        } else {
            //get file content
            $template_content = $this->_get_content_file($module_name, 'CONTROLLER');
            if ($template_content == true){
                $status           = $this->_write_file($file_name, $template_content);
            }
        }
        return $status;
    }

    public function copy_model_file($new_module_name, $module_name)
    {
        $model_path = APPPATH . 'models/';
        $file_name       = $model_path . ucfirst($new_module_name) . '_model.php';

        if (file_exists($file_name)) {
            echo 'MOdel file already exist <BR>';
            return false;
        } else {
            //get file content
            $template_content = $this->_get_content_file($module_name, 'MODEL');
            if ($template_content == true){
                $status           = $this->_write_file($file_name, $template_content);
            }
        }
        return $status;
    }

    public function copy_view_file($new_module_name, $module_name){
        $view_path = APPPATH . 'views/' . strtolower($new_module_name);
        if(is_dir($view_path) == false) {
            mkdir($view_path);
        }
        
        $file_name       = $view_path . '/list_' . strtolower($new_module_name) . '.php';

        if (file_exists($file_name)) {
            echo 'View file already exist <BR> ';
            return false;
        } else {
            //get file content
            $template_content = $this->_get_content_file($module_name , 'VIEW');
            $status           = $this->_write_file($file_name, $template_content);
        }
        return $status;

    }

    private function _get_content_file($module_name, $content_type){
        //initialize var for template file
        switch ($content_type) {
            case "CONTROLLER":
                $template_file = APPPATH . 'controllers/' . $module_name . '.php';
                break;

            case "MODEL":
                $template_file = APPPATH . 'models/' . $module_name . '_model.php';
                break;

            case "VIEW":
                $template_file = APPPATH . 'views/' . strtolower($module_name) . '/list_' . strtolower($module_name) . '.php' ;
                break;
        }
        if (file_exists($template_file)) {
            $file_content = file_get_contents($template_file);
            
        }else{
            echo 'file content is not found <BR>';
            return false;
        }

        return $file_content;
    }

    private function _write_file($file_name, $data)
    {
        $fp = fopen($file_name, "wb");
        if ($fp == false) {
            return false;
        } else {
            fwrite($fp, $data);
            fclose($fp);
            return true;
        }
    }

    public function create_migration_file($table_name, $migration_type)
    {
        //get migration path
        $this->config->load('migration');
        $migration_path = $this->config->item('migration_path');
        $revision       = $this->_get_file_revision($migration_path, $table_name);

        $data                 = $this->_generate_content($table_name, $migration_type, $revision);
        $migration_running_no = date('YmdHis');
        $migration_file_name  = $migration_running_no . '_' . $table_name . $revision . '.php';
        //echo $migration_file_name;
        $result = $this->_write_file($migration_path . $migration_file_name, $data);
        if ($result == true) {
            $this->update_migration_config($migration_running_no);
        }
    }

    public function update_migration_config($migration_running_no)
    {
        //this function will change new migration file
        $migration_config_file = APPPATH . 'config/migration.php';

        $handle    = fopen($migration_config_file, "r");
        $temp_file = APPPATH . 'config/migration_temp.php';

        if ($handle) {

            $file_handle = fopen($temp_file, "a");
            while (($buffer = fgets($handle, 4096)) !== false) {
                if (strstr($buffer, '$config[\'migration_version\'] = ')) {
                    $buffer = '$config[\'migration_version\'] = ' . $migration_running_no . ';' . PHP_EOL;
                }
                fwrite($file_handle, $buffer);
            }
            fclose($file_handle);
            if (!feof($handle)) {
                echo "Error: unexpected fgets() fail\n";
            }
            fclose($handle);

            //replace migration file
            unlink($migration_config_file);
            rename($temp_file, $migration_config_file);

        }

    }

    private function _get_file_revision($migration_path, $table_name)
    {
        $file_list   = array();
        $filter_file = $migration_path . '*_' . $table_name . '_*.php';
        foreach (glob($filter_file) as $filename) {
            //echo "$filename size " . filesize($filename) . "\n";
            $file_list[] = $filename;
        }

        $last_pieces_no = array();
        if (count($file_list) > 0) {
            foreach ($file_list as $file) {
                // echo $file;
                $pieces = explode('_', $file);

                $last_pieces = end($pieces);
                $last_pieces = str_replace('.php', '', $last_pieces);

                //to cater legacy features
                if (is_numeric($last_pieces)) {
                    $last_pieces_no[] = $last_pieces;
                }
            }
            $highest_pieces_no = max($last_pieces_no);

            $highest_pieces_no = $highest_pieces_no + 1;
            return '_' . $highest_pieces_no;

        } else {
            return '_1';
        }
    }

    private function _generate_content($table_name, $migration_type, $revision)
    {
        //only 2 type of migration, one is to change database structure while
        //the other is to modify /add data

        if ($migration_type == 'DB_VALUE') {
            $temp = $this->_get_template_file('MODIFY_VALUE');
        } else {

            //determine whether this is new table or update table
            if ($this->db->table_exists($table_name)) {
                $temp = $this->_get_template_file('EXISTING_TABLE');
            } else {
                $temp = $this->_get_template_file('NEW_TABLE');
            }
        }

        $temp = str_replace('{table_name}', $table_name, $temp);
        $temp = str_replace('{revision}', $revision, $temp);
        return $temp;
    }

}
