defined('BASEPATH') or exit('No direct script access allowed');

class Migration_{table_name}{revision} extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }

    public function up()
    {
        echo 'Complete update database value<BR>';
    }

    public function down()
    {
        echo 'Complete undo database value<BR>';
    }
}
