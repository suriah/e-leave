defined('BASEPATH') or exit('No direct script access allowed');

class Migration_{table_name}{revision} extends CI_Migration
{

    private $table_name;
    private $fields;

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
        $this->table_name = '{table_name}';

        $this->fields = array(
            'updated_timestamp'   => array(
                'type'       => 'TIMESTAMP',
                'NULL' => true
            ),
            'updated_by'   => array(
                'type'       => 'VARCHAR',
                'constraint' => 60,
                'NULL' => true
            ),
        );
    }

    public function up()
    {
        $this->dbforge->add_column($this->table_name, $this->fields);
        echo 'Add multiple column in table ' . $this->table_name . '<BR>';
    }

    public function down()
    {

        foreach ($this->fields as $key => $value){
            $this->dbforge->drop_column($this->table_name , $key);
            echo 'Drop column ' . $key . ' from table ' . $this->table_name  . '<BR>';
        }

    }
}
