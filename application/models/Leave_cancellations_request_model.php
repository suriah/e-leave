<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Leave_cancellations_request_model extends Ajax_datatable_model
{

    public $auth_info;
    var $table           = 'leave';
    var $column_order    = array('leave_name', 'leave_date_from', 'leave_date_to','total_leave','leave_reason','supervisor_id','manager_id','leave_status',null); //set column field database for datatable orderable
    // var $column_order    = null;
    var $column_search   = array('leave_name', 'leave_date_from', 'leave_date_to','total_leave','leave_reason','supervisor_id','manager_id','leave_status'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order           = array('id' => 'DESC'); // default order 
    var $filter_where_in = array('leave_status' => array('REQ_APPROVAL_SV','REQ_APPROVAL_MAN','APPROVED'));  //sample
    var $filter          = null;

    var $join = array(
        array(
            'table'     => 'leave_type',
            'map'       => 'leave.leave_type_id = leave_type.id',
            'join_type' => 'left'
        ),       
    );
    var $selection = 'leave.* , leave_type.leave_name';

    public function __construct()
    {
        parent::__construct();

    }

    public function cancel_request_notification_email($id)
    {
        //this function will send email along with reset code
        $this->load->library('email');
        $this->email->set_newline("\r\n");

        $this->db->select('leave.*,users.first_name,users.email,leave_type.leave_name');
        $this->db->where('leave.id',$id);
        $this->db->join('users','users.id = leave.staff_id','left');
        $this->db->join('leave_type','leave_type.id = leave.leave_type_id','left');
        $query=$this->db->get('leave');
        $result=$query->row();

            $this->db->where('id',$result->manager_id);
            $query_1=$this->db->get('users');
            $superior = $query_1->row();
        

        $data = array(
            'staff'=>$result,
            'superior'=>$superior,
        );

        $message = $this->load->view('email_template/cancel_request.php',$data,TRUE);
        $this->email->from('eleave_admin@geoinfo.com.my');
        $this->email->to($superior->email); 
        $this->email->message($message);
        if ($this->email->send()) 
        {
            return true;
        }else{
            
        //    show_error($this->email->print_debugger());
            return false;
        }     
    }
}