<?php

defined('BASEPATH') or exit('No direct script access allowed');

class System_admin_manage_level_model extends Ajax_datatable_model
{

    var $table           = 'levels';
    var $column_order    = array('level_code', 'level_name', 'level_description',null); //set column field database for datatable orderable
    var $column_search   = array('level_code', 'level_name', 'level_description'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order           = array('id' => 'asc'); // default order 
    //var $filter = array('role' => 'admin', 'email' => 'admin@admin.com');  //sample
    var $filter          = null;
    var $filter_where_in = null;

    public function __construct()
    {
        parent::__construct();
    }
}