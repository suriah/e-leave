<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Leave_request_model extends Ajax_datatable_model
{

    public $auth_info;
    var $table           = 'leave';
    var $column_order    = array('leave_name', 'leave_date_from', 'leave_date_to','total_leave','leave_reason','supervisor_id','manager_id','leave_status',null); //set column field database for datatable orderable
    // var $column_order    = null;
    var $column_search   = array('leave_name', 'leave_date_from', 'leave_date_to','total_leave','leave_reason','supervisor_id','manager_id','leave_status'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order           = array('id' => 'DESC'); // default order 
    // var $filter = array('staff_id' => $user);  //sample
    var $filter          = null;

    var $join = array(
        array(
            'table'     => 'leave_type',
            'map'       => 'leave.leave_type_id = leave_type.id',
            'join_type' => 'left'
        ),       
    );
    var $selection = 'leave.* , leave_type.leave_name';

    public function __construct()
    {
        parent::__construct();

    }

    public function has_supervisor($id){

        $this->db->where('id',$id);
        $query  = $this->db->get('leave');
        $result = $query->row()->has_supervisor;
    
        return $result;
    }
    public function list_leave_type_drop_down(){

        $query  = $this->db->get('leave_type');
        $result = $query->result_array();
    
        $data = array();
        $data[''] = 'Select leave type';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data[$row['id']] = $row['leave_name'];
            }
        }
        return $data;
    }

    public function list_supervisor_drop_down($group){
        foreach ($group as $key => $value) {
            if($value->name == 'superior'){
                $id = $value->id;
            }
        }
        $this->db->select('users.*');
        $this->db->join('users_groups', 'users.id = users_groups.user_id', 'left');
        $this->db->where('users_groups.group_id', $id);
        $this->db->where_not_in('username', 'administrator');
        $query  = $this->db->get('users');
        $result = $query->result_array();
    
        $data = array();
        $data[''] = 'Select supervisor';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data[$row['id']] = $row['first_name'];
            }
        }
        return $data;
    }

    public function list_manager_drop_down($group){
        
        foreach ($group as $key => $value) {
            if($value->name == 'superior'){
                $id = $value->id;
            }
        }
         $this->db->select('users.*');
         $this->db->join('users_groups', 'users.id = users_groups.user_id', 'left');
         $this->db->where('users_groups.group_id', $id);
         $this->db->where_not_in('username', 'administrator');
         $query  = $this->db->get('users');
         $result = $query->result_array();
    
        $data = array();
        $data[''] = 'Please select';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data[$row['id']] = $row['first_name'];
            }
        }
        return $data;
    }
    public function list_superior_drop_down($group){
        
        foreach ($group as $key => $value) {
            if($value->name == 'superior'){
                $id = $value->id;
            }
        }
        $this->db->select('users.*');
        $this->db->join('users_groups', 'users.id = users_groups.user_id', 'left');
        $this->db->where('users_groups.group_id', $id);
        $this->db->where_not_in('username', 'administrator');
        $query  = $this->db->get('users');
        $result = $query->result_array();
    
        $data = array();
        $data[''] = 'Select superior';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data[$row['id']] = $row['first_name'];
            }
        }
        return $data;
    }

    public function get_leave_type_name($id){
        
        $this->db->select('leave_name');
        $this->db->where('id', $id);
        $query  = $this->db->get('leave_type');
        $result = $query->row();
    
        return $result;
    }
    public function get_user_level($id){
        
        $this->db->select('levels.level_code');
        $this->db->where('users.id', $id);
        $this->db->join('levels', 'levels.id = staff_level_id','left');
        $query  = $this->db->get('users');
        $result = $query->row();
    
        return $result;
    }
    public function check_public_holiday($date){
        
        $date_array = str_replace('[','',$date);
        $date_array = str_replace(']','',$date_array);
        
        $this->db->select('*');
        $this->db->where('date(public_holiday_date) IN ('.$date_array.')');
        $query  = $this->db->get('public_holiday');
        $result = $query->num_rows();
    
        return $result;
    }
    public function check_dhearty($date){
        $date_array = str_replace('[','',$date);
        $date_array = str_replace(']','',$date_array);
        
        $this->db->select('*');
        $this->db->where('date(dhearty_date) IN ('.$date_array.')');
        $query  = $this->db->get('dhearty');
        $result = $query->num_rows();
    
        return $result;
    }

    public function leave_request_notification_email($id,$new_status)
    {
        //this function will send email along with reset code
        $this->load->library('email');
        $this->email->set_newline("\r\n");

        $this->db->select('leave.*,users.first_name,users.email,leave_type.leave_name');
        $this->db->where('leave.id',$id);
        $this->db->join('users','users.id = leave.staff_id','left');
        $this->db->join('leave_type','leave_type.id = leave.leave_type_id','left');
        $query=$this->db->get('leave');
        $result=$query->row();

        if($new_status == 'REQ_APPROVAL_SV'){
            $email_subject = 'Leave Request Approval From Supervisor';
            $this->db->where('id',$result->supervisor_id);
            $query=$this->db->get('users');
            $users = $query->row();
        }else if($new_status == 'REQ_APPROVAL_MAN'){
            $email_subject = 'Leave Request Approval From Superior';
            $this->db->where('id',$result->manager_id);
            $query=$this->db->get('users');
            $users = $query->row();
        }

        $data = array(
            'staff'=>$result,
            'superior'=>$users,
        );

        $message = $this->load->view('email_template/leave_request.php',$data,TRUE);
        $this->email->from('eleave_admin@geoinfo.com.my');
        $this->email->to($users->email); 
        $this->email->subject($email_subject);
        $this->email->message($message);
        if ($this->email->send()) 
        {
            return true;
        }else{
            
        //    show_error($this->email->print_debugger());
            return false;
        }     
    }

    public function get_leave_type($id){
        $this->db->select('leave_type_id');
        $this->db->where('id',$id);
        $query  = $this->db->get('leave');
        $result = $query->row();

        $result = $result->leave_type_id;
        return $result;
    }
    public function total_list_date($date_from,$date_to){

        $date = $this->displayDates($date_from, $date_to);
        
        return $date;
    }

    public function displayDates($date1, $date2, $format = 'Y-m-d') {
        $dates = array();
        $list_dates = array();
        $current = strtotime($date1);
        $date2 = strtotime($date2);
        $stepVal = '+1 day';
        while( $current <= $date2 ) {
           $dates[] = date($format, $current);
           $current = strtotime($stepVal, $current);
        }

        foreach ($dates as $key => $value) {
            $date=date_create($value);
            $query = $this->db->where('public_holiday_date',$value)->get('public_holiday');
            $is_public = $query->result();
            $day_num = $date->format("N");
            if($day_num != 6 && $day_num != 7) { /* weekday */
                if($is_public == null ){
                    $list_dates[] = $value;
                }
            }
        }
        $query_2 = $this->db->get('dhearty');
        $is_dhearty = $query_2->result();

        if($is_dhearty){

            foreach ($is_dhearty as $key => $value_dhearty) {
                foreach ($dates as $key => $value) {
                    $current = strtotime($value_dhearty->dhearty_date);
                    $dhearty_date=date('Y-m-d',$current);
                    if($value == $dhearty_date){
                        $list_dates[] =$dhearty_date;
                    }
                }
            }
        }
        return $list_dates;
    }
}