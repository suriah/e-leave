<?php

class Status_model extends Ajax_datatable_model {
    var $table           = 'registration';
    var $filter          = null;
    var $filter_where_in = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    } 

    function _check_data($ic_no)
    {
        $this->db->where('ic_no',$ic_no);
        $query = $this->db->get('members');
        $row = $query->row();
        if(isset($row)){
            $state=$this->list_state($row->member_state_code);
            $branch=$this->list_branch($row->member_branch_code);
            $data['data'] = $row;
            $data['state'] = $state;
            $data['branch'] = $branch;
            $data['status']       = 'true';
        }else{
            $data['status']       = 'false';
        }

        return $data;

    }

    public function list_state($state_code)
    {
        $this->db->where('state_code',$state_code);
        $query = $this->db->get('state');
        $result = $query->row();
        $data=$result->state_name;
        return $data;
    }

    public function list_branch($branch_code)
    {
        $this->db->where('branch_code',$branch_code);
        $query = $this->db->get('branch');
        $result = $query->row();
        $data=array(
            'branch_name'=>$result->branch_name,
            'branch_address'=>$result->address_1,
        );
        return $data;
    }

    
}