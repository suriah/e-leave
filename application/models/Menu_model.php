<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function list_menu($parent_id = 0) {
        $this->db->where('is_active', true);
        $this->db->where('parent_id', $parent_id);
        $this->db->order_by('menu_order');
        $query  = $this->db->get('menus');
        $result = $query->result_array();
        foreach ($result as $row) {
            //extend row data with accessbile property, this is required for show and hide menu
            $row['accessible']         = 0;
            $data[$row['id']]          = $row;
            $data[$row['id']]['child'] = $this->list_menu($row['id']);
        }

        if (isset($data)) {
            return $data;
        } else {
            return null;
        }
    }

    public function filter_menu($user_groups_array, $menu_data) {
    //    print '<pre>';
    //    print_r ($user_groups_array);
    //    print_r ($menu_data);
    //    print '</pre>';

    
    foreach ($user_groups_array as $value) {
        $group_id        = $value->id;
        $accessible_menu = $this->_get_privilege($group_id);
                //    print '<pre>';
                //    print $group_id;
                //    print_r($accessible_menu);
                //    print '</pre>';
        
        $menu_data = $this->_set_menu_privilege($menu_data, $accessible_menu);
    }
    
    //        print 'BEFORE';
    // print '<pre>';
    // print_r($menu_data);
    // print '</pre>';        
    
    $clean_menu_data = $this->_cleanup_inaccessible_menu($menu_data);
    // print 'AFTER';
    // print '<pre>';
    // print_r($clean_menu_data);
    // print '</pre>';
    
    // exit();


        $data = $clean_menu_data;
        return $data;
    }

    private function _cleanup_inaccessible_menu($updated_menu_data) {
        //this will remove array with accssbile 0
        foreach ($updated_menu_data as $key => $value) {
            if ($value['accessible'] == 0) {
                unset($updated_menu_data[$key]);
            }


            //second tier
            if (!empty($value['child'])) {
                foreach ($value['child'] as $child_key => $child_row) {
                    if ($child_row['accessible'] == 0) {
                        unset($updated_menu_data[$key]['child'][$child_key]);
                    }

                    //third tier
                    if (!empty($child_row['child'])) {
                        foreach ($child_row['child'] as $grand_child_key => $grand_child_row) {

                            if ($grand_child_row['accessible'] == 0) {
                                unset($updated_menu_data[$key]['child'][$child_key]['child'][$grand_child_key]);
                            }

                            //forth tier looping for child
                            if (!empty($grand_child_row['child'])) {
                                foreach ($grand_child_row['child'] as $great_grand_child_key => $great_grand_child_row) {

                                    if ($great_grand_child_row['accessible'] == 0) {
                                        // unset($updated_menu_data[$key]['child'][$child_key]['child'][$grand_child_key]);
                                        unset($updated_menu_data[$key]['child'][$child_key]['child'][$grand_child_key]['child'][$great_grand_child_key]);
                                    }
                                }

                            }

                        }
                    }

                }
            }
        }
        return $updated_menu_data;
    }

    private function _set_menu_privilege($menu_data, $accessible_menu) {

        foreach ($menu_data as $menu_key => $menu_value) {
            foreach ($accessible_menu as $access_key => $access_value) {
                //first tier
                if ($menu_value['menu_code'] == $access_value['privilege_code']) {
                    $menu_data[$menu_key]['accessible'] = 1;
                }

                //second tier
                if (!empty($menu_value['child'])) {
                    foreach ($menu_value['child'] as $child_key => $child_row) {
                        if ($child_row['menu_code'] == $access_value['privilege_code']) {
                            $menu_data[$menu_key]['child'][$child_key]['accessible'] = 1;
                            //set parrent as accessible
                            $menu_data[$menu_key]['accessible'] = 1;
                        }

                        //third tier
                        if (!empty($child_row['child'])) {
                            foreach ($child_row['child'] as $grand_child_key => $grand_child_row) {
                                if ($grand_child_row['menu_code'] == $access_value['privilege_code']) {
                                    $menu_data[$menu_key]['child'][$child_key]['child'][$grand_child_key]['accessible'] = 1;
                                    
                                    //set parent and grandparent
                                    $menu_data[$menu_key]['child'][$child_key]['accessible'] = 1;
                                    $menu_data[$menu_key]['accessible'] = 1;
                                }
                                //forth tier
                                if (!empty($grand_child_row['child'])) {
                                    foreach ($grand_child_row['child'] as $great_grand_child_key => $great_grand_child_row) {
                                        if ($great_grand_child_row['menu_code'] == $access_value['privilege_code']) {
                                            $menu_data[$menu_key]['child'][$child_key]['child'][$grand_child_key]['child'][$great_grand_child_key]['accessible'] = 1;
    
                                            //set parent , grandparent and great grand parent
                                            $menu_data[$menu_key]['accessible'] = 1;
                                            $menu_data[$menu_key]['child'][$child_key]['accessible'] = 1;
                                            $menu_data[$menu_key]['child'][$child_key]['child'][$grand_child_key]['accessible'] = 1;
                                        }
    
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }
        $updated_menu_data = $menu_data;

//        print 'AFTER';
//        print '<pre>';
//        print_r($updated_menu_data);
//        print '</pre>';

        return $updated_menu_data;
    }

    function _get_privilege($group_id) {
        $this->db->select('privilege_code');
        $this->db->join('privileges', 'privileges_groups.privilege_id = privileges.id', 'left');
        $this->db->where('group_id', $group_id);
        $this->db->where('permission', true);
        $query  = $this->db->get('privileges_groups');
        $result = $query->result_array();
        return $result;
    }

}
