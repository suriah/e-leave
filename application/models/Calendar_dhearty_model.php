<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Calendar_dhearty_model extends Ajax_datatable_model
{

    var $table           = 'dhearty';
    var $column_order    = array('dhearty', 'dhearty_date', 'dhearty_time_start','dhearty_time_end','dhearty_agenda',null); //set column field database for datatable orderable
    var $column_search   = array('dhearty', 'dhearty_date', 'dhearty_time_start','dhearty_time_end','dhearty_agenda'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order           = array('id' => 'asc'); // default order 
    //var $filter = array('role' => 'admin', 'email' => 'admin@admin.com');  //sample
    var $filter          = null;
    var $filter_where_in = null;

    public function __construct()
    {
        parent::__construct();
    }
}