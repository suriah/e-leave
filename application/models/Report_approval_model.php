<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Report_approval_model extends Ajax_datatable_model
{

    var $table           = 'leave';
    var $column_order    = array('staff_name', 'staff_email', null); //set column field database for datatable orderable
    var $column_search   = array('staff_name', 'staff_email',  null); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order           = array('id' => 'asc'); // default order 
    // var $filter = array('role' => 'admin', 'email' => 'admin@admin.com');  //sample
    // var $filter          = null;
    var $filter_where_in = array('leave_status' => array('APPROVED', 'REJECTED'));

    var $join = array(
        array(
            'table'     => 'leave_type',
            'map'       => 'leave.leave_type_id = leave_type.id',
            'join_type' => 'left'
        ),       
        array(
            'table'     => 'users',
            'map'       => 'leave.staff_id = users.id',
            'join_type' => 'left'
        ),       
    );
    var $selection = 'leave.* , leave_type.leave_name, users.first_name as staff_name, users.email as staff_email';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_supervisor_name($id)
    {
        $this->db->select('users.first_name');
        $this->db->join('users','leave.supervisor_id = users.id','left');
        $this->db->where('leave.supervisor_id',$id);
        $query = $this->db->get('leave');
        $result = $query->row();
        return $result;
    }
    public function get_manager_name($id)
    {
        $this->db->select('users.first_name');
        $this->db->join('users','leave.manager_id = users.id','left');
        $this->db->where('leave.manager_id',$id);
        $query = $this->db->get('leave');
        $result = $query->row();
        return $result;
    }
}