<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Report_wfh_model extends Ajax_datatable_model
{

    var $table           = 'wfh';
    var $column_order    = array('staff_name', 'staff_email', null); //set column field database for datatable orderable
    var $column_search   = array('staff_name', 'staff_email',  null); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order           = array('id' => 'asc'); // default order 
    // var $filter = array('role' => 'admin', 'email' => 'admin@admin.com');  //sample
    // var $filter          = null;
    var $filter_where_in = array('wfh_status' => array('APPROVED', 'REJECTED'));

    var $join = array(     
        array(
            'table'     => 'users',
            'map'       => 'wfh.staff_id = users.id',
            'join_type' => 'left'
        ),       
    );
    var $selection = 'wfh.* ,users.first_name as staff_name, users.email as staff_email';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_manager_name($id)
    {
        $this->db->select('users.first_name');
        $this->db->join('users','wfh.manager_id = users.id','left');
        $this->db->where('wfh.manager_id',$id);
        $query = $this->db->get('wfh');
        $result = $query->row();
        return $result;
    }

    public function get_staff_list()
    {
        $query          = $this->db->get('users');
        $list_staff_array = $query->result_array();

        $list_staff = array();
        $list_staff[''] = 'All';
        foreach ($list_staff_array as $staff)
        {
            $list_staff[$staff['id']] = $staff['first_name'];
        }

        return $list_staff;
    }
   
    public function get_month_list()
    {
        // $startDate = new \DateTime('first day of next month');
        // $endDate = new \DateTime('1st january next year');
        $startDate = new \DateTime('1st january');
        $endDate = new \DateTime('1st december');
       
        $interval = new \DateInterval('P1M');
        $period = new \DatePeriod($startDate, $interval, $endDate);
       
        // Start array with current date
        $dates = array();
       $dates[''] = ['All'];
    //    array_push($dates, 'All');
       // Add all remaining dates to array
       foreach ($period as $date) {
            $dates[$date->Format('n')]= $date->Format('F');
       }                                 
       

        return $dates;
    }
    public function get_year_list()
    {
        $year_list = array();
        $year_list[date('Y')] = [date('Y')];
        $years = range(date('Y'), 2020);

        foreach ($years as $value) {
            $year_list[$value] = $value;
        }

        return $year_list;
    }
    public function get_all_staff_name()
    {
        $this->db->select('id,first_name');
        $this->db->where_not_in('username', 'administrator');
        $query = $this->db->get('users');
        $result = $query->result();
        return $result;
    }

    public function get_staff_name($id)
    {
        $this->db->select('first_name');
        $this->db->where('id',$id);
        $query = $this->db->get('users');
        $result = $query->row();
        return $result;
    }
    public function get_wfh_type_name($id)
    {
        $this->db->select('wfh_name');
        $this->db->where('id',$id);
        $query = $this->db->get('wfh_type');
        $result = $query->row();
        return $result;
    }
    public function get_month($month)
    {
        $monthNum  = $month;
        $dateObj   = DateTime::createFromFormat('!m', $monthNum);
        $result = $dateObj->format('F');
        return $result;
    }
}