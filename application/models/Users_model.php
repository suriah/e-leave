<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends Ajax_datatable_model {

    var $table = 'users';
    var $column_order = array('email', 'first_name', 'phone', 'status', null); //set column field database for datatable orderable
    var $column_search = array('email', 'first_name', 'phone'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('id' => 'desc'); // default order 
    //var $filter = array('role' => 'admin', 'email' => 'admin@admin.com');  //sample
    var $filter = null;
    var $filter_where_in = null;

//    for join table factories and table departments
//    var $join = array(
//      array(  'table' => 'company',
//                            'map' => 'users.company = company.id', 
//                            'join_type' => 'left'
//      ),
//      array(  
//        'table' => 'departments',
//        'map'   => 'users.department_id = departments.id',
//        'join_type' => 'left'
//      )
//    );
//    for join table department
//    
//    var $selection = 'users.*, company.company_name';
    
    public function __construct() {
        parent::__construct();
    }

    public function list_users_drop_down() {
//        $this->db->where('customer_code', $customer_code);
//        $this->db->where('smartlink_site_name <> ', '');
        $query = $this->db->get('users');
        $result = $query->result_array();

        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data[$row['email']] = $row['first_name'];
            }
        } 
        //for empty value
        $data[''] = '';
        
        return $data;
    }

    public function list_positions_drop_down(){

        $query  = $this->db->get('positions');
        $result = $query->result_array();
    
        $data = array();
        $data[''] = 'Select position';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data[$row['id']] = $row['position_name'];
            }
        }
        return $data;
    }

    public function list_departments_drop_down(){

        $query  = $this->db->get('departments');
        $result = $query->result_array();
    
        $data = array();
        $data[''] = 'Select department';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data[$row['id']] = $row['department_name'];
            }
        }
        return $data;
    }

    public function list_employee_status_drop_down(){

        $query  = $this->db->get('employee_status');
        $result = $query->result_array();
    
        $data = array();
        $data[''] = 'Select employee type';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data[$row['id']] = $row['employee_type'];
            }
        }
        return $data;
    }

    public function list_level_drop_down(){

        $query  = $this->db->get('levels');
        $result = $query->result_array();
    
        $data = array();
        $data[''] = 'Select employee level';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data[$row['id']] = $row['level_name'];
            }
        }
        return $data;
    }
    public function get_leave($id){

        $query  = $this->db->where('staff_id', $id);
        $query  = $this->db->get('users_leaves');
        $result = $query->row();
    
        return $result;
    }


}
