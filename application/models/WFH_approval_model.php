<?php

defined('BASEPATH') or exit('No direct script access allowed');

class WFH_approval_model extends Ajax_datatable_model
{

    public $auth_info;
    var $table           = 'wfh';
    var $column_order    = array('wfh_date_from', 'wfh_date_to','total_wfh','wfh_reason','manager_id','wfh_status',null); //set column field database for datatable orderable
    var $column_search   = array('wfh_date_from', 'wfh_date_to','total_wfh','wfh_reason','manager_id','wfh_status'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order           = array('id' => 'asc'); // default order 
    var $filter_where_in = array('wfh_status' => array('REQ_APPROVAL'));  //sample
    // var $filter          = null;
    

    var $join = array(   
        array(
            'table'     => 'users',
            'map'       => 'wfh.staff_id = users.id',
            'join_type' => 'left'
        ),       
    );
    var $selection = 'wfh.* ,users.first_name as staff_name, users.email as staff_email';

    public function __construct()
    {
        parent::__construct();

    }

    public function list_manager_drop_down(){
        
        $this->db->select('users.* , levels.level_code');
        $this->db->join('levels', 'users.staff_level_id = levels.id', 'left');
        $bind = ['Man','DIR'];
        $this->db->where_in('level_code', $bind);
        $query  = $this->db->get('users');
        $result = $query->result_array();
    
        $data = array();
        $data[''] = 'Select manager';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data[$row['id']] = $row['first_name'];
            }
        }
        return $data;
    }


    public function view_process_data($id){
        
        $this->db->select('wfh.* , users.first_name as staff_name, users.email as staff_email');
        $this->db->where('wfh.id', $id);
        $this->db->join('users', 'wfh.staff_id = users.id','left');
        $query  = $this->db->get('wfh');
        $result = $query->row();
    
        return $result;
    }

    public function wfh_approved_notification_email($id)
    {
        //this function will send email along with reset code
        $this->load->library('email');
        $this->email->set_newline("\r\n");

        $this->db->select('wfh.*,users.first_name,users.email');
        $this->db->where('wfh.id',$id);
        $this->db->join('users','users.id = wfh.staff_id','left');
        $query=$this->db->get('wfh');
        $result=$query->row();
        $email=null;

      
        $this->db->where('id',$result->manager_id);
        $query=$this->db->get('users');
        $users = $query->row();
        

        $this->db->select('first_name, email');
        $this->db->where('position_code', 'HR');
        $this->db->join('positions','positions.id = users.staff_position_id','left');
        $query=$this->db->get('users');
        $hr = $query->row();

        $data = array(
            'staff'=>$result,
            'superior'=>$users,
            'hr'=>$hr,
        );

        $message = $this->load->view('email_template/wfh_approval.php',$data,TRUE);
        $this->email->from('eleave_admin@geoinfo.com.my');
        $this->email->to($result->email); 
        $this->email->cc($hr->email);
        $this->email->bcc('eleave_admin@geoinfo.com.my');
        $email_subject = 'WFH Application Approved';
        $this->email->subject($email_subject);
        $this->email->message($message);
        if ($this->email->send()) 
        {
            return true;
        }else{
            show_error($this->email->print_debugger());
            return false;
        }     
        
    }
    public function wfh_rejected_notification_email($id,$feedback)
    {
        //this function will send email along with reset code
        $this->load->library('email');
        $this->email->set_newline("\r\n");

        $this->db->select('wfh.*,users.first_name,users.email');
        $this->db->where('wfh.id',$id);
        $this->db->join('users','users.id = wfh.staff_id','left');
        $query=$this->db->get('wfh');
        $result=$query->row();
        $email=null;

      
            $this->db->where('id',$result->manager_id);
            $query=$this->db->get('users');
            $users = $query->row();
            $email = $users->email;

        $this->db->select('first_name, email');
        $this->db->where('position_code', 'HR');
        $this->db->join('positions','positions.id = users.staff_position_id','left');
        $query=$this->db->get('users');
        $hr = $query->row();

        $data = array(
            'staff'=>$result,
            'superior'=>$users,
            'hr'=>$hr,
            'info'=>$feedback,
        );

        $message = $this->load->view('email_template/wfh_rejected.php',$data,TRUE);
        $this->email->from('eleave_admin@geoinfo.com.my');
        $this->email->to($result->email); 
        $this->email->cc($hr->email);
        $this->email->bcc('eleave_admin@geoinfo.com.my');
        $email_subject = 'WFH Application Rejected';
        $this->email->subject($email_subject);
        $this->email->message($message);
        if ($this->email->send()) 
        {
            return true;
        }else{
            show_error($this->email->print_debugger());
            return false;
        }     
        
    }

    public function add_leave_date($id){
        $this->db->select('wfh_date_from,wfh_date_to,staff_id');
        $this->db->where('id',$id);
        $query  = $this->db->get('wfh');
        $result = $query->result_array();

        foreach ($result as $key => $value) {
            $date = $this->displayDates($value['wfh_date_from'], $value['wfh_date_to']);
        }
        return $date;
    }

    public function displayDates($date1, $date2, $format = 'Y-m-d') {
        $dates = array();
        $list_dates = array();
        $current = strtotime($date1);
        $date2 = strtotime($date2);
        $stepVal = '+1 day';
        while( $current <= $date2 ) {
           $dates[] = date($format, $current);
           $current = strtotime($stepVal, $current);
        }

        foreach ($dates as $key => $value) {
            $date=date_create($value);
            $query = $this->db->where('public_holiday_date',$value)->get('public_holiday');
            $is_public = $query->result();
            $day_num = $date->format("N");
            if($day_num != 6 && $day_num != 7) { /* weekday */
                if($is_public == null ){
                    $list_dates[] = $value;
                }
            }
        }
        $query_2 = $this->db->get('dhearty');
        $is_dhearty = $query_2->result();

        if($is_dhearty){

            foreach ($is_dhearty as $key => $value_dhearty) {
                foreach ($dates as $key => $value) {
                    $current = strtotime($value_dhearty->dhearty_date);
                    $dhearty_date=date('Y-m-d',$current);
                    if($value == $dhearty_date){
                        $list_dates[] =$dhearty_date;
                    }
                }
            }
        }
        return $list_dates;
    }
}