<?php

defined('BASEPATH') or exit('No direct script access allowed');

class WFH_request_model extends Ajax_datatable_model
{

    public $auth_info;
    var $table           = 'wfh';
    var $column_order    = array('wfh_date_from', 'wfh_date_to','total_wfh','wfh_reason','manager_id','wfh_status',null); //set column field database for datatable orderable
    var $column_search   = array('wfh_date_from', 'wfh_date_to','total_wfh','wfh_reason','manager_id','wfh_status'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order           = array('id' => 'asc'); // default order 
    // var $filter = array('staff_id' => $user);  //sample
    // var $filter          = null;
    

    public function __construct()
    {
        parent::__construct();

    }

    

    public function list_manager_drop_down($group){
        
        foreach ($group as $key => $value) {
            if($value->name == 'superior'){
                $id = $value->id;
            }
        }
         $this->db->select('users.*');
         $this->db->join('users_groups', 'users.id = users_groups.user_id', 'left');
         $this->db->where('users_groups.group_id', $id);
         $this->db->where_not_in('username', 'administrator');
         $query  = $this->db->get('users');
         $result = $query->result_array();
    
        $data = array();
        $data[''] = 'Please select';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data[$row['id']] = $row['first_name'];
            }
        }
        return $data;
    }
  
    public function check_public_holiday($date){
        
        $date_array = str_replace('[','',$date);
        $date_array = str_replace(']','',$date_array);
        
        $this->db->select('*');
        $this->db->where('date(public_holiday_date) IN ('.$date_array.')');
        $query  = $this->db->get('public_holiday');
        $result = $query->num_rows();
    
        return $result;
    }
    public function check_dhearty($date){
        $date_array = str_replace('[','',$date);
        $date_array = str_replace(']','',$date_array);
        
        $this->db->select('*');
        $this->db->where('date(dhearty_date) IN ('.$date_array.')');
        $query  = $this->db->get('dhearty');
        $result = $query->num_rows();
    
        return $result;
    }
    public function add_leave_date($id){
        $this->db->select('wfh_date_from,wfh_date_to,staff_id');
        $this->db->where('id',$id);
        $query  = $this->db->get('wfh');
        $result = $query->result_array();

        foreach ($result as $key => $value) {
            $date = $this->displayDates($value['wfh_date_from'], $value['wfh_date_to']);
        }
        return $date;
    }

    public function displayDates($date1, $date2, $format = 'Y-m-d') {
        $dates = array();
        $list_dates = array();
        $current = strtotime($date1);
        $date2 = strtotime($date2);
        $stepVal = '+1 day';
        while( $current <= $date2 ) {
           $dates[] = date($format, $current);
           $current = strtotime($stepVal, $current);
        }

        foreach ($dates as $key => $value) {
            $date=date_create($value);
            $query = $this->db->where('public_holiday_date',$value)->get('public_holiday');
            $is_public = $query->result();
            $day_num = $date->format("N");
            if($day_num != 6 && $day_num != 7) { /* weekday */
                if($is_public == null ){
                    $list_dates[] = $value;
                }
            }
        }
        $query_2 = $this->db->get('dhearty');
        $is_dhearty = $query_2->result();

        if($is_dhearty){

            foreach ($is_dhearty as $key => $value_dhearty) {
                foreach ($dates as $key => $value) {
                    $current = strtotime($value_dhearty->dhearty_date);
                    $dhearty_date=date('Y-m-d',$current);
                    if($value == $dhearty_date){
                        $list_dates[] =$dhearty_date;
                    }
                }
            }
        }
        return $list_dates;
    }
    

    public function wfh_request_notification_email($id,$new_status)
    {
        //this function will send email along with reset code
        $this->load->library('email');
        $this->email->set_newline("\r\n");

        $this->db->select('wfh.*,users.first_name,users.email');
        $this->db->where('wfh.id',$id);
        $this->db->join('users','users.id = wfh.staff_id','left');
        $query=$this->db->get('wfh');
        $result=$query->row();

        $email_subject = 'WFH Request Approval';
        $this->db->where('id',$result->manager_id);
        $query=$this->db->get('users');
        $users = $query->row();
        

        $data = array(
            'staff'=>$result,
            'superior'=>$users,
        );

        $message = $this->load->view('email_template/wfh_request.php',$data,TRUE);
        $this->email->from('eleave_admin@geoinfo.com.my');
        $this->email->to($users->email); 
        $this->email->subject($email_subject);
        $this->email->message($message);
        if ($this->email->send()) 
        {
            return true;
        }else{
        //    show_error($this->email->print_debugger());
            return false;
        }     
        
    }
}