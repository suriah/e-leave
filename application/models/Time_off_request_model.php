<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Time_off_request_model extends Ajax_datatable_model
{

    var $table           = 'time_off';
    var $column_order    = array('time_off', 'time_off_date', 'time_off_time_start','time_off_time_end','time_off_reason',null); //set column field database for datatable orderable
    var $column_search   = array('time_off', 'time_off_date', 'time_off_time_start','time_off_time_end','time_off_reason'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order           = array('id' => 'asc'); // default order 
    //var $filter = array('role' => 'admin', 'email' => 'admin@admin.com');  //sample
    var $filter          = null;
    var $filter_where_in = null;

    public function __construct()
    {
        parent::__construct();
    }

    public function list_manager_drop_down($group){
        
        foreach ($group as $key => $value) {
            if($value->name == 'superior'){
                $id = $value->id;
            }
        }
         $this->db->select('users.*');
         $this->db->join('users_groups', 'users.id = users_groups.user_id', 'left');
         $this->db->where('users_groups.group_id', $id);
         $this->db->where_not_in('username', 'administrator');
         $query  = $this->db->get('users');
         $result = $query->result_array();
    
        $data = array();
        $data[''] = 'Please select';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data[$row['id']] = $row['first_name'];
            }
        }
        return $data;
    }
}