<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Privilege_model extends CI_model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function list_privileges($group_list){
        //list all privilege from aray of group list

        $privileges = array();
        foreach ($group_list as $group){
            //print_r ($group);
            $this->db->where('group_id', $group->id);
            $this->db->where('permission', true);
            // $this->db->select('privileges_groups.id, privilege_id, group_id, permission, privileges.privilege_code');
            $this->db->select('privileges.privilege_code');
            $this->db->join('privileges', 'privileges_groups.privilege_id = privileges.id' , 'left');
            $query = $this->db->get('privileges_groups');
            $result_array = $query->result_array();
            foreach ($result_array as $detail){
                $privileges[] = $detail['privilege_code'];
            }
        }
        //clean up duplicate
        $privileges_list = array_unique($privileges);
        return $privileges_list;
    }

}
