<?php

class Authentication_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function forgot_password_send_email($email)
    {
        //this function will send email along with reset code
        $this->load->library('email');
        $this->email->set_newline("\r\n");
        $code = $this->ion_auth->forgotten_password($email);
        $data = array('code' => $code);
//        print '<pre>';
//        print_r ($data);
//        print '</pre>';
//        exit();
        $message = $this->load->view('email_template/forgot_password.php',$data,TRUE);
        $this->email->from('eleave_admin@geoinfo.com.my');
        $this->email->to($email); 
        $email_subject = 'e-leave Reset Password Request';
        $this->email->subject($email_subject);
        $this->email->message($message);
        if ($this->email->send()) 
        {
            return true;
        }else{
            
        //    show_error($this->email->print_debugger());
            return false;
        }     
        
    }
    
    public function forgot_password_reset($form_var)
    {
        $this->load->library('email');
        $this->email->set_newline("\r\n");
        
        $data = $this->ion_auth->forgotten_password_complete(trim($form_var['code']));
            
        if ($data)
        {
            $message = $this->load->view('email_template/reset_password.php',$data,TRUE);
            $this->email->from('eleave_admin@geoinfo.com.my');
            $this->email->to($data['identity']); 
            $email_subject = 'e-leave Reset Password';
            $this->email->subject($email_subject);
            $this->email->message($message);
            if ($this->email->send()) 
            {
                return true;
            }else{         
                show_error($this->email->print_debugger());
                return false;
            }
        }else{
            return false;
        }
    }
            
    
}