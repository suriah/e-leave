<?php

class Settings_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_system_settings() {
        $query = $this->db->get('system_settings');
        if ($query->num_rows() > 0) {
            $row = $query->row();
        }
        return $row;
    }

    public function update_system_settings($form_var, $settings) {

        //this function update application config var
        $data = array(
            'app_name'              => $form_var['app_name'],
            'app_description'       => $form_var['app_description'],
            'app_footer'            => $form_var['app_footer'],
            'multi_language'        => $form_var['multi_language'],
            'menu_color_options'    => $form_var['menu_color_options'],
            'navigation_color_type' => $form_var['navigation_color_type'],
            'navigation_color'      => $form_var['navigation_color'],
            'menu_border'           => $form_var['menu_border'],
            'bypass_menu_restriction' => $form_var['bypass_menu_restriction']
        );

        //upload image to server
        $config['upload_path']   = FCPATH . 'app-assets/logo/';
        $config['allowed_types'] = '*';
        $config['max_size']      = '1024';
        $config['overwrite']     = true;
        $config['encrypt_name']  = true;
        $this->load->library('upload', $config);

        if (isset($_FILES['app_logo_image']) && is_uploaded_file($_FILES['app_logo_image']['tmp_name'])) {
            if ($this->upload->do_upload('app_logo_image')) {
                $app_logo_image         = $this->upload->data();
                //upload image successful, so delete old image
                //echo FCPATH . 'profile_img/' . $this->data['auth_info']->profile_img;
                unlink(FCPATH . 'app-assets/logo/' . $settings->app_logo_image);
                $data['app_logo_image'] = $app_logo_image['file_name'];
            } else {
                $this->upload->display_errors();
                $this->session->set_flashdata('status', 'failed');
                $this->session->set_flashdata('message', $this->upload->display_errors());
                return false;
            }
        }

        if (isset($_FILES['app_icon_image']) && is_uploaded_file($_FILES['app_icon_image']['tmp_name'])) {
            if ($this->upload->do_upload('app_icon_image')) {
                $app_icon_image         = $this->upload->data();
                //upload image successful, so delete old image
                //echo FCPATH . 'profile_img/' . $this->data['auth_info']->profile_img;
                unlink(FCPATH . 'app-assets/logo/' . $settings->app_icon_image);
                $data['app_icon_image'] = $app_icon_image['file_name'];
                //exit();
            } else {
                $this->session->set_flashdata('status', 'failed');
                $this->session->set_flashdata('message', $this->upload->display_errors());
                return false;
                //redirect(base_url('settings'));
            }
        }

        $query = $this->db->get('system_settings');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $id  = $row->id;
            //update table
            $this->db->where('id', $id);
            $this->db->update('system_settings', $data);
            return true;
        } else {
            $this->db->insert('system_settings', $data);
        }
    }

}
