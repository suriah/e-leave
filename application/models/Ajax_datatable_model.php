<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Ajax_datatable_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query($post_var)
    {
        $this->db->from($this->table);

        //for selection
        if (isset($this->selection)) {
            $this->db->select($this->selection);
        }

        //$this->db->join($this->join['table'], $this->join['map'] , $this->join['join_type']);
        //for join operation                          
        if (isset($this->join)) {
            foreach ($this->join as $value) {
                $this->db->join($value['table'], $value['map'], $value['join_type']);
                //$this->db->join($this->join['table'], $this->join['map'] , $this->join['join_type']);
            }
        }

        if (isset($this->filter)) {
            foreach ($this->filter as $key => $value) {
                $this->db->where($key, $value);
            }
        }

        if (isset($this->filter_where_in)) {
            //code below is sample on how to use
            //var $filter_where_in = array('status' => array('SC ACKNOWLEDGE', 'SO DRAFT'));
            foreach ($this->filter_where_in as $key => $value) {
                //print_r ($value);
                $this->db->where_in($key, $value);
            }
        }

        //for group by
        if (isset($this->group_by)) {
            $this->db->group_by($this->group_by);
        }

        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($post_var['search']['value']) { // if datatable send POST for search

                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $post_var['search']['value']);
                } else {
                    $this->db->or_like($item, $post_var['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) { //last loop
                    $this->db->group_end(); //close bracket
                }
            }
            $i++;
        }

        if (isset($post_var['order'])) { // here order processing
            $this->db->order_by($this->column_order[$post_var['order']['0']['column']], $post_var['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($post_var)
    {
        $this->_get_datatables_query($post_var);
        if ($post_var['length'] != -1) {
            $this->db->limit($post_var['length'], $post_var['start']);
        }
        $query = $this->db->get();
        //        print '<pre>';       
        //        print_r ($query->result());
        //        print '</pre>';
        return $query->result();
    }

    function count_filtered($post_var)
    {
        $this->_get_datatables_query($post_var);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        //filter only related branch will be displayed
        if (isset($this->record_filter)) {
            $this->db->where($this->record_filter['filter_field'], $this->record_filter['filter_value']);
        }
        //end edited
        return $this->db->count_all_results();
    }

    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    public function insert($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
