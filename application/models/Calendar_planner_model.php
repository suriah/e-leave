<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Calendar_planner_model extends Ajax_datatable_model
{

    var $table           = 'public_holiday';
    var $column_order    = array('public_holiday', 'public_holiday_date', null); //set column field database for datatable orderable
    var $column_search   = array('public_holiday', 'public_holiday_date'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order           = array('id' => 'asc'); // default order 
    //var $filter = array('role' => 'admin', 'email' => 'admin@admin.com');  //sample
    var $filter          = null;
    var $filter_where_in = null;

    public function __construct()
    {
        parent::__construct();
    }
}