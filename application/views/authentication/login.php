
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <!-- <img src="<?php echo base_url('upload/background/background.gif')  ?>"> -->
      </div>
      <div class="content-body" style="background-image: url('upload/background/bc2.jpg'); background-size: cover;">
        <section class="flexbox-container">
          <!-- <div class="col-6">

          </div> -->
          <div class="col-12 d-flex align-items-center justify-content-end pr-5">
            <div class="col-md-3 col-10 box-shadow-2 p-0">
              <div class="card border-grey border-lighten-3 m-0">
                <div class="card-header border-0">
                  <div class="card-title text-center">
                    <div class="p-1">
                      <img src="<?php echo base_url('app-assets/logo/' . $settings->app_logo_image )  ?>" style="width:150px;">
                    </div>
                  </div>
                    
                 <?php
                    if ($this->session->flashdata('login_status')) {
                        if ($this->session->flashdata('login_status') == 'false') {
                            echo '<div class="alert alert-danger " role="alert">'
                            . $this->session->flashdata('message') . '</div>';

                        } 
                    }
                ?>
                    
                  <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                    <span>Login to <?php echo $settings->app_name ?></span>
                  </h6>
                   
                </div>
                <div class="card-content">
                  <div class="card-body">
                    <form class="form-horizontal form-simple" action="<?php echo base_url('auth/login'); ?>" method="POST" novalidate>
                      <fieldset class="form-group position-relative has-icon-left mb-0">
                        <input type="text" class="form-control form-control-lg input-lg" name="identity" placeholder="Your Username"
                        required>
                        <div class="form-control-position">
                          <i class="ft-user"></i>
                        </div>
                      </fieldset>
                      <fieldset class="form-group position-relative has-icon-left">
                        <input type="password" class="form-control form-control-lg input-lg" name="password"
                        placeholder="Enter Password" required>
                        <div class="form-control-position">
                          <i class="la la-key"></i>
                        </div>
                      </fieldset>
                      <div class="form-group row">
                        <div class="col-md-6 col-12 text-center text-md-left">
                          <fieldset>
                            <input type="checkbox" id="remember-me" class="chk-remember">
                            <label for="remember-me"> Remember Me</label>
                          </fieldset>
                        </div>
                        <div class="col-md-6 col-12 text-center text-md-right"><a href="<?php echo base_url('default_page/forgot_password_form') ?>" class="card-link">Forgot Password?</a></div>
                      </div>
                      <button type="submit" class="btn btn-lg text-light btn-block" style="background-color: #154c79;"><i class="ft-unlock"></i> Login</button>
                    </form>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="">
                      <!--
                    <p class="float-sm-left text-center m-0"><a href="recover-password.html" class="card-link">Recover password</a></p>
                    <p class="float-sm-right text-center m-0">New to Moden Admin? <a href="register-simple.html" class="card-link">Sign Up</a></p>
                      -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
