

<script>
    

</script>

  <div class="app-content content">
    <div class="content-wrapper">
      <!--<div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
          <h3 class="content-header-title mb-0 d-inline-block">Change Password</h3>
          <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html"></a>
                </li>
                <li class="breadcrumb-item"><a href="#">Profile</a>
                </li>
                <li class="breadcrumb-item active">Change Password
                </li>
              </ol>
            </div>
          </div>  
        </div>
          
          
        <div class="content-header-right col-md-6 col-12">
          <div class="dropdown float-md-right">
            <button class="btn btn-danger dropdown-toggle round btn-glow px-2" id="dropdownBreadcrumbButton"
            type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
            <div class="dropdown-menu" aria-labelledby="dropdownBreadcrumbButton"><a class="dropdown-item" href="#"><i class="la la-calendar-check-o"></i> Calender</a>
              <a class="dropdown-item" href="#"><i class="la la-cart-plus"></i> Cart</a>
              <a class="dropdown-item" href="#"><i class="la la-life-ring"></i> Support</a>
              <div class="dropdown-divider"></div><a class="dropdown-item" href="#"><i class="la la-cog"></i> Settings</a>
            </div>
          </div>
        </div>
          
          
      </div>
        -->
        
        
        
        
        <!-- Basic form layout section start -->
        <section id="basic-form-layouts">
          <div class="row match-height">
            <div class="col-md-4">
              <div class="card">

                <div class="card-content collapse show">
                  <div class="card-body">

                      <form class="form" action="<?php echo base_url('authentication/change_password_update') ?>" method="POST">   
                      <div class="form-body">
                        <h4 class="form-section"><i class="la la-key"></i> Change Password Form</h4>
                          
                            <?php
                        if ($this->session->flashdata('status')) {
                            if ($this->session->flashdata('status') == 'false') {
                                echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                                        <strong>Update password failed!</strong> ' . $this->session->flashdata('message') .
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>';
                            }else{
                                echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
                                        <strong>Update successful!</strong> ' . $this->session->flashdata('message') .
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>';
                                
                            }
                        }
                        ?>                        
                            
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="current_password">Current Password</label>
                              <input type="password" minlength="8" class="form-control" id="current_password" name="current_password" placeholder="Please enter current password"  required="true" >
                            </div>
                          </div>
                        </div>
                            
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="new_password">New Password</label>
                              <input type="password" minlength="8" class="form-control" id="new_password" name="new_password" placeholder="Please enter new password"  required="true" >
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="current_password">Confirm Password</label>
                              <input type="password" minlength="8" class="form-control" id="confirm_password" name="confirm_password" placeholder="Please confirm new password"  required="true" >
                            </div>
                          </div>
                        </div>                            

                        <div class="row">
                          <div class="col-md-12">                        
                            <button type="submit" class="btn btn-primary">Submit</button>
                          </div>    
                        </div>
                        
                        </form>    
                                                    
                  </div>
                </div>
              </div>
            </div>

          </div>

        </section>
        <!-- // Basic form layout section end -->
        
        
        
        
        
        
        
      
    </div>
  </div>




<!-- /.modal -->
<div class="modal fade" id="confirm-modal-dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white">
                    <i class="ft ft-alert-triangle"></i> Alert</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure want to delete selected account ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="delete_record(0)">Yes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="modal_form_edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white">
                    <i class="ft ft-edit"></i> Edit Employee Details</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <i class="ft ft-x white"></i>
                </button>
            </div>

            <div class="modal-body">
                <form action="#" id="form_edit" class="form-horizontal" role="form">
                    <input type="hidden" value="" name="id" />

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label" for="EmpNo">No Pekerja</label>
                                <input name="EmpNo" type="text" placeholder="Masukkan No Pekerja" autocomplete="off" class="form-control">
                                <span class="has-error"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label" for="EmpName">Nama Pekerja</label>
                                <input name="EmpName" type="text" placeholder="Masukkan nama pekerja" autocomplete="off" class="form-control">
                                <span class="has-error"></span>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label" for="EmpStatus">Status</label>
                                <select name="EmpStatus" class="form-control">
                                    <option value='ACT'>ACTIVE</option>
                                    <option value='RES'>RESIGN</option>
                                </select>
                                <span class="has-error"></span>
                            </div>
                        </div>
                    </div>



                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnEditSave" onclick="save()">Save Record</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
