
 <!-- ////////////////////////////////////////////////////////////////////////////-->
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <section class="flexbox-container">
          <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-md-4 col-10 box-shadow-2 p-0">
              <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
                <div class="card-header border-0 pb-0">
                  <div class="card-title text-center">
                    <img src="<?php echo base_url('app-assets/logo/'.$settings->app_logo_image ) ?>" alt="Geoinfo" style="width: 150px;">
                  </div>
<!--                  <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                    <span>Your reset password code had been sent to your email. Please input reset password code below.</span>
                  </h6>-->
                <BR>
                <h5 class="card-subtitle text-center">
                    Your reset password code had been sent to your email. Please input reset password code below.
                </h5>
                </div>
                <div class="card-content">
                  <div class="card-body">
                      
                    <form class="form-horizontal" action="<?php echo base_url('default_page/reset_password') ?>" novalidate method="POST">
                      <fieldset class="form-group position-relative has-icon-left">
                        <input type="text" class="form-control form-control-lg input-lg" name="code"
                        placeholder="Please Input Reset Code" required>
                        <div class="form-control-position">
                          <i class="ft-alert-triangle"></i>
                        </div>
                      </fieldset>
                      <button type="submit" class="btn btn-outline-info btn-lg btn-block"><i class="ft-unlock"></i> Reset Password</button>
                    </form>
                  </div>
                </div>
                <div class="card-footer border-0">
                  <p class="float-sm-left text-center"><a href="login-simple.html" class="card-link">Return to login page</a></p>
                  
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->