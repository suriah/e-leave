
 <!-- ////////////////////////////////////////////////////////////////////////////-->
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <section class="flexbox-container">
          <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-md-4 col-10 box-shadow-2 p-0">
              <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
                <div class="card-header border-0 pb-0">
                  <div class="card-title text-center">
                    <img src="<?php echo base_url('app-assets/logo/'.$settings->app_logo_image ) ?>" alt="Geoinfo" style="width: 150px;">
                  </div>
<!--                  <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                    <span>Your reset password code had been sent to your email. Please input reset password code below.</span>
                  </h6>-->
<BR>
<BR>
                <h5 class="card-subtitle text-center">
                    <?php  echo $message?>
                </h5>
                </div>
                  <BR>
                  <BR>

              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->