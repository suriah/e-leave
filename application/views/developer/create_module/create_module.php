<script>
$(document).ready(function() {

});
</script>

<div class="app-content content">
    <div class="content-wrapper">

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="col-md-6">
                        <div class="card">

                            <div class="card-content collapse show">
                                <div class="card-body">

                                    <form class="form" enctype="multipart/form-data" action="<?= base_url('developer/create_module/generate_module') ?>" method="POST">
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="ft-settings"></i> Create Module</h4>


                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Module Name</label>
                                                        <input type="text" class="form-control" name="module_name">
                                                    </div>
                                                </div>

                                                <!-- <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Migration Type</label>
                                                        <select name="migration_type" class="form-control">
                                                          <option value="DB_STRUCTURE">Database Structure</option>
                                                          <option value="DB_VALUE">Database Value</option>
                                                        </select>
                                                    </div>
                                                </div> -->
                                            </div>

                                            <div class="form-actions">
                                                <!-- <button type="reset" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </button> -->
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Create Module Skeleton File
                                                </button>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


                
            </section>
            <!-- // Basic form layout section end -->
        </div>


        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="col-md-6">
                        <div class="card">

                            <div class="card-content collapse show">
                                <div class="card-body">

                                    <form class="form" enctype="multipart/form-data" action="<?= base_url('developer/create_module/copy_module') ?>" method="POST">
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="ft-layers"></i> Copy Module</h4>


                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="projectinput1">New Module Name</label>
                                                        <input type="text" class="form-control" name="new_module_name">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Copy From Module</label>
                                                        <input type="text" class="form-control" name="copy_from_module">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-actions">
                                                <!-- <button type="reset" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </button> -->
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="ft-layers"></i> Copy Module File
                                                </button>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


                
            </section>
            <!-- // Basic form layout section end -->
        </div>
    </div>
</div>