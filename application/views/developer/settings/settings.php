
<style>
    /*Profile Pic Start*/
.picture-container{
    position: relative;
    cursor: pointer;
    text-align: center;
}
.picture{
    width: 106px;
    height: 106px;
    background-color: #999999;
    border: 4px solid #CCCCCC;
    color: #FFFFFF;
    border-radius: 10%;
    margin: 0px auto;
    overflow: hidden;
    transition: all 0.2s;
    -webkit-transition: all 0.2s;
}
.picture:hover{
    border-color: #2ca8ff;
}
.content.ct-wizard-green .picture:hover{
    border-color: #05ae0e;
}
.content.ct-wizard-blue .picture:hover{
    border-color: #3472f7;
}
.content.ct-wizard-orange .picture:hover{
    border-color: #ff9500;
}
.content.ct-wizard-red .picture:hover{
    border-color: #ff3b30;
}
.picture input[type="file"] {
    cursor: pointer;
    display: block;
    height: 100%;
    left: 0;
    opacity: 0 !important;
    position: absolute;
    top: 0;
    width: 100%;
}

.picture-src{
    width: 100%;
    
}
/*Profile Pic End*/
    
</style>

<script>
$(document).ready(function(){
// Prepare the preview for profile picture
    $("#wizard-logo").change(function(){
        readURLLogo(this);
    });
    
    $("#wizard-icon").change(function(){
        readURLIcon(this);
    });
    
    
});

function modify_color_options() {
    console.log($("#navigation_color_type").val());
    //get value
    if ($("#navigation_color_type").val() === 'semi_dark' || $("#navigation_color_type").val() === 'light'){
        //hide all  gradient color        
        $('#color_options').children('option[value="bg-gradient-x-grey-blue"]').hide();
        $('#color_options').children('option[value="bg-gradient-x-primary"]').hide();
        $('#color_options').children('option[value="bg-gradient-x-danger"]').hide();
        $('#color_options').children('option[value="bg-gradient-x-success"]').hide();
        $('#color_options').children('option[value="bg-gradient-x-blue"]').hide();
        $('#color_options').children('option[value="bg-gradient-x-cyan"]').hide();
        $('#color_options').children('option[value="bg-gradient-x-pink"]').hide();
    }else{
        $('#color_options').children('option[value="bg-gradient-x-grey-blue"]').show();
        $('#color_options').children('option[value="bg-gradient-x-primary"]').show();
        $('#color_options').children('option[value="bg-gradient-x-danger"]').show();
        $('#color_options').children('option[value="bg-gradient-x-success"]').show();
        $('#color_options').children('option[value="bg-gradient-x-blue"]').show();
        $('#color_options').children('option[value="bg-gradient-x-cyan"]').show();
        $('#color_options').children('option[value="bg-gradient-x-pink"]').show();
    }
}

function readURLLogo(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#wizardLogoPreview').attr('src', e.target.result).fadeIn('slow');
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function readURLIcon(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#wizardIconPreview').attr('src', e.target.result).fadeIn('slow');
        };
        reader.readAsDataURL(input.files[0]);
    }
}

</script>

<div class="app-content content">
    <div class="content-wrapper">

      <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="basic-form-layouts">
          <div class="row match-height">
            <div class="col-md-6">
              <div class="card">
                
                <div class="card-content collapse show">
                  <div class="card-body">
                    
                    <form class="form" enctype="multipart/form-data" action="<?= base_url('developer/settings/update_settings') ?>" method="POST" >
                      <div class="form-body">
                        <h4 class="form-section"><i class="ft-settings"></i> Application General Settings</h4>
                        
                        <?php
                           if ($this->session->flashdata('status')) {
                               if ($this->session->flashdata('status') == 'false') {
                                   echo '<div class="alert alert-danger " role="alert">'
                                   . $this->session->flashdata('message') . '</div>';

                               }else{
                                   echo '<div class="alert alert-primary " role="alert">'
                                   . $this->session->flashdata('message') . '</div>';
                                   
                               } 
                           }
                       ?>
                                            
                        <div class="row">
                          <div class="col-md-6 text-center">
                            <div class="form-group">
                                <label for="projectinput1">Application Logo</label>

                                <div class="picture-container">
                                    <div class="picture">
                                        <img src="<?= base_url('/app-assets/logo/' . $settings->app_logo_image)  ?>" class="picture-src" id="wizardLogoPreview" title="">
                                        <input type="file" id="wizard-logo" class="" name='app_logo_image'>
                                    </div>
<!--                                    <h6 class="">Choose Picture</h6>-->
                                </div>
                            </div>
                          </div>

                          <div class="col-md-6 text-center">
                            <div class="form-group">
                                <label for="projectinput1" >Application Icon</label>

                                <div class="picture-container">
                                    <div class="picture">
                                        <img src="<?= base_url('/app-assets/logo/' . $settings->app_icon_image)  ?>" class="picture-src" id="wizardIconPreview" title="">
                                        <input type="file" id="wizard-icon" class="" name='app_icon_image'>
                                    </div>
<!--                                    <h6 class="">Choose Picture</h6>-->
                                </div>
                            </div>
                          </div>


                        </div>
                        
                        
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="projectinput1">Application Name</label>
                              <input type="text" class="form-control" placeholder="Please input name"
                              name="app_name" value="<?= $settings->app_name ?>" required>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="projectinput1">Application Description</label>
                              <input type="text" class="form-control" placeholder="Please input application description"
                              name="app_description" value="<?= $settings->app_description ?>" required>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="projectinput1">Footer</label>
                              <input type="text" class="form-control" placeholder="Please input footer information"
                              name="app_footer" value="<?= $settings->app_footer ?>" required>
                            </div>
                          </div>
                        </div>
                        
                        
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="projectinput1">Multi Language</label>
                                                           
                                <?php
                                    $options = array(
                                            '0'         => 'Disable',
                                            '1'         => 'Enable',
                                    );
                                
                                    echo form_dropdown('multi_language', $options, $settings->multi_language , 'class="form-control" placeholder="Please input footer information"');                                
                                ?>                                                                                         
                            </div>
                          </div>
                            
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="projectinput1">Menu Color Options</label>
                                                           
                                <?php
                                    $options = array(
                                            '0'         => 'Dark Menu',
                                            '1'         => 'Light Menu',
                                    );
                                
                                    echo form_dropdown('menu_color_options', $options, $settings->menu_color_options , 'class="form-control" placeholder="Please input footer information"');
                                
                                ?>                                                                                         
                            </div>
                          </div>                                                       
                        </div>
                        

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="projectinput1">Navigation Color Type</label>
                                                           
                                <?php
                                    $options = array(
                                            'semi_dark'    => 'Semi Dark',
                                            'semi_light'   => 'Semi Light',
                                            'dark'         => 'Dark',
                                            'light'        => 'Light'
                                    );
                                
                                    echo form_dropdown('navigation_color_type', $options, $settings->navigation_color_type , 'class="form-control" id="navigation_color_type" onchange=modify_color_options() ');                                
                                ?>                                                                                         
                            </div>
                          </div>
                            
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="projectinput1">Navigation Bar Color </label>
                                                           
                                <?php
                                    $options = array(
                                            'bg-default'                => 'Default',
                                            'bg-primary'                => 'Primary',
                                            'bg-danger'                 => 'Danger',
                                            'bg-success'                => 'Success',
                                            'bg-blue'                   => 'Blue',
                                            'bg-cyan'                   => 'Cyan',
                                            'bg-pink'                   => 'Pink',
                                            'bg-gradient-x-grey-blue'   => 'Default Gradient',
                                            'bg-gradient-x-primary'     => 'Primary Gradient',
                                            'bg-gradient-x-danger'      => 'Danger Gradient',
                                            'bg-gradient-x-success'     => 'Success Gradient',
                                            'bg-gradient-x-blue'        => 'Blue Gradient',
                                            'bg-gradient-x-cyan'        => 'Cyan Gradient',
                                            'bg-gradient-x-pink'        => 'Pink Gradient',
                                        
                                    );
                                
                                    echo form_dropdown('navigation_color', $options, $settings->navigation_color , 'class="form-control" id="color_options" ');
                                
                                ?>                                                                                         
                            </div>
                          </div>                                                       
                        </div>
                        

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="projectinput1">Menu Border</label>
                                                           
                                <?php
                                    $options = array(
                                            '1'    => 'Yes',
                                            '0'   => 'No',
                                    );
                                
                                    echo form_dropdown('menu_border', $options, $settings->menu_border , 'class="form-control"');                                
                                ?>                                                                                         
                            </div>
                          </div>

                          
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="projectinput1">Bypass Menu Restriction</label>
                                                           
                                <?php
                                    $options = array(
                                            't'    => 'Yes',
                                            'f'   => 'No',
                                    );
                                
                                    echo form_dropdown('bypass_menu_restriction', $options, $settings->bypass_menu_restriction , 'class="form-control"');                                
                                    //echo form_dropdown('bypass_menu_restriction', $options, 'false' , 'class="form-control"');                                
                                ?>                                                                                         
                            </div>
                          </div>


                        </div>


                        
                      <div class="form-actions">
                        <button type="reset" class="btn btn-warning mr-1">
                          <i class="ft-x"></i> Cancel
                        </button>
                        <button type="submit" class="btn btn-primary">
                          <i class="la la-check-square-o"></i> Save
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
          
        </section>
        <!-- // Basic form layout section end -->
      </div>
    </div>
  </div>
