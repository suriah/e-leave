<script>
$(document).ready(function() {


    select = $('[name="migration_type"]').selectize();
    selectize = select[0].selectize;

    select = $('[name="migration_file"]').selectize();
    selectize = select[0].selectize;
});
</script>

<div class="app-content content">
    <div class="content-wrapper">

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="col-md-6">
                        <div class="card">

                            <div class="card-content collapse show">
                                <div class="card-body">

                                    <form class="form" enctype="multipart/form-data" action="<?= base_url('developer/create_migration/create_migration_file') ?>" method="POST">
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="ft-settings"></i> Create Migration File</h4>


                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Table Name</label>
                                                        <?php
                                                            //echo form_dropdown('table_name', $table_list, '', 'class="form-control" ');
                                                        ?>
                                                        <input type="text" name="table_name" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Migration Type</label>
                                                        <select name="migration_type" class="form-control">
                                                          <option value="DB_STRUCTURE">Database Structure</option>
                                                          <option value="DB_VALUE">Database Value</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-actions">
                                                <button type="reset" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </button>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Create Migration File
                                                </button>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </section>
            <!-- // Basic form layout section end -->
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">

                    <div class="col-md-6">
                        <div class="card">

                            <div class="card-content collapse show">
                                <div class="card-body">

                                    <form class="form" enctype="multipart/form-data" action="<?= base_url('developer/create_migration/run_migration_file') ?>" method="POST">
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="ft-settings"></i> Migration File Operation</h4>

                                            
                                                <div class="row">
                                                    <div class="col-md-10">
                                                        <?php
                                                                echo form_dropdown('migration_file', $migration_list, '', 'class="form-control " ');
                                                        ?>
                                                    </div>
                                                </div>


                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Run Migration File
                                                </button>
                                            </div>


                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </section>
            <!-- // Basic form layout section end -->
        </div>


    </div>
</div>