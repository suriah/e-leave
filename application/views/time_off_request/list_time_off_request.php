<script>
var list;
var save_method;
var selected_id;

// ******************show dialog box *********************
function delete_record_dialog(id) {
    $('#confirm-modal-dialog').modal('show'); // show bootstrap modal
    selected_id = id;
}

function submit_record_dialog(id) {
    $('#confirm-submit-modal-dialog').modal('show'); // show bootstrap modal
    selected_id = id;
}

//****************execution block*********************
function submit_record() {
        // ajax delete data to database
        $.ajax({
            url: "<?php echo site_url('Time_off_request/ajax_submit') ?>/" +
                selected_id,
            type: "POST",
            dataType: "JSON",
            success: function(data) {
                //if success reload ajax table
                $('#confirm-submit-modal-dialog').modal('hide');
                reload_table();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error submitting data');
            }
        });
    }

function add_record() {
    //TODO => currency and type change to selectize and auto complete
    selected_id = null;

    $('.help-block').empty(); // clear error string      
    $('#modal_form').find('input:text, input:password, select, textarea').val('');
    $('#modal_form').modal('show'); // show bootstrap modal when complete loaded        

    $('#time_off_date').each(function(index) {
        $(this).datetimepicker(
        'defaultDate', new Date());  
    });
    $('#time_off_time_start').each(function(index) {
        $(this).datetimepicker(
        'defaultDate', new Date());  
    });
    $('#time_off_time_end').each(function(index) {
        $(this).datetimepicker(
        'defaultDate', new Date());  
    });

        var selectize = $("#manager")[0].selectize;
        selectize.clear();
}

function edit_record(id) {
    selected_id = id;

    $('.help-block').empty(); // clear error string
    $('#modal_form').find('input:text, input:password, select, textarea').val('').end();

    //Ajax Load data from ajax
    $.ajax({
        url: "<?php echo site_url('Time_off_request/ajax_edit/') ?>" +
            id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {

            //TODO => update edit
            $('[name="time_off_date"]').val(moment(data.time_off_date, 'YYYY-MM-DD').format('DD-MM-YYYY'));
            $('[name="time_off_time_start"]').val(moment(data.time_off_time_start).format('hh:mm A'));
            $('[name="time_off_time_end"]').val(moment(data.time_off_time_end).format('hh:mm A'));
            $('[name="time_off_reason"]').val(data.time_off_reason);
            var selectize = $("#manager")[0].selectize;
            selectize.setValue(data.manager_id);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}



//******************end show dialog box *************************

//****************execution block*********************
function delete_record() {
    // ajax delete data to database
    $.ajax({
        url: "<?php echo site_url('Time_off_request/ajax_delete') ?>/" +
            selected_id,
        type: "POST",
        dataType: "JSON",
        success: function(data) {
            //if success reload ajax table
            $('#confirm-modal-dialog').modal('hide');
            reload_table();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Error deleting data');
        }
    });
}

function view_record(id) {
        selected_id = id;

        $('.help-block').empty(); // clear error string
        $('#modal_view_form').find('input:text, input:password, select, textarea').val('').end();

        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('Time_off_request/ajax_view/') ?>" +
                id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {


                //TODO => update edit
                $('[name="view_time_off_date"]').val(moment(data.time_off_date, 'YYYY-MM-DD').format('DD-MM-YYYY'));
                $('[name="view_time_off_time_start"]').val(moment(data.time_off_time_start).format('hh:mm A'));
                $('[name="view_time_off_time_end"]').val(moment(data.time_off_time_end).format('hh:mm A'));
                $('[name="view_time_off_reason"]').val(data.time_off_reason);

                var selectize = $("#view_manager")[0].selectize;
                selectize.setValue(data.manager_id);
                $('#modal_view_form').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

function save() {
    //intiate var for add or update

    url =
        "<?php echo base_url('Time_off_request/ajax_upsert') ?>";
    //form_to_submit = "#form_edit";          
    modal_form = '#modal_form';

    var modal_form_data = $("#form_edit").serializeArray();
    modal_form_data.push({
        name: "id",
        value: selected_id
    });
    form_to_submit = modal_form_data;
    btn_save = '#btnSave';

    //change button state after save is clicked
    $(btn_save).text('saving...'); //change button text
    $(btn_save).attr('disabled', true); //set button disable

    // var url;
    // ajax adding data to database
    $.ajax({
        url: url,
        type: "POST",
        data: $(form_to_submit),
        dataType: "JSON",
        success: function(data) {

            if (data.status) //if success close modal and reload ajax table
            {
                $(modal_form).modal('hide');
                //reset form
                //$(modal_form).reset();
                reload_table();
            } else {
                for (var i = 0; i < data.inputerror.length; i++) {
                    if (data.inputerror[i] == 'time_off_date' || data.inputerror[i] == 'time_off_time_start'|| data.inputerror[i] == 'time_off_time_end'){
                            $('[name="' + data.inputerror[i] + '"]').parent().next().text(data.error_string[i]); //select span help-block class set text error string             
                        }else if( data.inputerror[i] == 'manager_id' ){
                            $('[name="' + data.inputerror[i] + '"]').parent().parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                            $('[name="' + data.inputerror[i] + '"]').next().next().text(data.error_string[i]); //for selectize error
                        }
                        else{
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string                                   
                        }           
                }
            }
            $(btn_save).text('Save Record'); //change button text
            $(btn_save).attr('disabled', false); //set button enable

        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Error adding / update data');
            $(btn_save).text('Save Record'); //change button text
            $(btn_save).attr('disabled', false); //set button enable
        }
    });
}

function onchange_time(){
    time_off_time_start = $('#input_time_off_time_start').val();
    time_off_time_start = (moment(time_off_time_start,"hh:mm A").format("hh:mm:ss"))

    strtotime('+5 hours')
}

//******************end execution block*************************

function reload_table() {
    list.ajax.reload(null, false); //reload datatable ajax
}

$(document).ready(function() {

    $('#manager').selectize({
            maxItems: 1
    });
    $('#view_manager').selectize({
        maxItems: 1,
    });

    var now = new Date();
        $("#time_off_date").datetimepicker({
            format: "DD-MM-YYYY",
            "defaultDate": new Date(now.getFullYear(), now.getMonth(), now.getDate()),
            icons: {
                up: "ft ft-chevron-up",
                down: "ft ft-chevron-down",
                previous: "ft ft-chevron-left",
                next: "ft ft-chevron-right",
            }
        });
        
        $("#time_off_time_start").datetimepicker({
            format: 'hh:mm A',
            "defaultDate": new Date(now.getHours(), now.getMinutes(), now.getSeconds()),
            icons: {
                up: "ft ft-chevron-up",
                down: "ft ft-chevron-down",
            }
        });
        $("#time_off_time_end").datetimepicker({
            format: 'hh:mm A',
            "defaultDate": new Date(now.getHours(), now.getMinutes(), now.getSeconds()),
            icons: {
                up: "ft ft-chevron-up",
                down: "ft ft-chevron-down",
            }
        });

    list = $('#list').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"order": [], //Initial no order.
        "searching": true,
        "paging": true,
        "info": false,
        "autoWidth": true,
        // "scrollX": true,
        //"deferLoading": 0,
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('Time_off_request/ajax_list') ?>",
            "type": "POST",
            "data": function (d) {
                d.time_off_date = $('#input_time_off_date').val();   
            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [{
            "targets": [-1], //last column
            "orderable": false, //set not orderable
            "width": "75"
        }, ],

        // "columnDefs": [
        //    { "width": "150", "targets": -1 }],
    });

});
</script>

<div class="app-content content">
    <div class="content-wrapper">

        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">List of Time Off Request</h4>

                                <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>

                                <div class="heading-elements">
                                    <button class="btn btn-primary " onclick="add_record()"><i class="ft-plus white"></i> Request New Time Off</button>
                                    <!--<span class="dropdown">
                        <button id="btnSearchDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="true" class="btn btn-warning dropdown-toggle dropdown-menu-right "><i class="ft-download-cloud white"></i></button>
                        <span aria-labelledby="btnSearchDrop1" class="dropdown-menu mt-1 dropdown-menu-right">
                          <a href="#" class="dropdown-item"><i class="ft-upload"></i> Import</a>
                          <a href="#" class="dropdown-item"><i class="ft-download"></i> Export</a>
                          <a href="#" class="dropdown-item"><i class="ft-shuffle"></i> Find Duplicate</a>
                        </span>
                      </span> -->
                                    <button class="btn btn-info " onclick="reload_table()"><i class="ft-refresh-cw white"></i> Refresh</button>
                                </div>
                                

                            </div>
                            <div class="card-content collapse show">

                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered zero-configuration" id="list">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Date</th>
                                                <th>Time</th>
                                                <th>Reason</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>


                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

        </div>
    </div>
</div>

<!-- /.modal -->
<div class="modal fade" id="confirm-modal-dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-alert-triangle"></i> Delete Time Off</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete selected event? This process irreversible.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " data-dismiss="modal"
                    onclick="delete_record(0)">Yes</button>
                <button type="button" class="btn btn-secondary " data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- modal form -->
<div class="modal fade" id="modal_form">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-edit"></i> Time Off Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>

            <div class="modal-body">
                <form action="#" id="form_edit" class="form-horizontal" role="form">
                    <input type="hidden" value="" name="id" />
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group has-error ">
                                <label class="control-label">Time Start</label>
                                <div class="input-group date" id="time_off_time_start" data-target-input="nearest">
                                    <input type="text" name="time_off_time_start" placeholder="00:00:00" class="form-control datetimepicker-input" data-target="time_off_time_start" id="input_time_off_time_start"  required />
                                    <div  class="input-group-append input-group-addon " data-target="time_off_time_start" data-toggle="datetimepicker">
                                        <div class="input-group-with-icon">
                                            <i class="ft ft-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label">Time End</label>
                                <div class="input-group date" id="time_off_time_end" data-target-input="nearest">
                                    <input type="text" name="time_off_time_end" placeholder="00:00:00" class="form-control datetimepicker-input" data-target="time_off_time_end" id="input_time_off_time_end"  required />
                                    <div class="input-group-append input-group-addon" data-target="time_off_time_end" data-toggle="datetimepicker">
                                        <div class="input-group-with-icon">
                                            <i class="ft ft-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label">Date</label>
                                <div class="input-group date" id="time_off_date" data-target-input="nearest">
                                    <input type="text" name="time_off_date" placeholder="dd-mm-yyyy" class="form-control datetimepicker-input" data-target="time_off_date" id="input_time_off_date"  required />
                                    <div class="input-group-append input-group-addon" data-target="time_off_date" data-toggle="datetimepicker">
                                        <div class="input-group-with-icon">
                                            <i class="ft ft-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="status"><span id="manager_label">Manager</span></label>
                                <?php
                                echo form_dropdown('manager_id', $manager_list, '', 'id="manager" class="form-control"');
                                ?>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <!-- <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="">Reasons</label>
                                <input name="time_off_reason" type="text" placeholder="Reason" autocomplete="off"
                                    class="form-control ">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div> -->

                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group has-error">
                                <label class="control-label " for="">Reasons</label>
                                <input name="time_off_reason" type="text" placeholder="Reason" autocomplete="off"
                                    class="form-control ">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " id="btnSave" onclick="save()">Save Record</button>
                <button type="button" class="btn btn-secondary " data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- end modal for editing -->

<!-- modal form -->
<div class="modal fade" id="modal_view_form">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-edit"></i> View Time Off Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>

            <div class="modal-body">
                <form action="#" id="form_edit" class="form-horizontal" role="form">
                    <input type="hidden" value="" name="id" />
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group has-error ">
                                <label class="control-label">Time Start</label>
                                <div class="input-group date" id="view_time_off_time_start" data-target-input="nearest">
                                    <input type="text" name="view_time_off_time_start" placeholder="00:00:00" class="form-control datetimepicker-input" data-target="time_off_time_start" id="input_time_off_time_start"  required />
                                    <div  class="input-group-append input-group-addon " data-target="time_off_time_start" data-toggle="datetimepicker">
                                        <div class="input-group-with-icon">
                                            <i class="ft ft-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label">Time End</label>
                                <div class="input-group date" id="view_time_off_time_end" data-target-input="nearest">
                                    <input type="text" name="view_time_off_time_end" placeholder="00:00:00" class="form-control datetimepicker-input" data-target="time_off_time_end" id="input_time_off_time_end"  required />
                                    <div class="input-group-append input-group-addon" data-target="time_off_time_end" data-toggle="datetimepicker">
                                        <div class="input-group-with-icon">
                                            <i class="ft ft-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label">Date</label>
                                <div class="input-group date" id="view_time_off_date" data-target-input="nearest">
                                    <input type="text" name="view_time_off_date" placeholder="dd-mm-yyyy" class="form-control datetimepicker-input" data-target="time_off_date" id="input_time_off_date"  required />
                                    <div class="input-group-append input-group-addon" data-target="time_off_date" data-toggle="datetimepicker">
                                        <div class="input-group-with-icon">
                                            <i class="ft ft-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="status"><span id="manager_label">Manager</span></label>
                                <?php
                                echo form_dropdown('manager_id', $manager_list, '', 'id="view_manager" class="form-control"');
                                ?>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <!-- <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="">Reasons</label>
                                <input name="time_off_reason" type="text" placeholder="Reason" autocomplete="off"
                                    class="form-control ">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div> -->

                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group has-error">
                                <label class="control-label " for="">Reasons</label>
                                <input name="view_time_off_reason" type="text" placeholder="Reason" autocomplete="off"
                                    class="form-control ">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-primary " id="btnSave" onclick="save()">Save Record</button> -->
                <button type="button" class="btn btn-secondary " data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- /.modal -->
<div class="modal fade" id="confirm-submit-modal-dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-alert-triangle"></i> Submit Request Time Off Application
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to submit the request application? This process irreversible.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " data-dismiss="modal" onclick="submit_record(0)">Submit</button>
                <button type="button" class="btn btn-secondary " data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->