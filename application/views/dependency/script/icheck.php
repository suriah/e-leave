  <!-- BEGIN PAGE VENDOR JS-->
  <script src="<?php echo base_url('/app-assets/vendors/js/forms/icheck/icheck.min.js') ?>" type="text/javascript">
  </script>
  <!-- END PAGE VENDOR JS-->

  <!-- BEGIN PAGE LEVEL JS-->
  <script src="<?php echo base_url('/app-assets/js/scripts/forms/checkbox-radio.js') ?>" type="text/javascript">
  </script>
  <!-- END PAGE LEVEL JS-->