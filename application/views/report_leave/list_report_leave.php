<script>
var list;
var save_method;
var selected_id;
selected_staff = '';
selected_leave= '';
selected_month = '';
selected_year = '';




function reload_table() {
    list.ajax.reload(null, false); //reload datatable ajax
}

function query_record() {

        if(selected_year == ''){
            selected_year = "<?php echo  date('Y')?>";
        }
        //change parameter for excel download
        parameter = '?selected_staff=' + selected_staff;
        parameter = parameter + '&selected_leave=' + selected_leave;
        parameter = parameter + '&selected_month=' + selected_month;
        parameter = parameter + '&selected_year=' + selected_year;
        download_link = "<?php echo base_url('report_leave/generate_xls_file') ?>" + parameter;
        download_calendar_link = "<?php echo base_url('report_leave/generate_xls_calendar') ?>" + parameter;
        $('#download_file').attr("href", download_link);
        $('#download_calendar').attr("href", download_calendar_link);
        list.ajax.reload(null, false); //reload datatable ajax
    }

$(document).ready(function() {

    $selected_staff = $('#selected_staff').selectize({
        onChange: function(dropdown) {
            selected_staff = dropdown;
            query_record();
            reload_table();
        }
    })
    $selected_leave = $('#selected_leave').selectize({
        onChange: function(dropdown) {
            selected_leave = dropdown;
            query_record();
            reload_table();
        }
    })
    $selected_month = $('#selected_month').selectize({
        onChange: function(dropdown) {
            selected_month = dropdown;
            query_record();
            reload_table();
        }
    })
    $selected_year = $('#selected_year').selectize({
        onChange: function(dropdown) {
            selected_year = dropdown;
            query_record();
            reload_table();
        }
    })
        if(selected_year == ''){
            selected_year = "<?php echo  date('Y')?>";
        }

        parameter = '?selected_staff=' + selected_staff;
        parameter = parameter + '&selected_leave=' + selected_leave;
        parameter = parameter + '&selected_month=' + selected_month;
        parameter = parameter + '&selected_year=' + selected_year;
        download_link = "<?php echo base_url('report_leave/generate_xls_file') ?>" + parameter;
        download_calendar_link = "<?php echo base_url('report_leave/generate_xls_calendar') ?>" + parameter;
        $('#download_file').attr("href", download_link);
        $('#download_calendar').attr("href", download_calendar_link);

    list = $('#list').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"order": [], //Initial no order.
        "searching": true,
        "paging": true,
        "info": false,
        "autoWidth": true,
        "scrollX": true,
        //"deferLoading": 0,
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('Report_leave/ajax_list') ?>",
            "type": "POST",
            "data": function (d) {
               d.selected_staff = $('#selected_staff').val();
               d.selected_leave = $('#selected_leave').val();
               d.selected_month = $('#selected_month').val();
               d.selected_year = $('#selected_year').val();
            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [{
            "targets": [-1], //last column
            "orderable": false, //set not orderable
            "width": "75"
        }, ],

        // "columnDefs": [
        //    { "width": "150", "targets": -1 }],
    });

});
</script>

<div class="app-content content">
    <div class="content-wrapper">

        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Leave Report</h4>
                                <br>
                                <div class="row">
                                    <?php
                                        if($user_type == 'admin'){
                                            echo '<div class="col-3">
                                            <label class="control-label">Staff:</label>';
                                        
                                            echo form_dropdown('selected_staff', $staff_list, '', 'class="form-control" id="selected_staff" ');
                                       
                                            echo '</div>';
                                        }
                                    ?>
                                    <div class="col-3">
                                        <label class="control-label">Leave Type:</label>
                                        <?php
                                        echo form_dropdown('selected_leave', $leave_type_list, '', 'class="form-control" id="selected_leave" ');
                                        ?>
                                    </div>
                                    <div class="col-2">
                                        <label class="control-label">Month:</label>
                                        <?php
                                        echo form_dropdown('selected_month', $month_list, '', 'class="form-control" id="selected_month" ');
                                        ?>
                                    </div>
                                    <div class="col-2">
                                        <label class="control-label">Year:</label>
                                        <?php
                                        echo form_dropdown('selected_year', $year_list, '', 'class="form-control" id="selected_year" ');
                                        ?>
                                    </div>

                                    <div class="col-1"></div>
                                    <div class="col-1">
                                        <div class="form-group has-error">
                                            <label class="control-label"><BR></label>
                                            <button class="btn btn-primary form-control btn-sm" onclick="query_record()">Go <i class="ft-chevrons-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered zero-configuration" id="list">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Staff Name</th>
                                                <th>Staff Email</th>
                                                <th>Leave Type</th>
                                                <th>Date From</th>
                                                <th>Date To</th>
                                                <th>Total Leave</th>
                                                <th>Supervisor</th>
                                                <th>Manager</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>

                                    </table>
                                    <div class="card-header">
                                    <div class="row">

                                        <div class="col-2">
                                            <a href="" id="download_file" class="btn btn-info form-control"> Download
                                                Report <i class="ft-download"></i></a>
                                        </div>
                                        <div class="col-2">
                                            <a href="" id="download_calendar" class="btn btn-success form-control"> Download in
                                                Calendar <i class="ft-download"></i></a>
                                        </div>
                                    </div>

                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

        </div>
    </div>
</div>

<!-- /.modal -->
<div class="modal fade" id="confirm-modal-dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-alert-triangle"></i> Delete Zone</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete selected zone? This process irreversible.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " data-dismiss="modal"
                    onclick="delete_record(0)">Yes</button>
                <button type="button" class="btn btn-secondary " data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- modal form -->
<div class="modal fade" id="modal_form">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-edit"></i> Zone Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>

            <div class="modal-body">
                <form action="#" id="form_edit" class="form-horizontal" role="form">
                    <input type="hidden" value="" name="id" />

                    <div class="row">
                        <div class="col-4">
                            <div class="form-group has-error">
                                <label class="control-label " for="">Code</label>
                                <input name="zone_code" type="text" placeholder="Zone Code" autocomplete="off"
                                    class="form-control ">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="form-group has-error">
                                <label class="control-label " for="grade_value">Zone Name</label>
                                <input name="zone_name" type="text" placeholder="Zone Name" autocomplete="off"
                                    class="form-control ">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " id="btnSave" onclick="save()">Save Record</button>
                <button type="button" class="btn btn-secondary " data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- end modal for editing -->