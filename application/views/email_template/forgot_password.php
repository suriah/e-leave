<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>e-leave Reset Password Request</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
<div>
<!-- <hr>
<p style="text-align: center; Margin-top: 0;color: black;font-size: 18px;font-weight:bold;line-height: 25px;Margin-bottom: 15px">
GEOINFO SERVICES SDN BHD</p> 
<hr> -->
   <!--<div style="font-size: 26px;font-weight: 700;letter-spacing: -0.02em;line-height: 32px;color: #41637e;font-family: sans-serif;text-align: center" align="center" id="emb-email-header"><img style="border: 0;-ms-interpolation-mode: bicubic;display: block;Margin-left: auto;Margin-right: auto;max-width: 152px" src="<?php echo base_url() ?>assets/public/images/logo.png"></div> -->
    <p style="Margin-top: 0;color: #565656;font-size: 16px;line-height: 25px;Margin-bottom: 25px">Greetings,</p> 
    <p style="Margin-top: 0;color: #565656;font-size: 16px;line-height: 25px;Margin-bottom: 25px">You have requested to reset a password from e-leave system. </p>
    <p style="Margin-top: 0;color: #565656;font-size: 16px;line-height: 25px;Margin-bottom: 25px">Below is the code to reset the password<BR>
    Reset Code : <?php echo $code['forgotten_password_code'] ?>
    </p>
    <p style="Margin-top: 0;color: #565656;font-size: 16px;line-height: 25px;Margin-bottom: 25px">Please ignore this email if you did not request any reset password from e-leave system</p>
    <br>
    <br>
    <p style="Margin-top: 0;color: #1E226D;font-size: 16px;line-height: 25px;Margin-bottom: 5px;Margin-top: 30px">Thank and regards,</p>
    <p style="font-style: italic; Margin-top: 5;color: #1E226D;font-size: 14px;line-height: 5px;">Administrator (e-leave system)</p>
    <br>

    <p style="font-style: italic;color: green;font-size: 12px;line-height: 1px;">"To be Malaysia's Leading Geospatial Solutions Integrator"</p>
    <p style="color: #1E226D;font-size: 12px;line-height: 1px;">
        GEOINFO SERVICES SDN. BHD. (334306-T)<br>
        31, Jalan Bandar 2, Taman Melawati,<br>
        53100 Kuala Lumpur, Malaysia.<br>
        Tel : +603 4106 7122 @ +603 4107 4355/4366   Ext. 104<br>
        Fax : +603 4107 5433<br>
        H/p : +6017 234 2477<br>
        http://www.geoinfo.com.my
    </p>
</div>
</body>
</html>