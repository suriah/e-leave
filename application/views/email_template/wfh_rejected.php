<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>WFH Application Notification</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
<div>
<!-- <hr>
<p style="text-align: center; Margin-top: 0;color: black;font-size: 18px;font-weight:bold;line-height: 5px;Margin-bottom: 15px">
GEOINFO SERVICES SDN BHD</p> 
<hr> -->
<p style="Margin-top: 10;color: black;font-size: 16px;font-weight:bold;line-height: 5px;Margin-bottom: 5px">
Subject : WFH Application Rejected</p> <br>
    <hr>
    <p style="Margin-top: 0;color: #565656;font-size: 16px;line-height: 25px;Margin-bottom: 25px">Dear <?php echo $staff->first_name ?> , <br>
    Your WFH request on  
    <?php echo date("d-m-Y", strtotime($staff->wfh_date_from)) ?> - <?php echo date("d-m-Y", strtotime($staff->wfh_date_to))?> 
    have been rejected by <?php echo $superior->first_name ?>
    on <?php echo date("d-m-Y H:i:s")?>.</p> <br>

    <p style="color: red;">Status: REJECTED</p>
    <p style="color: #565656;">Reasons: <?php echo $info?></p><br>
    

    <br>
    <p style="Margin-top: 0;color: #1E226D;font-size: 16px;line-height: 25px;Margin-bottom: 5px;Margin-top: 30px">Thank and regards,</p>
    <p style="font-style: italic; Margin-top: 5;color: #1E226D;font-size: 14px;line-height: 5px;">Administrator (e-leave system)</p>
    <br>
    <p style="font-style: italic;color: green;font-size: 12px;line-height: 1px;">"To be Malaysia's Leading Geospatial Solutions Integrator"</p>
    <p style="color: #1E226D;font-size: 12px;line-height: 1px;">
        GEOINFO SERVICES SDN. BHD. (334306-T)<br>
        31, Jalan Bandar 2, Taman Melawati,<br>
        53100 Kuala Lumpur, Malaysia.<br>
        Tel : +603 4106 7122 @ +603 4107 4355/4366   Ext. 104<br>
        Fax : +603 4107 5433<br>
        H/p : +6017 234 2477<br>
        http://www.geoinfo.com.my
    </p>

</div>
</body>
</html>
<style>

    .tab {
        display: inline-block;
        margin-left: 40px;
    }
</style>