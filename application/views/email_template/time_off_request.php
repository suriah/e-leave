<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Time Off Request Notification</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
<div>
<!-- <hr>
<p style="text-align: center; Margin-top: 0;color: black;font-size: 18px;font-weight:bold;line-height: 5px;Margin-bottom: 15px">
GEOINFO SERVICES SDN BHD</p> 
<hr> -->
<p style="Margin-top: 10;color: black;font-size: 16px;font-weight:bold;line-height: 5px;Margin-bottom: 5px">
Subject : Time Off Request Application</p> <br>
   <!--<div style="font-size: 26px;font-weight: 700;letter-spacing: -0.02em;line-height: 32px;color: #41637e;font-family: sans-serif;text-align: center" align="center" id="emb-email-header"><img style="border: 0;-ms-interpolation-mode: bicubic;display: block;Margin-left: auto;Margin-right: auto;max-width: 152px" src="<?php echo base_url() ?>assets/public/images/logo.png"></div> -->
   <p style="Margin-top: 0;color: #565656;font-size: 16px;line-height: 25px;Margin-bottom: 25px">Dear <?php echo $superior->first_name ?>,<br>
    You have a time off application to approve. Please click the link below to proceed.</p>

    <a href="https://project.geoinfo.com.my/e-leave/">https://project.geoinfo.com.my/e-leave/ </a>   
    <hr>
     

    <p class="tab" style="color: #565656;font-size: 16px;line-height: 2px;">Staff Name: <?php echo $staff->first_name ?><br>
    Staff Email: <?php echo $staff->email ?><br>
    Date: <?php echo date("d-m-Y", strtotime($staff->time_off_date)) ?><br>
    Time: <?php  echo date("H:i:A", strtotime($staff->time_off_time_start)) ?> - <?php echo date("H:i:A", strtotime($staff->time_off_time_end))?><br>
    Reasons: <?php echo $staff->time_off_reason ?></p>

    <br>
    <p style="Margin-top: 0;color: #1E226D;font-size: 16px;line-height: 25px;Margin-bottom: 5px;Margin-top: 30px">Thank and regards,</p>
    <p style="font-style: italic; Margin-top: 5;color: #1E226D;font-size: 14px;line-height: 5px;">Administrator (e-leave system)</p>
    <br>
    <p style="font-style: italic;color: green;font-size: 12px;line-height: 1px;">"To be Malaysia's Leading Geospatial Solutions Integrator"</p>
    <p style="color: #1E226D;font-size: 12px;line-height: 1px;">
        GEOINFO SERVICES SDN. BHD. (334306-T)<br>
        31, Jalan Bandar 2, Taman Melawati,<br>
        53100 Kuala Lumpur, Malaysia.<br>
        Tel : +603 4106 7122 @ +603 4107 4355/4366   Ext. 104<br>
        Fax : +603 4107 5433<br>
        H/p : +6017 234 2477<br>
        http://www.geoinfo.com.my
    </p>

</div>
</body>
</html>
<style>

    .tab {
        display: inline-block;
        margin-left: 40px;
    }
</style>