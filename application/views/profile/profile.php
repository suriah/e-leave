

<style>
    /*Profile Pic Start*/
.picture-container{
    position: relative;
    cursor: pointer;
    text-align: center;
}
.picture{
    width: 106px;
    height: 106px;
    background-color: #999999;
    border: 4px solid #CCCCCC;
    color: #FFFFFF;
    border-radius: 50%;
    margin: 0px auto;
    overflow: hidden;
    transition: all 0.2s;
    -webkit-transition: all 0.2s;
}
.picture:hover{
    border-color: #2ca8ff;
}
.content.ct-wizard-green .picture:hover{
    border-color: #05ae0e;
}
.content.ct-wizard-blue .picture:hover{
    border-color: #3472f7;
}
.content.ct-wizard-orange .picture:hover{
    border-color: #ff9500;
}
.content.ct-wizard-red .picture:hover{
    border-color: #ff3b30;
}
.picture input[type="file"] {
    cursor: pointer;
    display: block;
    height: 100%;
    left: 0;
    opacity: 0 !important;
    position: absolute;
    top: 0;
    width: 100%;
}

.picture-src{
    width: 100%;
    
}
/*Profile Pic End*/
    
</style>

<script>
$(document).ready(function(){
// Prepare the preview for profile picture
    $("#wizard-picture").change(function(){
        readURL(this);
    });
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
        }
        reader.readAsDataURL(input.files[0]);
    }
}    
</script>
<div class="app-content content">
    <div class="content-wrapper">

      <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="basic-form-layouts">
          <div class="row match-height">
            <div class="col-md-6">
              <div class="card">
                
                <div class="card-content collapse show">
                  <div class="card-body">
                    
                    <form class="form" enctype="multipart/form-data" action="<?= base_url('profile/update_profile') ?>" method="POST" >
                      <div class="form-body">
                        <h4 class="form-section"><i class="ft-user"></i> Personal Info</h4>
                        
                        <?php
                           if ($this->session->flashdata('status')) {
                               if ($this->session->flashdata('status') == 'false') {
                                   echo '<div class="alert alert-danger " role="alert">'
                                   . $this->session->flashdata('message') . '</div>';

                               }else{
                                   echo '<div class="alert alert-primary " role="alert">'
                                   . $this->session->flashdata('message') . '</div>';
                                   
                               } 
                           }
                       ?>
                                            

                        <div class="row">
                          <div class="col-md-4">
                            <div class="form-group">
                                <label for="projectinput1">Profile Picture</label>

                                <div class="picture-container">
                                    <div class="picture">
                                        <img src="<?= base_url('/profile_img/' . $auth_info->profile_img)  ?>" class="picture-src" id="wizardPicturePreview" title="">
                                        <input type="file" id="wizard-picture" class="" name='profile_img'>
                                    </div>
<!--                                    <h6 class="">Choose Picture</h6>-->
                                </div>
                            </div>
                          </div>
                        </div>
                        
                        
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="projectinput1">Full Name</label>
                              <input type="text" class="form-control" placeholder="Please input name"
                              name="first_name" value="<?= $auth_info->first_name ?>" required>
                            </div>
                          </div>
<!--                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="projectinput2">Last Name</label>
                              <input type="text" id="projectinput2" class="form-control" placeholder="Last Name"
                              name="lname">
                            </div>
                          </div>-->
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="projectinput3">E-mail</label>
                              <input type="text" class="form-control" placeholder="Please input email address" required
                                     name="email" value="<?= $auth_info->email ?>">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="projectinput4">Contact Number</label>
                              <input type="text"  class="form-control" placeholder="Phone" required 
                                     name="phone" value="<?= $auth_info->phone ?>">
                            </div>
                          </div>
                        </div>
                        
                      <div class="form-actions">
                        <button type="reset" class="btn btn-warning mr-1">
                          <i class="ft-x"></i> Cancel
                        </button>
                        <button type="submit" class="btn btn-primary">
                          <i class="la la-check-square-o"></i> Save
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
          
        </section>
        <!-- // Basic form layout section end -->
      </div>
    </div>
  </div>