
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/vendors/css/ui/jquery-ui.min.css') ?>">
  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/vendors.css') ?>">
  <!-- BEGIN MODERN CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/app.css' ) ?>">
  <!-- END MODERN CSS-->
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/core/menu/menu-types/vertical-menu-modern.css' ) ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/core/colors/palette-gradient.css' ) ?>">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/style.css' ) ?>"> -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/registration_style.css' ) ?>">
  <!-- END Custom CSS-->
  
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
  rel="stylesheet">
  <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"
  rel="stylesheet">
  </head>

