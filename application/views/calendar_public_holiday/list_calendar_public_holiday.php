<script>
var list;
var save_method;
var selected_id;

// ******************show dialog box *********************
function delete_record_dialog(id) {
    $('#confirm-modal-dialog').modal('show'); // show bootstrap modal
    selected_id = id;
}

function add_record() {
    //TODO => currency and type change to selectize and auto complete
    selected_id = null;

    $('.help-block').empty(); // clear error string      
    $('#modal_form').find('input:text, input:password, select, textarea').val('');
    $('#modal_form').modal('show'); // show bootstrap modal when complete loaded        
}

function edit_record(id) {
    selected_id = id;

    $('.help-block').empty(); // clear error string
    $('#modal_form').find('input:text, input:password, select, textarea').val('').end();

    //Ajax Load data from ajax
    $.ajax({
        url: "<?php echo site_url('Calendar_public_holiday/ajax_edit/') ?>" +
            id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {

            //TODO => update edit
            $('[name="public_holiday"]').val(data.public_holiday);
            $('[name="public_holiday_date"]').val(moment(data.public_holiday_date, 'YYYY-MM-DD').format('DD-MM-YYYY'));
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}



//******************end show dialog box *************************

//****************execution block*********************
function delete_record() {
    // ajax delete data to database
    $.ajax({
        url: "<?php echo site_url('Calendar_public_holiday/ajax_delete') ?>/" +
            selected_id,
        type: "POST",
        dataType: "JSON",
        success: function(data) {
            //if success reload ajax table
            $('#confirm-modal-dialog').modal('hide');
            reload_table();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Error deleting data');
        }
    });
}

function save() {
    //intiate var for add or update

    url =
        "<?php echo base_url('Calendar_public_holiday/ajax_upsert') ?>";
    //form_to_submit = "#form_edit";          
    modal_form = '#modal_form';

    var modal_form_data = $("#form_edit").serializeArray();
    modal_form_data.push({
        name: "id",
        value: selected_id
    });
    form_to_submit = modal_form_data;
    btn_save = '#btnSave';

    //change button state after save is clicked
    $(btn_save).text('saving...'); //change button text
    $(btn_save).attr('disabled', true); //set button disable

    // var url;
    // ajax adding data to database
    $.ajax({
        url: url,
        type: "POST",
        data: $(form_to_submit),
        dataType: "JSON",
        success: function(data) {

            if (data.status) //if success close modal and reload ajax table
            {
                $(modal_form).modal('hide');
                //reset form
                //$(modal_form).reset();
                reload_table();
            } else {
                for (var i = 0; i < data.inputerror.length; i++) {

                    for (var i = 0; i < data.inputerror.length; i++) {
                        //cater for date input

                        if (data.inputerror[i] == 'public_holiday_date'){
                            $('[name="' + data.inputerror[i] + '"]').parent().next().text(data.error_string[i]); //select span help-block class set text error string             
                        }else{
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string                                   
                        }
                    }           
                }
            }
            $(btn_save).text('Save Record'); //change button text
            $(btn_save).attr('disabled', false); //set button enable

        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Error adding / update data');
            $(btn_save).text('Save Record'); //change button text
            $(btn_save).attr('disabled', false); //set button enable
        }
    });
}

//******************end execution block*************************

function reload_table() {
    list.ajax.reload(null, false); //reload datatable ajax
}

$(document).ready(function() {

    var now = new Date();
        $("#public_holiday_date").datetimepicker({
            format: "DD-MM-YYYY",
            "defaultDate": new Date(now.getFullYear(), now.getMonth(), now.getDate())
        });

    list = $('#list').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"order": [], //Initial no order.
        "searching": true,
        "paging": true,
        "info": false,
        "autoWidth": true,
        // "scrollX": true,
        //"deferLoading": 0,
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('Calendar_public_holiday/ajax_list') ?>",
            "type": "POST",
            "data": function (d) {
                d.public_holiday_date = $('#input_public_holiday_date').val();
            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [{
            "targets": [-1], //last column
            "orderable": false, //set not orderable
            "width": "75"
        }, ],

        // "columnDefs": [
        //    { "width": "150", "targets": -1 }],
    });

});
</script>

<div class="app-content content">
    <div class="content-wrapper">

        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">List of Public Holiday</h4>

                                <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>

                                <?php
                                if($user_type == 'admin'){
                                
                                echo    '<div class="heading-elements">
                                    <button class="btn btn-primary " onclick="add_record()"><i
                                            class="ft-plus white"></i> Add New Public Holiday</button>
                                    <button class="btn btn-info " onclick="reload_table()"><i
                                            class="ft-refresh-cw white"></i> Refresh</button>
                                </div>';
                                }else{
                                echo   '<div class="heading-elements">
                                    <button class="btn btn-info " onclick="reload_table()"><i
                                            class="ft-refresh-cw white"></i> Refresh</button>
                                    </div>';
                                }
                                ?>

                            </div>
                            <div class="card-content collapse show">

                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered zero-configuration" id="list">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Public Holiday</th>
                                                <th>Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>


                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

        </div>
    </div>
</div>

<!-- /.modal -->
<div class="modal fade" id="confirm-modal-dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-alert-triangle"></i> Delete Public Holiday</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete selected holiday? This process irreversible.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " data-dismiss="modal"
                    onclick="delete_record(0)">Yes</button>
                <button type="button" class="btn btn-secondary " data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- modal form -->
<div class="modal fade" id="modal_form">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-edit"></i> Public Holiday Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>

            <div class="modal-body">
                <form action="#" id="form_edit" class="form-horizontal" role="form">
                    <input type="hidden" value="" name="id" />

                    <div class="row">
                        
                        <div class="col-8">
                            <div class="form-group has-error">
                                <label class="control-label " for="grade_value">Public Holiday</label>
                                <input name="public_holiday" type="text" placeholder="Name" autocomplete="off"
                                    class="form-control ">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group has-error">
                                <label class="control-label">Date</label>
                                <div class="input-group date" id="public_holiday_date" data-target-input="nearest">
                                    <input type="text" name="public_holiday_date" placeholder="dd-mm-yyyy" class="form-control datetimepicker-input" data-target="public_holiday_date" id="input_public_holiday_date"  required />
                                    <div class="input-group-append input-group-addon" data-target="public_holiday_date" data-toggle="datetimepicker">
                                        <div class="input-group-with-icon">
                                            <i class="ft ft-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>

                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " id="btnSave" onclick="save()">Save Record</button>
                <button type="button" class="btn btn-secondary " data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- end modal for editing -->