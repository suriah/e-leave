
<script>

    var list;
    var save_method;
    var selected_id;
    var dhearty_count;
    
    function revert_record_dialog(id) {
        $('#modal_form').modal('show'); // show bootstrap modal
        selected_id = id;
    }

    function save() {

        $('.help-block').empty();
        //intiate var for add or update

        url =
            "<?php echo base_url('Leave_cancellations_request/ajax_upsert') ?>";
        //form_to_submit = "#form_edit";          
        modal_form = '#modal_form';

        form_to_submit = $("#form_edit");
        $("#id").val(selected_id);
        var formData = new FormData(form_to_submit[0]);
        // form_to_submit = modal_form_data;

        btn_save = '#btnSave';
        //change button state after save is clicked
        $(btn_save).text('saving...'); //change button text
        $(btn_save).attr('disabled', true); //set button disable

        // var url;
        // ajax adding data to database
        $.ajax({
            url: url,
            type: "POST",
            contentType: false,
            processData: false,
            enctype: 'multipart/form-data',
            data: formData,
            dataType: "JSON",
            success: function(data) {

                if (data.status) //if success close modal and reload ajax table
                {
                    $(modal_form).modal('hide');
                    //reset form
                    //$(modal_form).reset();
                    reload_table();
                } else {
                    for (var i = 0; i < data.inputerror.length; i++) {

                        $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    
                    }
                    $(modal_form).effect('shake');
                }
                $(btn_save).text('Save Record'); //change button text
                $(btn_save).attr('disabled', false); //set button enable

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error adding / update data');
                $(btn_save).text('Save Record'); //change button text
                $(btn_save).attr('disabled', false); //set button enable
            }
        });
    }


    //******************end execution block*************************

    function reload_table() {
        list.ajax.reload(null, false); //reload datatable ajax
    }

    $(document).ready(function() {


        list = $('#list').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            //"order": [], //Initial no order.
            "searching": true,
            "paging": true,
            "info": false,
            "autoWidth": true,
            "scrollX": true,
            //"deferLoading": 0,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('Leave_cancellations_request/ajax_list') ?>",
                "type": "POST",
                "data": function(data) {
                    data.date_from = $('#input_date_from').val();
                    data.date_to = $('#input_date_to').val();
                
                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [{
                "targets": [-1], //last column
                "orderable": false, //set not orderable
                "width": "100"
            }, ],

            // "columnDefs": [
            //    { "width": "150", "targets": -1 }],
        });

    });
</script>

<div class="app-content content">
    <div class="content-wrapper">

        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Cancel Leave request</h4>

                                <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>

                                <div class="heading-elements">
                                    <button class="btn btn-info " onclick="reload_table()"><i class="ft-refresh-cw white"></i> Refresh</button>
                                </div>

                            </div>
                            <div class="card-content collapse show">

                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered zero-configuration" id="list">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Leave Type</th>
                                                <th>Date From</th>
                                                <th>Date To</th>
                                                <th>Total Leave</th>
                                                <th>Reasons</th>
                                                <th>Supervisor</th>
                                                <th>Manager</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

        </div>
    </div>
</div>

<!-- modal form -->
<div class="modal fade" id="modal_form">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-alert-triangle"></i> Cancel Leave Application
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>

            <div class="modal-body">
                <form action="#" id="form_edit" class="form-horizontal" role="form" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id" id="id"/>
                    <div class="row">
                        <div class="col-12"><p><b>Please state the reasons for cancellation.</b> <b class="text-danger">This process irreversible.</b> </p></div><br>
                        <div class="col-12">
                            <div class="form-group has-error">
                                <label class="control-label " for="cancellations_reason">Reasons</label>
                                <input name="cancellations_reason" type="text" placeholder="Please state reasons" autocomplete="off" class="form-control" maxlength="120">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " id="btnSave" onclick="save()">Confirm</button>
                <button type="button" class="btn btn-secondary "data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>