<script>
var list;
var save_method;
var selected_id;
selected_staff = '';
selected_wfh= '';
selected_month = '';
selected_year = '';




function reload_table() {
    list.ajax.reload(null, false); //reload datatable ajax
}

function query_record() {
        //change parameter for excel download

        if(selected_year == ''){
            selected_year = "<?php echo  date('Y')?>";
        }
        parameter = '?selected_staff=' + selected_staff;
        parameter = parameter + '&selected_month=' + selected_month;
        parameter = parameter + '&selected_year=' + selected_year;
        download_link = "<?php echo base_url('report_wfh/generate_xls_file') ?>" + parameter;
        download_calendar_link = "<?php echo base_url('report_wfh/generate_xls_calendar') ?>" + parameter;
        $('#download_file').attr("href", download_link);
        $('#download_calendar').attr("href", download_calendar_link);
        list.ajax.reload(null, false); //reload datatable ajax
    }

$(document).ready(function() {

    $selected_staff = $('#selected_staff').selectize({
        onChange: function(dropdown) {
            selected_staff = dropdown;
            query_record();
            reload_table();
        }
    })
    $selected_month = $('#selected_month').selectize({
        onChange: function(dropdown) {
            selected_month = dropdown;
            query_record();
            reload_table();
        }
    })
    $selected_year = $('#selected_year').selectize({
        onChange: function(dropdown) {
            selected_year = dropdown;
            query_record();
            reload_table();
        }
    })

        if(selected_year == ''){
            selected_year = "<?php echo  date('Y')?>";
        }


        parameter = '?selected_staff=' + selected_staff;
        parameter = parameter + '&selected_month=' + selected_month;
        parameter = parameter + '&selected_year=' + selected_year;
        download_link = "<?php echo base_url('report_wfh/generate_xls_file') ?>" + parameter;
        download_calendar_link = "<?php echo base_url('report_wfh/generate_xls_calendar') ?>" + parameter;
        $('#download_file').attr("href", download_link);
        $('#download_calendar').attr("href", download_calendar_link);

    list = $('#list').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"order": [], //Initial no order.
        "searching": true,
        "paging": true,
        "info": false,
        "autoWidth": true,
        "scrollX": true,
        //"deferLoading": 0,
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('Report_wfh/ajax_list') ?>",
            "type": "POST",
            "data": function (d) {
               d.selected_staff = $('#selected_staff').val();
               d.selected_month = $('#selected_month').val();
               d.selected_year = $('#selected_year').val();
            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [{
            "targets": [-1], //last column
            "orderable": false, //set not orderable
            "width": "75"
        }, ],

        // "columnDefs": [
        //    { "width": "150", "targets": -1 }],
    });

});
</script>

<div class="app-content content">
    <div class="content-wrapper">

        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Work From Home Report</h4>
                                <br>
                                <div class="row">
                                    <?php
                                        if($user_type == 'admin'){
                                            echo '<div class="col-3">
                                            <label class="control-label">Staff:</label>';
                                        
                                            echo form_dropdown('selected_staff', $staff_list, '', 'class="form-control" id="selected_staff" ');
                                       
                                            echo '</div>';
                                        }
                                    ?>
                                    <div class="col-2">
                                        <label class="control-label">Month:</label>
                                        <?php
                                        echo form_dropdown('selected_month', $month_list, '', 'class="form-control" id="selected_month" ');
                                        ?>
                                    </div>
                                    <div class="col-2">
                                        <label class="control-label">Year:</label>
                                        <?php
                                        echo form_dropdown('selected_year', $year_list, '', 'class="form-control" id="selected_year" ');
                                        ?>
                                    </div>

                                    <div class="col-1"></div>
                                    <div class="col-1">
                                        <div class="form-group has-error">
                                            <label class="control-label"><BR></label>
                                            <button class="btn btn-primary form-control btn-sm" onclick="query_record()">Go <i class="ft-chevrons-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered zero-configuration" id="list">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Staff Name</th>
                                                <th>Staff Email</th>
                                                <th>Date From</th>
                                                <th>Date To</th>
                                                <th>Total wfh</th>
                                                <th>Manager</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>

                                    </table>
                                    <div class="card-header">
                                    <div class="row">

                                        <div class="col-2">
                                            <a href="" id="download_file" class="btn btn-info form-control"> Download
                                                Report <i class="ft-download"></i></a>
                                        </div>
                                        <div class="col-2">
                                            <a href="" id="download_calendar" class="btn btn-success form-control"> Download in
                                                Calendar <i class="ft-download"></i></a>
                                        </div>
                                    </div>

                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

        </div>
    </div>
</div>