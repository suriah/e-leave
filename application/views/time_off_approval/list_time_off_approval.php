<script>
    var list;
    var save_method;
    var selected_id;

    // ******************show dialog box *********************
    function total_wfh() {
        date_from = $('#input_date_from').val();
        date_from = (moment(date_from,"DD-MM-YYYY").format("MM-DD-YYYY"))
        date_to = $('#input_date_to').val();
        date_to = (moment(date_to,"DD-MM-YYYY").format("MM-DD-YYYY"))
        const diffDays = (date, otherDate) => Math.ceil(Math.abs(date - otherDate) / (1000 * 60 * 60 * 24));

        // Example
        total = diffDays(new Date(date_to),new Date(date_from)); 

        $('[name="total_wfh"]').val(total+1);
    }

    function daysdifference(firstDate, secondDate){
    var startDay = new Date(firstDate);
    var endDay = new Date(secondDate);
   
    var millisBetween = startDay.getTime() - endDay.getTime();
    var days = millisBetween / (1000 * 3600 * 24);
   
    return Math.round(Math.abs(days));
}
    function delete_record_dialog(id) {
        $('#confirm-modal-dialog').modal('show'); // show bootstrap modal
        selected_id = id;
    }
    function reject_dialog() {
        $('#confirm-reject-modal-dialog').modal('show'); // show bootstrap modal
    }
    function approve_dialog() {
        $('#confirm-approve-modal-dialog').modal('show'); // show bootstrap modal
    }

    function process_record(id) {
        selected_id = id;

        $('.help-block').empty(); // clear error string
        $('#modal_form').find('input:text, input:password, select, textarea').val('').end();

        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('Time_off_approval/ajax_view_process_data/') ?>" +
                id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {

            //TODO => update edit
                $('[name="staff_name"]').val(data.staff_name);
                $('[name="staff_email"]').val(data.staff_email);
                $('[name="time_off_date"]').val(moment(data.time_off_date, 'YYYY-MM-DD').format('DD-MM-YYYY'));
                $('[name="time_off_time_start"]').val(moment(data.time_off_time_start).format('hh:mm A'));
                $('[name="time_off_time_end"]').val(moment(data.time_off_time_end).format('hh:mm A'));
                $('[name="time_off_reason"]').val(data.time_off_reason);

                var selectize = $("#manager")[0].selectize;
                selectize.setValue(data.manager_id);
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    // function process_record(id) {
    //     selected_id = id;

    //     $('.help-block').empty(); // clear error string
    //     $('#modal_form').find('input:text, input:password, select, textarea').val('').end();

    //     //Ajax Load data from ajax
    //     $.ajax({
    //         url: "<?php echo site_url('time_off_approval/ajax_view_process_data/') ?>" +
    //             id,
    //         type: "GET",
    //         dataType: "JSON",
    //         success: function(data) {

    //             //TODO => update edit
    //             $('[name="staff_name"]').val(data.staff_name);
    //             $('[name="staff_email"]').val(data.staff_email);
    //             $('[name="time_off_date"]').val(moment(data.time_off_date, 'YYYY-MM-DD').format('DD-MM-YYYY'));
    //             $('[name="time_off_time_start"]').val(moment(data.time_off_time_start).format('hh:mm A'));
    //             $('[name="time_off_time_end"]').val(moment(data.time_off_time_end).format('hh:mm A'));
    //             $('[name="time_off_reason"]').val(data.time_off_reason);

    //             var selectize = $("#manager")[0].selectize;
    //             selectize.setValue(data.manager_id);

    //             $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
    //         },
    //         error: function(jqXHR, textStatus, errorThrown) {
    //             alert('Error get data from ajax');
    //         }
    //     });
    // }

    function view_record(id) {
        selected_id = id;

        $('.help-block').empty(); // clear error string
        $('#modal_form').find('input:text, input:password, select, textarea').val('').end();

        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('time_off_approval/ajax_view/') ?>" +
                id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {

                //TODO => update edit
                $('[name="view_date_from"]').val(moment(data.wfh_date_from, 'YYYY-MM-DD').format('DD-MM-YYYY'));
                $('[name="view_date_to"]').val(moment(data.wfh_date_to, 'YYYY-MM-DD').format('DD-MM-YYYY'));
                $('[name="view_total_wfh"]').val(data.total_wfh);
                $('[name="view_wfh_reason"]').val(data.wfh_reason);
                var selectize = $("#manager")[0].selectize;
                selectize.setValue(data.manager_id);

                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }



    //******************end show dialog box *************************

    //****************execution block*********************
    function approve_application() {
        // ajax delete data to database
        var modal_form_data = $("#form_edit").serializeArray();
        modal_form_data.push({
            name: "id",
            value: selected_id
        });
        form_to_submit = modal_form_data;
        btn_save = '#btnSave';

        $.ajax({
            url: "<?php echo site_url('time_off_approval/ajax_approve_application') ?>/" +
                selected_id,
            type: "POST",
            data: $(form_to_submit),
            dataType: "JSON",
            success: function(data) {
                //if success reload ajax table
                $('#confirm-approve-modal-dialog').modal('hide');
                $(modal_form).modal('hide');
                reload_table();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error submitting data');
            }
        });
    }
    //****************execution block*********************
    function reject_application() {
        // ajax delete data to database
        var modal_form_data = $("#form_edit").serializeArray();
        modal_form_data.push({
            name: "id",
            value: selected_id
        });
        form_to_submit = modal_form_data;
        btn_save = '#btnSave';

        $.ajax({
            url: "<?php echo site_url('time_off_approval/ajax_reject_application') ?>/" +
                selected_id,
            type: "POST",
            data: $(form_to_submit),
            dataType: "JSON",
            success: function(data) {
                //if success reload ajax table
                if (data.status) //if success close modal and reload ajax table
                {
                    $('#confirm-reject-modal-dialog').modal('hide');
                    $(modal_form).modal('hide');
                    reload_table();
                } else {
                    for (var i = 0; i < data.inputerror.length; i++) {

                   
                        $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    
                    }
                    $(modal_form).effect('shake');
                }
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error submitting data');
            }
        });
    }

    //****************execution block*********************
    function delete_record() {
        // ajax delete data to database
        $.ajax({
            url: "<?php echo site_url('time_off_approval/ajax_delete') ?>/" +
                selected_id,
            type: "POST",
            dataType: "JSON",
            success: function(data) {
                //if success reload ajax table
                $('#confirm-modal-dialog').modal('hide');
                reload_table();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error deleting data');
            }
        });
    }

    function save() {
        //intiate var for add or update

        url =
            "<?php echo base_url('time_off_approval/ajax_upsert') ?>";
        //form_to_submit = "#form_edit";          
        modal_form = '#modal_form';

        var modal_form_data = $("#form_edit").serializeArray();
        modal_form_data.push({
            name: "id",
            value: selected_id
        });
        form_to_submit = modal_form_data;
        btn_save = '#btnSave';

        //change button state after save is clicked
        $(btn_save).text('saving...'); //change button text
        $(btn_save).attr('disabled', true); //set button disable

        // var url;
        // ajax adding data to database
        $.ajax({
            url: url,
            type: "POST",
            data: $(form_to_submit),
            dataType: "JSON",
            success: function(data) {

                if (data.status) //if success close modal and reload ajax table
                {
                    $(modal_form).modal('hide');
                    //reset form
                    //$(modal_form).reset();
                    reload_table();
                } else {
                    for (var i = 0; i < data.inputerror.length; i++) {

                 
                        $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    
                    }
                    $(modal_form).effect('shake');
                }
                $(btn_save).text('Save Record'); //change button text
                $(btn_save).attr('disabled', false); //set button enable

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error adding / update data');
                $(btn_save).text('Save Record'); //change button text
                $(btn_save).attr('disabled', false); //set button enable
            }
        });
    }

    //******************end execution block*************************

    function reload_table() {
        list.ajax.reload(null, false); //reload datatable ajax
    }

    $(document).ready(function() {

    $('#manager').selectize({
        maxItems: 1
    });
    var now = new Date();
        $("#time_off_date").datetimepicker({
            format: "DD-MM-YYYY",
            "defaultDate": new Date(now.getFullYear(), now.getMonth(), now.getDate()),
            icons: {
                up: "ft ft-chevron-up",
                down: "ft ft-chevron-down",
                previous: "ft ft-chevron-left",
                next: "ft ft-chevron-right",
            }
        });
        
        $("#time_off_time_start").datetimepicker({
            format: 'hh:mm A',
            "defaultDate": new Date(now.getHours(), now.getMinutes(), now.getSeconds()),
            icons: {
                up: "ft ft-chevron-up",
                down: "ft ft-chevron-down",
            }
        });
        $("#time_off_time_end").datetimepicker({
            format: 'hh:mm A',
            "defaultDate": new Date(now.getHours(), now.getMinutes(), now.getSeconds()),
            icons: {
                up: "ft ft-chevron-up",
                down: "ft ft-chevron-down",
            }
        });


        list = $('#list').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            //"order": [], //Initial no order.
            "searching": true,
            "paging": true,
            "info": false,
            "autoWidth": true,
            "scrollX": true,
            //"deferLoading": 0,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('time_off_approval/ajax_list') ?>",
                "type": "POST",
                "data": function(data) {
                    data.date_from = $('#input_date_from').val();
                    data.date_to = $('#input_date_to').val();
                    data.manager_id = $('#manager').val();
                

                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [{
                "targets": [-1], //last column
                "orderable": false, //set not orderable
                "width": "75"
            }, ],

            // "columnDefs": [
            //    { "width": "150", "targets": -1 }],
        });

    });
</script>

<div class="app-content content">
    <div class="content-wrapper">

        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Time Off Approval</h4>

                                <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>

                                <div class="heading-elements">
                                   
                                    <button class="btn btn-info " onclick="reload_table()"><i class="ft-refresh-cw white"></i> Refresh</button>
                                </div>

                            </div>
                            <div class="card-content collapse show">

                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered zero-configuration" id="list">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Staff Name</th>
                                                <th>Staff Email</th>
                                                <th>Date</th>
                                                <th>Time</th>
                                                <th>Reason</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>


                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

        </div>
    </div>
</div>


<!-- modal form -->
<div class="modal fade" id="modal_form">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-edit"></i> Time Off Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>

            <div class="modal-body">
                <form action="#" id="form_edit" class="form-horizontal" role="form">
                    <input type="hidden" value="" name="id" />
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="grade_value">Staff Name</label>
                                <input name="staff_name" type="text" placeholder="" autocomplete="off" class="form-control " readonly>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="grade_value">Staff Email</label>
                                <input name="staff_email" type="text" placeholder="" autocomplete="off" class="form-control " readonly>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group has-error ">
                                <label class="control-label">Time Start</label>
                                <div class="input-group date" id="time_off_time_start" data-target-input="nearest">
                                    <input type="text" name="time_off_time_start" placeholder="00:00:00" class="form-control datetimepicker-input" data-target="time_off_time_start" id="input_time_off_time_start"  required />
                                    <div  class="input-group-append input-group-addon " data-target="time_off_time_start" data-toggle="datetimepicker">
                                        <div class="input-group-with-icon">
                                            <i class="ft ft-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label">Time End</label>
                                <div class="input-group date" id="time_off_time_end" data-target-input="nearest">
                                    <input type="text" name="time_off_time_end" placeholder="00:00:00" class="form-control datetimepicker-input" data-target="time_off_time_end" id="input_time_off_time_end"  required />
                                    <div class="input-group-append input-group-addon" data-target="time_off_time_end" data-toggle="datetimepicker">
                                        <div class="input-group-with-icon">
                                            <i class="ft ft-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label">Date</label>
                                <div class="input-group date" id="time_off_date" data-target-input="nearest">
                                    <input type="text" name="time_off_date" placeholder="dd-mm-yyyy" class="form-control datetimepicker-input" data-target="time_off_date" id="input_time_off_date"  required />
                                    <div class="input-group-append input-group-addon" data-target="time_off_date" data-toggle="datetimepicker">
                                        <div class="input-group-with-icon">
                                            <i class="ft ft-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="status"><span id="manager_label">Manager</span></label>
                                <?php
                                echo form_dropdown('manager_id', $manager_list, '', 'id="manager" class="form-control"');
                                ?>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group has-error">
                                <label class="control-label " for="">Reasons</label>
                                <input name="time_off_reason" type="text" placeholder="Reason" autocomplete="off"
                                    class="form-control ">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success " id="btnSave" onclick="approve_dialog()">Approve</button>
                <button type="button" class="btn btn-danger " id="btnSave" onclick="reject_dialog()">Reject</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- /.modal -->
<div class="modal fade" id="confirm-reject-modal-dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-alert-triangle"></i> Reject Application
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to reject the application? This process irreversible.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " data-dismiss="modal" onclick="reject_application(0)">Confirm</button>
                <button type="button" class="btn btn-secondary " data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- /.modal -->
<div class="modal fade" id="confirm-approve-modal-dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-alert-triangle"></i> Approve Application
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to approve the application? This process irreversible.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " data-dismiss="modal" onclick="approve_application(0)">Confirm</button>
                <button type="button" class="btn btn-secondary " data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- end modal for editing -->