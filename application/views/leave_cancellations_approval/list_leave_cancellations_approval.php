<script>
    var list;
    var save_method;
    var selected_id;

    // ******************show dialog box *********************
    function total_leave() {
        date_from = $('#input_date_from').val();
        date_from = (moment(date_from,"DD-MM-YYYY").format("MM-DD-YYYY"))
        date_to = $('#input_date_to').val();
        date_to = (moment(date_to,"DD-MM-YYYY").format("MM-DD-YYYY"))
        const diffDays = (date, otherDate) => Math.ceil(Math.abs(date - otherDate) / (1000 * 60 * 60 * 24));

        // Example
        total = diffDays(new Date(date_to),new Date(date_from)); 

        $('[name="total_leave"]').val(total+1);
    }

    function daysdifference(firstDate, secondDate){
    var startDay = new Date(firstDate);
    var endDay = new Date(secondDate);
   
    var millisBetween = startDay.getTime() - endDay.getTime();
    var days = millisBetween / (1000 * 3600 * 24);
   
    return Math.round(Math.abs(days));
}
    function delete_record_dialog(id) {
        $('#confirm-modal-dialog').modal('show'); // show bootstrap modal
        selected_id = id;
    }
    function reject_dialog() {
        $('#confirm-reject-modal-dialog').modal('show'); // show bootstrap modal
    }
    function approve_dialog() {
        $('#confirm-approve-modal-dialog').modal('show'); // show bootstrap modal
    }

    function process_record(id) {
        selected_id = id;

        $('.help-block').empty(); // clear error string
        $('#modal_form').find('input:text, input:password, select, textarea').val('').end();

        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('Leave_cancellations_approval/ajax_view_process_data/') ?>" +
                id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data)

                //TODO => update edit
                $('[name="staff_name"]').val(data.staff_name);
                $('[name="staff_email"]').val(data.staff_email);
                $('[name="date_from"]').val(moment(data.leave_date_from, 'YYYY-MM-DD').format('DD-MM-YYYY'));
                $('[name="date_to"]').val(moment(data.leave_date_to, 'YYYY-MM-DD').format('DD-MM-YYYY'));
                $('[name="total_leave"]').val(data.total_leave);
                $('[name="leave_reason"]').val(data.leave_reason);
                $('[name="cancellations_reason"]').val(data.cancellations_reason);
                $('[name="cancellations_timestamp"]').val(moment(data.cancellations_timestamp, 'YYYY-MM-DD H:i:s').format('DD-MM-YYYY h:mm A'));

                if(data.supporting_document==null){
                $('#attachment_url').hide();
                }else{
                    $('[name="supporting_document"]').hide();
                    $('#attachment_url').show();
                    $('#attachment_url').show().text(data.supporting_document);
                    $('#attachment_url').attr("href","<?php echo site_url('Leave_cancellations_approval/download_file/') ?>" + selected_id);
                }

                var selectize = $("#leave_type")[0].selectize;
                selectize.setValue(data.leave_type_id);
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }




    //******************end show dialog box *************************

    //****************execution block*********************
    function approve_application() {
        // ajax delete data to database
        var modal_form_data = $("#form_edit").serializeArray();
        modal_form_data.push({
            name: "id",
            value: selected_id
        });
        form_to_submit = modal_form_data;
        btn_save = '#btnSave';

        $.ajax({
            url: "<?php echo site_url('Leave_cancellations_approval/ajax_approve_application') ?>/" +
                selected_id,
            type: "POST",
            data: $(form_to_submit),
            dataType: "JSON",
            success: function(data) {
                //if success reload ajax table
                $('#confirm-approve-modal-dialog').modal('hide');
                $(modal_form).modal('hide');
                reload_table();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error submitting data');
            }
        });
    }
    //****************execution block*********************
    function reject_application() {
        // ajax delete data to database
        var modal_form_data = $("#form_edit").serializeArray();
        modal_form_data.push({
            name: "id",
            value: selected_id
        });
        form_to_submit = modal_form_data;
        btn_save = '#btnSave';

        $.ajax({
            url: "<?php echo site_url('Leave_cancellations_approval/ajax_reject_application') ?>/" +
                selected_id,
            type: "POST",
            data: $(form_to_submit),
            dataType: "JSON",
            success: function(data) {
                //if success reload ajax table
                if (data.status) //if success close modal and reload ajax table
                {
                    $('#confirm-reject-modal-dialog').modal('hide');
                    $(modal_form).modal('hide');
                    reload_table();
                } else {
                    for (var i = 0; i < data.inputerror.length; i++) {

                   
                        $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    
                    }
                    $(modal_form).effect('shake');
                }
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error submitting data');
            }
        });
    }

    //****************execution block*********************
    function delete_record() {
        // ajax delete data to database
        $.ajax({
            url: "<?php echo site_url('Leave_cancellations_approval/ajax_delete') ?>/" +
                selected_id,
            type: "POST",
            dataType: "JSON",
            success: function(data) {
                //if success reload ajax table
                $('#confirm-modal-dialog').modal('hide');
                reload_table();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error deleting data');
            }
        });
    }

    function save() {
        console.log('masuk')
        //intiate var for add or update

        url =
            "<?php echo base_url('Leave_cancellations_approval/ajax_upsert') ?>";
        //form_to_submit = "#form_edit";          
        modal_form = '#modal_form';

        var modal_form_data = $("#form_edit").serializeArray();
        modal_form_data.push({
            name: "id",
            value: selected_id
        });
        form_to_submit = modal_form_data;
        btn_save = '#btnSave';

        //change button state after save is clicked
        $(btn_save).text('saving...'); //change button text
        $(btn_save).attr('disabled', true); //set button disable

        // var url;
        // ajax adding data to database
        $.ajax({
            url: url,
            type: "POST",
            data: $(form_to_submit),
            dataType: "JSON",
            success: function(data) {

                if (data.status) //if success close modal and reload ajax table
                {
                    $(modal_form).modal('hide');
                    //reset form
                    //$(modal_form).reset();
                    reload_table();
                } else {
                    for (var i = 0; i < data.inputerror.length; i++) {

                    if (data.inputerror[i] == 'leave_type_id' || data.inputerror[i] == 'manager_id') {
                        $('[name="' + data.inputerror[i] + '"]').parent().parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="' + data.inputerror[i] + '"]').next().next().text(data.error_string[i]); //for selectize error
                    } else {
                        $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    }
                    }
                    $(modal_form).effect('shake');
                }
                $(btn_save).text('Save Record'); //change button text
                $(btn_save).attr('disabled', false); //set button enable

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error adding / update data');
                $(btn_save).text('Save Record'); //change button text
                $(btn_save).attr('disabled', false); //set button enable
            }
        });
    }

    //******************end execution block*************************

    function reload_table() {
        list.ajax.reload(null, false); //reload datatable ajax
    }

    $(document).ready(function() {


        $('#leave_type').selectize({
            maxItems: 1
        });
        $('#view_leave_type').selectize({
            maxItems: 1
        });

        var now = new Date();
        $("#date_from").datetimepicker({
            format: "DD-MM-YYYY",
            "defaultDate": new Date(now.getFullYear(), now.getMonth(), now.getDate())
        }).on('dp.change', function(e){total_leave()});

        $("#date_to").datetimepicker({
            format: "DD-MM-YYYY",
            "defaultDate": new Date(now.getFullYear(), now.getMonth(), now.getDate())
        }).on('dp.change', function(e){total_leave()});
        var now = new Date();
        $("#view_date_from").datetimepicker({
            format: "DD-MM-YYYY",
            "defaultDate": new Date(now.getFullYear(), now.getMonth(), now.getDate())
        });

        $("#view_date_to").datetimepicker({
            format: "DD-MM-YYYY",
            "defaultDate": new Date(now.getFullYear(), now.getMonth(), now.getDate())
        });


        list = $('#list').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            //"order": [], //Initial no order.
            "searching": true,
            "paging": true,
            "info": false,
            "autoWidth": true,
            "scrollX": true,
            //"deferLoading": 0,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('Leave_cancellations_approval/ajax_list') ?>",
                "type": "POST",
                "data": function(data) {
                    data.date_from = $('#input_date_from').val();
                    data.date_to = $('#input_date_to').val();
                

                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [{
                "targets": [-1], //last column
                "orderable": false, //set not orderable
                "width": "75"
            }, ],

            // "columnDefs": [
            //    { "width": "150", "targets": -1 }],
        });

    });
</script>

<div class="app-content content">
    <div class="content-wrapper">

        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Cancellations Approval</h4>

                                <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>

                                <div class="heading-elements">
                                   
                                    <button class="btn btn-info " onclick="reload_table()"><i class="ft-refresh-cw white"></i> Refresh</button>
                                </div>

                            </div>
                            <div class="card-content collapse show">

                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered zero-configuration" id="list">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Staff Name</th>
                                                <th>Staff Email</th>
                                                <th>Leave Type</th>
                                                <th>Date From</th>
                                                <th>Date To</th>
                                                <th>Total Leave</th>
                                                <th>Cancellation Reason</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>


                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

        </div>
    </div>
</div>


<!-- modal form -->
<div class="modal fade" id="modal_form">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-edit"></i> Leave Request Details
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>

            <div class="modal-body">
                <form action="#" id="form_edit" class="form-horizontal" role="form">
                    <input type="hidden" value="" name="id" />
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="grade_value">Staff Name</label>
                                <input name="staff_name" type="text" placeholder="" autocomplete="off" class="form-control " readonly>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="grade_value">Staff Email</label>
                                <input name="staff_email" type="text" placeholder="" autocomplete="off" class="form-control " readonly>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="status">Leave Type</label>
                                <?php
                                echo form_dropdown('leave_type_id', $leave_type_list, '', 'id="leave_type" class="form-control"');
                                ?>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="grade_value">Total Leave</label>
                                <input name="total_leave" type="text" placeholder="" autocomplete="off" class="form-control " readonly>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label">Date From</label>
                                <div class="input-group date" id="date_from" data-target-input="nearest">
                                    <input type="text" name="date_from" placeholder="From" class="form-control datetimepicker-input" data-target="date_from" id="input_date_from"  required />
                                    <div class="input-group-append input-group-addon" data-target="date_from" data-toggle="datetimepicker">
                                        <div class="input-group-with-icon">
                                            <i class="ft ft-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>


                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label">Date To</label>
                                <div class="input-group date" id="date_to" data-target-input="nearest">
                                    <input type="text" name="date_to" placeholder="To" class="form-control datetimepicker-input" data-target="date_to" id="input_date_to" />
                                    <div class="input-group-append input-group-addon" data-target="date_to" data-toggle="datetimepicker">
                                        <div class="input-group-with-icon">
                                            <i class="ft ft-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="leave_reason">Reasons</label>
                                <input name="leave_reason" type="text" placeholder="Please state reasons" autocomplete="off" class="form-control" maxlength="120">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label mb-1" for=""> Supporting Document : </label><br><a id="attachment_url" href=""></a>
                                <input name="supporting_document" type="file" autocomplete="none" class="form-control ">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>
                    <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                        <span>Cancellations Details</span>
                    </h6>
                    <div class="row">
                        <div class="col-8">
                            <div class="form-group has-error">
                                <label class="control-label " for="cancellations_reason">Cancel reasons</label>
                                <input name="cancellations_reason" type="description" placeholder="" autocomplete="off" class="form-control" maxlength="120">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group has-error">
                                <label class="control-label " for="cancellations_timestamp">Request Timestamp</label>
                                <input name="cancellations_timestamp" type="description" placeholder="" autocomplete="off" class="form-control" maxlength="120">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success " id="btnSave" onclick="approve_dialog()">Approve</button>
                <button type="button" class="btn btn-danger " id="btnSave" onclick="reject_dialog()">Reject</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- /.modal -->
<div class="modal fade" id="confirm-reject-modal-dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-alert-triangle"></i> Reject Request
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to reject the request? This process irreversible.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " data-dismiss="modal" onclick="reject_application(0)">Confirm</button>
                <button type="button" class="btn btn-secondary " data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- /.modal -->
<div class="modal fade" id="confirm-approve-modal-dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-alert-triangle"></i> Approve Request
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to approve the request? This process irreversible.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " data-dismiss="modal" onclick="approve_application(0)">Confirm</button>
                <button type="button" class="btn btn-secondary " data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- end modal for editing -->