<script>
var list;
var selected_id;

// ******************show dialog box *********************
function delete_record_dialog(id) {
    $('#confirm-modal-dialog').modal('show'); // show bootstrap modal
    id_to_delete = id;
}

function add_group() {
    $('[name="id"]').val(null);
    $('.help-block').empty(); // clear error string        
    //clear name and descrption
    $('[name="name"]').val('');
    $('[name="description"]').val('');
    $('input[type=checkbox]').prop('checked',false); 

    $('#modal_form_edit').modal('show'); // show bootstrap modal when complete loaded        
}

function edit(id) {
    selected_id = id;

    $('input[type=checkbox]').prop('checked',false); 
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url: "<?php echo site_url('system_admin_manage_role/ajax_edit/') ?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('[name="id"]').val(data.id);
            $('[name="name"]').val(data.name);
            $('[name="description"]').val(data.description);
            
            for (index = 0; index < data.privileges_detail.length; index++) { 
                var_name = '#check_' + data.privileges_detail[index].privilege_id;
                permission = data.privileges_detail[index].permission == "t" ? true : false ;

                $(var_name).prop("checked", permission);
            }            

            $('#modal_form_edit').modal('show'); // show bootstrap modal when complete loaded
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function check_all_permission(){
    $status = $('#check_all').prop('checked')
    $('.switch').prop('checked', $status)
}

//******************end show dialog box *************************

//****************execution block*********************
function delete_record() {
    // ajax delete data to database
    $.ajax({
        url: "<?php echo site_url('system_admin_manage_role/ajax_delete') ?>/" + id_to_delete,
        type: "POST",
        dataType: "JSON",
        success: function(data) {
            //if success reload ajax table
            if (data.status == true){
                $('#confirm-modal-dialog').modal('hide');
                reload_table();
            }else{
                alert('Failed to delete role, please unsure some users is not belong to the group')
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Error deleting data');
        }
    });
}


function save() {

    url = "<?php echo base_url('system_admin_manage_role/ajax_update') ?>";
    //form_to_submit = "#form_edit";
    //modify data before submit            
    modal_form = '#modal_form_edit';

    var modal_form_data = $("#form_edit").serializeArray();
    btn_save = '#btnEditSave';

    //change button state after save is clicked
    $(btn_save).text('saving...'); //change button text
    $(btn_save).attr('disabled', true); //set button disable
    var url;

    // ajax adding data to database
    $.ajax({
        url: url,
        type: "POST",
        data: $(modal_form_data),
        dataType: "JSON",
        success: function(data) {

            if (data.status) //if success close modal and reload ajax table
            {
                $(modal_form).modal('hide');
                //reset form
                //$(modal_form).reset();
                reload_table();
            } else {
                for (var i = 0; i < data.inputerror.length; i++) {
                    $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string                        
                }
            }
            $(btn_save).text('Save Record'); //change button text
            $(btn_save).attr('disabled', false); //set button enable

        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Error adding / update data');
            $(btn_save).text('Save Record'); //change button text
            $(btn_save).attr('disabled', false); //set button enable
        }
    });
}



//******************end execution block*************************

function reload_table() {
    list.ajax.reload(null, false); //reload datatable ajax
}
$(document).ready(function() {

    $('#role').selectize(); //activate selectize
    $('#role_new').selectize(); //activate selectize

    list = $('#list').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"order": [], //Initial no order.
        "searching": true,
        "paging": true,
        "info": false,
        "autoWidth": true,
        // "scrollX": true,
        
        //"deferLoading": 0,
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('system_admin_manage_role/ajax_list') ?>",
            "type": "POST"
            //"data": function (d) {
            //    d.outlet_code = outlet_code;
            //}
        },

        //Set column definition initialisation properties.
        "columnDefs": [{
            "targets": [-1], //last column
            "orderable": false, //set not orderable
            "width": "100"
        }, ],
        //"columnDefs": [
        //    { "width": "150", "targets": -1 }],
    });

});
</script>

<div class="app-content content">
    <div class="content-wrapper">

        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">List Of Role</h4>

                                <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>

                                <div class="heading-elements">
                                    <button class="btn btn-primary " onclick="add_group()"><i class="ft-plus white"></i> Add New Role</button>
                                    <button class="btn btn-info " onclick="reload_table()"><i class="ft-refresh-cw white"></i> Refresh</button>
                                </div>

                            </div>
                            <div class="card-content collapse show">

                                <div class="card-body card-dashboard">
                                    <!--<p class="card-text">Senarai pekerja yang masih aktif.</p> -->
                                    <table class="table table-striped table-bordered zero-configuration" id="list">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Role Name</th>
                                                <th>Role Description</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>


                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

        </div>
    </div>
</div>




<!-- /.modal -->
<div class="modal fade" id="confirm-modal-dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-alert-triangle"></i> Alert</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure want to delete selected role ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " data-dismiss="modal" onclick="delete_record(0)">Yes</button>
                <button type="button" class="btn btn-secondary " data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- modal for editing -->
<div class="modal fade" id="modal_form_edit">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-edit"></i> Role Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>

            <div class="modal-body">
                <form action="#" id="form_edit" class="form-horizontal" role="form">
                    <input type="hidden" value="" name="id" />

                    <div class="row">
                        <div class="col-4">
                            <div class="form-group has-error">
                                <label class="control-label " for="name">Role Name</label>
                                <input name="name" type="text" placeholder="Please input role name" autocomplete="off" class="form-control ">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-5">
                            <div class="form-group has-error">
                                <label class="control-label " for="description">Description</label>
                                <input name="description" type="text" placeholder="Please input description" autocomplete="off" class="form-control ">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>

                    <HR>

                    <div class="row">
                        <div class="col-8">
                            <label class="control-label " for="name">Privilege Description</label>
                        </div>

                        <div class="col-2">
                            <label class="control-label " for="name">Permission</label>  
                            </span>
                            
                            <BR>
                        </div>

                        <div class="col-2">
                            <!-- <label class="control-label " for="name">Status</label>   -->
                                <span class="switch">
                            <input type="checkbox" value="true"  class="switch" id="check_all" onClick="check_all_permission()">
                            <label for="check_all">All</label>
                            </span>
                            
                            <BR>
                        </div>
                    </div>

                    <?php
                        //list all privileges
                        // print_r ($list_privileges);
                        
                        foreach ($list_privileges as $key => $module){
                            //add module header
                            echo "<p><strong>Module Name : {$key}</strong></p>";

                            foreach ($module as $detail){
                                //cater for false checkbox
                                echo '<input type="hidden" value="false" name="privilege_id[' .$detail['id'] . ']"/>';
    
                                echo '<div class="row form-group">';
                                echo    '<div class="col-8 ">'; 
                                echo        '<input name="" type="text" value= "' . $detail['privilege_description']   . '" '  . 'readonly class="form-control" >';
                                echo    '</div>';
    
                                echo    '<div class="col-2">';
                                echo        '<span class="switch">';
                                echo            '<input type="checkbox" value="true" name="privilege_id[' . $detail['id'] . ']" class="switch" id="check_' . $detail['id'] . '">';
                                echo            '<label for="check_' .  $detail['id'] . '"></label>';
                                echo        '</span>';
                                echo    '</div>';
                                echo '</div>';
                                
                            }

                        }


                    ?>


                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " id="btnEditSave" onclick="save()">Save Record</button>
                <button type="button" class="btn btn-secondary " data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- end modal for editing -->