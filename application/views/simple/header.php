
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Wetrack is the most powerfull financial integration system">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <title><?= $settings->app_name ?>
  </title>
  
    <?php
        if (isset($settings->app_icon_image)) {
            echo '<link rel="apple-touch-icon" href="' . base_url('app-assets/logo/' . $settings->app_icon_image ) . '">';
            echo '<link rel="shortcut icon" type="image/x-icon" href="' . base_url('app-assets/logo/' . $settings->app_icon_image ) .'">';
        }
    ?>
  
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
  rel="stylesheet">
  <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"
  rel="stylesheet">
  <!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/vendors.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/vendors/css/forms/icheck/icheck.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/vendors/css/forms/icheck/custom.css') ?>">
  <!-- END VENDOR CSS-->
  <!-- BEGIN MODERN CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/app.css') ?>">
  <!-- END MODERN CSS-->
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/core/menu/menu-types/vertical-menu-modern.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/core/colors/palette-gradient.css' ) ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/pages/login-register.css' ) ?>">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css') ?>">
  <!-- END Custom CSS-->
</head>
<body class="vertical-layout vertical-menu-modern 1-column   menu-expanded blank-page blank-page"
data-open="click" data-menu="vertical-menu-modern" data-col="1-column">