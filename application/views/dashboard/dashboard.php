<script>
  $(document).ready(function() {
    var id = document.getElementById("id").value;
    if (id != 'admin') {

      var ctx = document.getElementById("myChart").getContext('2d');
      var myChart = new Chart(ctx, {
        type: 'pie',
        // data: {"labels":["Remittance","MVNO","MM Card"],"datasets":[{"label":"My First Dataset","data":[50,30,20],"backgroundColor":["rgb(255, 99, 132)","rgb(54, 162, 235)","rgb(255, 205, 86)"]}]}
        data: <?php echo json_encode($user_dashboard_data['leave_chart']); ?>,
        // options: animation.animateScale
      });
    }
  });
</script>


<?php 
if ($user_data->name == 'admin') : ?>
  <input type="hidden" id="id" value="<?php echo $user_data->name ?>">
  <div class="app-content content" id="user-view">
    <div class="content-wrapper">
      <div class="row">
        <div class="col-xl-3 col-lg-6 col-12">
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <h3 class="info"><?php echo $admin_dashboard_data['total_staff'] ?></h3>
                    <h6>Total Staff</h6>
                  </div>
                  <div>
                    <i class="ft-globe info font-large-2 float-right"></i>
                  </div>
                </div>
                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                  <div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: <?php echo $admin_dashboard_data['total_staff'] ?>%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-12">
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <h3 class="warning"><?php echo $admin_dashboard_data['total_leave_approved_year'] ?></h3>
                    <h6>Total Application <?php echo $admin_dashboard_data['current_year'] ?></h6>
                  </div>
                  <div>
                    <i class="la la-bar-chart warning font-large-2 float-right"></i>
                  </div>
                </div>
                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                  <div class="progress-bar bg-gradient-x-warning" role="progressbar" style="width: <?php echo $admin_dashboard_data['total_leave_approved_year'] ?>%" aria-valuenow="<?php echo $admin_dashboard_data['total_leave_approved_year'] ?>" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-12">
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <h3 class="success"><?php echo $admin_dashboard_data['total_leave_approved_month'] ?></h3>
                    <h6>Total Application <?php echo $admin_dashboard_data['current_month'] ?></h6>
                  </div>
                  <div>
                    <i class="ft-trending-up success font-large-2 float-right"></i>
                  </div>
                </div>
                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                  <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: <?php echo $admin_dashboard_data['total_leave_approved_month'] ?>%" aria-valuenow="<?php echo $admin_dashboard_data['total_leave_approved_month'] ?>" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-12">
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <h3 class="danger"><?php echo $admin_dashboard_data['total_leave_approved_day']  ?></h3>
                    <h6>Total Staff Leave Today </h6>
                  </div>
                  <div>
                    <i class="la la-user-secret danger font-large-2 float-right"></i>
                  </div>
                </div>
                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                  <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 100%" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--/ leave statistic -->
      <div class="row">
        <div id="recent-transactions" class="col-12">
          <div class="card">
            <ul class="nav nav-tabs">
              <li class="nav-item ">
                <a class="nav-link active" href="#admin-day" data-toggle="tab">Day</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#admin-week" data-toggle="tab">Week</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#admin-month" data-toggle="tab">Month</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#admin-next-month" data-toggle="tab">Next Month</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#admin-all-status" data-toggle="tab">All Status</a>
              </li>
            </ul>
            <div class="tab-content clearfix">
              <div class="tab-pane active" id="admin-day">
                <div class="card-header">
                  <h4 class="card-title">Leave Details Today</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                  </div>
                </div>
                <div class="card-content">
                  <div class="table-responsive">
                    <table id="recent-orders" class="table table-hover table-lg mb-0">
                      <thead>
                        <tr>
                          <th class="border-top-0">Staff ID</th>
                          <th class="border-top-0">Staff Name</th>
                          <th class="border-top-0">Total Leave</th>
                          <th class="border-top-0">Date from</th>
                          <th class="border-top-0">Date to</th>
                          <th class="border-top-0">Status</th>

                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        foreach ($admin_dashboard_data['leave_details_day'] as $row) {
                          $date_from = date("d-m-Y", strtotime($row['leave_date_from']));
                          $date_to = date("d-m-Y", strtotime($row['leave_date_to']));
                          $today = date('d-m-Y');
                          $today = date('d-m-Y', strtotime($today));

                          if (($today >= $date_from) && ($today <= $date_to)) {
                            $class = '<TR class="success">';
                          } else {
                            $class = '<TR class="">';
                          }
                          echo $class;
                          echo '<TD>' . $row['staff_id'] . '</TD>';
                          echo '<TD>' . $row['first_name'] . '</TD>';
                          echo '<TD class="text-center">' . $row['total_leave'] . '</TD>';
                          echo '<TD>' . $date_from . '</TD>';
                          echo '<TD>' . $date_to . '</TD>';
                          echo '<TD>' . $row['leave_status'] . '</TD>';
                          echo '</TR>';
                        }
                        ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- week -->
              <div class="tab-pane" id="admin-week">

                <div class="card-header">
                  <h4 class="card-title">Leave Details This Week</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <!--                  <ul class="list-inline mb-0">
                        <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right"
                          href="invoice-summary.html" target="_blank">Invoice Summary</a></li>
                      </ul>-->
                  </div>
                </div>
                <div class="card-content">
                  <div class="table-responsive">
                    <table id="recent-orders" class="table table-hover table-lg mb-0">
                      <thead>
                        <tr>
                          <th class="border-top-0">Staff ID</th>
                          <th class="border-top-0">Staff Name</th>
                          <th class="border-top-0">Total Leave</th>
                          <th class="border-top-0">Date from</th>
                          <th class="border-top-0">Date to</th>
                          <th class="border-top-0">Status</th>

                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        foreach ($admin_dashboard_data['leave_details_week'] as $row) {
                          $date_from = date("d-m-Y", strtotime($row['leave_date_from']));
                          $date_to = date("d-m-Y", strtotime($row['leave_date_to']));
                          $today = date('d-m-Y');
                          $today = date('d-m-Y', strtotime($today));

                          if (($today >= $date_from) && ($today <= $date_to)) {
                            $class = '<TR class="success">';
                          } else {
                            $class = '<TR class="">';
                          }
                          echo $class;
                          echo '<TD>' . $row['staff_id'] . '</TD>';
                          echo '<TD>' . $row['first_name'] . '</TD>';
                          echo '<TD class="text-center">' . $row['total_leave'] . '</TD>';
                          echo '<TD>' . $date_from . '</TD>';
                          echo '<TD>' . $date_to . '</TD>';
                          echo '<TD>' . $row['leave_status'] . '</TD>';
                          echo '</TR>';
                        }
                        ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- week -->
              <!-- month -->
              <div class="tab-pane" id="admin-month">

                <div class="card-header">
                  <h4 class="card-title">Leave Details (<?php echo $admin_dashboard_data['current_month'] ?>)</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <!--                  <ul class="list-inline mb-0">
                        <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right"
                          href="invoice-summary.html" target="_blank">Invoice Summary</a></li>
                      </ul>-->
                  </div>
                </div>
                <div class="card-content">
                  <div class="table-responsive">
                    <table id="recent-orders" class="table table-hover table-lg mb-0">
                      <thead>
                        <tr>
                          <th class="border-top-0">Staff ID</th>
                          <th class="border-top-0">Staff Name</th>
                          <th class="border-top-0">Total Leave</th>
                          <th class="border-top-0">Date from</th>
                          <th class="border-top-0">Date to</th>
                          <th class="border-top-0">Status</th>

                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        foreach ($admin_dashboard_data['leave_details_month'] as $row) {
                          $date_from = date("d-m-Y", strtotime($row['leave_date_from']));
                          $date_to = date("d-m-Y", strtotime($row['leave_date_to']));
                          $today = date('d-m-Y');
                          $today = date('d-m-Y', strtotime($today));

                          if (($today >= $date_from) && ($today <= $date_to)) {
                            $class = '<TR class="success">';
                          } else {
                            $class = '<TR class="">';
                          }
                          echo $class;
                          echo '<TD>' . $row['staff_id'] . '</TD>';
                          echo '<TD>' . $row['first_name'] . '</TD>';
                          echo '<TD class="text-center">' . $row['total_leave'] . '</TD>';
                          echo '<TD>' . $date_from . '</TD>';
                          echo '<TD>' . $date_to . '</TD>';
                          echo '<TD>' . $row['leave_status'] . '</TD>';
                          echo '</TR>';
                        }
                        ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- month -->
              <!-- next  month -->
              <div class="tab-pane" id="admin-next-month">

                <div class="card-header">
                  <h4 class="card-title">Leave Details (<?php echo $admin_dashboard_data['next_month'] ?>)</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <!--                  <ul class="list-inline mb-0">
                        <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right"
                          href="invoice-summary.html" target="_blank">Invoice Summary</a></li>
                      </ul>-->
                  </div>
                </div>
                <div class="card-content">
                  <div class="table-responsive">
                    <table id="recent-orders" class="table table-hover table-lg mb-0">
                      <thead>
                        <tr>
                          <th class="border-top-0">Staff ID</th>
                          <th class="border-top-0">Staff Name</th>
                          <th class="border-top-0">Total Leave</th>
                          <th class="border-top-0">Date from</th>
                          <th class="border-top-0">Date to</th>
                          <th class="border-top-0">Status</th>

                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        foreach ($admin_dashboard_data['leave_details_next_month'] as $row) {
                          $date_from = date("d-m-Y", strtotime($row['leave_date_from']));
                          $date_to = date("d-m-Y", strtotime($row['leave_date_to']));
                          $today = date('d-m-Y');
                          $today = date('d-m-Y', strtotime($today));

                          if (($today >= $date_from) && ($today <= $date_to)) {
                            $class = '<TR class="success">';
                          } else {
                            $class = '<TR class="">';
                          }
                          echo $class;
                          echo '<TD>' . $row['staff_id'] . '</TD>';
                          echo '<TD>' . $row['first_name'] . '</TD>';
                          echo '<TD class="text-center">' . $row['total_leave'] . '</TD>';
                          echo '<TD>' . $date_from . '</TD>';
                          echo '<TD>' . $date_to . '</TD>';
                          echo '<TD>' . $row['leave_status'] . '</TD>';
                          echo '</TR>';
                        }
                        ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- next month -->
              <!-- all-status -->
              <div class="tab-pane" id="admin-all-status">

                <div class="card-header">
                  <h4 class="card-title">Leave Details For All Status (<?php echo $admin_dashboard_data['current_month'] ?>)</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <!--                  <ul class="list-inline mb-0">
                        <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right"
                          href="invoice-summary.html" target="_blank">Invoice Summary</a></li>
                      </ul>-->
                  </div>
                </div>
                <div class="card-content">
                  <div class="table-responsive">
                    <table id="recent-orders" class="table table-hover table-lg mb-0">
                      <thead>
                        <tr>
                          <th class="border-top-0">Staff ID</th>
                          <th class="border-top-0">Staff Name</th>
                          <th class="border-top-0">Total Leave</th>
                          <th class="border-top-0">Date from</th>
                          <th class="border-top-0">Date to</th>
                          <!-- <th class="border-top-0">Supervisor</th>
                          <th class="border-top-0">Manager</th> -->
                          <th class="border-top-0">Status</th>

                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        foreach ($admin_dashboard_data['leave_details_all_status'] as $row) {
                          $date_from = date("d-m-Y", strtotime($row['leave_date_from']));
                          $date_to = date("d-m-Y", strtotime($row['leave_date_to']));
                          $today = date('d-m-Y');
                          $today = date('d-m-Y', strtotime($today));

                          if (($today >= $date_from) && ($today <= $date_to)) {
                            $class = '<TR class="success">';
                          } else {
                            $class = '<TR class="">';
                          }
                          echo $class;
                          echo '<TD>' . $row['staff_id'] . '</TD>';
                          echo '<TD>' . $row['first_name'] . '</TD>';
                          echo '<TD class="text-center">' . $row['total_leave'] . '</TD>';
                          echo '<TD>' . $date_from . '</TD>';
                          echo '<TD>' . $date_to . '</TD>';
                          // echo '<TD>' . $row['supervisor'] . '</TD>';
                          // echo '<TD>' . $row['manager'] . '</TD>';
                          echo '<TD>' . $row['leave_status'] . '</TD>';
                          echo '</TR>';
                        }
                        ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- month -->
            </div>

          </div>
        </div>
      </div>
      <!-- event -->
      <div class="row">
        <div class="col-xl-12 col-lg-12 col-12">
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <h3 class="pink">Upcoming Event</h3>
                    <!-- <h6>Total Overall Leave Applied</h6> -->
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-12">

                      </div>
                      <div class="col-xl-4 col-lg-4 col-12">
                        <div class="card-content">
                          <h6><strong>Public Holiday</strong></h6>
                          <?php
                          foreach ($event_data['public_holiday'] as $row) {
                            $date = date("d/m/Y", strtotime($row['public_holiday_date']));
                            echo '<h6 style="text-align:left;"><em>
                              ' . $date . '
                              <span style="float:center;">
                                  ' . $row['public_holiday'] . '
                              </span>
                              </em></h6>';
                          }
                          ?>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-12">
                        <div class="card-content">
                          <h6><strong>D'Hearty</strong></h6>
                          <?php
                          foreach ($event_data['d_hearty'] as $row) {
                            $date = date("d/m/Y", strtotime($row['dhearty_date']));
                            echo '<h6 style="text-align:left;"><em>
                              ' . $date . '
                              <span style="float:center;">
                                  ' . $row['dhearty'] . '
                              </span>
                              </em></h6>';
                          }
                          ?>
                        </div>
                      </div>
                    </div>
                    <!-- <div class="card-content">
                    <?php
                    foreach ($event_data['public_holiday'] as $row) {
                      $date = date("d/m/Y", strtotime($row['public_holiday_date']));
                      echo '<h5 style="text-align:left;"><em>
                              ' . $date . '
                              <span style="float:center;">
                                  ' . $row['public_holiday'] . '
                              </span>
                              </em></h5>';
                    }
                    foreach ($event_data['d_hearty'] as $row) {
                      $date = date("d/m/Y", strtotime($row['dhearty_date']));
                      echo '<h5 style="text-align:left;"><em>
                              ' . $date . '
                              <span style="float:center;">
                                  ' . $row['dhearty'] . '
                              </span>
                              </em></h5>';
                    }
                    ?>
                    </div> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end event -->
    </div>
  </div>
<?php endif; ?>

<!-- user dashboard -->
<!-- **************************************************************************************************************************************** -->


<?php if ($user_data->name == 'user') : ?>
  <input type="hidden" id="id" value="<?php echo $user_data->name ?>">
  <div class="app-content content" id="user-view">
    <div class="content-wrapper">
      <div class="row">
        <div class="col-xl-3 col-lg-6 col-12">
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <h3 class="info"><?php echo $user_dashboard_data['total_leave'] ?></h3>
                    <h6>Total Leave <?php echo $user_dashboard_data['current_year'] ?></h6>
                  </div>
                  <div>
                    <i class="ft-globe info font-large-2 float-right"></i>
                  </div>
                </div>
                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                  <div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 100%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-12">
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <h3 class="warning"><?php echo $user_dashboard_data['balance_leave'] ?></h3>
                    <h6>Annual & Emergency Leave</h6>
                  </div>
                  <div>
                    <i class="la la-bar-chart warning font-large-2 float-right"></i>
                  </div>
                </div>
                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                  <div class="progress-bar bg-gradient-x-warning" role="progressbar" style="width: 100%" aria-valuenow="12" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-12">
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <h3 class="pink"><?php echo $user_dashboard_data['mc']  ?></h3>
                    <h6>Total MC</h6>
                  </div>
                  <div>
                    <i class="la la-heart-o pink font-large-2 float-right"></i>
                  </div>
                </div>
                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                  <div class="progress-bar bg-gradient-x-pink" role="progressbar" style="width: 100%" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-12">
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <h3 class="primary"><?php echo $user_dashboard_data['advance_leave'] ?></h3>
                    <h6>Total Advance</h6>
                  </div>
                  <div>
                    <i class="ft-trending-up primary font-large-2 float-right"></i>
                  </div>
                </div>
                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                  <div class="progress-bar bg-gradient-x-primary" role="progressbar" style="width: <?php echo $user_dashboard_data['advance_leave'] ?>%" aria-valuenow="<?php echo $user_dashboard_data['advance_leave'] ?>" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- chart -->
      <div class="row">
        <div class="col-xl-9 col-lg-12 col-12 h-100">
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <h3 class="info">Leave Chart</h3>
                    <h6>Total Overall Leave Applied</h6>
                    <!-- </div> -->
                    <canvas class="col-xl-9 col-lg-9 col-12" id="myChart"></canvas>
                    <!-- <i class="ft-globe info font-large-2 float-right"></i> -->
                    <!-- </div> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-12 h-100">
          <div class="card pull-up ">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <div>
                      <i class="la la-check-circle-o success font-large-2 float-left"></i>
                    </div>
                    <h6 class="success mt-1 ml-5">Application Approved</h6>
                  </div>
                  <div>
                    <div>
                      <h1 class="success"><?php echo $user_dashboard_data['leave_approved'] ?></h1>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <div>
                      <i class="la la-times-circle danger font-large-2 float-left"></i>
                    </div>
                    <h6 class="danger mt-1 ml-5">Application Rejected</h6>
                  </div>
                  <div>
                    <div>
                      <h1 class="danger"><?php echo $user_dashboard_data['leave_rejected'] ?></h1>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <div>
                      <i class="la la-trash warning font-large-2 float-left"></i>
                    </div>
                    <h6 class="warning mt-1 ml-5">Application Cancelled</h6>
                  </div>
                  <div>
                    <div>
                      <h1 class="warning"><?php echo $user_dashboard_data['leave_cancelled'] ?></h1>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <div>
                      <i class="la la-spinner primary font-large-2 float-left"></i>
                    </div>

                    <h6 class="primary mt-1 ml-5">Application Pending</h6>
                  </div>
                  <div>
                    <h1 class="primary"><?php echo $user_dashboard_data['leave_pending'] ?></h1>
                  </div>
                </div>
                <!-- <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                  <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 100%" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                </div> -->
              </div>

            </div>
          </div>
        </div>
      </div>
      <!--/ eCommerce statistic -->
      <div class="row">
        <div class="col-xl-12 col-lg-12 col-12">
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <h3 class="pink">Upcoming Event</h3>
                    <!-- <h6>Total Overall Leave Applied</h6> -->
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-12">

                      </div>
                      <div class="col-xl-4 col-lg-4 col-12">
                        <div class="card-content">
                          <h6><strong>Public Holiday</strong></h6>
                          <?php
                          foreach ($event_data['public_holiday'] as $row) {
                            $date = date("d/m/Y", strtotime($row['public_holiday_date']));
                            echo '<h6 style="text-align:left;"><em>
                              ' . $date . '
                              <span style="float:center;">
                                  ' . $row['public_holiday'] . '
                              </span>
                              </em></h6>';
                          }
                          ?>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-12">
                        <div class="card-content">
                          <h6><strong>D'Hearty</strong></h6>
                          <?php
                          foreach ($event_data['d_hearty'] as $row) {
                            $date = date("d/m/Y", strtotime($row['dhearty_date']));
                            echo '<h6 style="text-align:left;"><em>
                              ' . $date . '
                              <span style="float:center;">
                                  ' . $row['dhearty'] . '
                              </span>
                              </em></h6>';
                          }
                          ?>
                        </div>
                      </div>
                    </div>
                    <!-- <div class="card-content">
                    <?php
                    foreach ($event_data['public_holiday'] as $row) {
                      $date = date("d/m/Y", strtotime($row['public_holiday_date']));
                      echo '<h5 style="text-align:left;"><em>
                              ' . $date . '
                              <span style="float:center;">
                                  ' . $row['public_holiday'] . '
                              </span>
                              </em></h5>';
                    }
                    foreach ($event_data['d_hearty'] as $row) {
                      $date = date("d/m/Y", strtotime($row['dhearty_date']));
                      echo '<h5 style="text-align:left;"><em>
                              ' . $date . '
                              <span style="float:center;">
                                  ' . $row['dhearty'] . '
                              </span>
                              </em></h5>';
                    }
                    ?>
                    </div> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<!-- superior dashboard -->
<!-- **************************************************************************************************************************************** -->


<?php if ($user_data->name == 'superior') : ?>
  
  <input type="hidden" id="id" value="<?php echo $user_data->name?>">
  <div class="app-content content" id="superior-view">
    <div class="content-wrapper">
    <?php 
    if($superior_dashboard_data['total_approval'] != 0){
        echo  '<div class="alert alert-warning alert-dismissible fade show" role="alert">
          <strong>'.$superior_dashboard_data['total_approval'].' leave request!</strong> Please click <a href = leave_approval> here </a> to approve the application
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>';
      };
    if($superior_dashboard_data['total_cancelled'] != 0){
        echo  '<div class="alert alert-warning alert-dismissible fade show" role="alert">
          <strong>'.$superior_dashboard_data['total_cancelled'].' cancellations leave request!</strong> Please click <a href = leave_cancellations_approval> here </a> to approve the application
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>';
      };
      ?>
      <div class="row">
        <div class="col-xl-3 col-lg-6 col-12">
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <h3 class="info"><?php echo $superior_dashboard_data['total_leave'] ?></h3>
                    <h6>Total Leave <?php echo $superior_dashboard_data['current_year'] ?></h6>
                  </div>
                  <div>
                    <i class="ft-globe info font-large-2 float-right"></i>
                  </div>
                </div>
                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                  <div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 100%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-12">
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <h3 class="warning"><?php echo $superior_dashboard_data['balance_leave'] ?></h3>
                    <h6>Annual & Emergency Leave</h6>
                  </div>
                  <div>
                    <i class="la la-bar-chart warning font-large-2 float-right"></i>
                  </div>
                </div>
                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                  <div class="progress-bar bg-gradient-x-warning" role="progressbar" style="width: 100%" aria-valuenow="12" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-12">
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <h3 class="success"><?php echo $superior_dashboard_data['advance_leave'] ?></h3>
                    <h6>Total Advance</h6>
                  </div>
                  <div>
                    <i class="ft-trending-up success font-large-2 float-right"></i>
                  </div>
                </div>
                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                  <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: <?php echo $superior_dashboard_data['advance_leave'] ?>%" aria-valuenow="<?php echo $superior_dashboard_data['advance_leave'] ?>" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-12">
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <h3 class="danger"><?php echo $superior_dashboard_data['mc']  ?></h3>
                    <h6>Total MC</h6>
                  </div>
                  <div>
                    <i class="la la-heart-o danger font-large-2 float-right"></i>
                  </div>
                </div>
                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                  <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 100%" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- chart -->
      <div class="row">
        <div class="col-xl-6 col-lg-6 col-12">
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <h3 class="info">Leave Chart</h3>
                    <h6>Total Overall Leave Applied</h6>
                    <!-- </div> -->
                    <canvas style="height: 26rem;" class="col-xl-12 col-lg-12 col-12" id="myChart"></canvas>
                    <!-- <i class="ft-globe info font-large-2 float-right"></i> -->
                    <!-- </div> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-6 col-lg-6 col-12">
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">

                    <h3 class="primary">Staff Details</h3><br>
                    <!-- <h6>Total staff leave today ( <?php echo $superior_dashboard_data['staff_leave']  ?> )</h6> -->
                    <ul class="nav nav-tabs">
                      <li class="nav-item ">
                        <a class="nav-link active" href="#superior-day" data-toggle="tab">Day</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#superior-week" data-toggle="tab">Week</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#superior-month" data-toggle="tab">Month</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#superior-next-month" data-toggle="tab">Next Month</a>
                      </li>
                    </ul>
                    <div class="tab-content clearfix">
                      <!-- day -->
                      <div class="tab-pane active" id="superior-day">
                        <div class="card-content">
                          <div class="table-responsive" style="height: 23rem;">
                            <table id="recent-orders" class="table table-hover table-lg mb-0">
                              <thead>
                                <tr>
                                  <!-- <th class="border-top-0">Staff ID</th> -->
                                  <th class="border-top-0">Staff Name</th>
                                  <th class="border-top-0">Total Leave</th>
                                  <th class="border-top-0">Date</th>

                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                foreach ($superior_dashboard_data['staff_leave_details_day'] as $row) {
                                    $date_from = date("d-m-Y", strtotime($row['leave_date_from']));
                                    $date_to = date("d-m-Y", strtotime($row['leave_date_to']));
                                    $today = date('d-m-Y');
                                    $today = date('d-m-Y', strtotime($today));

                                  if (($today >= $date_from) && ($today <= $date_to)) {
                                    $class = '<TR class="warning">';
                                  } else {
                                    $class = '<TR class="">';
                                  }
                                  echo $class;
                                  echo '<TD>' . $row['first_name'] . '</TD>';
                                  echo '<TD class="text-center">' . $row['total_leave'] . '</TD>';
                                  echo '<TD>' . $date_from . ' - ' . $date_to . '</TD>';
                                  echo '</TR>';
                                }
                                ?>

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                      <!-- div day -->
                      <!-- week -->
                      <div class="tab-pane" id="superior-week">
                        <div class="card-content">
                          <div class="table-responsive" style="height: 23rem;">
                            <table id="recent-orders" class="table table-hover table-lg mb-0">
                              <thead>
                                <tr>
                                  <!-- <th class="border-top-0">Staff ID</th> -->
                                  <th class="border-top-0">Staff Name</th>
                                  <th class="border-top-0">Total Leave</th>
                                  <th class="border-top-0">Date</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                foreach ($superior_dashboard_data['staff_leave_details_week'] as $row) {
                                  $date_from = date("d-m-Y", strtotime($row['leave_date_from']));
                                  $date_to = date("d-m-Y", strtotime($row['leave_date_to']));
                                  $today = date('d-m-Y');
                                  $today = date('d-m-Y', strtotime($today));

                                  if (($today >= $date_from) && ($today <= $date_to)) {
                                    $class = '<TR class="warning">';
                                  } else {
                                    $class = '<TR class="">';
                                  }
                                  echo $class;
                                  // echo '<TD>' . $row['staff_id'] . '</TD>';
                                  echo '<TD>' . $row['first_name'] . '</TD>';
                                  echo '<TD class="text-center">' . $row['total_leave'] . '</TD>';
                                  echo '<TD>' . $date_from . ' - ' . $date_to . '</TD>';
                                  echo '</TR>';
                                }
                                ?>

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                      <!-- div week -->
                      <!-- month -->
                      <div class="tab-pane" id="superior-month">
                        <div class="card-content">
                          <div class="table-responsive" style="height: 23rem;">
                            <table id="recent-orders" class="table table-hover table-lg mb-0">
                              <thead>
                                <tr>
                                  <!-- <th class="border-top-0">Staff ID</th> -->
                                  <th class="border-top-0">Staff Name</th>
                                  <th class="border-top-0">Total Leave</th>
                                  <th class="border-top-0">Date</th>

                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                foreach ($superior_dashboard_data['staff_leave_details_month'] as $row) {
                                  $date_from = date("d-m-Y", strtotime($row['leave_date_from']));
                                  $date_to = date("d-m-Y", strtotime($row['leave_date_to']));
                                  $today = date('d-m-Y');
                                  $today = date('d-m-Y', strtotime($today));

                                  if (($today >= $date_from) && ($today <= $date_to)) {
                                    $class = '<TR class="warning">';
                                  } else {
                                    $class = '<TR class="">';
                                  }
                                  echo $class;
                                  // echo '<TD>' . $row['staff_id'] . '</TD>';
                                  echo '<TD>' . $row['first_name'] . '</TD>';
                                  echo '<TD class="text-center">' . $row['total_leave'] . '</TD>';
                                  echo '<TD>' . $date_from . ' - ' . $date_to . '</TD>';
                                  echo '</TR>';
                                }
                                ?>

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <!-- div month -->
                      <!-- month -->
                      <div class="tab-pane" id="superior-next-month">
                        <div class="card-content">
                          <div class="table-responsive" style="height: 23rem;">
                            <table id="recent-orders" class="table table-hover table-lg mb-0">
                              <thead>
                                <tr>
                                  <!-- <th class="border-top-0">Staff ID</th> -->
                                  <th class="border-top-0">Staff Name</th>
                                  <th class="border-top-0">Total Leave</th>
                                  <th class="border-top-0">Date</th>

                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                foreach ($superior_dashboard_data['staff_leave_details_next_month'] as $row) {
                                  $date_from = date("d-m-Y", strtotime($row['leave_date_from']));
                                  $date_to = date("d-m-Y", strtotime($row['leave_date_to']));
                                  $today = date('d-m-Y');
                                  $today = date('d-m-Y', strtotime($today));

                                  if (($today >= $date_from) && ($today <= $date_to)) {
                                    $class = '<TR class="warning">';
                                  } else {
                                    $class = '<TR class="">';
                                  }
                                  echo $class;
                                  // echo '<TD>' . $row['staff_id'] . '</TD>';
                                  echo '<TD>' . $row['first_name'] . '</TD>';
                                  echo '<TD class="text-center">' . $row['total_leave'] . '</TD>';
                                  echo '<TD>' . $date_from . ' - ' . $date_to . '</TD>';
                                  echo '</TR>';
                                }
                                ?>

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <!-- div month -->
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--/ event statistic -->
      <div class="row">
        <div class="col-xl-12 col-lg-12 col-12">
          <div class="card pull-up">
            <div class="card-content">
              <div class="card-body">
                <div class="media d-flex">
                  <div class="media-body text-left">
                    <h3 class="pink">Upcoming Event</h3>
                    <!-- <h6>Total Overall Leave Applied</h6> -->
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-12">

                      </div>
                      <div class="col-xl-4 col-lg-4 col-12">
                        <div class="card-content">
                          <h6><strong>Public Holiday</strong></h6>
                          <?php
                          foreach ($event_data['public_holiday'] as $row) {
                            $date = date("d/m/Y", strtotime($row['public_holiday_date']));
                            echo '<h6 style="text-align:left;"><em>
                              ' . $date . '
                              <span style="float:center;">
                                  ' . $row['public_holiday'] . '
                              </span>
                              </em></h6>';
                          }
                          ?>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-12">
                        <div class="card-content">
                          <h6><strong>D'Hearty</strong></h6>
                          <?php
                          foreach ($event_data['d_hearty'] as $row) {
                            $date = date("d/m/Y", strtotime($row['dhearty_date']));
                            echo '<h6 style="text-align:left;"><em>
                              ' . $date . '
                              <span style="float:center;">
                                  ' . $row['dhearty'] . '
                              </span>
                              </em></h6>';
                          }
                          ?>
                        </div>
                      </div>
                    </div>
                    <!-- <div class="card-content">
                    <?php
                    foreach ($event_data['public_holiday'] as $row) {
                      $date = date("d/m/Y", strtotime($row['public_holiday_date']));
                      echo '<h5 style="text-align:left;"><em>
                              ' . $date . '
                              <span style="float:center;">
                                  ' . $row['public_holiday'] . '
                              </span>
                              </em></h5>';
                    }
                    foreach ($event_data['d_hearty'] as $row) {
                      $date = date("d/m/Y", strtotime($row['dhearty_date']));
                      echo '<h5 style="text-align:left;"><em>
                              ' . $date . '
                              <span style="float:center;">
                                  ' . $row['dhearty'] . '
                              </span>
                              </em></h5>';
                    }
                    ?>
                    </div> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>