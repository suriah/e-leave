
<script>

    var list;
    var save_method;
    var selected_id;
    var dhearty_count;

    // *********************show dialog box *********************
    function total_wfh() {
        date_from = $('#input_date_from').val();
        date_from = (moment(date_from,"DD-MM-YYYY").format("MM-DD-YYYY"))
        date_to = $('#input_date_to').val();
        date_to = (moment(date_to,"DD-MM-YYYY").format("MM-DD-YYYY"))
        const diffDays = (date, otherDate) => Math.ceil(Math.abs(date - otherDate) / (1000 * 60 * 60 * 24));

        // Example
        total = diffDays(new Date(date_to),new Date(date_from)); 
        total = total + 1; // include today date

        var weekend = is_weekend(moment(date_from),moment(date_to))
        var dhearty = is_dhearty(moment(date_from),moment(date_to))
        var public = is_public_holiday(moment(date_from),moment(date_to))

        $('[name="total_wfh"]').val(total-weekend+dhearty-public);

    }
    var getDaysBetweenDates = function(startDate, endDate) {
        var now = startDate.clone(), dates = [];

        while (now.isSameOrBefore(endDate)) {
            dates.push(now.format('YYYY-MM-DD'));
            now.add(1, 'days');
        }
        return dates;
    };

    function is_weekend (date_from, date_to) {
        var count=0;
        var dates = getDaysBetweenDates(date_from,date_to)

        dates.forEach(element => {
            var day = new Date(element);
            var dt = day.getDay();
            if(dt == 0 || dt == 6){
            count++;
            }
        });
        return count;
    }

    function is_dhearty (date_from, date_to) {
        var dates = getDaysBetweenDates(date_from,date_to);
        if(dates == ''){
            dates = null;
        }
        param = '?list_date=' + dates;
        var result;
            $.ajax({
                url: "<?php echo site_url('wfh_request/ajax_check_dhearty') ?>" +
                param,
                type: "GET",
                dataType: "JSON",
                async:false,
                success: function(data) {
                    result =  data;
                },
                error: function(jqXHR, textStatus, errorThrown) {
                }
            });
        return result;
    }
    function is_public_holiday (date_from, date_to) {
        var dates = getDaysBetweenDates(date_from,date_to);
        if(dates == ''){
            dates = null;
        }
        param = '?list_date=' + dates;
        var result;
            $.ajax({
                url: "<?php echo site_url('wfh_request/ajax_check_public_holiday') ?>" +
                param,
                type: "GET",
                dataType: "JSON",
                async:false,
                success: function(data) {
                    result =  data;
                },
                error: function(jqXHR, textStatus, errorThrown) {
                }
            });
        return result;
    }

    function delete_record_dialog(id) {
        $('#confirm-modal-dialog').modal('show'); // show bootstrap modal
        selected_id = id;
    }

    function submit_record_dialog(id) {
        $('#confirm-submit-modal-dialog').modal('show'); // show bootstrap modal
        selected_id = id;
    }

    function add_record() {
        //TODO => currency and type change to selectize and auto complete
        selected_id = null;

        $('.help-block').empty(); // clear error string      
        $('#modal_form').find('input:text, input:password, select, textarea').val('');
        $('#date_from').each(function(index) {
            $(this).datetimepicker(
            'defaultDate', new Date());  
        });
        $('#date_to').each(function(index) {
            $(this).datetimepicker('defaultDate', new Date());
        });

        var selectize = $("#manager")[0].selectize;
        selectize.clear();

        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded        
    }

    function edit_record(id) {
        selected_id = id;

        $('.help-block').empty(); // clear error string
        $('#modal_form').find('input:text, input:password, select, textarea').val('').end();

        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('wfh_request/ajax_edit/') ?>" + selected_id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {

                //TODO => update edit
                $('[name="date_from"]').val(moment(data.wfh_date_from, 'YYYY-MM-DD').format('DD-MM-YYYY'));
                $('[name="date_to"]').val(moment(data.wfh_date_to, 'YYYY-MM-DD').format('DD-MM-YYYY'));
                $('[name="total_wfh"]').val(data.total_wfh);
                $('[name="wfh_reason"]').val(data.wfh_reason);

                var selectize = $("#manager")[0].selectize;
                selectize.setValue(data.manager_id);
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function view_record(id) {
        selected_id = id;

        $('.help-block').empty(); // clear error string
        $('#modal_view_form').find('input:text, input:password, select, textarea').val('').end();

        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('wfh_request/ajax_view/') ?>" +
                id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {

                
                //TODO => update edit
                $('[name="view_feedback"]').val(data.manager_feedback);
                $('[name="view_date_from"]').val(moment(data.wfh_date_from, 'YYYY-MM-DD').format('DD-MM-YYYY'));
                $('[name="view_date_to"]').val(moment(data.wfh_date_to, 'YYYY-MM-DD').format('DD-MM-YYYY'));
                $('[name="view_total_wfh"]').val(data.total_wfh);
                $('[name="view_wfh_reason"]').val(data.wfh_reason);
                $('[name="view_application_status"]').val(data.wfh_status);

                
                var selectize = $("#view_manager")[0].selectize;
                selectize.setValue(data.manager_id);
                $('#modal_view_form').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }



    //******************end show dialog box *************************

    //****************execution block*********************
    function delete_record() {
        // ajax delete data to database
        $.ajax({
            url: "<?php echo site_url('wfh_request/ajax_delete') ?>/" +
                selected_id,
            type: "POST",
            dataType: "JSON",
            success: function(data) {
                //if success reload ajax table
                $('#confirm-modal-dialog').modal('hide');
                reload_table();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error deleting data');
            }
        });
    }

    //****************execution block*********************
    function submit_record() {
        // ajax delete data to database
        $.ajax({
            url: "<?php echo site_url('wfh_request/ajax_submit') ?>/" +
                selected_id,
            type: "POST",
            dataType: "JSON",
            success: function(data) {
                //if success reload ajax table
                $('#confirm-submit-modal-dialog').modal('hide');
                reload_table();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error submitting data');
            }
        });
    }

    function save() {

        $('.help-block').empty();
        //intiate var for add or update

        url =
            "<?php echo base_url('wfh_request/ajax_upsert') ?>";
        //form_to_submit = "#form_edit";          
        modal_form = '#modal_form';

        form_to_submit = $("#form_edit");
        $("#id").val(selected_id);
        var formData = new FormData(form_to_submit[0]);
        // form_to_submit = modal_form_data;

        btn_save = '#btnSave';
        //change button state after save is clicked
        $(btn_save).text('saving...'); //change button text
        $(btn_save).attr('disabled', true); //set button disable

        // var url;
        // ajax adding data to database
        $.ajax({
            url: url,
            type: "POST",
            contentType: false,
            processData: false,
            enctype: 'multipart/form-data',
            data: formData,
            dataType: "JSON",
            success: function(data) {

                if (data.status) //if success close modal and reload ajax table
                {
                    $(modal_form).modal('hide');
                    //reset form
                    //$(modal_form).reset();
                    reload_table();
                } else {
                    for (var i = 0; i < data.inputerror.length; i++) {

                    if (data.inputerror[i] == 'manager_id') {
                        $('[name="' + data.inputerror[i] + '"]').parent().parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="' + data.inputerror[i] + '"]').next().next().text(data.error_string[i]); //for selectize error
                    } else {
                        $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    }
                    }
                    $(modal_form).effect('shake');
                }
                $(btn_save).text('Save Record'); //change button text
                $(btn_save).attr('disabled', false); //set button enable

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error adding / update data');
                $(btn_save).text('Save Record'); //change button text
                $(btn_save).attr('disabled', false); //set button enable
            }
        });
    }

    //******************end execution block*************************

    function reload_table() {
        list.ajax.reload(null, false); //reload datatable ajax
    }

    $(document).ready(function() {

        $('#manager').selectize({
            maxItems: 1
        });
        $('#view_manager').selectize({
            maxItems: 1,
        });

        var now = new Date();
        $("#view_date_from").datetimepicker({
            format: "DD-MM-YYYY",
            minDate: new Date(now.getFullYear(), now.getMonth(), now.getDate())
        });
        
        $("#view_date_to").datetimepicker({
            format: "DD-MM-YYYY",
            minDate: new Date(now.getFullYear(), now.getMonth(), now.getDate()),
        });
        
        $("#date_from").datetimepicker({
            format: "DD-MM-YYYY",
            minDate: new Date(now.getFullYear(), now.getMonth(), now.getDate()),
            // minDate: new Date(),
        }).on('dp.change', function(e){total_wfh()});

        $("#date_to").datetimepicker({
            format: "DD-MM-YYYY",
            minDate: new Date(now.getFullYear(), now.getMonth(), now.getDate())
        }).on('dp.change', function(e){total_wfh()});

        list = $('#list').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            //"order": [], //Initial no order.
            "searching": true,
            "paging": true,
            "info": false,
            "autoWidth": true,
            "scrollX": true,
            //"deferLoading": 0,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('wfh_request/ajax_list') ?>",
                "type": "POST",
                "data": function(data) {
                    data.date_from = $('#input_date_from').val();
                    data.date_to = $('#input_date_to').val();
                
                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [{
                "targets": [-1], //last column
                "orderable": false, //set not orderable
                "width": "100"
            }, ],

            // "columnDefs": [
            //    { "width": "150", "targets": -1 }],
        });

    });
</script>

<div class="app-content content">
    <div class="content-wrapper">

        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">WFH request</h4>

                                <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>

                                <div class="heading-elements">
                                    <button class="btn btn-primary " onclick="add_record()"><i class="ft-plus white"></i> Request New WFH</button>
                                    <!--<span class="dropdown">
                        <button id="btnSearchDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="true" class="btn btn-warning dropdown-toggle dropdown-menu-right "><i class="ft-download-cloud white"></i></button>
                        <span aria-labelledby="btnSearchDrop1" class="dropdown-menu mt-1 dropdown-menu-right">
                          <a href="#" class="dropdown-item"><i class="ft-upload"></i> Import</a>
                          <a href="#" class="dropdown-item"><i class="ft-download"></i> Export</a>
                          <a href="#" class="dropdown-item"><i class="ft-shuffle"></i> Find Duplicate</a>
                        </span>
                      </span> -->
                                    <button class="btn btn-info " onclick="reload_table()"><i class="ft-refresh-cw white"></i> Refresh</button>
                                </div>

                            </div>
                            <div class="card-content collapse show">

                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered zero-configuration" id="list">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Date From</th>
                                                <th>Date To</th>
                                                <th>Total Day</th>
                                                <th>Reasons</th>
                                                <th>Manager</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>


                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

        </div>
    </div>
</div>

<!-- /.modal -->
<div class="modal fade" id="confirm-modal-dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-alert-triangle"></i> Delete WFH
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete selected WFH request? This process irreversible.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " data-dismiss="modal" onclick="delete_record(0)">Yes</button>
                <button type="button" class="btn btn-secondary " data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- /.modal -->
<div class="modal fade" id="confirm-submit-modal-dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-alert-triangle"></i> Submit Request WFH Application
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to submit the WFH request application? This process irreversible.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " data-dismiss="modal" onclick="submit_record(0)">Submit</button>
                <button type="button" class="btn btn-secondary " data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- modal form -->
<div class="modal fade" id="modal_form">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-edit"></i> WFH Request Details
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>

            <div class="modal-body">
                <form action="#" id="form_edit" class="form-horizontal" role="form" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id" id="id"/>
                    <input type="hidden" value="" name="total_dhearty" id="total_dhearty"/>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label">Date From</label>
                                <div class="input-group date" id="date_from" data-target-input="nearest">
                                    <input type="text" name="date_from" placeholder="From" class="form-control datetimepicker-input" data-target="date_from" id="input_date_from"  required />
                                    <div class="input-group-append input-group-addon" data-target="date_from" data-toggle="datetimepicker">
                                        <div class="input-group-with-icon">
                                            <i class="ft ft-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>


                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label">Date To</label>
                                <div class="input-group date" id="date_to" data-target-input="nearest">
                                    <input type="text" name="date_to" placeholder="To" class="form-control datetimepicker-input" data-target="date_to" id="input_date_to" />
                                    <div class="input-group-append input-group-addon" data-target="date_to" data-toggle="datetimepicker">
                                        <div class="input-group-with-icon">
                                            <i class="ft ft-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="status"><span id="manager_label">Manager</span></label>
                                <?php
                                echo form_dropdown('manager_id', $manager_list, '', 'id="manager" class="form-control"');
                                ?>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="grade_value">Total WFH</label>
                                <input name="total_wfh" type="text" placeholder="" autocomplete="off" class="form-control " readonly>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        
                        <div class="col-12">
                            <div class="form-group has-error">
                                <label class="control-label " for="wfh_reason">Reasons</label>
                                <input name="wfh_reason" type="text" placeholder="Please state reasons" autocomplete="off" class="form-control" maxlength="120">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " id="btnSave" onclick="save()">Save Record</button>
                <button type="button" class="btn btn-secondary "data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- modal form -->
<div class="modal fade" id="modal_view_form">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-edit"></i> WFH Request Details
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>

            <div class="modal-body">
                <form action="#" id="form_view" class="form-horizontal" role="form">
                    <input type="hidden" value="" name="id" />
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label">Date From</label>
                                <div class="input-group date" id="view_date_from" data-target-input="nearest">
                                    <input type="text" name="view_date_from" placeholder="From" class="form-control datetimepicker-input" data-target="view_date_from" id="input_view_date_from"  readonly />
                                    <div class="input-group-append input-group-addon" data-target="view_date_from" data-toggle="datetimepicker">
                                        <div class="input-group-with-icon">
                                            <i class="ft ft-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>


                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label">Date To</label>
                                <div class="input-group date" id="view_date_to" data-target-input="nearest">
                                    <input type="text" name="view_date_to" placeholder="To" class="form-control datetimepicker-input" data-target="view_date_to" id="input_view_date_to" readonly/>
                                    <div class="input-group-append input-group-addon" data-target="view_date_to" data-toggle="datetimepicker">
                                        <div class="input-group-with-icon">
                                            <i class="ft ft-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="status"><span id="manager_view_label">Manager</span></label>
                                <?php
                                echo form_dropdown('manager_id', $manager_list, '', 'id="view_manager" class="form-control" ');
                                ?>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="grade_value">Total WFH</label>
                                <input name="view_total_wfh" type="text" placeholder="" autocomplete="off" class="form-control " readonly>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group has-error">
                                <label class="control-label " for="wfh_reason">Reasons</label>
                                <input name="view_wfh_reason" type="text" placeholder="Please state reasons" autocomplete="off" class="form-control" maxlength="120" readonly>
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>
                    <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                        <span>Supervisor/Manager Feedback</span>
                    </h6>
                    <div id="feedback_form" class="row">
                        <div class="col-4">
                            <div class="form-group has-error">
                                <label class="control-label " for="view_application_status">Application Status</label>
                                <input name="view_application_status" type="description" placeholder="Please state reasons" autocomplete="off" class="form-control" maxlength="120">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div id="feedback_form" class="col-8">
                            <div class="form-group has-error">
                                <label class="control-label " for="view_feedback">Feedback</label>
                                <input name="view_feedback" type="description" placeholder="Please state reasons" autocomplete="off" class="form-control" maxlength="120">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary "data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- end modal for editing -->