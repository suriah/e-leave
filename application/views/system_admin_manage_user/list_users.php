<script>
    var list;
    var save_method;
    var selected_id;

    function calculate_total_leave() {
        total_leave = parseInt($('[name="staff_annual_leave"]').val()) + parseInt($('[name="staff_emergency_leave"]').val()) + parseInt($('[name="staff_forwarded_leave"]').val());
        $('[name="staff_total_leave"]').val(total_leave)
        total_leave_new = parseInt($('[name="staff_annual_leave_new"]').val()) + parseInt($('[name="staff_emergency_leave_new"]').val()) + parseInt($('[name="staff_forwarded_leave_new"]').val());
        $('[name="staff_total_leave_new"]').val(total_leave_new)
    }
    // ******************show dialog box *********************
    function delete_record_dialog(id) {
        $('#confirm-modal-dialog').modal('show'); // show bootstrap modal
        id_to_delete = id;
    }

    function nav_tab(id) {
        btn_save = '#btnSave';
        btn_next = '#btnNext';
        if (id == 'staff') {
            $(btn_save).hide();
            $(btn_next).show();
        } else {
            $(btn_save).show();
            $(btn_next).hide();
        }
    }

    function nav_tab_edit(id) {
        btn_save = '#btnEditSave';
        btn_next = '#btnEditNext';
        if (id == 'staff') {
            $(btn_save).hide();
            $(btn_next).show();
        } else {
            $(btn_save).show();
            $(btn_next).hide();
        }
    }

    function next() {
        $('#nav a[href="#add_leave_details"]').tab('show')
        btn_save = '#btnSave';
        btn_next = '#btnNext';
        $(btn_next).hide();
        $(btn_save).show();
    }

    function next_edit() {
        $('#nav_edit a[href="#leave_details"]').tab('show')
        btn_save = '#btnEditSave';
        btn_next = '#btnEditNext';
        $(btn_next).hide();
        $(btn_save).show();
    }

    function add_user() {
        save_method = 'add';
        $('.help-block').empty(); // clear error string        
        $('#modal_form_add').find('input:text,input:password, select, textarea').val('');
        $('#modal_form_add').find(':input[type="number"]').val(0);

        var id = $('.tab-content .active').attr('id');
        if (id == 'staff_details') {
            $('#btnSave').hide();
        }

        var selectize = $("#role_new")[0].selectize;
        selectize.clear();
        var selectize = $("#employee_status_new")[0].selectize;
        selectize.clear();
        var selectize = $("#department_new")[0].selectize;
        selectize.clear();
        var selectize = $("#position_new")[0].selectize;
        selectize.clear();
        var selectize = $("#level_new")[0].selectize;
        selectize.clear();

        $('#modal_form_add').modal('show'); // show bootstrap modal when complete loaded        
    }

    function edit(id) {
        save_method = 'update';
        selected_id = id;

        $('.help-block').empty(); // clear error string
        $('#modal_form_edit').find('input:text, input:password, select, textarea').val('');


        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('system_admin_manage_user/ajax_edit/') ?>" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data);

                //$('[name="id"]').val(data.id);
                $('[name="email"]').val(data.user.email);
                $('[name="first_name"]').val(data.user.first_name);
                $('[name="phone"]').val(data.user.phone);
                $('[name="active"]').val(data.user.active);
                $('[name="staff_id"]').val(data.user.staff_id);
                $('[name="staff_annual_leave"]').val(data.leave.staff_annual_leave);
                $('[name="staff_emergency_leave"]').val(data.leave.staff_emergency_leave);
                $('[name="staff_mc"]').val(data.leave.staff_mc);
                $('[name="staff_forwarded_leave"]').val(data.leave.staff_forwarded_leave);
                $('[name="staff_maternity_leave"]').val(data.leave.staff_maternity_leave);
                $('[name="staff_total_leave"]').val(data.leave.staff_total_leave);
                $('[name="staff_balance_leave"]').val(data.leave.staff_balance_leave);

                var selectize = $("#employee_status")[0].selectize;
                selectize.setValue(data.user.staff_employee_status_id);

                var selectize = $("#position")[0].selectize;
                selectize.setValue(data.user.staff_position_id);

                var selectize = $("#department")[0].selectize;
                selectize.setValue(data.user.staff_department_id);

                var selectize = $("#level")[0].selectize;
                selectize.setValue(data.user.staff_level_id);

                var selectize = $("#role")[0].selectize;
                selectize.clear();
                var $temp = data.role;
                $user_role = $temp.split(',');
                selectize.setValue($user_role);

                $('#modal_form_edit').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    //******************end show dialog box *************************

    //****************execution block*********************
    function delete_record() {
        // ajax delete data to database
        $.ajax({
            url: "<?php echo site_url('system_admin_manage_user/ajax_delete') ?>/" + id_to_delete,
            type: "POST",
            dataType: "JSON",
            success: function(data) {
                //if success reload ajax table
                $('#confirm-modal-dialog').modal('hide');
                reload_table();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error deleting data');
            }
        });
    }


    function save() {

        if (save_method === 'add') {
            url = "<?php echo base_url('system_admin_manage_user/ajax_add') ?>";

            var modal_form_data = $("#form_add").serializeArray();

            form_to_submit = modal_form_data;
            modal_form = '#modal_form_add';
            btn_save = '#btnSave';

        } else {
            url = "<?php echo base_url('system_admin_manage_user/ajax_update') ?>";
            //form_to_submit = "#form_edit";
            //modify data before submit            
            modal_form = '#modal_form_edit';

            var modal_form_data = $("#form_edit").serializeArray();
            modal_form_data.push({
                name: "id",
                value: selected_id
            });
            form_to_submit = modal_form_data;
            btn_save = '#btnEditSave';
        }
        //change button state after save is clicked
        $(btn_save).text('saving...'); //change button text
        $(btn_save).attr('disabled', true); //set button disable
        var url;
        //var form_to_submit;

        // ajax adding data to database
        $.ajax({
            url: url,
            type: "POST",
            data: $(form_to_submit),
            dataType: "JSON",
            success: function(data) {

                if (data.status) //if success close modal and reload ajax table
                {
                    $(modal_form).modal('hide');
                    //reset form
                    //$(modal_form).reset();
                    reload_table();
                } else {
                    for (var i = 0; i < data.inputerror.length; i++) {

                        if (data.inputerror[i] == 'role[]' || data.inputerror[i] == 'staff_employee_status_id' || data.inputerror[i] == 'staff_department_id' || data.inputerror[i] == 'staff_level_id' || data.inputerror[i] == 'staff_position_id') {
                            $('[name="' + data.inputerror[i] + '"]').parent().parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                            $('[name="' + data.inputerror[i] + '"]').next().next().text(data.error_string[i]); //for selectize error
                        } else {
                            $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                        }
                    }
                    $(modal_form).effect('shake');
                }
                $(btn_save).text('Save Record'); //change button text
                $(btn_save).attr('disabled', false); //set button enable

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error adding / update data');
                $(btn_save).text('Save Record'); //change button text
                $(btn_save).attr('disabled', false); //set button enable
            }
        });
    }

    //******************end execution block*************************

    function reload_table() {
        list.ajax.reload(null, false); //reload datatable ajax
    }

    $(document).ready(function() {

        $('#role').selectize({
            maxItems: 1
        });
        $('#active_status').selectize({});

        //activate selectize for new user forms
        $('#role_new').selectize({
            maxItems: 1
        });
        $('#employee_status').selectize({
            maxItems: 1
        });
        $('#department').selectize({
            maxItems: 1
        });
        $('#position').selectize({
            maxItems: 1
        });
        $('#level').selectize({
            maxItems: 1
        });
        $('#employee_status_new').selectize({
            maxItems: 1
        });
        $('#department_new').selectize({
            maxItems: 1
        });
        $('#position_new').selectize({
            maxItems: 1
        });
        $('#level_new').selectize({
            maxItems: 1
        });

        download_link = "<?php echo base_url('system_admin_manage_user/generate_xls_file') ?>";
        $('#download_file').attr("href", download_link);

        list = $('#list').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            //"order": [], //Initial no order.
            "searching": true,
            "paging": true,
            "info": false,
            "autoWidth": true,
            // "scrollX": true,
            // "autoWidth": true,
            //"deferLoading": 0,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('system_admin_manage_user/ajax_list') ?>",
                "type": "POST"
                //"data": function (d) {
                //    d.outlet_code = outlet_code;
                //}
            },

            //Set column definition initialisation properties.
            "columnDefs": [{
                "targets": [-1], //last column
                "orderable": false, //set not orderable
                "width": "100"
            }, ],
            //"columnDefs": [
            //    { "width": "150", "targets": -1 }],
        });

    });
</script>

<div class="app-content content">
    <div class="content-wrapper">

        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">List of Users</h4>

                                <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>

                                <div class="heading-elements">
                                    <button class="btn btn-primary" onclick="add_user()"><i class="ft-plus white"></i> Add New Users</button>
                                    <button class="btn btn-info" onclick="reload_table()"><i class="ft-refresh-cw white"></i> Refresh</button>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered zero-configuration" id="list">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Email</th>
                                                <th>Name</th>
                                                <th>Contact</th>
                                                <th>Active</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <div class="card-header">
                                    <div class="row">

                                        <div class="col-2">
                                            <a href="" id="download_file" class="btn btn-info form-control"> Download
                                                Report <i class="ft-download"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->
        </div>
    </div>
</div>




<!-- /.modal -->
<div class="modal fade" id="confirm-modal-dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-alert-triangle"></i> Alert
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure want to delete selected user ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="delete_record(0)">Yes</button>
                <button type="button" class="btn btn-secondary " data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->


<!-- modal for editing -->
<div class="modal fade" id="modal_form_edit">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-edit"></i> Edit Users Details
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>

            <div class="modal-body">
                <form action="#" id="form_edit" class="form-horizontal" role="form">
                    <input type="hidden" value="" name="id" />

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group has-error">
                                <label class="control-label " for="first_name">Full Name</label>
                                <input name="first_name" type="text" placeholder="Please input name" autocomplete="off" class="form-control ">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="email">Email</label>
                                <input name="email" type="text" placeholder="Please input email address" autocomplete="off" class="form-control ">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="phone">Phone No</label>
                                <input name="phone" type="text" placeholder="Please input telephone number" autocomplete="off" class="form-control">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="status">User's Role</label>


                                <?php
                                echo form_multiselect('role[]', $ion_auth_groups, '', 'id="role" class="form_control "');
                                ?>

                                <span class="has-error"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="status">Active Status</label>
                                <select name="active" class="form-control" id='active_status'>
                                    <option value='1'>Yes</option>
                                    <option value='0'>No</option>
                                </select>
                                <span class="has-error"></span>
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs" id="nav_edit">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#staff_details">Staff Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#leave_details">Leave Details</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane container active" id="staff_details">
                            <BR>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="staff_id">Staff Id</label>
                                        <input name="staff_id" type="text" placeholder="Staff Id" autocomplete="off" class="form-control">
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="status">Employee Type</label>
                                        <?php
                                        echo form_dropdown('staff_employee_status_id', $employee_status_list, '', 'id="employee_status" class="form-control"');
                                        ?>
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="status">Position</label>
                                        <?php
                                        echo form_dropdown('staff_position_id', $position_list, '', 'id="position" class="form-control"');
                                        ?>
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="status">Level</label>
                                        <?php
                                        echo form_dropdown('staff_level_id', $level_list, '', 'id="level" class="form-control"');
                                        ?>
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="status">Department</label>
                                        <?php
                                        echo form_dropdown('staff_department_id', $department_list, '', 'id="department" class="form-control"');
                                        ?>
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane container fade" id="leave_details">
                            <BR>
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group has-error">
                                        <label class="control-label text-center" for="annual_leave">Annual Leave</label>
                                        <input name="staff_annual_leave" type="number" value="0" placeholder="Please input total annual leave" autocomplete="off" class="form-control" onchange="calculate_total_leave()">
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="emergency_leave">+ Emergency Leave</label>
                                        <input name="staff_emergency_leave" type="number" value="0" placeholder="Please input total emergency leave" autocomplete="off" class="form-control" onchange="calculate_total_leave()">
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="balance_forwarded_leave">+ Balance Forward Leave</label>
                                        <input name="staff_forwarded_leave" type="number" value="0" placeholder="Please input total balance leave" autocomplete="off" class="form-control" onchange="calculate_total_leave()">
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="total">= Total Leave</label>
                                        <input name="staff_total_leave" type="number" value="0" autocomplete="off" class="form-control">
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-9">
                                </div>
                                <div class="col-3">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="mc">Total MC</label>
                                        <input name="staff_mc" type="number" value="0" placeholder="Please input total mc" autocomplete="off" class="form-control">
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-9">
                                </div>
                                <div class="col-3">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="mc">Total Maternity Leave</label>
                                        <input name="staff_maternity_leave" type="number" value="0" placeholder="Please input total mc" autocomplete="off" class="form-control">
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-9">
                                </div>
                                <div class="col-3">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="mc">Current Balance Leave</label>
                                        <input name="staff_balance_leave" type="number" value="0" placeholder="" autocomplete="off" class="form-control">
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-9">
                                    <label class="control-label text-warning" for="">** Please do update the current balance if any changes on total leave</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " id="btnEditSave" onclick="save()">Save Record</button>
                <button type="button" class="btn btn-secondary " data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- end modal for editing -->


<!-- modal for new users -->
<div class="modal fade" id="modal_form_add">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">
                    <i class="ft ft-user-plus"></i> Add New User
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ft ft-x white"></i>
                </button>
            </div>

            <div class="modal-body">
                <form action="#" id="form_add" class="form-horizontal" role="form">

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group has-error">
                                <label class="control-label " for="first_name">Full Name</label>
                                <input name="first_name" type="text" placeholder="Please input name" autocomplete="off" class="form-control ">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="email">Email</label>
                                <input name="email" type="text" placeholder="Please input email address" autocomplete="off" class="form-control ">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="phone">Phone No</label>
                                <input name="phone" type="text" placeholder="Please input telephone number" autocomplete="off" class="form-control">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="password">Password</label>
                                <input name="password" type="password" placeholder="Please input password" autocomplete="off" class="form-control ">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group has-error">
                                <label class="control-label " for="confirm_password">Confirmation Password</label>
                                <input name="confirm_password" type="password" placeholder="Please input name" autocomplete="off" class="form-control ">
                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group has-error">
                                <label class="control-label " for="status">User's Role</label>

                                <?php
                                echo form_multiselect('role[]', $ion_auth_groups, '', 'id="role_new" class="form-control"');
                                ?>

                                <span class="has-error help-block text-danger"></span>
                            </div>
                        </div>

                    </div>

                    <ul class="nav nav-tabs" id="nav">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" onclick="nav_tab('staff')" href="#add_staff_details">Staff Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" onclick="nav_tab('leave')" href="#add_leave_details">Leave Details</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane container active" id="add_staff_details">
                            <BR>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="staff_id">Staff Id</label>
                                        <input name="staff_id" type="text" placeholder="Staff Id" autocomplete="off" class="form-control">
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="status">Employee Type</label>
                                        <?php
                                        echo form_dropdown('staff_employee_status_id', $employee_status_list, '', 'id="employee_status_new" class="form-control"');
                                        ?>
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="status">Position</label>
                                        <?php
                                        echo form_dropdown('staff_position_id', $position_list, '', 'id="position_new" class="form-control"');
                                        ?>
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="status">Level</label>
                                        <?php
                                        echo form_dropdown('staff_level_id', $level_list, '', 'id="level_new" class="form-control"');
                                        ?>
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="status">Department</label>
                                        <?php
                                        echo form_dropdown('staff_department_id', $department_list, '', 'id="department_new" class="form-control"');
                                        ?>
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane container fade" id="add_leave_details">
                            <BR>
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group has-error">
                                        <label class="control-label text-center" for="annual_leave">Annual Leave</label>
                                        <input name="staff_annual_leave_new" type="number" value="0" placeholder="Please input total annual leave" autocomplete="off" class="form-control" onchange="calculate_total_leave()">
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="emergency_leave">+ Emergency Leave</label>
                                        <input name="staff_emergency_leave_new" type="number" value="0" placeholder="Please input total emergency leave" autocomplete="off" class="form-control" onkeyup="calculate_total_leave()">
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="balance_forwarded_leave">+ Balance Forward Leave</label>
                                        <input name="staff_forwarded_leave_new" type="number" value="0" placeholder="Please input total balance leave" autocomplete="off" class="form-control" onkeyup="calculate_total_leave()">
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="total">= Total Leave</label>
                                        <input name="staff_total_leave_new" type="number" value="0" autocomplete="off" class="form-control">
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-9">
                                </div>
                                <div class="col-3">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="mc">Total MC</label>
                                        <input name="staff_mc_new" type="number" value="0" placeholder="Please input total mc" autocomplete="off" class="form-control">
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-9">
                                </div>
                                <div class="col-3">
                                    <div class="form-group has-error">
                                        <label class="control-label " for="mc">Total Maternity Leave</label>
                                        <input name="staff_maternity_leave_new" type="number" value="0" placeholder="Please input total mc" autocomplete="off" class="form-control">
                                        <span class="has-error help-block text-danger"></span>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>


                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-warning " id="btnNext" onclick="next()">Next</button>
                <button type="button" class="btn btn-primary " id="btnSave" onclick="save()">Save Record</button>
                <button type="button" class="btn btn-secondary " data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
<!-- end modal for add user -->