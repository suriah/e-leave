<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Telegram
{
    
    

    var $api_token = '';
    public function __construct()
    {
        //load config file
        $this->CI = &get_instance();
        $this->CI->load->config('telegram');
        $this->api_token = $this->CI->config->item('api_token');
    }

    public function send_message($chat_id, $message)
    {
        // https://api.telegram.org/bot1352084363:AAGHvLuVPT8lg7UuDXdeGZnp624EEO79eqI/sendMessage?chat_id=-1001418447831&text=hello

        // $apiToken = "1352084363:AAGHvLuVPT8lg7UuDXdeGZnp624EEO79eqI";

        // $data = [
        //     'chat_id' => '-1001418447831',
        //     'text'    => $message,
        // ];
        $data = [
            'chat_id' => $chat_id,
            'text'    => $message,
        ];

        $response = file_get_contents("https://api.telegram.org/bot$this->api_token/sendMessage?" . http_build_query($data));
        return $response;
    }

}
