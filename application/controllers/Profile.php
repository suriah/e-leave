<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile extends MY_Controller {

    public function __construct() {
        parent::__construct();
        
    }

    public function index() {
        $this->load->view('standard/header_open', $this->data);
        $this->load->view('dependency/style/datatable');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('profile/profile');
        $this->load->view('standard/footer_open');
        $this->load->view('standard/footer_close');
    }

    public function update_profile() {
        //update setting information
        $form_var = $this->input->post();
        $id = $this->data['auth_info']->id;

        $data = array(
            'first_name' => $form_var['first_name'],
            'email' => $form_var['email'],
            'phone' => $form_var['phone']
        );
        
        //upload image to server
        $config['upload_path'] = FCPATH . 'profile_img/';
        $config['allowed_types'] = '*';
        $config['max_size'] = '1024';
        $config['overwrite'] = true;
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);

        if (isset($_FILES['profile_img']) && is_uploaded_file($_FILES['profile_img']['tmp_name'])) {
            if ($this->upload->do_upload('profile_img')) {
                $profile_img = $this->upload->data();
                //upload image successful, so delete old image
                //echo FCPATH . 'profile_img/' . $this->data['auth_info']->profile_img;
                unlink(FCPATH . 'profile_img/' . $this->data['auth_info']->profile_img); 
                $data['profile_img'] = $profile_img['file_name'];
                //exit();
            } else {
                $this->session->set_flashdata('status', 'failed');
                $this->session->set_flashdata('message', 'Failed to upload profile image');                
                redirect(base_url('profile'));
            }
        }
        
        
        $result = $this->ion_auth->update($id, $data);
        
        
        if ($result == true){
            $this->session->set_flashdata('status', 'true');
            $this->session->set_flashdata('message', 'Profile update successful');
            
        }else{
            $this->session->set_flashdata('status', 'failed');
            $this->session->set_flashdata('message', 'Failed to update profile');
            
        }
        redirect(base_url('profile'));
    }

}
