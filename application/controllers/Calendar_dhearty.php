<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Calendar_dhearty extends MY_Controller
{
    private $current_model;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Calendar_dhearty_model');
        $this->current_model = $this->Calendar_dhearty_model;
        $this->current_model->auth_info = $this->data['auth_info'];
    }

    public function index()
    {
        if ($this->ion_auth->is_admin())
        {
            $this->data['user_type'] = 'admin';
        }else{
            $this->data['user_type'] = 'user';
        }
        $this->load->view('standard/header_open', $this->data);
        $this->load->view('standard/header_open', $this->data);
        //load style dependency
        $this->load->view('dependency/style/datetimepicker');
        $this->load->view('dependency/style/datatable');
        $this->load->view('dependency/style/selectize');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('calendar_dhearty/list_calendar_dhearty');
        $this->load->view('standard/footer_open');
        //load script dependency
        $this->load->view('dependency/script/moment');
        $this->load->view('dependency/script/datetimepicker');
        $this->load->view('dependency/script/datatable');
        $this->load->view('dependency/script/selectize');
        $this->load->view('standard/footer_close');
    }

    public function ajax_list()
    {
        $post_var = $this->input->post();

        //$curent_model = $this->Manage_company_model;
        $list = $this->current_model->get_datatables($post_var);
        $data = array();
        $no   = $post_var['start'];

        foreach ($list as $record) {

            $dhearty_date = date_create($record->dhearty_date);
            $dhearty_time_start = date_create($record->dhearty_time_start);
            $dhearty_time_end = date_create($record->dhearty_time_end);

            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = $record->dhearty;
            $row[] = $dhearty_date->format('d-m-Y');
            $row[] = $dhearty_time_start->format('h:i A') .' - '. $dhearty_time_end->format('h:i A') ;
            $row[] = $record->dhearty_agenda;

            $edit_button = '<a class="btn btn-sm btn-secondary" href="javascript:void(0)"
                title="Edit" onclick="edit_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-edit"></i> </a>';
            $delete_button = '<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete"
                onclick="delete_record_dialog(' . "'" . $record->id . "'" . ')"><i class="ft ft-trash-2"></i> </a>';

            if($this->ion_auth->is_admin()){
                $row[]  = $edit_button . ' ' . $delete_button;
            }else{
                $row[]  = null;
            }
            $data[] = $row;
        }

        $output = array(
            "draw"            => $post_var['draw'],
            "recordsTotal"    => $this->current_model->count_all(),
            "recordsFiltered" => $this->current_model->count_filtered($post_var),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_upsert()
    {
        $this->_validate();
        $post_var = $this->input->post();

        $dhearty_date = $post_var['dhearty_date'];
        $dhearty_time_start = $post_var['dhearty_time_start'];
        $dhearty_time_end = $post_var['dhearty_time_end'];

        if (empty($post_var['id'])) {
            $data = array(
                'dhearty'           => $post_var['dhearty'],
                'dhearty_date'      => date("Y-m-d", strtotime($dhearty_date)),
                'dhearty_time_start'=> date("Y-m-d H:i:s", strtotime($dhearty_time_start)),
                'dhearty_time_end'  => date("Y-m-d H:i:s", strtotime($dhearty_time_end)),
                'dhearty_agenda'    => $post_var['dhearty_agenda'],
                'created_timestamp' => date('Y-m-d H:i:s'),
                'created_by'        => $this->data['auth_info']->email,
            );

            $status = $this->current_model->insert($data);
        } else {
            $data = array(
                'dhearty'           => $post_var['dhearty'],
                'dhearty_date'      => date("Y-m-d", strtotime($dhearty_date)),
                'dhearty_time_start'=> date("Y-m-d H:i:s", strtotime($dhearty_time_start)),
                'dhearty_time_end'  => date("Y-m-d H:i:s", strtotime($dhearty_time_end)),
                'dhearty_agenda'    => $post_var['dhearty_agenda'],
                'updated_timestamp' => date('Y-m-d H:i:s'),
                'updated_by'        => $this->data['auth_info']->email,
            );

            $where  = array('id' => $post_var['id']);
            $status = $this->current_model->update($where, $data);
        }

        if ($status) {
            //echo 'status is ' . $registered;
            echo json_encode(array("status" => true));
        } else {
            echo json_encode(array("status" => false));
        }
    }

    private function _validate()
    {
        $data                 = array();
        $data['error_string'] = array();
        $data['inputerror']   = array();
        $data['status']       = true;

        if (trim($this->input->post('dhearty')) == '') {
            $data['inputerror'][]   = 'dhearty';
            $data['error_string'][] = 'Dhearty is required';
            $data['status']         = false;
        }
        if (trim($this->input->post('dhearty_date')) == '') {
            $data['inputerror'][]   = 'dhearty_date';
            $data['error_string'][] = 'Date is required';
            $data['status']         = false;
        }
        if (trim($this->input->post('dhearty_agenda')) == '') {
            $data['inputerror'][]   = 'dhearty_agenda';
            $data['error_string'][] = 'Date is required';
            $data['status']         = false;
        }

        if ($data['status'] === false) {
            echo json_encode($data);
            exit();
        }
    }

    public function ajax_delete($id)
    {
        $status = $this->current_model->delete_by_id($id);
        echo json_encode(array("status" => $status));
    }

    public function ajax_edit($id)
    {
        $data = $this->current_model->get_by_id($id);
        echo json_encode($data);
    }
}
