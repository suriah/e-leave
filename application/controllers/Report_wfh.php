<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Report_wfh extends MY_Controller
{
    private $current_model;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Report_wfh_model');
        $this->current_model = $this->Report_wfh_model;
    }

    public function index()
    {

        if ($this->ion_auth->is_admin())
        {
            $this->data['user_type'] = 'admin';
        }else{
            $this->data['user_type'] = 'user';
        }
        $this->data['staff_list'] = $this->current_model->get_staff_list();
        $this->data['month_list'] = $this->current_model->get_month_list();
        $this->data['year_list'] = $this->current_model->get_year_list();
        
        $this->load->view('standard/header_open', $this->data);
        //load style dependency
        $this->load->view('dependency/style/datatable');
        $this->load->view('dependency/style/selectize');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('report_wfh/list_report_wfh');
        $this->load->view('standard/footer_open');
        //load script dependency
        $this->load->view('dependency/script/datatable');
        $this->load->view('dependency/script/selectize');
        $this->load->view('standard/footer_close');
    }

    public function ajax_list()
    {
        $post_var = $this->input->post();
        $filter = array();
        $user_id = $this->data['auth_info']->id;

        if($this->ion_auth->is_admin()){
            $selected_staff = $post_var['selected_staff'];
        }else{
            $selected_staff = $user_id;
        }
        
        if ($selected_staff == true) {
            $filter['wfh.staff_id'] = $selected_staff;
        }
        if ($post_var['selected_month'] == true) {
            // EXTRACT('wfh.wfh_date_from')
            $filter['extract(month from wfh.wfh_date_from)='] =$post_var['selected_month'];
        }
        if ($post_var['selected_year'] == true) {
            // EXTRACT('wfh.wfh_date_from')
            $filter['extract(year from wfh.wfh_date_from)='] =$post_var['selected_year'];
        }
        $this->current_model->filter = $filter;

        //$curent_model = $this->Manage_company_model;
        $list = $this->current_model->get_datatables($post_var);
        $data = array();
        $no   = $post_var['start'];

        $user_id = $this->data['auth_info']->id;

        foreach ($list as $record) {
            if ($this->ion_auth->is_admin()) {
                $manager = $this->current_model->get_manager_name($record->manager_id);
                $date_from = strtotime($record->wfh_date_from);
                $date_to = strtotime($record->wfh_date_to);
                $no++;
                $row   = array();
                $row[] = $no;
                $row[] = $record->staff_name;
                $row[] = $record->staff_email;
                $row[] = date("d-m-Y", $date_from);
                $row[] = date("d-m-Y", $date_to);
                $row[] = $record->total_wfh;
                $row[] = $manager->first_name;
                $row[] = $record->wfh_status;

                $download_button = '<a class="btn btn-sm btn-primary" href="javascript:void(0)"
                title="Download" onclick="download_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-download"></i> </a>';
                $edit_button = '<a class="btn btn-sm btn-secondary" href="javascript:void(0)"
                title="Edit" onclick="edit_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-edit"></i> </a>';
                $delete_button = '<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete"
                onclick="delete_record_dialog(' . "'" . $record->id . "'" . ')"><i class="ft ft-trash-2"></i> </a>';

                $row[] = $download_button;
                //  = $edit_button . ' ' . $delete_button;
                $data[] = $row;
            } else {
                if ($user_id == $record->staff_id) {
                    $manager = $this->current_model->get_manager_name($record->manager_id);
                    $date_from = strtotime($record->wfh_date_from);
                    $date_to = strtotime($record->wfh_date_to);
                    $no++;
                    $row   = array();
                    $row[] = $no;
                    $row[] = $record->staff_name;
                    $row[] = $record->staff_email;
                    $row[] = date("d-m-Y", $date_from);
                    $row[] = date("d-m-Y", $date_to);
                    $row[] = $record->total_wfh;
                    $row[] = $manager->first_name;
                    $row[] = $record->wfh_status;

                    $download_button = '<a class="btn btn-sm btn-primary" href="javascript:void(0)"
                title="Download" onclick="download_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-download"></i> </a>';
                    $edit_button = '<a class="btn btn-sm btn-secondary" href="javascript:void(0)"
                title="Edit" onclick="edit_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-edit"></i> </a>';
                    $delete_button = '<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete"
                onclick="delete_record_dialog(' . "'" . $record->id . "'" . ')"><i class="ft ft-trash-2"></i> </a>';

                    $row[] = $download_button;
                    //  = $edit_button . ' ' . $delete_button;
                    $data[] = $row;
                }
            }
        }

        $output = array(
            "draw"            => $post_var['draw'],
            "recordsTotal"    => $this->current_model->count_all(),
            "recordsFiltered" => $this->current_model->count_filtered($post_var),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function generate_xls_file()
    {
        $post_var = $this->input->get();

        $user_id = $this->data['auth_info']->id;

        if($this->ion_auth->is_admin()){

            $selected_staff = $post_var['selected_staff'];
        }else{
            $selected_staff = $user_id;
        }

        $selected_month = $post_var['selected_month'];
        $selected_year  = $post_var['selected_year'];
        $staff_name = '';$wfh_type='';$month='';

        
        if($selected_staff == true)
        {
            $staff_name = $this->current_model->get_staff_name($selected_staff);
        }
        if($selected_month == true)
        {
            $month = $this->current_model->get_month($selected_month);
        }

        $this->db->select('wfh.* , users.staff_id as staff_id,users.first_name as staff_name, users.email as staff_email');

        if($selected_staff == true)
        {
            $this->db->where('wfh.staff_id', $selected_staff);
        }
        if($selected_month == true)
        {
            $this->db->where('extract(month from wfh.wfh_date_from)=', $selected_month);
        }
        if($selected_year == true)
        {
            $this->db->where('extract(year from wfh.wfh_date_from)=', $selected_year);
        }
        $this->db->order_by('wfh_date_from', 'DESC');

        $query  = $this->db->join('users','users.id = wfh.staff_id','left');
        $query  = $this->db->get('wfh');
        $result = $query->result_array();
        // print_r($result);

       

        $template_file = FCPATH . 'report_template/wfh_report.xlsx';
        $spreadsheet   = \PhpOffice\PhpSpreadsheet\IOFactory::load($template_file);
        $worksheet     = $spreadsheet->getActiveSheet();

        if(isset($staff_name->first_name)){
            $staff_name = $staff_name->first_name;
        }else{
            $staff_name = 'All';
        }
        //set date from and date to
        $worksheet->getCell('C2')->setValue($month);
        $worksheet->getCell('C3')->setValue($selected_year);
        $worksheet->getCell('E2')->setValue($staff_name);

        $row_location = 7;

        $no = 1;
        foreach ($result as $row) {

            $date_from = date_create($row['wfh_date_from']);
            $date_to = date_create($row['wfh_date_to']);
            
            $manager = $this->current_model->get_manager_name($row['manager_id']);
            $worksheet->getCell('B' . $row_location)->setValue($no);
            $worksheet->getCell('C' . $row_location)->setValue($row['staff_id']);
            $worksheet->getCell('D' . $row_location)->setValue($row['staff_name']);
            $worksheet->getCell('E' . $row_location)->setValue($row['staff_email']);
            $worksheet->getCell('F' . $row_location)->setValue(number_format($row['total_wfh']));
            $worksheet->getCell('G' . $row_location)->setValue($row['wfh_reason']);
            $worksheet->getCell('H' . $row_location)->setValue(date_format($date_from, "d-m-Y"));
            $worksheet->getCell('I' . $row_location)->setValue(date_format($date_to, "d-m-Y"));
            $worksheet->getCell('J' . $row_location)->setValue($manager->first_name);
            $worksheet->getCell('K' . $row_location)->setValue($row['wfh_status']);
            // $worksheet->getCell('Q' . $row_location)->setValue(number_format($row['cash_in_machine_uncredited'] / 100, 2));


            $row_location = $row_location + 1;
            $no           = $no + 1;
        }
        // Rename sheet
        $name = substr($staff_name , 0, 31);
        $worksheet->setTitle($name);

        $writer   = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $filename = 'wfh_report_'.$staff_name ;
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output'); // download file 
    }

    public function generate_xls_calendar()
    {

        $post_var = $this->input->get();
        $user_id = $this->data['auth_info']->id;

        if ($this->ion_auth->is_admin()) {
            $selected_staff = $post_var['selected_staff'];
        } else {
            $selected_staff = $user_id;
        }
        // $staff_name = '';

        // $this->generate_single_staff_calendar($selected_staff);

        if ($selected_staff == 'All' || $selected_staff == '') {
            $this->generate_all_staff_calendar($post_var);
        } else {
            $this->generate_single_staff_calendar($post_var, $selected_staff);
        }
    }

    public function generate_single_staff_calendar($post_var, $selected_staff)
    {
        $staff_name = $this->current_model->get_staff_name($selected_staff);

        $template_file = FCPATH . 'report_template/wfh_report_calendar.xlsx';
        $spreadsheet   = \PhpOffice\PhpSpreadsheet\IOFactory::load($template_file);
        $worksheet     = $spreadsheet->getActiveSheet();


        $worksheet->getCell('C3')->setValue($post_var['selected_year']);
        $worksheet->getCell('C4')->setValue($staff_name->first_name);

        $weekend=array();

        $now = strtotime("1st January this year");
        $end_date = strtotime("1st January next year");

        while (date("Y-m-d", $now) != date("Y-m-d", $end_date)) {
            $day_index = date("w", $now);
            if ($day_index == 0 || $day_index == 6) {
                $weekend[]=date("Y-m-d", $now);
            }
            $now = strtotime(date("Y-m-d", $now) . "+1 day");
        }

        //weekend
        $no = 1;
        foreach ($weekend as $row) {
            $row_location = 7;
            $column_location = 3;
            $dateObj   = date_create($row);
            $month = $dateObj->Format('n');
            $day = $dateObj->Format('j');

            $column = $column_location+intval($day);
            $row = $row_location+intval($month);
            $cell = $worksheet->getCellByColumnAndRow($column_location+intval($day), $row_location+intval($month))->getCoordinate();
            $worksheet->getStyle($cell)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('808080');

            $row_location = $row_location + 1;
            $no           = $no + 1;
        }

        // get public holiday
        $query = $this->db->select('public_holiday_date');
        $query = $this->db->get('public_holiday');
        $result = $query->result_array();
        $no = 1;
        foreach ($result as $row) {
            $row_location = 7;
            $column_location = 3;
            $dateObj   = date_create($row['public_holiday_date']);
            $month = $dateObj->Format('n');
            $day = $dateObj->Format('j');

            // public holiday
            $column = $column_location+intval($day);
            $row = $row_location+intval($month);
            $cell = $worksheet->getCellByColumnAndRow($column_location+intval($day), $row_location+intval($month))->getCoordinate();
            $worksheet->getStyle($cell)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('7030A0');

            $row_location = $row_location + 1;
            $no           = $no + 1;
        }

        // get dhearty
        $query = $this->db->select('dhearty_date');
        $query = $this->db->get('dhearty');
        $result = $query->result_array();
        $no = 1;
        foreach ($result as $row) {
            $row_location = 7;
            $column_location = 3;
            $dateObj   = date_create($row['dhearty_date']);
            $month = $dateObj->Format('n');
            $day = $dateObj->Format('j');

            // public holiday
            $column = $column_location+intval($day);
            $row = $row_location+intval($month);
            $cell = $worksheet->getCellByColumnAndRow($column_location+intval($day), $row_location+intval($month))->getCoordinate();
            $worksheet->getStyle($cell)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('31869B');

            $row_location = $row_location + 1;
            $no           = $no + 1;
        }

        $this->db->select('wfh_date');
            $this->db->join('wfh','wfh.id = wfh_date.wfh_id','right');
            $this->db->where('wfh.staff_id',$selected_staff);
            $query = $this->db->get('wfh_date');
            $result = $query->result_array();
            $no = 1;
            foreach ($result as $row) {
                $row_location = 7;
                $column_location = 3;
                $dateObj   = date_create($row['wfh_date']);
                $month = $dateObj->Format('n');
                $day = $dateObj->Format('j');

                // public holiday
                $column = $column_location+intval($day);
                $row = $row_location+intval($month);
                $cell = $worksheet->getCellByColumnAndRow($column_location+intval($day), $row_location+intval($month))->getCoordinate();
                $worksheet->getStyle($cell)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('C00000');

                $row_location = $row_location + 1;
                $no           = $no + 1;
            }
            
        // Rename sheet
        $name = substr($staff_name->first_name, 0, 31);
        $worksheet->setTitle($name);

        $writer   = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $filename = 'leave_report_'.$staff_name->first_name ;
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output'); // download file 

    }

    public function generate_all_staff_calendar($post_var)
    {
        $staff = $this->current_model->get_all_staff_name();

        
        $template_file = FCPATH . 'report_template/wfh_report_calendar_all_staff.xlsx';
        $spreadsheet   = \PhpOffice\PhpSpreadsheet\IOFactory::load($template_file);
        foreach ($staff as $key => $value) {
            $spreadsheet->setActiveSheetIndex($key);
            $worksheet     = $spreadsheet->getActiveSheet();
            $name = substr($value->first_name, 0, 31);
            $worksheet->setTitle($name);
            $clonedWorksheet = clone $spreadsheet->getSheetByName($name);
            $clonedWorksheet->setTitle('New Sheet');
            $spreadsheet->addSheet($clonedWorksheet);
            // Create new PHPExcel object

            $worksheet->getCell('C3')->setValue($post_var['selected_year']);
            $worksheet->getCell('C4')->setValue($value->first_name);

            $weekend = array();

            $now = strtotime("1st January this year");
            $end_date = strtotime("1st January next year");

            while (date("Y-m-d", $now) != date("Y-m-d", $end_date)) {
                $day_index = date("w", $now);
                if ($day_index == 0 || $day_index == 6) {
                    $weekend[] = date("Y-m-d", $now);
                }
                $now = strtotime(date("Y-m-d", $now) . "+1 day");
            }

            //weekend
            $no = 1;
            foreach ($weekend as $row) {
                $row_location = 7;
                $column_location = 3;
                $dateObj   = date_create($row);
                $month = $dateObj->Format('n');
                $day = $dateObj->Format('j');

                $column = $column_location + intval($day);
                $row = $row_location + intval($month);
                $cell = $worksheet->getCellByColumnAndRow($column_location + intval($day), $row_location + intval($month))->getCoordinate();
                $worksheet->getStyle($cell)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('808080');

                $row_location = $row_location + 1;
                $no           = $no + 1;
            }

            // get public holiday
            $query = $this->db->select('public_holiday_date');
            $query = $this->db->get('public_holiday');
            $result = $query->result_array();
            $no = 1;
            foreach ($result as $row) {
                $row_location = 7;
                $column_location = 3;
                $dateObj   = date_create($row['public_holiday_date']);
                $month = $dateObj->Format('n');
                $day = $dateObj->Format('j');

                // public holiday
                $column = $column_location + intval($day);
                $row = $row_location + intval($month);
                $cell = $worksheet->getCellByColumnAndRow($column_location + intval($day), $row_location + intval($month))->getCoordinate();
                $worksheet->getStyle($cell)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('7030A0');

                $row_location = $row_location + 1;
                $no           = $no + 1;
            }

            // get dhearty
            $query = $this->db->select('dhearty_date');
            $query = $this->db->get('dhearty');
            $result = $query->result_array();
            $no = 1;
            foreach ($result as $row) {
                $row_location = 7;
                $column_location = 3;
                $dateObj   = date_create($row['dhearty_date']);
                $month = $dateObj->Format('n');
                $day = $dateObj->Format('j');

                // public holiday
                $column = $column_location + intval($day);
                $row = $row_location + intval($month);
                $cell = $worksheet->getCellByColumnAndRow($column_location + intval($day), $row_location + intval($month))->getCoordinate();
                $worksheet->getStyle($cell)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('31869B');

                $row_location = $row_location + 1;
                $no           = $no + 1;
            }

            $this->db->select('wfh_date');
            $this->db->join('wfh','wfh.id = wfh_date.wfh_id','right');
            $this->db->where('wfh.staff_id',$value->id);
            $query = $this->db->get('wfh_date');
            $result = $query->result_array();
            $no = 1;
            foreach ($result as $row) {
                $row_location = 7;
                $column_location = 3;
                $dateObj   = date_create($row['wfh_date']);
                $month = $dateObj->Format('n');
                $day = $dateObj->Format('j');

                // public holiday
                $column = $column_location+intval($day);
                $row = $row_location+intval($month);
                $cell = $worksheet->getCellByColumnAndRow($column_location+intval($day), $row_location+intval($month))->getCoordinate();
                $worksheet->getStyle($cell)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('C00000');

                $row_location = $row_location + 1;
                $no           = $no + 1;
            }
            
        }
        $writer   = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $filename = 'leave_report_';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output'); // download file 
    }


    // public function generate_xls_calendar()
    // {
        
    //     $template_file = FCPATH . 'report_template/wfh_report_calendar.xlsx';
    //     $spreadsheet   = \PhpOffice\PhpSpreadsheet\IOFactory::load($template_file);
    //     $worksheet     = $spreadsheet->getActiveSheet();

    //     $post_var = $this->input->get();
    //     $user_id = $this->data['auth_info']->id;

    //     if($this->ion_auth->is_admin()){

    //         $selected_staff = $post_var['selected_staff'];
    //     }else{
    //         $selected_staff = $user_id;
    //     }
    //     $staff_name = '';
        
    //     if($selected_staff == true)
    //     {
    //         $staff_name = $this->current_model->get_staff_name($selected_staff);
    //     }
        
    //     if(isset($staff_name->first_name)){
    //         $staff_name = $staff_name->first_name;
    //     }else{
    //         $staff_name = 'All';
    //     }
    //     $worksheet->getCell('C3')->setValue($post_var['selected_year']);
    //     $worksheet->getCell('C4')->setValue($staff_name);

    //     $weekend=array();

    //     $now = strtotime("1st January this year");
    //     $end_date = strtotime("1st January next year");

    //     while (date("Y-m-d", $now) != date("Y-m-d", $end_date)) {
    //         $day_index = date("w", $now);
    //         if ($day_index == 0 || $day_index == 6) {
    //             $weekend[]=date("Y-m-d", $now);
    //         }
    //         $now = strtotime(date("Y-m-d", $now) . "+1 day");
    //     }

    //     //weekend
    //     $no = 1;
    //     foreach ($weekend as $row) {
    //         $row_location = 7;
    //         $column_location = 3;
    //         $dateObj   = date_create($row);
    //         $month = $dateObj->Format('n');
    //         $day = $dateObj->Format('j');

    //         $column = $column_location+intval($day);
    //         $row = $row_location+intval($month);
    //         $cell = $worksheet->getCellByColumnAndRow($column_location+intval($day), $row_location+intval($month))->getCoordinate();
    //         $worksheet->getStyle($cell)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('808080');

    //         $row_location = $row_location + 1;
    //         $no           = $no + 1;
    //     }

    //     // get public holiday
    //     $query = $this->db->select('public_holiday_date');
    //     $query = $this->db->get('public_holiday');
    //     $result = $query->result_array();
    //     $no = 1;
    //     foreach ($result as $row) {
    //         $row_location = 7;
    //         $column_location = 3;
    //         $dateObj   = date_create($row['public_holiday_date']);
    //         $month = $dateObj->Format('n');
    //         $day = $dateObj->Format('j');

    //         // public holiday
    //         $column = $column_location+intval($day);
    //         $row = $row_location+intval($month);
    //         $cell = $worksheet->getCellByColumnAndRow($column_location+intval($day), $row_location+intval($month))->getCoordinate();
    //         $worksheet->getStyle($cell)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('1F497D');

    //         $row_location = $row_location + 1;
    //         $no           = $no + 1;
    //     }

    //     // get dhearty
    //     $query = $this->db->select('dhearty_date');
    //     $query = $this->db->get('dhearty');
    //     $result = $query->result_array();
    //     $no = 1;
    //     foreach ($result as $row) {
    //         $row_location = 7;
    //         $column_location = 3;
    //         $dateObj   = date_create($row['dhearty_date']);
    //         $month = $dateObj->Format('n');
    //         $day = $dateObj->Format('j');

    //         // public holiday
    //         $column = $column_location+intval($day);
    //         $row = $row_location+intval($month);
    //         $cell = $worksheet->getCellByColumnAndRow($column_location+intval($day), $row_location+intval($month))->getCoordinate();
    //         $worksheet->getStyle($cell)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('31869B');

    //         $row_location = $row_location + 1;
    //         $no           = $no + 1;
    //     }

    //     // get wfh
    //     $this->db->select('wfh_date');
    //     $this->db->join('wfh','wfh.id = wfh_date.wfh_id','right');
    //     $this->db->where('wfh.staff_id',$selected_staff);
    //     $query = $this->db->get('wfh_date');
    //     $result = $query->result_array();
    //     $no = 1;
    //     foreach ($result as $row) {
    //         $row_location = 7;
    //         $column_location = 3;
    //         $dateObj   = date_create($row['wfh_date']);
    //         $month = $dateObj->Format('n');
    //         $day = $dateObj->Format('j');

    //         // public holiday
    //         $column = $column_location+intval($day);
    //         $row = $row_location+intval($month);
    //         $cell = $worksheet->getCellByColumnAndRow($column_location+intval($day), $row_location+intval($month))->getCoordinate();
    //         $worksheet->getStyle($cell)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('C00000');

    //         $row_location = $row_location + 1;
    //         $no           = $no + 1;
    //     }

    //     $writer   = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
    //     $filename = 'wfh_report_'.$staff_name ;
    //     header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    //     header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    //     header('Cache-Control: max-age=0');

    //     $writer->save('php://output'); // download file 
    // }

}
