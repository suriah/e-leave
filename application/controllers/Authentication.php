<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends MY_Controller {

    /**
     * Log the user out
     */
    public function logout() {

        // log the user out
        $this->ion_auth->logout();

        // redirect them to the login page
        //$this->session->set_flashdata('message', $this->ion_auth->messages());
        redirect('auth/login', 'refresh');
    }

    public function change_password() {
//        print_r ($this->data['ion_auth']);
//        exit();
        $this->load->view('standard/header_open', $this->data);
        //load style dependency
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        //$this->load->view('standard/test_content');
        $this->load->view('authentication/change_password');
        $this->load->view('standard/footer_open');
        //load script dependency
        $this->load->view('dependency/script/datatable');
        $this->load->view('standard/footer_close');
    }

    public function change_password_update() {
        //update setting information
        $form_var = $this->input->post();
        $result = $this->_validate_password($form_var);
        if ($result['status'] == 'true') {
            //validation pass, now call function to change password
            //echo $this->data['auth_info']->id;
            $data = array('password' => $form_var['new_password']);
            $update_status = $this->ion_auth->update($this->data['auth_info']->id, $data);
            if ($update_status == true) {
                $result = array('status' => 'true',
                    'message' => 'Password change succesful'
                );
            } else {
                $result = array('status' => 'false',
                    'message' => 'Password change unsuccessful, please try again'
                );
            }
        }
//        print_r($result);
//        exit();
        $this->session->set_flashdata('status', $result['status']);
        $this->session->set_flashdata('message', $result['message']);
        redirect(base_url('authentication/change_password'));
    }

    private function _validate_password($form_var) {

        if ($form_var['confirm_password'] != $form_var['new_password']) {
            $message = "New password and confirm password does not match";
            $status = 'false';
        } else if (strlen($form_var['new_password']) < 8) {
            $message = "New password too short, please input 8 character";
            $status = 'false';
        } else if ($this->ion_auth->hash_password_db($this->data['auth_info']->id, $form_var['current_password']) !== TRUE) {
            $message = "Invalid current password";
            $status = 'false';
        } else {
            $message = '';
            $status = 'true';
        }
        $result = array('status' => $status,
            'message' => $message
        );
        return $result;
    }

}
