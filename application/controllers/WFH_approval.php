<?php

defined('BASEPATH') or exit('No direct script access allowed');

class WFH_approval extends MY_Controller
{
    private $current_model;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('WFH_approval_model');
        $this->load->model('Users_model');
        $this->user_model = $this->Users_model;
        $this->current_model = $this->WFH_approval_model;
        $this->current_model->auth_info = $this->data['auth_info'];
        
    }

    public function index()
    {
        $this->data['manager_list'] = $this->current_model->list_manager_drop_down();
        $this->data['user_data'] = $this->data['auth_info'];
        
        $this->load->view('standard/header_open', $this->data);
        //load style dependency
        $this->load->view('dependency/style/datetimepicker');
        $this->load->view('dependency/style/datatable');
        $this->load->view('dependency/style/selectize');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('wfh_approval/list_wfh_approval');
        $this->load->view('standard/footer_open');
        //load script dependency
        $this->load->view('dependency/script/moment');
        $this->load->view('dependency/script/datetimepicker');
        $this->load->view('dependency/script/datatable');
        $this->load->view('dependency/script/selectize');
        $this->load->view('standard/footer_close');
    }

    public function ajax_list()
    {
        $post_var = $this->input->post();

        
        $filter = array(
            'manager_id =' => $this->data['auth_info']->id,
            'wfh_status =' => 'REQ_APPROVAL',
        );
     
        $this->current_model->filter = $filter;
        
        $list = $this->current_model->get_datatables($post_var);
        $data = array();
        $no   = $post_var['start'];
        
        foreach ($list as $record) {


            $date_from = strtotime($record->wfh_date_from);
            $date_to = strtotime($record->wfh_date_to);
            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = $record->staff_name;
            $row[] = $record->staff_email;
            $row[] = date("d-m-Y", $date_from);
            $row[] = date("d-m-Y", $date_to);
            $row[] = $record->total_wfh;
            $row[] = $record->wfh_status;

            $view_button = '<a class="btn btn-sm btn-primary" href="javascript:void(0)"
                title="View" onclick="view_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-eye"></i> </a>';
            $process_button = '<a class="btn btn-sm btn-warning" href="javascript:void(0)"
                title="Process" onclick="process_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-loader"></i> </a>';

            if($record->wfh_status == 'REQ_APPROVAL'){
                $row[]  = $process_button;
            }else{
                $row[]  = $view_button ;
            }
            $data[] = $row;
        }

        $output = array(
            "draw"            => $post_var['draw'],
            "recordsTotal"    => $this->current_model->count_all(),
            "recordsFiltered" => $this->current_model->count_filtered($post_var),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_upsert()
    {
        $this->_validate();
        $post_var = $this->input->post();
        // print_r($post_var);die;
        
        $date_from = $post_var['date_from'];
        $date_to = $post_var['date_to'];

        if (empty($post_var['id'])) {

            $data = array(
                'wfh_date_from'           => date("Y-m-d", strtotime($date_from)),
                'wfh_date_to'             => date("Y-m-d", strtotime($date_to)),
                'total_wfh'               => $post_var['total_wfh'],
                'wfh_reason'              => $post_var['wfh_reason'],
                'wfh_status'              => 'PENDING',
                'staff_id'                  => $this->data['auth_info']->id,
                'manager_id'                => $post_var['manager_id'],
                'created_timestamp'         => date('Y-m-d H:i:s'),
                'created_by'                => $this->data['auth_info']->email,
            );

            $status = $this->current_model->insert($data);
        } else {
            $data = array(
                'wfh_date_from'           => date("Y-m-d", strtotime($date_from)),
                'wfh_date_to'             => date("Y-m-d", strtotime($date_to)),
                'total_wfh'               => $post_var['total_wfh'],
                'wfh_reason'              => $post_var['wfh_reason'],
                'manager_id'                => $post_var['manager_id'],
                'updated_timestamp'         => date('Y-m-d H:i:s'),
                'updated_by'                => $this->data['auth_info']->email,
            );

            $where  = array('id' => $post_var['id']);
            $status = $this->current_model->update($where, $data);
        }

        if ($status) {
            //echo 'status is ' . $registered;
            echo json_encode(array("status" => true));
        } else {
            echo json_encode(array("status" => false));
        }
    }

    private function _validate()
    {
        $data                 = array();
        $data['error_string'] = array();
        $data['inputerror']   = array();
        $data['status']       = true;

        if (trim($this->input->post('feedback')) == '') {
            $data['inputerror'][]   = 'feedback';
            $data['error_string'][] = 'Feedback required for rejecting the application';
            $data['status']         = false;
        }
       

        if ($data['status'] === false) {
            echo json_encode($data);
            exit();
        }
    }

    public function ajax_delete($id)
    {
        $status = $this->current_model->delete_by_id($id);
        echo json_encode(array("status" => $status));
    }

    public function ajax_approve_application($id)
    {
        $post_var = $this->input->post();
            $data = array(
                'wfh_status'              => 'APPROVED',
                'manager_is_approved'       => true,
                'updated_timestamp'         => date('Y-m-d H:i:s'),
                'updated_by'                => $this->data['auth_info']->email,
            );

            
        
        $where  = array('id' => $post_var['id']);
        $status = $this->current_model->update($where, $data);

        if($status){
            $date = $this->current_model->add_leave_date($id);
            foreach ($date as $key => $value) {
                # code...
                $data = array(
                    'wfh_id'            => $id,
                    'wfh_date'            => $value,
                    'created_timestamp'   => date('Y-m-d H:i:s'),
                    'created_by'          => $this->data['auth_info']->email,
                );
                $status = $this->db->insert('wfh_date',$data);
                
                //approval email
                $this->current_model->wfh_approved_notification_email($id);
            }
        }
        // $status = $this->current_model->email_notification($id,$data);
        echo json_encode(array("status" => $status));
    }
    
    public function ajax_reject_application($id)
    {
        $this->_validate();
        $post_var = $this->input->post();
        
            $data = array(
                'wfh_status'              => 'REJECTED',
                'manager_is_approved'       => false,
                'manager_feedback'    => $post_var['feedback'],
                'updated_timestamp'         => date('Y-m-d H:i:s'),
                'updated_by'                => $this->data['auth_info']->email,
            );
        
        $where  = array('id' => $post_var['id']);
        $status = $this->current_model->update($where, $data);
        $status =$this->current_model->wfh_rejected_notification_email($id,$post_var['feedback']);
        echo json_encode(array("status" => $status));
    }

    public function ajax_edit($id)
    {
        $data = $this->current_model->get_by_id($id);
        echo json_encode($data);
    }
    public function ajax_view($id)
    {
        $data = $this->current_model->view_process_data($id);
        echo json_encode($data);
    }
    public function ajax_view_process_data($id)
    {
        $data = $this->current_model->view_process_data($id);
        echo json_encode($data);
    }
}
