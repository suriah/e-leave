<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        $this->load->helper('url');

        $this->load->database();

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
        $this->load->helper('language');
    }

    //redirect if needed, otherwise display the user list
    function index()
    {
        if (!$this->ion_auth->logged_in())
        {
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        }
        elseif (!$this->ion_auth->is_admin()) //remove this elseif if you want to enable this for non-admins
        {
            //redirect them to the home page because they must be an administrator to view this
            return show_error('You must be an administrator to view this page.');
        }
        else
        {
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            //list the users
            $this->data['users'] = $this->ion_auth->users()->result();
            foreach ($this->data['users'] as $k => $user)
            {
                $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
            }
            $this->_render_page('auth/index', $this->data);
        }
    }

    //log the user in
    function login()
    {
        $this->data['title'] = "Login";
        //validate form input
        $this->form_validation->set_rules('identity', 'Identity', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == true)
        {
            //check to see if the user is logging in
            //check for "remember me"
            $remember = (bool) $this->input->post('remember');

            if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
            {
                    //if the login is successful
                    //redirect them back to the home page
                    $this->session->set_flashdata('login_status', "true" );
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
                    //$user_role = $this->ion_auth->user()->row()->role;
                    //redirect all csa user to daily sales form
                    redirect('/dashboard', 'refresh');
            }
            else
            {
                    //if the login was un-successful
                    //redirect them back to the login page
                    $this->session->set_flashdata('login_status', "false" );
                    $this->session->set_flashdata('message', $this->ion_auth->errors());                    
                    redirect('/', 'refresh');
            }
        }
        else
        {
            //the user is not logging in so display the login page
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $this->data['identity'] = array('name' => 'identity',
                    'id' => 'identity',
                    'type' => 'text',
                    'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['password'] = array('name' => 'password',
                    'id' => 'password',
                    'type' => 'password',
            );

            //redirect('/', 'refresh');
        }
    }

    //log the user out
    function logout()
    {
        $this->data['title'] = "Logout";

        //log the user out
        $this->ion_auth->logout();
        //redirect them to the login page        
        $this->session->set_flashdata('message', $this->ion_auth->messages());        
        redirect('/', 'refresh');
    }

    
    public function forgot_password_form()
    {
        //this function provide form for forgot password
        $this->load->view('templates/public/header');        
        $this->load->view('auth/forgot_password');
        $this->load->view('templates/public/footer_open');       
        $this->load->view('templates/public/footer_close');        
    }
    
    public function forgot_password()
    {
        //this function execute 
        // 1. Send email with code
        // 2. Direct to reset password form
        
        $email = $this->input->post('identity');
        //send email
        $this->load->model('Auth_model');
        $this->Auth_model->forgot_password_send_email($email);
        //exit();
        redirect('auth/forgot_password_reset_form', 'refresh');
    }
    
    public function forgot_password_reset_form()
    {
        //display reset password form
        $this->load->view('templates/public/header');        
        $this->load->view('auth/forgot_password_reset_form');
        $this->load->view('templates/public/footer_open');  
        $this->load->view('templates/dependency/bootstrap-validator');
        $this->load->view('templates/public/footer_close');     
    }
    
    public function reset_password()
    {
        $form_var = $this->input->post();
        $this->load->model('Auth_model');
        if ($this->Auth_model->forgot_password_reset($form_var) == true)
        {
            //success
            $message = 'Reset password successful, please check your email to retrieve new password.Click <a href="' .
                    base_url() . '">here</a> to login.';
        }else{
            //failed
            $message = 'Reset password failed, please <a href="' . base_url() . 'auth/forgot_password_form" >retry</a> again';
        }
        $data = array('message' => $message);
        $this->load->view('templates/public/header', $data);        
        $this->load->view('auth/reset_password_status');
        $this->load->view('templates/public/footer_open');  
        $this->load->view('templates/dependency/bootstrap-validator');
        $this->load->view('templates/public/footer_close'); 
        
    }
}