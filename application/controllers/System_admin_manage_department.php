<?php

defined('BASEPATH') or exit('No direct script access allowed');

class System_admin_manage_department extends MY_Controller
{
    private $current_model;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('System_admin_manage_department_model');
        $this->current_model = $this->System_admin_manage_department_model;
    }

    public function index()
    {
        $this->load->view('standard/header_open', $this->data);
        //load style dependency
        $this->load->view('dependency/style/datatable');
        $this->load->view('dependency/style/selectize');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('system_admin_manage_department/list_system_admin_manage_department');
        $this->load->view('standard/footer_open');
        //load script dependency
        $this->load->view('dependency/script/datatable');
        $this->load->view('dependency/script/selectize');
        $this->load->view('standard/footer_close');
    }

    public function ajax_list()
    {
        $post_var = $this->input->post();

        //$curent_model = $this->Manage_company_model;
        $list = $this->current_model->get_datatables($post_var);
        $data = array();
        $no   = $post_var['start'];

        foreach ($list as $record) {
            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = $record->department_code;
            $row[] = $record->department_name;
            $row[] = $record->department_description;

            $edit_button = '<a class="btn btn-sm btn-secondary" href="javascript:void(0)"
                title="Edit" onclick="edit_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-edit"></i> </a>';
            $delete_button = '<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete"
                onclick="delete_record_dialog(' . "'" . $record->id . "'" . ')"><i class="ft ft-trash-2"></i> </a>';

            $row[]  = $edit_button . ' ' . $delete_button;
            $data[] = $row;
        }

        $output = array(
            "draw"            => $post_var['draw'],
            "recordsTotal"    => $this->current_model->count_all(),
            "recordsFiltered" => $this->current_model->count_filtered($post_var),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_upsert()
    {
        $this->_validate();
        $post_var = $this->input->post();

        if (empty($post_var['id'])) {
            $data = array(
                'department_code'         => $post_var['department_code'],
                'department_name'         => $post_var['department_name'],
                'department_description'  => $post_var['department_description'],
                'created_timestamp' => date('Y-m-d H:i:s'),
                'created_by'        => $this->data['auth_info']->email,
            );

            $status = $this->current_model->insert($data);
        } else {
            $data = array(
                'department_code'         => $post_var['department_code'],
                'department_name'         => $post_var['department_name'],
                'department_description'  => $post_var['department_description'],
                'updated_timestamp' => date('Y-m-d H:i:s'),
                'updated_by'        => $this->data['auth_info']->email,
            );

            $where  = array('id' => $post_var['id']);
            $status = $this->current_model->update($where, $data);
        }

        if ($status) {
            //echo 'status is ' . $registered;
            echo json_encode(array("status" => true));
        } else {
            echo json_encode(array("status" => false));
        }
    }

    private function _validate()
    {
        $data                 = array();
        $data['error_string'] = array();
        $data['inputerror']   = array();
        $data['status']       = true;

        if (trim($this->input->post('department_code')) == '') {
            $data['inputerror'][]   = 'department_code';
            $data['error_string'][] = 'department code is required';
            $data['status']         = false;
        }
        if (trim($this->input->post('department_name')) == '') {
            $data['inputerror'][]   = 'department_name';
            $data['error_string'][] = 'department name is required';
            $data['status']         = false;
        }

        if ($data['status'] === false) {
            echo json_encode($data);
            exit();
        }
    }

    public function ajax_delete($id)
    {
        $status = $this->current_model->delete_by_id($id);
        echo json_encode(array("status" => $status));
    }

    public function ajax_edit($id)
    {
        $data = $this->current_model->get_by_id($id);
        echo json_encode($data);
    }
}
