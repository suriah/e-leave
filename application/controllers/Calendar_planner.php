<?php

defined('BASEPATH') or exit('No direct script access allowed');

include 'Calendar.php';
class Calendar_planner extends MY_Controller
{
    private $current_model;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Calendar_planner_model');
        $this->current_model = $this->Calendar_planner_model;
        $this->current_model->auth_info = $this->data['auth_info'];
    }

    public function index()
    {
        
        $this->load->view('standard/header_open', $this->data);
        //load style dependency
        $this->load->view('dependency/style/datetimepicker');
        $this->load->view('dependency/style/datatable');
        $this->load->view('dependency/style/selectize');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('calendar_planner/list_calendar_planner');
        $this->load->view('standard/footer_open');
        //load script dependency
        $this->load->view('dependency/script/moment');
        $this->load->view('dependency/script/datetimepicker');
        $this->load->view('dependency/script/datatable');
        $this->load->view('dependency/script/selectize');
        $this->load->view('standard/footer_close');
    }
}
