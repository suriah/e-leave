<?php

defined('BASEPATH') or exit('No direct script access allowed');

class System_admin_manage_leave_type extends MY_Controller
{
    private $current_model;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('System_admin_manage_leave_type_model');
        $this->current_model = $this->System_admin_manage_leave_type_model;
    }

    public function index()
    {
        $this->load->view('standard/header_open', $this->data);
        //load style dependency
        $this->load->view('dependency/style/datatable');
        $this->load->view('dependency/style/selectize');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('system_admin_manage_leave_type/list_system_admin_manage_leave_type');
        $this->load->view('standard/footer_open');
        //load script dependency
        $this->load->view('dependency/script/datatable');
        $this->load->view('dependency/script/selectize');
        $this->load->view('standard/footer_close');
    }

    public function ajax_list()
    {
        $post_var = $this->input->post();

        //$curent_model = $this->Manage_company_model;
        $list = $this->current_model->get_datatables($post_var);
        $data = array();
        $no   = $post_var['start'];

        foreach ($list as $record) {
            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = $record->leave_code;
            $row[] = $record->leave_name;

            $edit_button = '<a class="btn btn-sm btn-secondary" href="javascript:void(0)"
                title="Edit" onclick="edit_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-edit"></i> </a>';
            $delete_button = '<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete"
                onclick="delete_record_dialog(' . "'" . $record->id . "'" . ')"><i class="ft ft-trash-2"></i> </a>';

            $row[]  = $edit_button . ' ' . $delete_button;
            $data[] = $row;
        }

        $output = array(
            "draw"            => $post_var['draw'],
            "recordsTotal"    => $this->current_model->count_all(),
            "recordsFiltered" => $this->current_model->count_filtered($post_var),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_upsert()
    {
        $this->_validate();
        $post_var = $this->input->post();

        if (empty($post_var['id'])) {
            $data = array(
                'leave_code'         => $post_var['leave_code'],
                'leave_name'         => $post_var['leave_name'],
                'created_timestamp' => date('Y-m-d H:i:s'),
                'created_by'        => $this->data['auth_info']->email,
            );

            $status = $this->current_model->insert($data);
        } else {
            $data = array(
                'leave_code'         => $post_var['leave_code'],
                'leave_name'         => $post_var['leave_name'],
                'updated_timestamp' => date('Y-m-d H:i:s'),
                'updated_by'        => $this->data['auth_info']->email,
            );

            $where  = array('id' => $post_var['id']);
            $status = $this->current_model->update($where, $data);
        }

        if ($status) {
            //echo 'status is ' . $registered;
            echo json_encode(array("status" => true));
        } else {
            echo json_encode(array("status" => false));
        }
    }

    private function _validate()
    {
        $data                 = array();
        $data['error_string'] = array();
        $data['inputerror']   = array();
        $data['status']       = true;

        if (trim($this->input->post('leave_code')) == '') {
            $data['inputerror'][]   = 'leave_code';
            $data['error_string'][] = 'leave code is required';
            $data['status']         = false;
        }
        if (trim($this->input->post('leave_name')) == '') {
            $data['inputerror'][]   = 'leave_name';
            $data['error_string'][] = 'leave name is required';
            $data['status']         = false;
        }

        if ($data['status'] === false) {
            echo json_encode($data);
            exit();
        }
    }

    public function ajax_delete($id)
    {
        $status = $this->current_model->delete_by_id($id);
        echo json_encode(array("status" => $status));
    }

    public function ajax_edit($id)
    {
        $data = $this->current_model->get_by_id($id);
        echo json_encode($data);
    }
}
