<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class System_admin_manage_role extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Groups_model');
    }
    
    public function index() {


        $list_privileges = $this->Groups_model->list_privileges();
        $this->data['list_privileges'] = $list_privileges;

        // echo '<pre>';
        // print_r ($this->data);
        // echo '</pre>';
        // exit();
        $this->load->view('standard/header_open', $this->data);
        //load style dependency
        $this->load->view('dependency/style/switch');
        $this->load->view('dependency/style/datatable');
        $this->load->view('dependency/style/selectize');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('system_admin_manage_role/list_groups');
        $this->load->view('standard/footer_open');
        //load script dependency
        $this->load->view('dependency/script/datatable');
        $this->load->view('dependency/script/selectize');
        $this->load->view('standard/footer_close');
    }

    public function ajax_list() {
        $post_var = $this->input->post();

        $curent_model = $this->Groups_model;
        $list = $curent_model->get_datatables($post_var);
        $data = array();
        $no = $post_var['start'];

        foreach ($list as $record) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $record->name;
            $row[] = $record->description;

            $edit_button = '<a class="btn btn-sm btn-secondary" href="javascript:void(0)"
                title="Edit" onclick="edit(' . "'" . $record->id . "'" . ')"><i class="ft ft-edit"></i> </a>';
            $delete_button = '<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete"
                onclick="delete_record_dialog(' . "'" . $record->id . "'" . ')"><i class="ft ft-trash-2"></i> </a>';

            $row[] = $edit_button . ' ' . $delete_button;
            $data[] = $row;
        }

        $output = array(
            "draw" => $post_var['draw'],
            "recordsTotal" => $curent_model->count_all(),
            "recordsFiltered" => $curent_model->count_filtered($post_var),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_delete($id) {
        //do not allow deletion if there is a user in the group
        $this->db->where('group_id', $id);
        $query = $this->db->get('users_groups');
        if ($query->num_rows() > 0){
            $status = false;
        }else{
            $status =  $this->ion_auth->delete_group($id);
            //delete privilege as well
            $this->db->where('group_id', $id);
            $this->db->delete('privileges_groups');
        }
        echo json_encode(array("status" => $status));
    }

    public function ajax_edit($id) {
        //$where = array('id' => $id);
        $data = $this->Groups_model->get_by_id($id);
        $data->privileges_detail = $this->Groups_model->get_privileges_detail($data->id);
        echo json_encode($data);
    }

    public function ajax_update() {
        $this->_validate();
        $post_var = $this->input->post();

        $group_id = $post_var['id'];
        if ($group_id != null ){
            $status = $this->ion_auth->update_group($group_id, $post_var['name'], $post_var['description']);
        }else{
            $status = $this->ion_auth->create_group($post_var['name'], $post_var['description']);
            if ($status != false){
                $group_id = $status;
                $status = true;
            }
        }

        //update privilege
        if ($status == true){
            $this->Groups_model->update_privilege($group_id, $post_var['privilege_id']);
        }
        
        $data = array();
        $data['status'] = $status;
        echo json_encode($data);
    }

    private function _validate() {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = true;

        if (trim($this->input->post('name')) == '') {
            $data['inputerror'][] = 'name';
            $data['error_string'][] = 'Group name is required';
            $data['status'] = false;
        }

        if (trim($this->input->post('description')) == '') {
            $data['inputerror'][] = 'description';
            $data['error_string'][] = 'Description is required';
            $data['status'] = false;
        }

        if ($data['status'] === false) {
            echo json_encode($data);
            exit();
        }
    }


}
