<?php

defined('BASEPATH') or exit('No direct script access allowed');

class System_admin_manage_user extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model');
        $this->current_model = $this->Users_model;
    }

    public function index()
    {

        //get list of user group and pass it to page
        $groups              = $this->ion_auth->groups()->result();
        $ion_auth_groups[''] = 'No role selected';
        foreach ($groups as $value) {
            $ion_auth_groups[$value->id] = $value->name;
        }

        $this->data['ion_auth_groups']  = $ion_auth_groups;

        $this->data['position_list'] = $this->current_model->list_positions_drop_down();
        $this->data['department_list'] = $this->current_model->list_departments_drop_down();
        $this->data['employee_status_list'] = $this->current_model->list_employee_status_drop_down();
        $this->data['level_list'] = $this->current_model->list_level_drop_down();

        $this->load->view('standard/header_open', $this->data);
        //load style dependency
        $this->load->view('dependency/style/datatable');
        $this->load->view('dependency/style/selectize');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('system_admin_manage_user/list_users');
        $this->load->view('standard/footer_open');
        //load script dependency
        $this->load->view('dependency/script/datatable');
        $this->load->view('dependency/script/selectize');
        $this->load->view('standard/footer_close');
    }

    public function ajax_list()
    {
        $post_var = $this->input->post();

        $curent_model = $this->Users_model;
        $list         = $curent_model->get_datatables($post_var);
        $data         = array();
        $no           = $post_var['start'];

        foreach ($list as $record) {
            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = $record->email;
            $row[] = $record->first_name;
            $row[] = $record->phone;
            if ($record->active == 1) {
                $status = 'Yes';
            } else {
                $status = 'No';
            }
            $row[] = $status;

            $edit_button = '<a class="btn btn-secondary btn-sm" href="javascript:void(0)"
                title="Edit" onclick="edit(' . "'" . $record->id . "'" . ')"><i class="ft ft-edit"></i> </a>';
            $delete_button = '<a class="btn btn-danger btn-sm" href="javascript:void(0)" title="Delete"
                onclick="delete_record_dialog(' . "'" . $record->id . "'" . ')"><i class="ft ft-trash-2"></i> </a>';

            $row[]  = $edit_button . ' ' . $delete_button;
            $data[] = $row;
        }

        $output = array(
            "draw"            => $post_var['draw'],
            "recordsTotal"    => $curent_model->count_all(),
            "recordsFiltered" => $curent_model->count_filtered($post_var),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_delete($id)
    {
        $status = $this->ion_auth->delete_user($id);
        echo json_encode(array("status" => $status));
    }

    public function ajax_edit($id)
    {
        //$where = array('id' => $id);
        $data['user'] = $this->Users_model->get_by_id($id);
        $data['leave'] = $this->Users_model->get_leave($id);
        //get group id
        $user_groups = $this->ion_auth->get_users_groups($id)->result();

        foreach ($user_groups as $value) {
            $temp_array[] = $value->id;
        }

        if (isset($temp_array)) {
            $user_group_string = implode(',', $temp_array);
        } else {
            $user_group_string = '';
        }

        $data['role' ]= $user_group_string;

        // print_r($data);

        echo json_encode($data);
    }

    public function ajax_update()
    {
        $this->_validate();
        $post_var = $this->input->post();

        $data = array(
            'email'                         => $post_var['email'],
            'first_name'                    => $post_var['first_name'],
            'phone'                         => $post_var['phone'],
            'active'                        => $post_var['active'],
            'staff_id'                      => $post_var['staff_id'],
            'staff_employee_status_id'      => $post_var['staff_employee_status_id'],
            'staff_position_id'             => $post_var['staff_position_id'],
            'staff_department_id'           => $post_var['staff_department_id'],
            'staff_level_id'                => $post_var['staff_level_id'],
            // 'staff_annual_leave'            => $post_var['staff_annual_leave'],
            // 'staff_emergency_leave'         => $post_var['staff_emergency_leave'],
            // 'staff_mc'                      => $post_var['staff_mc'],
            // 'staff_balance_forwarded_leave' => $post_var['staff_balance_forwarded_leave'],
            // 'staff_maternity_leave'         => $post_var['staff_maternity_leave'],
            // 'staff_total_leave'             => $post_var['staff_total_leave'],
            // 'staff_balance_leave'           => $post_var['staff_total_leave'],
            'updated_timestamp'             => date('Y-m-d H:i:s'),
            'updated_by'                    => $this->data['auth_info']->email,
        );

        $status = $this->ion_auth->update($post_var['id'], $data);

        if($status){
            $leave_data = array(
                'staff_annual_leave'            => $post_var['staff_annual_leave'],
                'staff_emergency_leave'         => $post_var['staff_emergency_leave'],
                'staff_forwarded_leave'         => $post_var['staff_forwarded_leave'],
                'staff_balance_leave'           => $post_var['staff_balance_leave'],
                'staff_mc'                      => $post_var['staff_mc'],
                'staff_maternity_leave'         => $post_var['staff_maternity_leave'],
                'staff_total_leave'             => $post_var['staff_total_leave'],
            );
        }
        $this->db->where('staff_id', $post_var['id']);
        $status = $this->db->update('users_leaves', $leave_data);
        //now update group
        //remove all group first

        if ($status) {
            $this->ion_auth->remove_from_group(false, $post_var['id']);
            foreach ($post_var['role'] as $group_id) {
                $this->ion_auth->add_to_group($group_id, $post_var['id']);
            }

            //echo 'status is ' . $registered;
            echo json_encode(array("status" => true));
        } else {
            echo json_encode(array("status" => false));
        }
    }

    public function ajax_add()
    {
        $this->_validate_new_user();
        $post_var = $this->input->post();

        $username = 'user' . date('YmdHis');
        $password = $post_var['password'];
        $email    = $post_var['email'];

        $additional_data = array(
            'first_name'                    => $post_var['first_name'],
            'phone'                         => $post_var['phone'],
            'staff_id'                      => $post_var['staff_id'],
            'staff_employee_status_id'      => $post_var['staff_employee_status_id'],
            'staff_position_id'             => $post_var['staff_position_id'],
            'staff_department_id'           => $post_var['staff_department_id'],
            'staff_level_id'                => $post_var['staff_level_id'],
            'updated_timestamp'             => date('Y-m-d H:i:s'),
            'updated_by'                    => $this->data['auth_info']->email,
        );
        $group = $post_var['role'];

        $id = $this->ion_auth->register($username, $password, $email, $additional_data, $group);
        if($id){
            $leave_data = array(
                'staff_id'                      => $id,
                'staff_annual_leave'            => $post_var['staff_annual_leave_new'],
                'staff_emergency_leave'         => $post_var['staff_emergency_leave_new'],
                'staff_forwarded_leave'         => $post_var['staff_forwarded_leave_new'],
                'staff_balance_leave'           => $post_var['staff_total_leave_new'],
                'staff_mc'                      => $post_var['staff_mc_new'],
                'staff_maternity_leave'         => $post_var['staff_maternity_leave_new'],
                'staff_total_leave'             => $post_var['staff_total_leave_new'],
            );
        }
        $status = $this->db->insert('users_leaves',$leave_data);
        
        if ($status) {
            echo json_encode(array("status" => true));
        } else {
            echo json_encode(array("status" => false));
        }
    }


    private function _validate()
    {
        $data                 = array();
        $data['error_string'] = array();
        $data['inputerror']   = array();
        $data['status']       = true;

        if (trim($this->input->post('email')) == '') {
            $data['inputerror'][]   = 'email';
            $data['error_string'][] = 'Email is required';
            $data['status']         = false;
        }

        if (trim($this->input->post('first_name')) == '') {
            $data['inputerror'][]   = 'first_name';
            $data['error_string'][] = 'User Name is required';
            $data['status']         = false;
        }

        if ($this->input->post('role') == '') {
            $data['inputerror'][]   = 'role[]';
            $data['error_string'][] = 'Please select role';
            $data['status']         = false;
        }

        if (trim($this->input->post('staff_id')) == '') {
            $data['inputerror'][]   = 'staff_id';
            $data['error_string'][] = 'Staff ID is required';
            $data['status']         = false;
        }
       


        if ($data['status'] === false) {
            echo json_encode($data);
            exit();
        }
    }

    private function _validate_new_user()
    {
        $data                 = array();
        $data['error_string'] = array();
        $data['inputerror']   = array();
        $data['status']       = true;

        if (trim($this->input->post('email')) == '') {
            $data['inputerror'][]   = 'email';
            $data['error_string'][] = 'Email is required';
            $data['status']         = false;
        }

        if (trim($this->input->post('first_name')) == '') {
            $data['inputerror'][]   = 'first_name';
            $data['error_string'][] = 'User Name is required';
            $data['status']         = false;
        }

        if (trim($this->input->post('password')) == '') {
            $data['inputerror'][]   = 'password';
            $data['error_string'][] = 'Password is required';
            $data['status']         = false;
        }

        if (trim($this->input->post('confirm_password')) == '') {
            $data['inputerror'][]   = 'confirm_password';
            $data['error_string'][] = 'Confirmation password is required';
            $data['status']         = false;
        }

        if (trim($this->input->post('confirm_password')) != trim($this->input->post('password'))) {
            $data['inputerror'][]   = 'password';
            $data['error_string'][] = 'password not match';
            $data['status']         = false;
        }

        if ($this->input->post('role') == '') {
            $data['inputerror'][]   = 'role[]';
            $data['error_string'][] = 'Please select role';
            $data['status']         = false;
        }

        //check if email has been used or not
        $email = trim($this->input->post('email'));
        if ($this->ion_auth->email_check($email) == true) {
            $data['inputerror'][]   = 'email';
            $data['error_string'][] = 'Email already registered';
            $data['status']         = false;
        }

        if (trim($this->input->post('staff_id')) == '') {
            $data['inputerror'][]   = 'staff_id';
            $data['error_string'][] = 'Staff ID is required';
            $data['status']         = false;
        }
        if (trim($this->input->post('staff_employee_status_id')) == '') {
            $data['inputerror'][]   = 'staff_employee_status_id';
            $data['error_string'][] = 'Employee type is required';
            $data['status']         = false;
        }
        if (trim($this->input->post('staff_position_id')) == '') {
            $data['inputerror'][]   = 'staff_position_id';
            $data['error_string'][] = 'Position is required';
            $data['status']         = false;
        }
        if (trim($this->input->post('staff_department_id')) == '') {
            $data['inputerror'][]   = 'staff_department_id';
            $data['error_string'][] = 'Department is required';
            $data['status']         = false;
        }
        if (trim($this->input->post('staff_level_id')) == '') {
            $data['inputerror'][]   = 'staff_level_id';
            $data['error_string'][] = 'Level is required';
            $data['status']         = false;
        }

        if ($data['status'] === false) {
            echo json_encode($data);
            exit();
        }
    }

    public function generate_xls_file()
    {

        $this->db->select('staff_id as staff_id,first_name as staff_name, email as staff_email');
        $this->db->where_not_in('username','administrator');
        $this->db->where('active',1);
        $query  = $this->db->get('users');
        $result = $query->result_array();
        // print_r($result);

        $template_file = FCPATH . 'report_template/user_report.xlsx';
        $spreadsheet   = \PhpOffice\PhpSpreadsheet\IOFactory::load($template_file);
        $worksheet     = $spreadsheet->getActiveSheet();
        $worksheet->getCell('C2')->setValue(date('Y'));
        $row_location = 6;

        $no = 1;
        foreach ($result as $row) {
            $worksheet->getCell('B' . $row_location)->setValue($no);
            $worksheet->getCell('C' . $row_location)->setValue($row['staff_id']);
            $worksheet->getCell('D' . $row_location)->setValue($row['staff_name']);
            $worksheet->getCell('E' . $row_location)->setValue($row['staff_email']);


            $row_location = $row_location + 1;
            $no           = $no + 1;
        }

        $writer   = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $filename = 'list_users';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output'); // download file 

    }

}
