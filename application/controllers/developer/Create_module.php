<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Create_module extends MY_Controller
{
    private $current_model;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('developer/create_module_model');
        $this->current_model            = $this->create_module_model;
        $this->current_model->auth_info = $this->data['auth_info'];
    }

    public function index()
    {
        $this->load->view('standard/header_open', $this->data);
        $this->load->view('dependency/style/datatable');
        $this->load->view('dependency/style/selectize');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('developer/create_module/create_module');
        $this->load->view('standard/footer_open');
        //load script dependency
        $this->load->view('dependency/script/datatable');
        $this->load->view('dependency/script/selectize');
        $this->load->view('dependency/script/moment');
        $this->load->view('standard/footer_close');
    }

    public function generate_module(){
        $post_var = $this->input->post();
        $module_name = $post_var['module_name'];
        $status = $this->current_model->generate_controller_file($module_name);
        if ($status == true){
            echo 'Controller for ' . $module_name . ' is generated <BR>'; 
        }else{
            echo 'Failed to generate controller file <BR>';
        }

        $status = $this->current_model->generate_model_file($module_name);
        if ($status == true){
            echo 'Model file for ' . $module_name . ' is generated <BR>'; 
        }else{
            echo 'Failed to generate model file <BR>';
        }

        $status = $this->current_model->generate_view_file($module_name);
        if ($status == true){
            echo 'View file for ' . $module_name . ' is generated <BR>'; 
        }else{
            echo 'Failed to generate view file <BR>';
        }


        // print_r ($post_var);
    }

    public function copy_module(){
        $post_var = $this->input->post();
        $new_module_name = $post_var['new_module_name'];
        $copy_from_module = $post_var['copy_from_module'];

        $status = $this->current_model->copy_controller_file($new_module_name, $copy_from_module);
        if ($status == true){
            echo 'Controller for ' . $new_module_name . ' is generated based on ' .$copy_from_module . '<BR>'; 
        }else{
            echo 'Failed to generate controller file <BR>';
        }

        $status = $this->current_model->copy_model_file($new_module_name, $copy_from_module);
        if ($status == true){
            echo 'Model for ' . $new_module_name . ' is generated based on ' .$copy_from_module . '<BR>'; 
        }else{
            echo 'Failed to generate model file <BR>';
        }

        $status = $this->current_model->copy_view_file($new_module_name, $copy_from_module);
        if ($status == true){
            echo 'View for ' . $new_module_name . ' is generated based on ' .$copy_from_module . '<BR>'; 
        }else{
            echo 'Failed to generate view file <BR>';
        }

        
    }

}