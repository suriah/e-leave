<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Create_migration extends MY_Controller
{
    private $current_model;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('developer/create_migration_model');
        $this->current_model            = $this->create_migration_model;
        $this->current_model->auth_info = $this->data['auth_info'];
    }

    public function index()
    {
        $this->data['migration_list'] = $this->current_model->list_migration();
        $this->data['table_list'] = $this->current_model->list_table();

        $this->load->view('standard/header_open', $this->data);
        $this->load->view('dependency/style/datatable');
        $this->load->view('dependency/style/selectize');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('developer/create_migration/create_migration');
        $this->load->view('standard/footer_open');
        //load script dependency
        $this->load->view('dependency/script/datatable');
        $this->load->view('dependency/script/selectize');
        $this->load->view('dependency/script/moment');
        $this->load->view('standard/footer_close');
    }

    public function create_migration_file(){
        $post_var = $this->input->post();
        // print_r ($post_var);
        $status = $this->current_model->create_migration_file($post_var['table_name'], $post_var['migration_type']);
        if ($status == true) {
            echo 'Migration file generated';
        }else{
            echo 'Failed to generated migration file';
        }
    }

    public function run_migration_file(){
        $post_var = $this->input->post();
        $this->current_model->update_migration_config($post_var['migration_file']);
        redirect(base_url('migrate') , 'refresh');
        // print_r ($post_var);
    }

}