<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('standard/header_open', $this->data);
        $this->load->view('dependency/style/datatable');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('developer/settings/settings');
        $this->load->view('standard/footer_open');
        $this->load->view('standard/footer_close');
    }

    public function update_settings() {
        //update setting information
        $form_var = $this->input->post();
        
        //need to pass data setting to model becoz model do not have access to this variable
        $result = $this->Settings_model->update_system_settings($form_var, $this->data['settings']);
        
//        if ($result == true){
//            $this->session->set_flashdata('status', 'true');
//            $this->session->set_flashdata('message', 'Profile update successful');
//            
//        }else{
//            $this->session->set_flashdata('status', 'failed');
//            $this->session->set_flashdata('message', 'Failed to update profile');
//            
//        }
//        exit();
        redirect(base_url('developer/settings'));
    }

}
