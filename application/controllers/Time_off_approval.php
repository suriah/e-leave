<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Time_off_approval extends MY_Controller
{
    private $current_model;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Time_off_approval_model');
        $this->load->model('Users_model');
        $this->user_model = $this->Users_model;
        $this->current_model = $this->Time_off_approval_model;
        $this->current_model->auth_info = $this->data['auth_info'];
        
    }

    public function index()
    {
        $this->data['manager_list'] = $this->current_model->list_manager_drop_down();
        $this->data['user_data'] = $this->data['auth_info'];
        
        $this->load->view('standard/header_open', $this->data);
        //load style dependency
        $this->load->view('dependency/style/datetimepicker');
        $this->load->view('dependency/style/datatable');
        $this->load->view('dependency/style/selectize');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('time_off_approval/list_time_off_approval');
        $this->load->view('standard/footer_open');
        //load script dependency
        $this->load->view('dependency/script/moment');
        $this->load->view('dependency/script/datetimepicker');
        $this->load->view('dependency/script/datatable');
        $this->load->view('dependency/script/selectize');
        $this->load->view('standard/footer_close');
    }

    public function ajax_list()
    {
        $post_var = $this->input->post();

        
        $filter = array(
            'manager_id =' => $this->data['auth_info']->id,
            'time_off_status =' => 'REQ_APPROVAL',
        );
     
        $this->current_model->filter = $filter;
        
        $list = $this->current_model->get_datatables($post_var);
        $data = array();
        $no   = $post_var['start'];
        
        foreach ($list as $record) {
            $time_off_date = date_create($record->time_off_date);
            $time_off_time_start = date_create($record->time_off_time_start);
            $time_off_time_end = date_create($record->time_off_time_end);

            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = $record->staff_name;
            $row[] = $record->staff_email;
            $row[] = $time_off_date->format('d-m-Y');
            $row[] = $time_off_time_start->format('h:i A') .' - '. $time_off_time_end->format('h:i A') ;
            $row[] = $record->time_off_reason;
            $row[] = $record->time_off_status;

            $view_button = '<a class="btn btn-sm btn-primary" href="javascript:void(0)"
                title="View" onclick="view_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-eye"></i> </a>';
            $process_button = '<a class="btn btn-sm btn-warning" href="javascript:void(0)"
                title="Process" onclick="process_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-loader"></i> </a>';

            if($record->time_off_status == 'REQ_APPROVAL'){
                $row[]  = $process_button;
            }else{
                $row[]  = $view_button ;
            }
            $data[] = $row;
        }

        $output = array(
            "draw"            => $post_var['draw'],
            "recordsTotal"    => $this->current_model->count_all(),
            "recordsFiltered" => $this->current_model->count_filtered($post_var),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    

    public function ajax_approve_application($id)
    {
        $post_var = $this->input->post();
            $data = array(
                'time_off_status'           => 'APPROVED',
                'manager_is_approved'       => true,
                'updated_timestamp'         => date('Y-m-d H:i:s'),
                'updated_by'                => $this->data['auth_info']->email,
            );

            //approval email
            $this->current_model->time_off_approved_notification_email($id);
        
        $where  = array('id' => $post_var['id']);
        $status = $this->current_model->update($where, $data);
        // $status = $this->current_model->email_notification($id,$data);
        echo json_encode(array("status" => $status));
    }
    
    public function ajax_reject_application($id)
    {
        $post_var = $this->input->post();
        
            $data = array(
                'time_off_status'           => 'REJECTED',
                'manager_is_approved'       => false,
                'updated_timestamp'         => date('Y-m-d H:i:s'),
                'updated_by'                => $this->data['auth_info']->email,
            );
        
        $where  = array('id' => $post_var['id']);
        $status = $this->current_model->update($where, $data);
        $status =$this->current_model->time_off_rejected_notification_email($id);
        echo json_encode(array("status" => $status));
    }

    public function ajax_view_process_data($id)
    {
        $data = $this->current_model->view_process_data($id);
        echo json_encode($data);
    }
}
