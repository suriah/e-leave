<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Time_off_request extends MY_Controller
{
    private $current_model;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Time_off_request_model');
        $this->current_model = $this->Time_off_request_model;
        $this->current_model->auth_info = $this->data['auth_info'];
    }

    public function index()
    {
        if ($this->ion_auth->is_admin())
        {
            $this->data['user_type'] = 'admin';
        }else{
            $this->data['user_type'] = 'user';
        }
        
        $group = $this->ion_auth->groups()->result();

        $this->data['manager_list'] = $this->current_model->list_manager_drop_down($group);
        $this->load->view('standard/header_open', $this->data);
        $this->load->view('standard/header_open', $this->data);
        //load style dependency
        $this->load->view('dependency/style/datetimepicker');
        $this->load->view('dependency/style/datatable');
        $this->load->view('dependency/style/selectize');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('time_off_request/list_time_off_request');
        $this->load->view('standard/footer_open');
        //load script dependency
        $this->load->view('dependency/script/moment');
        $this->load->view('dependency/script/datetimepicker');
        $this->load->view('dependency/script/datatable');
        $this->load->view('dependency/script/selectize');
        $this->load->view('standard/footer_close');
    }

    public function ajax_list()
    {
        $post_var = $this->input->post();

        $filter = array(
            'staff_id =' => $this->data['auth_info']->id,
        );
        $this->current_model->filter = $filter;
        $list = $this->current_model->get_datatables($post_var);
        $data = array();
        $no   = $post_var['start'];

        foreach ($list as $record) {

            $time_off_date = date_create($record->time_off_date);
            $time_off_time_start = date_create($record->time_off_time_start);
            $time_off_time_end = date_create($record->time_off_time_end);

            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = $time_off_date->format('d-m-Y');
            $row[] = $time_off_time_start->format('h:i A') .' - '. $time_off_time_end->format('h:i A') ;
            $row[] = $record->time_off_reason;

            $view_button = '<a class="btn btn-sm btn-primary" href="javascript:void(0)"
            title="View" onclick="view_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-eye"></i> </a>';
            $edit_button = '<a class="btn btn-sm btn-secondary" href="javascript:void(0)"
            title="Edit" onclick="edit_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-edit"></i> </a>';
            $submit_button = '<a class="btn btn-sm btn-success" href="javascript:void(0)"
            title="Pending Submit" onclick="submit_record_dialog(' . "'" . $record->id . "'" . ')"><i class="ft ft-navigation"></i> </a>';
            $delete_button = '<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete"
            onclick="delete_record_dialog(' . "'" . $record->id . "'" . ')"><i class="ft ft-trash-2"></i> </a>';
            
            if($record->time_off_status == 'PENDING'){
                $row[] = $record->time_off_status;
                $row[]  = $edit_button . ' ' . $submit_button. ' ' . $delete_button;
            }else if ($record->time_off_status == 'REQ_APPROVAL'){
                $row[] = 'REQ_APPROVAL';
                $row[]  = $view_button ;
            }else{
                $row[] = $record->time_off_status;
                $row[]  = $view_button ;
            }
            $data[] = $row;
        }

        $output = array(
            "draw"            => $post_var['draw'],
            "recordsTotal"    => $this->current_model->count_all(),
            "recordsFiltered" => $this->current_model->count_filtered($post_var),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_upsert()
    {
        $this->_validate();
        $post_var = $this->input->post();

        $time_off_date = $post_var['time_off_date'];
        $time_off_time_start = $post_var['time_off_time_start'];
        $time_off_time_end = $post_var['time_off_time_end'];

        if (empty($post_var['id'])) {
            $data = array(
                'time_off_date'      => date("Y-m-d", strtotime($time_off_date)),
                'time_off_time_start'=> date("Y-m-d H:i:s", strtotime($time_off_time_start)),
                'time_off_time_end'  => date("Y-m-d H:i:s", strtotime($time_off_time_end)),
                'time_off_reason'    => $post_var['time_off_reason'],
                'time_off_status'   => 'PENDING',
                'staff_id'           => $this->data['auth_info']->id,
                'manager_id'         => $post_var['manager_id'],
                'created_timestamp'  => date('Y-m-d H:i:s'),
                'created_by'         => $this->data['auth_info']->email,
            );

            $status = $this->current_model->insert($data);
        } else {
            $data = array(
                'time_off_date'      => date("Y-m-d", strtotime($time_off_date)),
                'time_off_time_start'=> date("Y-m-d H:i:s", strtotime($time_off_time_start)),
                'time_off_time_end'  => date("Y-m-d H:i:s", strtotime($time_off_time_end)),
                'time_off_reason'    => $post_var['time_off_reason'],
                'manager_id'         => $post_var['manager_id'],
                'updated_timestamp' => date('Y-m-d H:i:s'),
                'updated_by'        => $this->data['auth_info']->email,
            );

            $where  = array('id' => $post_var['id']);
            $status = $this->current_model->update($where, $data);
        }

        if ($status) {
            //echo 'status is ' . $registered;
            echo json_encode(array("status" => true));
        } else {
            echo json_encode(array("status" => false));
        }
    }

    public function ajax_view($id)
    {
        $data = $this->current_model->get_by_id($id);
        echo json_encode($data);
    }

    public function ajax_submit($id)
    {

        $new_status = 'REQ_APPROVAL';

        $data = array(
            'time_off_status'           => $new_status,
            'updated_timestamp'         => date('Y-m-d H:i:s'),
            'updated_by'                => $this->data['auth_info']->email,
        );

        $where  = array('id' => $id);
        $status = $this->current_model->update($where, $data);

        // $status = $this->current_model->time_off_request_notification_email($id,$new_status);
        
        echo json_encode(array("status" => $status));
    }

    private function _validate()
    {
        $data                 = array();
        $data['error_string'] = array();
        $data['inputerror']   = array();
        $data['status']       = true;

        if (trim($this->input->post('time_off_date')) == '') {
            $data['inputerror'][]   = 'time_off_date';
            $data['error_string'][] = 'Date is required';
            $data['status']         = false;
        }
        if (trim($this->input->post('time_off_time_start')) == '') {
            $data['inputerror'][]   = 'time_off_time_start';
            $data['error_string'][] = 'Time is required';
            $data['status']         = false;
        }
        if (trim($this->input->post('time_off_time_end')) == '') {
            $data['inputerror'][]   = 'time_off_time_end';
            $data['error_string'][] = 'Time is required';
            $data['status']         = false;
        }
        if (trim($this->input->post('time_off_reason')) == '') {
            $data['inputerror'][]   = 'time_off_reason';
            $data['error_string'][] = 'Reasons is required';
            $data['status']         = false;
        }
        if (trim($this->input->post('manager_id')) == '') {
            $data['inputerror'][]   = 'manager_id';
            $data['error_string'][] = 'Manager is required';
            $data['status']         = false;
        }

        if ($data['status'] === false) {
            echo json_encode($data);
            exit();
        }
    }

    public function ajax_delete($id)
    {
        $status = $this->current_model->delete_by_id($id);
        echo json_encode(array("status" => $status));
    }

    public function ajax_edit($id)
    {
        $data = $this->current_model->get_by_id($id);
        echo json_encode($data);
    }
}
