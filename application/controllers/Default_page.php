<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Default_page extends CI_Controller {

    protected $data;
    public function __construct() {
        parent::__construct();
        $this->load->model('developer/Settings_model');
    }

    public function index() {
        /*
          $data['logged_in'] = $this->_check_logged_in();
          $data['admin'] = $this->_check_admin();        
         */
        $domain_array = explode('.', $_SERVER['HTTP_HOST']);
        $sub_domain = $domain_array[0];        
        $data['settings'] = $this->Settings_model->get_system_settings($sub_domain);
        $this->load->view('simple/header', $data);
        $this->load->view('authentication/login');
        $this->load->view('simple/footer');
    }

    private function _check_logged_in() {
        if ($this->ion_auth->logged_in()) {
            return true;
        } else {
            return false;
        }
    }

    private function _check_admin() {
        if ($this->ion_auth->is_admin()) {
            return true;
        } else {
            return false;
        }
    }
    
    public function forgot_password_form()
    {
        $domain_array = explode('.', $_SERVER['HTTP_HOST']);
        $sub_domain = $domain_array[0];        
        $data['settings'] = $this->Settings_model->get_system_settings($sub_domain);
        //this function provide form for forgot password
        $this->load->view('simple/header', $data);
        $this->load->view('authentication/forgot_password_email');
        $this->load->view('simple/footer');     
    }
    
    public function forgot_password()
    {
        //this function execute 
        // 1. Send email with code
        // 2. Direct to reset password form
        
        $email = $this->input->post('email');
        //send email        
        $this->load->model('Authentication_model');
        $this->Authentication_model->forgot_password_send_email($email);
        //exit();
        redirect('default_page/forgot_password_reset_form', 'refresh');
    }
    
    public function forgot_password_reset_form()
    {
        $domain_array = explode('.', $_SERVER['HTTP_HOST']);
        $sub_domain = $domain_array[0];        
        $data['settings'] = $this->Settings_model->get_system_settings($sub_domain);
        //this function provide form for forgot password
        $this->load->view('simple/header', $data);
        $this->load->view('authentication/forgot_password_code');
        $this->load->view('simple/footer');     
    }
    
    public function reset_password()
    {
        $domain_array = explode('.', $_SERVER['HTTP_HOST']);
        $sub_domain = $domain_array[0];        
        $data['settings'] = $this->Settings_model->get_system_settings($sub_domain);

        $form_var = $this->input->post();
        $this->load->model('Authentication_model');
        if ($this->Authentication_model->forgot_password_reset($form_var) == true)
        {
            //success
            $message = 'Reset password successful, please check your email to retrieve new password.Click <a href="' .
                    base_url() . '">here</a> to login.';
        }else{
            //failed
            $message = 'Reset password failed, please <a href="' . base_url() . 'default_page/forgot_password_reset_form" >retry</a> again';
        }
        // $this->data = array('message' => $message);
        $data['message'] = $message;

        $this->load->view('simple/header', $data);
        $this->load->view('authentication/reset_password_status');
        $this->load->view('simple/footer');
    }
    
    
    
    
}

