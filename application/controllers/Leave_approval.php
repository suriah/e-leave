<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Leave_approval extends MY_Controller
{
    private $current_model;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Leave_approval_model');
        $this->load->model('Users_model');
        $this->user_model = $this->Users_model;
        $this->current_model = $this->Leave_approval_model;
        $this->current_model->auth_info = $this->data['auth_info'];
        
    }

    public function index()
    {
        $this->data['leave_type_list'] = $this->current_model->list_leave_type_drop_down();
        $this->data['supervisor_list'] = $this->current_model->list_supervisor_drop_down();
        $this->data['manager_list'] = $this->current_model->list_manager_drop_down();
        $this->data['user_data'] = $this->data['auth_info'];
        
        $this->load->view('standard/header_open', $this->data);
        //load style dependency
        $this->load->view('dependency/style/datetimepicker');
        $this->load->view('dependency/style/datatable');
        $this->load->view('dependency/style/selectize');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('leave_approval/list_leave_approval');
        $this->load->view('standard/footer_open');
        //load script dependency
        $this->load->view('dependency/script/moment');
        $this->load->view('dependency/script/datetimepicker');
        $this->load->view('dependency/script/datatable');
        $this->load->view('dependency/script/selectize');
        $this->load->view('standard/footer_close');
    }

    public function ajax_list()
    {
        $post_var = $this->input->post();

        $user_id = $this->data['auth_info']->id;

        $filter= "(manager_id =  $user_id OR supervisor_id =  $user_id)";
        $this->db->where($filter);
        $list = $this->current_model->get_datatables($post_var);
        $data = array();
        $no   = $post_var['start'];
        
        foreach ($list as $record) {


            $date_from = strtotime($record->leave_date_from);
            $date_to = strtotime($record->leave_date_to);
            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = $record->staff_name;
            $row[] = $record->staff_email;
            $row[] = $record->leave_name;
            $row[] = date("d-m-Y", $date_from);
            $row[] = date("d-m-Y", $date_to);
            $row[] = $record->total_leave;
            $row[] = $record->leave_status;

            $view_button = '<a class="btn btn-sm btn-primary" href="javascript:void(0)"
                title="View" onclick="view_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-eye"></i> </a>';
            $process_button = '<a class="btn btn-sm btn-warning" href="javascript:void(0)"
                title="Process" onclick="process_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-loader"></i> </a>';

            if($record->leave_status == 'REQ_APPROVAL_SV' || $record->leave_status == 'REQ_APPROVAL_MAN'){
                $row[]  = $process_button;
            }else{
                $row[]  = $view_button ;
            }
            $data[] = $row;
        }

        $output = array(
            "draw"            => $post_var['draw'],
            "recordsTotal"    => $this->current_model->count_all(),
            "recordsFiltered" => $this->current_model->count_filtered($post_var),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_upsert()
    {
        $this->_validate();
        $post_var = $this->input->post();
        // print_r($post_var);die;
        
        $date_from = $post_var['date_from'];
        $date_to = $post_var['date_to'];

        if (empty($post_var['id'])) {

            $data = array(
                'leave_type_id'                => $post_var['leave_type_id'],
                'leave_date_from'           => date("Y-m-d", strtotime($date_from)),
                'leave_date_to'             => date("Y-m-d", strtotime($date_to)),
                'total_leave'               => $post_var['total_leave'],
                'leave_reason'              => $post_var['leave_reason'],
                'leave_status'              => 'PENDING',
                'staff_id'                  => $this->data['auth_info']->id,
                'supervisor_id'             => empty($post_var['supervisor_id']) ? null: $post_var['supervisor_id'],
                'manager_id'                => $post_var['manager_id'],
                'created_timestamp'         => date('Y-m-d H:i:s'),
                'created_by'                => $this->data['auth_info']->email,
            );

            $status = $this->current_model->insert($data);
        } else {
            $data = array(
                'leave_type_id'             => $post_var['leave_type_id'],
                'leave_date_from'           => date("Y-m-d", strtotime($date_from)),
                'leave_date_to'             => date("Y-m-d", strtotime($date_to)),
                'total_leave'               => $post_var['total_leave'],
                'leave_reason'              => $post_var['leave_reason'],
                // 'leave_status'            => $post_var['leave_status'],
                // 'staff_id'                => $this->data['auth_info']->id,
                'supervisor_id'             => empty($post_var['supervisor_id']) ? null: $post_var['supervisor_id'],
                'manager_id'                => $post_var['manager_id'],
                'updated_timestamp'         => date('Y-m-d H:i:s'),
                'updated_by'                => $this->data['auth_info']->email,
            );

            $where  = array('id' => $post_var['id']);
            $status = $this->current_model->update($where, $data);
        }

        if ($status) {
            //echo 'status is ' . $registered;
            echo json_encode(array("status" => true));
        } else {
            echo json_encode(array("status" => false));
        }
    }

    private function _validate()
    {
        $data                 = array();
        $data['error_string'] = array();
        $data['inputerror']   = array();
        $data['status']       = true;

        if (trim($this->input->post('decline_reason')) == '') {
            $data['inputerror'][]   = 'decline_reason';
            $data['error_string'][] = 'Reasons required for rejecting the application';
            $data['status']         = false;
        }
       

        if ($data['status'] === false) {
            echo json_encode($data);
            exit();
        }
    }

    public function ajax_delete($id)
    {
        $status = $this->current_model->delete_by_id($id);
        echo json_encode(array("status" => $status));
    }

    public function ajax_approve_application($id)
    {
        $post_var = $this->input->post();
        
        $current_status = $this->current_model->check_current_status($id);
        if($current_status->leave_status == 'REQ_APPROVAL_SV'){
            $data = array(
                'leave_status'              => 'REQ_APPROVAL_MAN',
                'supervisor_is_approved'    => true,
                'updated_timestamp'         => date('Y-m-d H:i:s'),
                'updated_by'                => $this->data['auth_info']->email,
            );
            // to notify the manager on approval 
            $this->current_model->leave_request_notification_email_superior($id);
        }else if($current_status->leave_status == 'REQ_APPROVAL_MAN'){
            $status = $this->current_model->update_users_leaves($id);
            $date = $this->current_model->add_leave_date($id);
            $leave_type_id = $this->current_model->get_leave_type($id);
            foreach ($date as $key => $value) {
                $data = array(
                    'leave_id'            => $id,
                    'leave_date'          => $value,
                    'leave_type_id'       => $leave_type_id ,
                    'created_timestamp'   => date('Y-m-d H:i:s'),
                    'created_by'          => $this->data['auth_info']->email,
                );
                $status = $this->db->insert('leave_date',$data);
            }

            if($status){

                $data = array(
                    'leave_status'              => 'APPROVED',
                    'manager_is_approved'       => true,
                    'updated_timestamp'         => date('Y-m-d H:i:s'),
                    'updated_by'                => $this->data['auth_info']->email,
                );
    
                //approval email
                $this->current_model->leave_approved_notification_email($id);
            }

        }
        $where  = array('id' => $post_var['id']);
        $status = $this->current_model->update($where, $data);
        // $status = $this->current_model->email_notification($id,$data);
        echo json_encode(array("status" => $status));
    }
    public function ajax_reject_application($id)
    {
        $this->_validate();
        $post_var = $this->input->post();
        
        $current_status = $this->current_model->check_current_status($id);
        if($current_status->leave_status == 'REQ_APPROVAL_SV'){

            $data = array(
                'leave_status'              => 'REJECTED',
                'supervisor_is_approved'    => false,
                'supervisor_decline_reason' => $post_var['decline_reason'],
                'updated_timestamp'         => date('Y-m-d H:i:s'),
                'updated_by'                => $this->data['auth_info']->email,
            );
        }else{
            $data = array(
                'leave_status'              => 'REJECTED',
                'manager_is_approved'       => false,
                'manager_decline_reason'    => $post_var['decline_reason'],
                'updated_timestamp'         => date('Y-m-d H:i:s'),
                'updated_by'                => $this->data['auth_info']->email,
            );
        }
        $where  = array('id' => $post_var['id']);
        $status = $this->current_model->update($where, $data);
        $status =$this->current_model->leave_rejected_notification_email($id,$post_var['decline_reason']);
        echo json_encode(array("status" => $status));
    }

    public function ajax_edit($id)
    {
        $data = $this->current_model->get_by_id($id);
        echo json_encode($data);
    }
    public function ajax_view_process_data($id)
    {
        $data = $this->current_model->view_process_data($id);
        echo json_encode($data);
    }

    public function download_file($id)
    {
        $this->db->where('id',$id);
        $query=$this->db->get('leave');
        $row = $query->row();
        $data = file_get_contents(FCPATH.'upload/leave_supporting_document/'. $id);
        $name = $row->supporting_document;
        force_download($name,$data);
    }
}