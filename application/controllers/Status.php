<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Status extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Status_model');
        
        $this->current_model = $this->Status_model;
    }

    public function index()
    {
        $this->load->model('Settings_model');
        $this->data['settings'] = $this->Settings_model->get_system_settings();
        $this->load->view('standard/header_open', $this->data);
        // //load style dependency
        $this->load->view('dependency/style/datatable');
        $this->load->view('dependency/style/selectize');
        $this->load->view('dependency/style/datetimepicker');
        $this->load->view('standard/header_close');
        
        $this->load->view('registration/list_register_status');
        $this->load->view('standard/footer_open');
        // //load script dependency
        $this->load->view('dependency/script/datatable');
        $this->load->view('dependency/script/moment');
        $this->load->view('dependency/script/datetimepicker');
        $this->load->view('dependency/script/selectize');
        $this->load->view('standard/footer_close');

        // $this->load->view('simple/header', $this->data);
        // $this->load->view('registration/list_register_status');
        // $this->load->view('simple/footer');
    }

    public function ajax_check()
    {
        $this->_validate();
        $post_var = $this->input->post();
        
        $data = $this->current_model->_check_data($post_var['ic_no']);

        if($data['status'] == 'false'){
            $this->_validate_no_record($data);
        } else{
            // $this->data['data_member'] = $data_in_registration;
            echo json_encode($data);
        }
    }
   


    private function _validate()
    {
        $data                 = array();
        $data['error_string'] = array();
        $data['inputerror']   = array();
        $data['status']       = true;

        if (trim($this->input->post('ic_no')) == '') {
            $data['inputerror'][]   = 'ic_no';
            $data['error_string'][] = 'Required';
            $data['status']         = false;
        }

        if ($data['status'] === false) {
            echo json_encode($data);
            exit();
        }
    }
    private function _validate_no_record($data_member)
    {
        $data                 = array();
        $data['error_string'] = array();
        $data['inputerror']   = array();
        $data['status']       = true;

        if (trim($data_member['status']) == 'false') {
            $data['inputerror'][]   = 'ic_no';
            $data['error_string'][] = 'No record found in the system. Please register as a member';
            $data['status']         = false;
        }

        if ($data['status'] === false) {
            echo json_encode($data);
            exit();
        }
    }
}