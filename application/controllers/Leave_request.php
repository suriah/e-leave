<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Leave_request extends MY_Controller
{
    private $current_model;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Leave_request_model');
        $this->current_model = $this->Leave_request_model;
        $this->current_model->auth_info = $this->data['auth_info'];
        
    }

    public function index()
    {
        $group = $this->ion_auth->groups()->result();
        $this->data['leave_type_list'] = $this->current_model->list_leave_type_drop_down();
        $this->data['supervisor_list'] = $this->current_model->list_supervisor_drop_down($group);
        $this->data['manager_list'] = $this->current_model->list_manager_drop_down($group);
        $this->data['superior_list'] = $this->current_model->list_superior_drop_down($group);
        
        $this->load->view('standard/header_open', $this->data);
        //load style dependency
        $this->load->view('dependency/style/datetimepicker');
        $this->load->view('dependency/style/datatable');
        $this->load->view('dependency/style/selectize');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('leave_request/list_leave_request');
        $this->load->view('standard/footer_open');
        //load script dependency
        $this->load->view('dependency/script/moment');
        $this->load->view('dependency/script/datetimepicker');
        $this->load->view('dependency/script/datatable');
        $this->load->view('dependency/script/selectize');
        $this->load->view('standard/footer_close');
    }

    public function get_user_level(){
        $id = $this->data['auth_info']->id;
        $data = $this->current_model->get_user_level($id);
        
        echo json_encode($data);
    }

    public function ajax_list()
    {
        $post_var = $this->input->post();
        $filter = array(
            'staff_id =' => $this->data['auth_info']->id,
        );
        $this->current_model->filter = $filter;
        
        $list = $this->current_model->get_datatables($post_var);
        $data = array();
        $no   = $post_var['start'];
        
        foreach ($list as $record) {

            $date_from = strtotime($record->leave_date_from);
            $date_to = strtotime($record->leave_date_to);
            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = $record->leave_name;
            $row[] = date("d-m-Y", $date_from);
            $row[] = date("d-m-Y", $date_to);
            $row[] = $record->total_leave;
            $row[] = $record->leave_reason;
            if(isset($record->supervisor_id)){
                $row[] = $this->ion_auth->user($record->supervisor_id)->row()->first_name;
            }else{
                $row[] = null;
            }
            if(isset($record->manager_id)){
                $row[] = $this->ion_auth->user($record->manager_id)->row()->first_name;
            }else{
                $row[] = null;
            }
            
            $view_button = '<a class="btn btn-sm btn-primary" href="javascript:void(0)"
            title="View" onclick="view_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-eye"></i> </a>';
            $edit_button = '<a class="btn btn-sm btn-secondary" href="javascript:void(0)"
            title="Edit" onclick="edit_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-edit"></i> </a>';
            $submit_button = '<a class="btn btn-sm btn-success" href="javascript:void(0)"
            title="Pending Submit" onclick="submit_record_dialog(' . "'" . $record->id . "'" . ')"><i class="ft ft-navigation"></i> </a>';
            $delete_button = '<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete"
            onclick="delete_record_dialog(' . "'" . $record->id . "'" . ')"><i class="ft ft-trash-2"></i> </a>';
            
            if($record->leave_status == 'PENDING'){
                $row[] = $record->leave_status;
                $row[]  = $edit_button . ' ' . $submit_button. ' ' . $delete_button;
            }else if ($record->leave_status == 'REQ_APPROVAL_SV' || $record->leave_status == 'REQ_APPROVAL_MAN' ){
                $row[] = 'REQ_APPROVAL';
                $row[]  = $view_button ;
            }else{
                $row[] = $record->leave_status;
                $row[]  = $view_button ;
            }
            $data[] = $row;
        }

        $output = array(
            "draw"            => $post_var['draw'],
            "recordsTotal"    => $this->current_model->count_all(),
            "recordsFiltered" => $this->current_model->count_filtered($post_var),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_upsert()
    {
        $this->_validate();
        $post_var = $this->input->post();
        // print_r($post_var);die;
        
        $date_from = $post_var['date_from'];
        $date_to = $post_var['date_to'];

        if (empty($post_var['id'])) {

            $data = array(
                'leave_type_id'             => $post_var['leave_type_id'],
                'leave_date_from'           => date("Y-m-d", strtotime($date_from)),
                'leave_date_to'             => date("Y-m-d", strtotime($date_to)),
                'total_leave'               => $post_var['total_leave'],
                'leave_reason'              => $post_var['leave_reason'],
                'leave_status'              => 'PENDING',
                'staff_id'                  => $this->data['auth_info']->id,
                'has_supervisor'            => empty($post_var['supervisor_id']) ? false: true,
                'supervisor_id'             => empty($post_var['supervisor_id']) ? null: $post_var['supervisor_id'],
                'supervisor_is_approved'    => empty($post_var['supervisor_id']) ? true: false,
                'manager_id'                => $post_var['manager_id'],
                'created_timestamp'         => date('Y-m-d H:i:s'),
                'created_by'                => $this->data['auth_info']->email,
            );

            $status = $this->current_model->insert($data);
            $id = $this->db->insert_id();
        } else {
            $data = array(
                'leave_type_id'             => $post_var['leave_type_id'],
                'leave_date_from'           => date("Y-m-d", strtotime($date_from)),
                'leave_date_to'             => date("Y-m-d", strtotime($date_to)),
                'total_leave'               => $post_var['total_leave'],
                'leave_reason'              => $post_var['leave_reason'],
                'has_supervisor'            => empty($post_var['supervisor_id']) ? false: true,
                'supervisor_id'             => empty($post_var['supervisor_id']) ? null: $post_var['supervisor_id'],
                'supervisor_is_approved'    => empty($post_var['supervisor_id']) ? true: false,
                'manager_id'                => $post_var['manager_id'],
                'updated_timestamp'         => date('Y-m-d H:i:s'),
                'updated_by'                => $this->data['auth_info']->email,
            );

            $where  = array('id' => $post_var['id']);
            $status = $this->current_model->update($where, $data);
            $id = $post_var['id'];
        }

        // print_r($id);die;
        $config['allowed_types'] = '*';
        $config['max_size'] = '5120';
        $config['overwrite'] = true;
        $this->load->library('upload', $config);
        $config['upload_path'] = FCPATH . 'upload/leave_supporting_document/';

        $this->upload->initialize($config);
        if (isset($_FILES['supporting_document']) && is_uploaded_file($_FILES['supporting_document']['tmp_name'])) {
            if ($this->upload->do_upload('supporting_document')) {

                // dapatkan new file name
                $file_name = $this->upload->data('file_name');
                rename($config['upload_path'] . $file_name, $config['upload_path'] . $id);
            }
            $updated_data = array(
            'supporting_document'     => $file_name,
            );
        }

        if(isset($updated_data)){
            $where  = array('id' => $id);
            $status = $this->current_model->update($where, $updated_data);
        }
        if ($status) {
            //echo 'status is ' . $registered;
            echo json_encode(array("status" => true));
        } else {
            echo json_encode(array("status" => false));
        }
    }

    private function _validate()
    {
        $data                 = array();
        $data['error_string'] = array();
        $data['inputerror']   = array();
        $data['status']       = true;

        if (trim($this->input->post('leave_type_id')) == '') {
            $data['inputerror'][]   = 'leave_type_id';
            $data['error_string'][] = 'Type of leave is required';
            $data['status']         = false;
        }
        if (trim($this->input->post('leave_reason')) == '') {
            $data['inputerror'][]   = 'leave_reason';
            $data['error_string'][] = 'Reasons are required';
            $data['status']         = false;
        }
        if (trim($this->input->post('manager_id')) == '') {
            $data['inputerror'][]   = 'manager_id';
            $data['error_string'][] = 'Manager is required';
            $data['status']         = false;
        }

        if ($data['status'] === false) {
            echo json_encode($data);
            exit();
        }
    }

    public function ajax_submit($id)
    {
        $has_supervisor = $this->current_model->has_supervisor($id);

        if($has_supervisor == 't'){
            $new_status = 'REQ_APPROVAL_SV';
        }else{
            $new_status = 'REQ_APPROVAL_MAN';
        }

        $data = array(
            'leave_status'              => $new_status,
            'updated_timestamp'         => date('Y-m-d H:i:s'),
            'updated_by'                => $this->data['auth_info']->email,
        );

        $where  = array('id' => $id);
        $status = $this->current_model->update($where, $data);

        if($status){
            // $date = $this->current_model->add_leave_date($id);
            // $leave_type_id = $this->current_model->get_leave_type($id);
            // foreach ($date as $key => $value) {
            //     $data = array(
            //         'leave_id'            => $id,
            //         'leave_date'          => $value,
            //         'leave_type_id'       => $leave_type_id ,
            //         'created_timestamp'   => date('Y-m-d H:i:s'),
            //         'created_by'          => $this->data['auth_info']->email,
            //     );
            //     $status = $this->db->insert('leave_date',$data);
            // }
            $status = $this->current_model->leave_request_notification_email($id,$new_status);
        }
        echo json_encode(array("status" => $status));
    }
    public function ajax_delete($id)
    {
        $status = $this->current_model->delete_by_id($id);
        echo json_encode(array("status" => $status));
    }

    public function ajax_edit($id)
    {
        $data = $this->current_model->get_by_id($id);
        echo json_encode($data);
    }
    public function ajax_view($id)
    {
        $data = $this->current_model->get_by_id($id);
        echo json_encode($data);
    }
    public function ajax_check_public_holiday()
    {
        $post_var = $this->input->get();
        // print_r($post_var);die;
        $date_array =[];
        
        $date= $post_var['list_date'];
        if($date == 'null'){
            $data = 0;
        }else{
            $date_array=explode(",",$date);
            
            $date_array = json_encode($date_array);
            $date_array = str_replace('"',"'",$date_array);
            
            $data = $this->current_model->check_public_holiday($date_array);
        }
        echo json_encode($data);
    }
    public function ajax_check_dhearty()
    {

        $post_var = $this->input->get();
        // print_r($post_var);die;
        $date_array =[];
        
        $date= $post_var['list_date'];
        if($date == 'null'){
            $data = 0;
        }else{
            $date_array=explode(",",$date);
            
            $date_array = json_encode($date_array);
            $date_array = str_replace('"',"'",$date_array);
            
            $data = $this->current_model->check_dhearty($date_array);
        }
            

        echo json_encode($data);
    }

    public function download_file($id)
    {
        $this->db->where('id',$id);
        $query=$this->db->get('leave');
        $row = $query->row();
        $data = file_get_contents(FCPATH.'upload/leave_supporting_document/'. $id);
        $name = $row->supporting_document;
        force_download($name,$data);
    }

    public function ajax_count_total_day()
    {

        $post_var = $this->input->get();
        $data = $this->current_model->total_list_date($post_var['date_from'],$post_var['date_to']);

        echo json_encode($data);
    }
}
