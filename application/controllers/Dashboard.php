<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Dashboard_model');
    }

    public function index()
    {
        $this->data['user_dashboard_data'] = $this->Dashboard_model->load_user_dashboard_data($this->data['auth_info']);
        $this->data['admin_dashboard_data'] = $this->Dashboard_model->load_admin_dashboard_data($this->data['auth_info']);
        $this->data['superior_dashboard_data'] = $this->Dashboard_model->load_superior_dashboard_data($this->data['auth_info']);
        $this->data['event_data'] = $this->Dashboard_model->load_event_data($this->data['auth_info']);
        $this->data['user_data'] = $this->ion_auth->get_users_groups($this->data['auth_info']->id)->row();

        $this->load->view('standard/header_open', $this->data);
        $this->load->view('dependency/style/datatable');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('dashboard/dashboard');
        $this->load->view('standard/footer_open');
        $this->load->view('dependency/script/chartjs');
        $this->load->view('standard/footer_close');
    }
}
