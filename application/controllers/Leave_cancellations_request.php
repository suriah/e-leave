<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Leave_cancellations_request extends MY_Controller
{
    private $current_model;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Leave_cancellations_request_model');
        $this->current_model = $this->Leave_cancellations_request_model;
        $this->current_model->auth_info = $this->data['auth_info'];
        
    }

    public function index()
    {
        
        $this->load->view('standard/header_open', $this->data);
        //load style dependency
        $this->load->view('dependency/style/datetimepicker');
        $this->load->view('dependency/style/datatable');
        $this->load->view('dependency/style/selectize');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('leave_cancellations_request/list_leave_cancellations_request');
        $this->load->view('standard/footer_open');
        //load script dependency
        $this->load->view('dependency/script/moment');
        $this->load->view('dependency/script/datetimepicker');
        $this->load->view('dependency/script/datatable');
        $this->load->view('dependency/script/selectize');
        $this->load->view('standard/footer_close');
    }

    public function get_user_level(){
        $id = $this->data['auth_info']->id;
        $data = $this->current_model->get_user_level($id);
        
        echo json_encode($data);
    }

    public function ajax_list()
    {
        $post_var = $this->input->post();
        $filter = array(
            'staff_id =' => $this->data['auth_info']->id,
        );
        $this->current_model->filter = $filter;
        
        $list = $this->current_model->get_datatables($post_var);
        $data = array();
        $no   = $post_var['start'];
        
        foreach ($list as $record) {

            $date_from = strtotime($record->leave_date_from);
            $date_to = strtotime($record->leave_date_to);
            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = $record->leave_name;
            $row[] = date("d-m-Y", $date_from);
            $row[] = date("d-m-Y", $date_to);
            $row[] = $record->total_leave;
            $row[] = $record->leave_reason;
            if(isset($record->supervisor_id)){
                $row[] = $this->ion_auth->user($record->supervisor_id)->row()->first_name;
            }else{
                $row[] = null;
            }
            if(isset($record->manager_id)){
                $row[] = $this->ion_auth->user($record->manager_id)->row()->first_name;
            }else{
                $row[] = null;
            }
            
            $revert_button = '<a class="btn btn-sm btn-danger" href="javascript:void(0)"
            title="revert" onclick="revert_record_dialog(' . "'" . $record->id . "'" . ')"><i class="ft ft-rotate-ccw"></i> </a>';
         
            $row[] = $record->leave_status;
  
            $row[]  = $revert_button ;
            $data[] = $row;
        }

        $output = array(
            "draw"            => $post_var['draw'],
            "recordsTotal"    => $this->current_model->count_all(),
            "recordsFiltered" => $this->current_model->count_filtered($post_var),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_upsert()
    {
        $this->_validate();
        $post_var = $this->input->post();
        
            $data = array(
                
                'cancellations_reason'              => $post_var['cancellations_reason'],
                'leave_status'                      => 'REQ_CANCEL',
                'cancellations_timestamp'           => date('Y-m-d H:i:s'),
                'cancellations_by'                  => $this->data['auth_info']->email,
            );

            $id = $post_var['id'];
            $where  = array('id' => $id);
            $status = $this->current_model->update($where, $data);
      
        if ($status) {
            $status = $this->current_model->cancel_request_notification_email($id);
            echo json_encode(array("status" => true));
        } else {
            echo json_encode(array("status" => false));
        }
    }

    private function _validate()
    {
        $data                 = array();
        $data['error_string'] = array();
        $data['inputerror']   = array();
        $data['status']       = true;

       
        if (trim($this->input->post('cancellations_reason')) == '') {
            $data['inputerror'][]   = 'cancellations_reason';
            $data['error_string'][] = 'Reasons are required';
            $data['status']         = false;
        }

        if ($data['status'] === false) {
            echo json_encode($data);
            exit();
        }
    }
}
