<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Report_approval extends MY_Controller
{
    private $current_model;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Report_approval_model');
        $this->current_model = $this->Report_approval_model;
    }

    public function index()
    {
        $this->load->view('standard/header_open', $this->data);
        //load style dependency
        $this->load->view('dependency/style/datatable');
        $this->load->view('dependency/style/selectize');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('report_approval/list_report_approval');
        $this->load->view('standard/footer_open');
        //load script dependency
        $this->load->view('dependency/script/datatable');
        $this->load->view('dependency/script/selectize');
        $this->load->view('standard/footer_close');
    }

    public function ajax_list()
    {
        $post_var = $this->input->post();

        //$curent_model = $this->Manage_company_model;
        $list = $this->current_model->get_datatables($post_var);
        $data = array();
        $no   = $post_var['start'];

        $user_id = $this->data['auth_info']->id;
        
        foreach ($list as $record) {
            if($user_id == $record->supervisor_id ||$user_id ==  $record->manager_id){
            $supervisor = $this->current_model->get_supervisor_name($record->supervisor_id);
            $manager = $this->current_model->get_manager_name($record->manager_id);
            $date_from = strtotime($record->leave_date_from);
            $date_to = strtotime($record->leave_date_to);
            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = $record->staff_name;
            $row[] = $record->staff_email;
            $row[] = $record->leave_name;
            $row[] = date("d-m-Y",$date_from);
            $row[] = date("d-m-Y",$date_to);
            $row[] = $record->total_leave;
            $row[] = $supervisor->first_name;
            $row[] = $manager->first_name;
            $row[] = $record->leave_status;

            $download_button = '<a class="btn btn-sm btn-primary" href="javascript:void(0)"
                title="Download" onclick="download_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-download"></i> </a>';
            $edit_button = '<a class="btn btn-sm btn-secondary" href="javascript:void(0)"
                title="Edit" onclick="edit_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-edit"></i> </a>';
            $delete_button = '<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete"
                onclick="delete_record_dialog(' . "'" . $record->id . "'" . ')"><i class="ft ft-trash-2"></i> </a>';

            $row[] = $download_button;
            //  = $edit_button . ' ' . $delete_button;
            $data[] = $row; 
            }
            
        }

        $output = array(
            "draw"            => $post_var['draw'],
            "recordsTotal"    => $this->current_model->count_all(),
            "recordsFiltered" => $this->current_model->count_filtered($post_var),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

//     public function ajax_upsert()
//     {
//         $this->_validate();
//         $post_var = $this->input->post();

//         if (empty($post_var['id'])) {
//             $data = array(
//                 'zone_code'         => $post_var['zone_code'],
//                 'zone_name'         => $post_var['zone_name'],
//                 'created_timestamp' => date('Y-m-d H:i:s'),
//                 'created_by'        => $this->data['auth_info']->email,
//             );

//             $status = $this->current_model->insert($data);
//         } else {
//             $data = array(
//                 'zone_code'         => $post_var['zone_code'],
//                 'zone_name'         => $post_var['zone_name'],
//                 'updated_timestamp' => date('Y-m-d H:i:s'),
//                 'updated_by'        => $this->data['auth_info']->email,
//             );

//             $where  = array('id' => $post_var['id']);
//             $status = $this->current_model->update($where, $data);
//         }

//         if ($status) {
//             //echo 'status is ' . $registered;
//             echo json_encode(array("status" => true));
//         } else {
//             echo json_encode(array("status" => false));
//         }
//     }

//     private function _validate()
//     {
//         $data                 = array();
//         $data['error_string'] = array();
//         $data['inputerror']   = array();
//         $data['status']       = true;

//         if (trim($this->input->post('zone_name')) == '') {
//             $data['inputerror'][]   = 'zone_name';
//             $data['error_string'][] = 'Zone name is required';
//             $data['status']         = false;
//         }

//         if ($data['status'] === false) {
//             echo json_encode($data);
//             exit();
//         }
//     }

//     public function ajax_delete($id)
//     {
//         $status = $this->current_model->delete_by_id($id);
//         echo json_encode(array("status" => $status));
//     }

//     public function ajax_edit($id)
//     {
//         $data = $this->current_model->get_by_id($id);
//         echo json_encode($data);
//     }
}
