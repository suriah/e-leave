<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Leave_cancellations_approval extends MY_Controller
{
    private $current_model;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Leave_cancellations_approval_model');
        $this->load->model('Users_model');
        $this->user_model = $this->Users_model;
        $this->current_model = $this->Leave_cancellations_approval_model;
        $this->current_model->auth_info = $this->data['auth_info'];
        
    }

    public function index()
    {
        $this->data['leave_type_list'] = $this->current_model->list_leave_type_drop_down();
        $this->data['supervisor_list'] = $this->current_model->list_supervisor_drop_down();
        $this->data['manager_list'] = $this->current_model->list_manager_drop_down();
        $this->data['user_data'] = $this->data['auth_info'];
        
        $this->load->view('standard/header_open', $this->data);
        //load style dependency
        $this->load->view('dependency/style/datetimepicker');
        $this->load->view('dependency/style/datatable');
        $this->load->view('dependency/style/selectize');
        $this->load->view('standard/header_close');
        $this->load->view('standard/navigation');
        $this->load->view('leave_cancellations_approval/list_leave_cancellations_approval');
        $this->load->view('standard/footer_open');
        //load script dependency
        $this->load->view('dependency/script/moment');
        $this->load->view('dependency/script/datetimepicker');
        $this->load->view('dependency/script/datatable');
        $this->load->view('dependency/script/selectize');
        $this->load->view('standard/footer_close');
    }

    public function ajax_list()
    {
        $post_var = $this->input->post();

        $user_id = $this->data['auth_info']->id;

        $filter= "(manager_id =  $user_id)";
        $this->db->where($filter);
        $list = $this->current_model->get_datatables($post_var);
        $data = array();
        $no   = $post_var['start'];
        
        foreach ($list as $record) {


            $date_from = strtotime($record->leave_date_from);
            $date_to = strtotime($record->leave_date_to);
            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = $record->staff_name;
            $row[] = $record->staff_email;
            $row[] = $record->leave_name;
            $row[] = date("d-m-Y", $date_from);
            $row[] = date("d-m-Y", $date_to);
            $row[] = $record->total_leave;
            $row[] = $record->cancellations_reason;
            $row[] = $record->leave_status;

            $process_button = '<a class="btn btn-sm btn-warning" href="javascript:void(0)"
                title="Process" onclick="process_record(' . "'" . $record->id . "'" . ')"><i class="ft ft-loader"></i> </a>';

            $row[]  = $process_button;
           
            $data[] = $row;
        }

        $output = array(
            "draw"            => $post_var['draw'],
            "recordsTotal"    => $this->current_model->count_all(),
            "recordsFiltered" => $this->current_model->count_filtered($post_var),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }



    public function ajax_approve_application($id)
    {
        $post_var = $this->input->post();

        $this->db->where('leave_id', $id);
        $status = $this->db->delete('leave_date');
        
        if($status){

            $data = array(
                'leave_status'                      => 'CANCELLED',
                'cancellations_approved_timestamp'         => date('Y-m-d H:i:s'),
                'cancellations_approved_by'  => $this->data['auth_info']->email,
            );
            $where  = array('id' => $id);
            $status = $this->current_model->update($where, $data);

            if($status){
                //approval email
                $this->current_model->cancel_approved_notification_email($id);
            }
        }

        echo json_encode(array("status" => $status));
    }

    public function ajax_reject_application($id)
    {
        $post_var = $this->input->post();
        
     
        $data = array(
            'leave_status'                            => 'APPROVED',
            'cancellations_approved_timestamp'         => date('Y-m-d H:i:s'),
            'cancellations_approved_by'                => $this->data['auth_info']->email,
        );
        
        $where  = array('id' => $post_var['id']);
        $status = $this->current_model->update($where, $data);
        $status =$this->current_model->cancel_rejected_notification_email($id);
        echo json_encode(array("status" => $status));
    }

    public function ajax_edit($id)
    {
        $data = $this->current_model->get_by_id($id);
        echo json_encode($data);
    }
    public function ajax_view_process_data($id)
    {
        $data = $this->current_model->view_process_data($id);
        echo json_encode($data);
    }

    public function download_file($id)
    {
        $this->db->where('id',$id);
        $query=$this->db->get('leave');
        $row = $query->row();
        $data = file_get_contents(FCPATH.'upload/leave_supporting_document/'. $id);
        $name = $row->supporting_document;
        force_download($name,$data);
    }
}