<?php

$lang['list_of_factories'] = 'List Of Lines';
$lang['add_factory'] = 'Add Line';
$lang['refresh'] = 'Refresh';

//-------------------dialog box message----------------
$lang['alert'] = 'Alert';
$lang['warning_delete'] = 'Are you sure want to delete selected line ?';
$lang['edit_record'] = 'Edit Line Detail';
$lang['add_record'] = 'Add New Line';

/*******************require field and placeholder *************************/
$lang['factory_code'] = 'Factory Code';
//$lang['factory_id_placeholder'] = 'Please input factory ID';
$lang['factory_id_require'] = 'Factory ID is required';

$lang['line_code'] = 'Line Code';
$lang['line_code_placeholder'] = 'Please input line code';
$lang['line_code_require'] = 'Line code is required';


$lang['line_name'] = 'Line Name';
$lang['line_name_placeholder'] = 'Please input line name';
$lang['line_name_require'] = 'Line name is required';

$lang['line_description'] = 'Description';
$lang['line_description_placeholder'] = 'Please input description';
