<?php

$lang['list_of_factories'] = 'List Of Factories';
$lang['add_factory'] = 'Add Factory';
$lang['refresh'] = 'Refresh';

//-------------------dialog box message----------------
$lang['alert'] = 'Alert';
$lang['warning_delete'] = 'Are you sure want to delete selected factory ?';
$lang['edit_factory_detail'] = 'Edit Factory Detail';
$lang['add_new_factory'] = 'Add New Factory';

/*******************require field and placeholder *************************/
$lang['factory_code'] = 'Factory ID';
$lang['factory_code_placeholder'] = 'Please input factory ID';
$lang['factory_code_require'] = 'Factory Code is required';

$lang['factory_name'] = 'Factory Name';
$lang['factory_name_placeholder'] = 'Please input factory name';
$lang['factory_name_require'] = 'Factory name is required';

$lang['description'] = 'Description';
$lang['description_placeholder'] = 'Please input description';
