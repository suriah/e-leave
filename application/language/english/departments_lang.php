<?php

$lang['list_of_Departments'] = 'List Of Factories';
$lang['add_department'] = 'Add Department';
$lang['refresh'] = 'Refresh';

//-------------------dialog box message----------------
$lang['alert'] = 'Alert';
$lang['warning_delete'] = 'Are you sure want to delete selected department ?';
$lang['edit_department_detail'] = 'Edit Department Detail';
$lang['add_new_department'] = 'Add New Department';

/*******************require field and placeholder *************************/
$lang['department_code'] = 'Department code';
$lang['department_code_placeholder'] = 'Please input department code';
$lang['department_code_require'] = 'Department Code is required';

$lang['department_name'] = 'Department Name';
$lang['department_name_placeholder'] = 'Please input department name';
$lang['department_name_require'] = 'Department name is required';

$lang['description'] = 'Description';
$lang['description_placeholder'] = 'Please input description';
