<?php

/*
 * navigation menu part
 */

$lang['menu_dashboard'] = 'Dashboard';
$lang['menu_system_admin'] = 'System Admin';
$lang['menu_developer'] = 'Developer';
$lang['menu_manage_user'] = 'Manage Users';
$lang['menu_edit_profile'] = 'Edit Profile';
$lang['menu_change_password'] = 'Change Password';
$lang['menu_log_out'] = 'Log Out';
$lang['menu_manage_factories'] = 'Manage Factories';
$lang['menu_manage_lines'] = 'Manage Lines';
//end menu navigation part

//**********general**********************
$lang['yes'] = 'Yes';
$lang['no'] = 'No';