<?php

class MY_Controller extends CI_Controller {

    protected $data;

    function __construct() {
        parent::__construct();

        //load config information          
        $this->load->model('Settings_model');
        $this->data['settings'] = $this->Settings_model->get_system_settings();

        $this->load->model('Menu_model');
        $this->load->model('Privilege_model');

        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('login_status', 'failed');
            $this->session->set_flashdata('message', 'You session has been expired, please login again');
            redirect('default_page', 'refresh');
        } else {
            $this->data['auth_info'] = $this->ion_auth->user()->row();
            $this->data['user_groups'] = $this->ion_auth->get_users_groups($this->data['auth_info']->id)->result();

            //privilege information
            $privilege_list = $this->Privilege_model->list_privileges($this->data['user_groups']);
            $this->data['privilege_list'] = $privilege_list;

            $full_menu = $this->Menu_model->list_menu();

            //bypass menu restriction if setting is bypass_menu_restriction
            if ($this->data['settings']->bypass_menu_restriction == 't') {
                $this->data['menu'] = $full_menu;
            } else {
                $this->data['menu'] = $this->Menu_model->filter_menu($this->data['user_groups'], $full_menu);
            }
        }

        //load language file based on coookie setup
        $lang = get_cookie('lang');
        $this->lang->load('general_lang', $lang);
        
        //***********************note about profiler*************************
        //1. because of theme use, profiler will not allogn properly, so i had to hack file
        //   system/libraries/profiler.php at public function run and add <footer></footer>
        //   if it happen you are update codeigniter library, please update the code as well
        //2. in order this profiler work properly when using ajax, I had to enable and disable based on 
        //   input request
        
        if ($this->config->item('debug_mode')== true){
            if ($this->input->is_ajax_request()) {
                $this->output->enable_profiler(false);
            } else {
                $this->output->enable_profiler(true);
            }
        }
    }

}
