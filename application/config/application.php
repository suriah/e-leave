<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author : Muhammad Jauhari Saealal
 * Date   : 24/10/2018
 * Developer_mode 
 * ---------------------------------------------
 */
$config['developer_mode'] = true;
$config['debug_mode'] = false;
